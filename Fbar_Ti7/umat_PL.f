	SUBROUTINE UMAT_CPFEM(STRESS,STATEV,PROPS,DDSDDE,NTENS,NSTATV,
     1             NPROPS,PNEWDT,DFGRD0,DFGRD1,KINC,DTIME,JELEM)

	IMPLICIT DOUBLE PRECISION (A-H,O-Z)

C     islip = max no. of slip systems allowed
      parameter (nslip=100,itotslips=8)
              
	dimension stress(ntens),statev(nstatv),re_tau_t(3,3),
     &     ddsdde(ntens,ntens),dfgrd0(3,3),dfgrd1(3,3),
     &     euler(3),g_alpha_t(nslip),g_alpha_tau(nslip),fp_t(3,3),
     &     dsrot(3,nslip),dmrot(3,nslip),s_tan(3,3,nslip),
     &     ddsdde_4d(3,3,3,3),de_gg(3,3,3,3),de_gg_2d(6,6),
     &     gst(3,nslip),gmt(3,nslip),delta_gamma(nslip),fp_tau(3,3),
     &     tr(3,3),tpk_1d_t(6),tpk_1d_tau(6),tot_gamma(nslip),
     &     c_alpha_1d(6,nslip),tau(nslip),fe_tau(3,3),
     &     re_tau(3,3),ue_tau(3,3),tlg(3,3),ics(itotslips),
     &     ga(itotslips),xh0a(itotslips),xs_bara(itotslips),
     &     xs_bar(nslip),xh0(nslip),
     &     nslips(itotslips),PROPS(NPROPS),
     &     cst(3,nslip),cmt(3,nslip),
     &     xn(nslip),xr(nslip),xri(itotslips),xni(itotslips),cst_1d(6),
     &     q_bcc_hcp(3,3),tr1(3,3),TEMPERATURE_MODULI_DECREASE(5),	
     &     xh_0(5),xh_inf(5),xg_0(5),xg_inf(5),a_dot_bcc(5),
     &     rate_exp_bcc(5),g0_bcc(5),creep(6),
     &     stress1(6),ddsdde1(6,6),wt(2),q_bcc_hcpt(3,3),
     &     xlast_rot(3,3),tr3(3,3),xn_trb_comp(itotslips),
     &     xr_trb_comp(itotslips),xh0_trb_comp(itotslips),
     &     xs_trb_comp(itotslips),g0_trb_comp(itotslips),
     &     xn_pra_comp(itotslips),xr_pra_comp(itotslips),
     &     xh0_pra_comp(itotslips),xs_pra_comp(itotslips),
     &     g0_pra_comp(itotslips),ps(3),cauchy_2d(3,3),
     &     fe_tau_inv_t(3,3),cbasal(3),bas_tract(3),
     &     chi_t(nslip),chi_tau(nslip),s_gg_2d(6,6),g1_bcc(5),
     &     g2_bcc(5),dfgrd1_t(3,3),f_t_f(3,3),eigvalue(3),
     &     eigvector(3,3),strain_2d(3,3),tpk(6,1),tpk_2d_t(3,3),
     &     G_ALPHA_COMP(NSLIP),G_ALPHA_TENS(NSLIP),G_EVOLUTION(NSLIP)

	dimension bn_stress(1),strain_1d(6),strain(6),passing_gnd(nslip)
C************** KOUROSH_TEMPERATURE ********************
       DIMENSION::F_THERMAL_TAU(3,3),F_THERMAL_CRYSTAL_TAU(3,3),
     &            F_THERMAL_T(3,3)
C************** KOUROSH_TEMPERATURE ********************
	
!	kourosh
	dimension fp_t_t(3,3),cc(3,3),eye(3,3),eps_plastic(3,3)
	dimension forest(30),parallel(30)
		
	common/soften1/gamma_dot_0_basal,gamma_dot_0_prism,
     &gamma_dot_0_pyram,gamma_dot_0_capyr
	common/soften2/xk_star_basal,xk_star_prism,xk_star_pyram,
     &xk_star_capyr
	common/soften3/xlp_basal,xlp_prism,xlp_pyram,xlp_capyr
	common/soften4/xk_basal,xk_prism,xk_pyram,xk_capyr
	common/soften5/gamma_dot_ini_basal,gamma_dot_ini_prism,
     &gamma_dot_ini_pyram,gamma_dot_ini_capyr
	common/soften6/rate_basal,rate_prism,rate_pyram,rate_capyr
      common/temp_reduction/TEMPERATURE_MODULI_DECREASE
	

	data iread/0/
	common/alaki/jelemm
	
	   
	  jelemm=jelem
	
		if(iread.eq.0)then
           iread=1
		   
		   open(301,file='softening.dat')
		   read(301,*) gamma_dot_0_basal,gamma_dot_0_prism,
     &	gamma_dot_0_pyram,gamma_dot_0_capyr
		   read(301,*) xk_star_basal,xk_star_prism,
     &	xk_star_pyram,xk_star_capyr
		   read(301,*) xlp_basal,xlp_prism,xlp_pyram,xlp_capyr
		   read(301,*) xk_basal,xk_prism,xk_pyram,xk_capyr
		   read(301,*) gamma_dot_ini_basal,gamma_dot_ini_prism,
     &	gamma_dot_ini_pyram,gamma_dot_ini_capyr
		  read(301,*) rate_basal,rate_prism,rate_pyram,rate_capyr
        read(301,*) TEMPERATURE_MODULI_DECREASE(1:5)
		   close(301)
		   
        endif
!	kourosh


	pnewdt1=pnewdt	



	do i=1,3
	   do j=1,3
	      xlast_rot(i,j)=0.d0
	   enddo
	enddo

	xlast_rot(1,1)=1.d0
	xlast_rot(2,2)=1.d0
	xlast_rot(3,3)=1.d0

	do i=1,6
	   creep(i)=0.d0
	   strain(i)=0.d0	! added by Kourosh
	   stress1(i)=0.d0
	   do j=1,6
	      ddsdde1(i,j)=0.d0
	   enddo
	enddo

c.......ipoly=1......Primary Alpha................
c.......ipoly=2......Transformed Beta.............
C	ipoly_switch=ipoly(noel)

	IPHASE=STATEV(304) ! PHASE NUMBER OF THE ELEMENT: 1=PRIMARY, 2=TRANSFORMED
	ipoly_switch=IPHASE


	if(ipoly_switch.eq.1)then
	   wt(1)=1.d0
	   wt(2)=0.d0
	elseif(ipoly_switch.eq.2)then
	   wt(1)=0.93d0
	   wt(2)=0.07d0
	endif


	ikin_inp=208
	ikin_state=220
	xc=props(ikin_inp)
	xd=props(ikin_inp+1)
	
	Act_Energy=props(210)
	gnd_const1=props(211)
	gnd_const2=props(212)
	bur=props(213)
	shear_modulus=props(214)
       
C************** KOUROSH_TEMPERATURE ********************
      TEMPERATURE0=PROPS(215) ! INITIAL TEMPERATURE
      DENSITY=PROPS(216) ! MATERIAL DENSITY
      THERMAL_EXPANSION_A=PROPS(217) ! THERMAL EXPANSION COEFFICIENT
      THERMAL_EXPANSION_C=PROPS(218) ! THERMAL EXPANSION COEFFICIENT
      BETA=PROPS(219) ! COEFFICIENT FOR PLASTIC WORK CONVERSION TO HEAT
      SPECIFIC_HEAT=PROPS(220) ! SPECIFIC HEAT CAPACITY
      REF_TEMPERATURE=PROPS(221) ! REFERENCE TEMPERATURE
      SOFTENING_POWER=PROPS(222) ! SPECIFIC HEAT CAPACITY
      TEMPERATURE_MODULI=PROPS(223) ! TEMPERATURE AT WHICH ELASTIC MODULI ARE GIVEN  
!      TEMPERATURE_MODULI_DECREASE=PROPS(224) ! dMODULUS/dTEMPERATURE
C************** KOUROSH_TEMPERATURE ********************

	
	do itag_el=1,IPHASE
	   if(itag_el.eq.1)then

	      itot=itotslips
	      c11=props(1)
	      c12=props(2)
	      c13=props(3)
	      c33=props(4)
	      c55=props(5)
         
         

         IF (KINC.EQ.1) THEN
         

               TEMP=TEMPERATURE0
        
            
            C11=C11*1.D0-TEMPERATURE_MODULI_DECREASE(1)*(TEMP-
     &               TEMPERATURE_MODULI)
            C12=C12*1.D0-TEMPERATURE_MODULI_DECREASE(2)*(TEMP-
     &               TEMPERATURE_MODULI)
            C13=C13*1.D0-TEMPERATURE_MODULI_DECREASE(3)*(TEMP-
     &               TEMPERATURE_MODULI)
            C33=C33*1.D0-TEMPERATURE_MODULI_DECREASE(4)*(TEMP-
     &               TEMPERATURE_MODULI)
            C55=C55*1.D0-TEMPERATURE_MODULI_DECREASE(5)*(TEMP-
     &               TEMPERATURE_MODULI)  


            IF (C11.LT.0.D0.OR.C12.LT.0.D0.OR.C13.LT.0.D0.OR.
     &          C33.LT.0.D0.OR.C55.LT.0.D0) THEN
               WRITE(*,*) 'TOO HIGH TEMPERATURE',TEMPERATURE0,JELEM
               STOP
            ENDIF     
         
         ELSE


               TEMP=STATEV(412)
           
            
            C11=C11*1.D0-TEMPERATURE_MODULI_DECREASE(1)*(TEMP-
     &               TEMPERATURE_MODULI)
            C12=C12*1.D0-TEMPERATURE_MODULI_DECREASE(2)*(TEMP-
     &               TEMPERATURE_MODULI)
            C13=C13*1.D0-TEMPERATURE_MODULI_DECREASE(3)*(TEMP-
     &               TEMPERATURE_MODULI)
            C33=C33*1.D0-TEMPERATURE_MODULI_DECREASE(4)*(TEMP-
     &               TEMPERATURE_MODULI)
            C55=C55*1.D0-TEMPERATURE_MODULI_DECREASE(5)*(TEMP-
     &               TEMPERATURE_MODULI)         

            IF (C11.LT.0.D0.OR.C12.LT.0.D0.OR.C13.LT.0.D0.OR.
     &          C33.LT.0.D0.OR.C55.LT.0.D0) THEN
               WRITE(*,*) 'TOO HIGH TEMPERATURE',TEMPERATURE0,JELEM
               STOP
            ENDIF   
         ENDIF
	      


		 IF (IPHASE.EQ.1) THEN
			
			a_dot_hcp=props(126)
			rate_exp_hcp=props(127)

			ICOUNT=304
			DO I=1,ITOT
				ICOUNT=ICOUNT+1
				GA(I)=STATEV(ICOUNT)  ! STATEV(305-312)
			ENDDO

			do I=1,ITOT
			    ICOUNT=ICOUNT+1
			    g0_pra_comp(I)=STATEV(ICOUNT) ! STATEV(313-320)
			enddo


		 ELSE

			a_dot_hcp=props(6)
			rate_exp_hcp=props(7)
		
			ICOUNT=320
			DO I=1,ITOT
				ICOUNT=ICOUNT+1
				GA(I)=STATEV(ICOUNT) ! STATEV(321-328)
			ENDDO

			do I=1,ITOT
			    ICOUNT=ICOUNT+1
			    g0_trb_comp(I)=STATEV(ICOUNT) ! STATEV(329-336)
			enddo


		 ENDIF
				
				

		 IF (IPHASE.EQ.1) IND1=143
		 IF (IPHASE.EQ.2) IND1=23
	
	      
	      do i=1,itot
		 	ind1=ind1+1
		 	xni(i)=props(ind1) ! props(24-31) or props(144-151)
	      enddo

	      if(IPHASE.eq.2)then
		 do i=1,itot
		    ind1=ind1+1
		    xn_trb_comp(i)=props(ind1) ! props(32-39)
		 enddo
	      endif 

	      if(IPHASE.eq.1)then
		 do i=1,itot
		    ind1=ind1+1
		    xn_pra_comp(i)=props(ind1) ! props(152-159)
		 enddo
	      endif 


	      do i=1,itot
	        	 ind1=ind1+1
			 xri(i)=props(ind1) ! props(40-47) or props(160-167)
	      enddo

	      
	      if(IPHASE.eq.2)then
		 do i=1,itot
		    ind1=ind1+1
		    xr_trb_comp(i)=props(ind1) ! props(48-55)
		 enddo
	      endif 

	      if(IPHASE.eq.1)then
		 do i=1,itot
		    ind1=ind1+1
		    xr_pra_comp(i)=props(ind1) ! props(168-175)
		 enddo
	      endif 


	      do i=1,itot
		 	ind1=ind1+1
		 	xh0a(i)=props(ind1) ! props(56-63) or props(176-183)
	      enddo
	      
	      
	      if(IPHASE.eq.2)then
		 do i=1,itot
		    ind1=ind1+1
		    xh0_trb_comp(i)=props(ind1) ! props(64-71)
		 enddo
	      endif 


	      if(IPHASE.eq.1)then
		 do i=1,itot
		    ind1=ind1+1
		    xh0_pra_comp(i)=props(ind1) ! props(184-191)
		 enddo
	      endif 

	      do i=1,itot
		 	ind1=ind1+1
		 	xs_bara(i)=props(ind1) ! props(72-79) or props(192-199)
	      enddo

	      
	      if(IPHASE.eq.2)then
		 do i=1,itot
		    ind1=ind1+1
		    xs_trb_comp(i)=props(ind1) ! props(80-87)
		 enddo
	      endif 

	      if(IPHASE.eq.1)then
		 do i=1,itot
		    ind1=ind1+1
		    xs_pra_comp(i)=props(ind1) ! props(200-207)
		 enddo
	      endif


	      ihcp=1
	      ibcc=0

	      call inicrys(icrys,ics,cst,cmt,nslips,islip,ihcp,ibcc)		


	      ic=0
	      do i=1,itot
		 do isys=1,nslips(i)
		    xh0(isys+ic)=xh0a(i)
		    xs_bar(isys+ic)=xs_bara(i)
		    xn(isys+ic)=xni(i)
		    xr(isys+ic)=xri(i)
		 enddo
		 ic=ic+nslips(i)
	      enddo

C       Forming the elasticity matrix

	      call rclear66(ddsdde)
	      ddsdde(1,1)=c11
	      ddsdde(2,2)=c11
	      ddsdde(3,3)=c33
	      ddsdde(4,4)=(c11-c12)
	      ddsdde(5,5)=c55*2.d0
	      ddsdde(6,6)=c55*2.d0
	      ddsdde(1,2)=c12
	      ddsdde(2,1)=c12
	      ddsdde(1,3)=c13
	      ddsdde(3,1)=c13
	      ddsdde(2,3)=c13
	      ddsdde(3,2)=c13
	      call tr2to4(ddsdde,ddsdde_4d,islip)
	      
	      
	      euler(1)=STATEV(301)
	      euler(2)=STATEV(302)
	      euler(3)=STATEV(303)
	      call euler_slip(euler,tr1)
	      call mat33(tr,xlast_rot,tr1,3)
	      call rotate_crys_vector(cst,tr,gst,islip)
	      call rotate_crys_vector(cmt,tr,gmt,islip)
	      call rotate4d(ddsdde_4d,tr,de_gg)
	      call tr4to2(de_gg,de_gg_2d)	

	 
c--------------------------------------------
c
c     for feTfe
c              
	      do j=1,6
		 do i=1,6
		    s_gg_2d(i,j)=de_gg_2d(i,j)
		 enddo
	      enddo
              
           call matinv(s_gg_2d,6,6)
c----------------------------------------------    
	   endif      ! if(itag_el.eq.1)then
	   
	   if(itag_el.eq.2)then
	      index=ind1
	      index=index+1
	      c1=props(index) ! props(88)
	      index=index+1
	      c2=props(index) ! props(89)
	      index=index+1
	      c3=props(index) ! props(90)
	      
	      do i=1,5
		 	index=index+1
		 	a_dot_bcc(i)=props(index) ! props(91-95)
	      enddo
	      
	      do i=1,5
		 	index=index+1
		 	rate_exp_bcc(i)=props(index) ! props(96-100)
	      enddo
	      
		
C	      do i=1,5
C		 index=index+1
C		 g0_bcc(i)=props(index) ! props(101-105)
C	      enddo

		 INDEX=336
	      do i=1,5
		 	index=index+1
		 	g0_bcc(i)=STATEV(index) ! STATEV(337-341)
	      enddo

	      INDEX=105
	      do i=1,5
		 	index=index+1
		 	xh_0(i)=props(index) ! props(106-110)
	      enddo
	      
	      do i=1,5
		 	index=index+1
		 	xh_inf(i)=props(index) ! props(111-115)
	      enddo
	      
	      do i=1,5
		 	index=index+1
		 	xg_0(i)=props(index) ! props(116-120)
	      enddo
	      
	      do i=1,5
		 	index=index+1
		 	xg_inf(i)=props(index) ! props(121-125)
	      enddo

	      do i=1,5
		 	g1_bcc(i)=STATEV(341+i) ! STATEV(342-346)
		 	g2_bcc(i)=STATEV(346+i) ! STATEV(347-351)
	      enddo

	      
	      ihcp=0
	      ibcc=1
	      call inicrys(icrys,ics,cst,cmt,nslips,islip,ihcp,ibcc)
	      
C       Forming the elasticity matrix
	      call rclear66(ddsdde)
	      ddsdde(1,1)=c1
	      ddsdde(2,2)=c1
	      ddsdde(3,3)=c1
	      ddsdde(4,4)=c3
	      ddsdde(5,5)=c3
	      ddsdde(6,6)=c3
	      ddsdde(1,2)=c2
	      ddsdde(2,1)=c2
	      ddsdde(1,3)=c2
	      ddsdde(3,1)=c2
	      ddsdde(2,3)=c2
	      ddsdde(3,2)=c2
	      
	      
	      call tr2to4(ddsdde,ddsdde_4d,islip)

	      euler(1)=STATEV(301)
	      euler(2)=STATEV(302)
	      euler(3)=STATEV(303)
	      call euler_slip(euler,tr1)
	      
c-----------------Orientation Relation------------------------	  
	      
	      bc1=dsqrt(2.d0)
	      bc2=2.d0*dsqrt(3.d0)
	      bc3=bc2**2.d0+6.d0
	      bc4=dsqrt(bc3)
	      bc5=(bc4-bc2)/12.d0		 
	      q_bcc_hcp(1,1)=bc5
	      q_bcc_hcp(1,2)=2.d0*bc5+dsqrt(3.d0)/2.d0
	      q_bcc_hcp(1,3)=-bc5
	      q_bcc_hcp(3,1)=1.d0/bc1
	      q_bcc_hcp(3,2)=0.0d0
	      q_bcc_hcp(3,3)=1.d0/bc1
	      q_bcc_hcp(2,1)=q_bcc_hcp(3,2)*q_bcc_hcp(1,3)-
     &        q_bcc_hcp(1,2)*q_bcc_hcp(3,3)
	      q_bcc_hcp(2,2)=q_bcc_hcp(3,3)*q_bcc_hcp(1,1)-
     &        q_bcc_hcp(3,1)*q_bcc_hcp(1,3)
	      q_bcc_hcp(2,3)=q_bcc_hcp(3,1)*q_bcc_hcp(1,2)-
     &        q_bcc_hcp(1,1)*q_bcc_hcp(3,2)	 
	      
c--------------------------------------------------------------------------
	      
	      call mat33(tr3,tr1,q_bcc_hcp,3)
	      call mat33(tr,xlast_rot,tr3,3)
	      call rotate_crys_vector(cst,tr,gst,islip)
	      call rotate_crys_vector(cmt,tr,gmt,islip)
	      call rotate4d(ddsdde_4d,tr,de_gg)
	      call tr4to2(de_gg,de_gg_2d)

c-------------------------------------------------
c
c     for feTfe
c              
	      do j=1,6
		 do i=1,6
		    s_gg_2d(i,j)=de_gg_2d(i,j)
		 enddo
	      enddo
              
           call matinv(s_gg_2d,6,6)

c--------------------------------------------------
	   endif ! if(itag_el.eq.2)then
	   
	   if(itag_el.eq.1)then
	      ihcp=1
	      ibcc=0
	   else
	      ihcp=0
	      ibcc=1
	   endif
	   
	   s_tan=0.0d0	
	   do isys=1,islip
	      do j=1,3
		 do i=1,3
		    s_tan(i,j,isys)=gst(i,isys)*gmt(j,isys)
		 enddo
	      enddo
	   enddo
	   

          
C********************* KOUROSH_ASSYMETRY ******************************                                                   
       ic=0
       do i=1,itot
         do isys=1,nslips(i)
              G_ALPHA_COMP(isys+ic)=g0_pra_comp(i)
              G_ALPHA_TENS(isys+ic)=ga(i)
         enddo
              ic=ic+nslips(i)
       enddo           
C********************* KOUROSH_ASSYMETRY ******************************          
          
           	   
	   if (KINC.eq.1) then

	      do isys=1,islip
		 	chi_t(isys)=0.d0
	      enddo
		  
		  epsilon_p_bar=0.d0 ! kourosh
	      

	      if(itag_el.eq.1)then
C********************* KOUROSH_ASSYMETRY ******************************
C      calculate strain assuming all the strain is elastic strain
              G_EVOLUTION=0.D0
              call trans(dfgrd1,dfgrd1_t)
              call mat33(f_t_f,dfgrd1_t,dfgrd1,3)

              call jacobi(f_t_f,3,3,eigvalue,eigvector,nrot)
	
              do i=1,3
              do j=1,3
			strain_2d(i,j)=0.d0
			do k=1,3
				
				if (eigvalue(k).le.0.d0) then
					write(*,*) 'Negative Eigenvalue @ Strain Cal.'
					stop
				endif
			
				strain_2d(i,j)=strain_2d(i,j)+0.5d0*dlog(eigvalue(k))*
     &                         eigvector(i,k)*eigvector(j,k)
			enddo
		enddo
              enddo
	
              call tr2to1(strain_2d,strain_1d)
              call matmulx(de_gg_2d,strain_1d,tpk,6,6,1)
              
              hydro_pressure=1./3.*(tpk(1,1)+tpk(2,1)+tpk(3,1))
              
              if (hydro_pressure.lt.0.d0) then
                                         
		 ic=0
		 do i=1,itot
		    do isys=1,nslips(i)
		       g_alpha_t(isys+ic)=g0_pra_comp(i)
		    enddo
		    ic=ic+nslips(i)
		 enddo
              
              else
             
		 ic=0
		 do i=1,itot
		    do isys=1,nslips(i)
		       g_alpha_t(isys+ic)=ga(i)
		    enddo
		    ic=ic+nslips(i)
		 enddo 
              endif       
              
C********************* KOUROSH_ASSYMETRY ******************************            
	      endif
	   
	      if(itag_el.eq.2)then
		 do isys=1,12
		    g_alpha_t(isys)=g0_bcc(1)
		 enddo
		 do isys=13,24
		    g_alpha_t(isys)=g0_bcc(2)
		 enddo
		 do isys=25,48
		    g_alpha_t(isys)=g0_bcc(4)
		 enddo
c--------------------------------------------------------------------------
		 do isys=1,48
c       for b1 and b2 bcc props
		    R3=1.0D0/DSQRT(3.0D0)
c       b1 system
		    x1=(cst(1,isys)*(-R3))+(cst(2,isys)*R3)+(cst(3,isys)*R3) 
c       b2 system
		    x2=(cst(1,isys)*R3)+(cst(2,isys)*R3)+(cst(3,isys)*(-R3))

		    if(dabs(x1-1.d0).le.1d-8)then
		       if(isys.le.12)then
			  g_alpha_t(isys)=g1_bcc(1)
		       elseif(isys.le.24)then
			  g_alpha_t(isys)=g1_bcc(2)
		       else
			  g_alpha_t(isys)=g1_bcc(4)
		       endif
		    elseif(dabs(x2-1.d0).le.1d-8)then
		       if(isys.le.12)then
			  g_alpha_t(isys)=g2_bcc(1)
		       elseif(isys.le.24)then
			  g_alpha_t(isys)=g2_bcc(2)
		       else
			  g_alpha_t(isys)=g2_bcc(4)
		       endif		       
		    endif
		 enddo ! do isys		 
c--------------------------------------------------------------------------
	      endif ! if (itag_el.eq.2)


	      do isys=1,islip
		 	tot_gamma(isys)=0.d0
	      enddo 
	      call rclear(tpk_1d_t,6)
	      call cal_ckrone_2d(fp_t) 
	      w_p=0.0d0        !Masoud_crack 
             
C************** KOUROSH_TEMPERATURE ********************             
             TEMPERATURE=TEMPERATURE0
             PLASTIC_WORK=0.D0
             CALL CAL_CKRONE_2D(F_THERMAL_T)
C************** KOUROSH_TEMPERATURE ********************		  
		  
	   endif ! if(KINC.eq.1)


  		   
	   if (KINC.gt.1) then     
	      if(itag_el.eq.1)index2=0
	      do isys=1,islip
		 	index2=index2+1
			 g_alpha_t(isys)=statev(index2) ! statev(1-30) for HCP or statev(76-123) for BCC
	      enddo

	      do isys=1,islip
		 	index2=index2+1
		 	tot_gamma(isys)=statev(index2) ! statev(31-60) or statev(124-171) 
	      enddo

	      do i=1,6
		 	index2=index2+1
		 	tpk_1d_t(i)=statev(index2) ! statev(61-66) or statev(172-177)
	      enddo

	      do i=1,3
		 do j=1,3
		    index2=index2+1
		    fp_t(i,j)=statev(index2) ! statev(67-75) oe statev(178-186)
		 enddo
	      enddo

	      ikin_init_state=0
	      if(itag_el.eq.2)ikin_init_state=30
	      do isys=1,islip
		 	chi_t(isys)=statev(ikin_init_state+ikin_state+isys) !statev(221-250) or statev(251-298)
	      enddo
	      w_p=statev(299)      !Masoud_crack
		  
C********************* KOUROSH_ASSYMETRY ******************************
              CALL sigmat(tpk_1d_t,tpk_2d_t)   
              call jacobi(tpk_2d_t,3,3,eigvalue,eigvector,nrot)
              IF (DABS(eigvalue(1)).GE.DABS(eigvalue(2)).AND.
     &      DABS(eigvalue(1)).GE.DABS(eigvalue(3))) THEN
     
              ICOMP=1
              
              ELSEIF (DABS(eigvalue(2)).GE.DABS(eigvalue(1)).AND.
     &      DABS(eigvalue(2)).GE.DABS(eigvalue(3))) THEN
     
              ICOMP=2          

              ELSEIF (DABS(eigvalue(3)).GE.DABS(eigvalue(1)).AND.
     &      DABS(eigvalue(3)).GE.DABS(eigvalue(2))) THEN
     
              ICOMP=3

              ENDIF              
        
              G_EVOLUTION(1:30)=STATEV(423:452)
        
              IF (eigvalue(ICOMP).LT.0.D0) THEN
              g_alpha_t=G_ALPHA_COMP+G_EVOLUTION
              ELSE
              g_alpha_t=G_ALPHA_TENS+G_EVOLUTION
              ENDIF

  
C********************* KOUROSH_ASSYMETRY ****************************** 		  
		  		  
c***************** kourosh start ************************
		call trans(fp_t,fp_t_t)
		call mat33(cc,fp_t_t,fp_t,3)
		call cal_ckrone_2d(eye)
		eps_plastic=0.5d0*(cc-eye)
		epsilon_p_bar=0.d0
		do irow=1,3
			do jcol=1,3
				epsilon_p_bar=epsilon_p_bar+eps_plastic(irow,jcol)*
     &						  eps_plastic(irow,jcol)
			enddo
		enddo
		
		epsilon_p_bar=dsqrt(2./3.*epsilon_p_bar)
c***************** kourosh end **************************	  



C************** KOUROSH_TEMPERATURE ********************
              TEMPERATURE=STATEV(412)
              PLASTIC_WORK=STATEV(413)
              
              I=0
              DO IROW=1,3
                     DO JCOL=1,3
                            I=I+1
                            F_THERMAL_T(IROW,JCOL)=STATEV(413+I)              
                     ENDDO
              ENDDO
C************** KOUROSH_TEMPERATURE ********************
		  
	   endif   ! if(KINC.gt.1)
	   
	   if(itag_el.eq.1)then
	      a_dot=a_dot_hcp
	      rate_exp=rate_exp_hcp
	   endif

	   
	   
	   

           do i=1,30    
		forest(i)=statev(351+i)   ! statev(352-381)
		parallel(i)=statev(351+30+i)   ! statev(382-411)
		passing_gnd(i)=gnd_const2*shear_modulus*bur*
     &                       dsqrt(parallel(i)/bur)
           end do      
	

C************** KOUROSH_TEMPERATURE ********************
           SPECIFIC_HEAT=SPECIFIC_HEAT-0.1473D12*TEMPERATURE+
     &                   4.2949D8*TEMPERATURE**2.D0   
           TEMPERATURE=TEMPERATURE+DTIME*BETA*PLASTIC_WORK/
     &                      DENSITY/SPECIFIC_HEAT
     
           TOTAL_TEMP_INCREASE=TEMPERATURE-TEMPERATURE0
           TEMPERATURE_INCREMENT=DTIME*BETA*PLASTIC_WORK/
     &                      DENSITY/SPECIFIC_HEAT
     
    
           F_THERMAL_CRYSTAL_TAU=0.D0
           F_THERMAL_CRYSTAL_TAU(1,1)=DEXP(THERMAL_EXPANSION_A*
     &                        TEMPERATURE_INCREMENT)
           F_THERMAL_CRYSTAL_TAU(2,2)=DEXP(THERMAL_EXPANSION_A*
     &                        TEMPERATURE_INCREMENT)           
           F_THERMAL_CRYSTAL_TAU(3,3)=DEXP(THERMAL_EXPANSION_C*
     &                        TEMPERATURE_INCREMENT)
     
           CALL ROTATE(F_THERMAL_CRYSTAL_TAU,TR,F_THERMAL_TAU)
           F_THERMAL_TAU=MATMUL(F_THERMAL_TAU,F_THERMAL_T)   
C************** KOUROSH_TEMPERATURE ********************

       
	   call ssmatc3(dfgrd1,g_alpha_t,tpk_1d_t,s_tan,dtime,
     &     fp_t,de_gg_2d,
     &     c_alpha_1d,delta_gamma,tpk_1d_tau,g_alpha_tau,tau,xn,xr,
     &     xs_bar,xh0,s_gg_2d,a_dot,rate_exp,pnewdt1,ifirst_check,
     &     islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     &     a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,ipoly_switch,
     &     xn_trb_comp,xr_trb_comp,xh0_trb_comp,xs_trb_comp,
     &     xn_pra_comp,xr_pra_comp,xh0_pra_comp,xs_pra_comp,isign,
     &     xc,xd,chi_t,chi_tau,epsilon_p_bar,forest,parallel,
     &     Act_Energy,gnd_const1,gnd_const2,shear_modulus,bur,
     &     passing_gnd,F_THERMAL_TAU,
     &     TEMPERATURE,REF_TEMPERATURE,SOFTENING_POWER) !kourosh, KOUROSH_TEMPERATURE
	
	   if(ifirst_check.eq.1)then
	      pnewdt=0.5d0
		 return
	   endif

	
	   call dstress_fp_fe(dfgrd1,fp_t,s_tan,delta_gamma,tpk_1d_tau,
     &     gst,gmt,fp_tau,fe_tau,stress,dsrot,dmrot,ntens,e_plastic,
     &     cst_1d,tr,cauchy_2d,fe_tau_inv_t,w_p,islip,strain_1d,
     &     F_THERMAL_TAU,PLASTIC_WORK,DTIME) ! KOUROSH_TEMPERATURE      
	
C       Calculating Stresses normal to the slip plane   
c***************************************************************
	   
	   if(itag_el.eq.1)then
	     do k1=1,1
	      do i11=1,3
		 cbasal(i11)=0.d0
		 do j11=1,3
		    cbasal(i11)=cbasal(i11)+fe_tau_inv_t(i11,j11)*
     &              gmt(j11,k1)
		 enddo
	      enddo	      
	      
	      do i11=1,3
		 bas_tract(i11)=0.d0
		 do j11=1,3
		    bas_tract(i11)=bas_tract(i11)+cauchy_2d(i11,j11)*
     &              cbasal(j11)
		 enddo
	      enddo
	      
	      bn_stress(k1)=0.d0
	      do i11=1,3
		 bn_stress(k1)=bn_stress(k1)+bas_tract(i11)*cbasal(i11)
	      enddo
	     enddo ! k1
	      
	   endif !itag_el.eq.1


         statev(299)=w_p    !Masoud_crack

	   call rudcmp(fe_tau,re_tau,ue_tau)
	   call trans(re_tau,re_tau_t)
	   call mat33(tlg,re_tau,tr,3)
	   call stereograph(tlg(3,1),tlg(3,2),tlg(3,3),xpole,ypole)
	
	   call jacobian_new(dfgrd0,dfgrd1,fp_t,fp_tau,s_tan,c_alpha_1d,
     &     tau,delta_gamma,dtime,rate_exp,a_dot,de_gg,tpk_1d_tau,
     &     g_alpha_tau,ddsdde,ntens,islip,tot_gamma,xh_0,xh_inf,
     &     xg_0,xg_inf,itag_el,a_dot_bcc,rate_exp_bcc,a_dot_hcp,
     &     rate_exp_hcp,epsilon_p_bar,passing_gnd,
     &     TEMPERATURE,REF_TEMPERATURE,SOFTENING_POWER) 

c******************************************************************************

	   if(itag_el.eq.1) index1=0

	   do isys=1,islip
	      index1=index1+1
	      statev(index1)=g_alpha_tau(isys)
	   enddo

	   do isys=1,islip
	      index1=index1+1
	      statev(index1)=dabs(delta_gamma(isys))
	   enddo
	
	   do i=1,6
	      index1=index1+1
	      statev(index1)=tpk_1d_tau(i)
	   enddo

	   do i=1,3
	      do j=1,3
		 index1=index1+1
		 statev(index1)=fp_tau(i,j)

	      enddo
	   enddo
	   
	   do i=1,6
	      creep(i)=creep(i)+wt(itag_el)*cst_1d(i)
		 strain(i)=strain(i)+wt(itag_el)*strain_1d(i) ! added by Kourosh
	      stress1(i)=stress1(i)+wt(itag_el)*stress(i)
	      do j=1,6
		 ddsdde1(i,j)=ddsdde1(i,j)+wt(itag_el)*ddsdde(i,j)
	      enddo
	   enddo

	   
	   ikin_init_state=0
	   if(itag_el.eq.2)ikin_init_state=30

	   do isys=1,islip
	      statev(ikin_init_state+ikin_state+isys)=chi_tau(isys)
	   enddo



	   pnewdt=pnewdt1

	enddo ! itag_el=1,ipoly(noel)


	do i=1,6
	   stress(i)=stress1(i)
	   do j=1,6
	      ddsdde(i,j)=ddsdde1(i,j)
	   enddo
	enddo

	do i=1,6
	   statev(210+i)=stress(i)
	   statev(186+i)=strain(i)	 ! added by Kourosh   
	enddo
       
C************** KOUROSH_TEMPERATURE ********************
       STATEV(412)=TEMPERATURE
       STATEV(413)=PLASTIC_WORK
                     
       I=0
       DO IROW=1,3
              DO JCOL=1,3
                     I=I+1
                     STATEV(413+I)=F_THERMAL_TAU(IROW,JCOL)              
              ENDDO
       ENDDO
C************** KOUROSH_TEMPERATURE ********************


C********************* KOUROSH_ASSYMETRY ******************************
       STATEV(423:452)=G_EVOLUTION(1:30)+G_ALPHA_TAU(1:30)-
     &                 G_ALPHA_T(1:30)
C********************* KOUROSH_ASSYMETRY ******************************


! JIAHAO
       I=0
       DO IROW=1,3
              DO JCOL=1,3
                     I=I+1
                     STATEV(452+I)=dfgrd1(IROW,JCOL)              
              ENDDO
       ENDDO      
! JIAHAO

	
	RETURN
	end
                
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C  CALCULATES THE INVERSE OF A 3*3 MATRIX

	subroutine matinv3(a,ai,det)
c	implicit double precision*8(a-h,o-z)
       implicit double precision (a-h,o-z)

c      include 'aba_param.inc'

      dimension a(3,3), ai(3,3)
c
      det=(a(1,1)*a(2,2)*a(3,3)-a(1,1)*a(2,3)*a(3,2)-a(2,1)*a(1,2)
     &     *a(3,3)+a(2,1)*a(1,3)*a(3,2)+a(3,1)*a(1,2)*a(2,3)-a(3,1)
     &     *a(1,3)*a(2,2))
      ai(1,1) =  ( a(2,2)*a(3,3)-a(2,3)*a(3,2))/det
      ai(1,2) = -( a(1,2)*a(3,3)-a(1,3)*a(3,2))/det
      ai(1,3) = -(-a(1,2)*a(2,3)+a(1,3)*a(2,2))/det
      ai(2,1) = -( a(2,1)*a(3,3)-a(2,3)*a(3,1))/det
      ai(2,2) =  ( a(1,1)*a(3,3)-a(1,3)*a(3,1))/det
      ai(2,3) = -( a(1,1)*a(2,3)-a(1,3)*a(2,1))/det
      ai(3,1) =  ( a(2,1)*a(3,2)-a(2,2)*a(3,1))/det
      ai(3,2) = -( a(1,1)*a(3,2)-a(1,2)*a(3,1))/det
      ai(3,3) =  ( a(1,1)*a(2,2)-a(1,2)*a(2,1))/det
      return
      end


C-------------------------------------------------------------------------

C    THIS SUBROUTINE INITIALISES THE CRYSTAL SLIP SYSTEMS, 
C    CALCULATES THE TRANSFORMATION MATRIX AND CONTAINS 
C    STATE VARIABLES

c----------------------------------------------------------
c     cmt - slip plane in crystal system
c     cst - slip directions in crystal system
c     tlg0 - transformation matrix (kalidindis paper)
c------------------------------------------------------------------------


      subroutine inicrys(icrys,ics,cst,cmt,nslips1,islip,ihcp,ibcc)
     
      implicit real*8 (a-h,o-z)
c     include 'aba_param.inc'    

	parameter(nslip=100)
	parameter(itotslips=8)
c	common/slips/islip
	dimension cmt(3,nslip),cst(3,nslip),ics(itotslips),
     &  nslips(itotslips),nslips1(itotslips)
  	
c	common/crys_switch/ihcp,ibcc
c	common/poly_inicrys/ipoly_switch
	r2=1.d0/2.d0
	r3=dsqrt(3.d0)
	ic=0


	if(ihcp.eq.1)then
	   nslips1(1)=1
	   nslips1(2)=1
	   nslips1(3)=1
	   
	   nslips1(4)=1
	   nslips1(5)=1
	   nslips1(6)=1
	   
	   nslips1(7)=6
	   nslips1(8)=18
	endif


	nslips(1)=3
	nslips(2)=3
	nslips(3)=6
	nslips(4)=12
	nslips(5)=6
	nslips(6)=48	


	if(ihcp.eq.1) then
c---basal slip system
	   do is=1,3
	      cmt(1,is)=  0.0
	      cmt(2,is)=  0.0 
	      cmt(3,is)=  1.0d0
	   enddo

	   cst(1,1)=  r2
	   cst(2,1)= -r2*r3 
	   cst(3,1)=  0.0
         
	   cst(1,2)=  r2
	   cst(2,2)=  r3*r2
	   cst(3,2)=  0.0
      
	   cst(1,3)= -1.0d0
	   cst(2,3)=  0.0
	   cst(3,3)=  0.0
	   ic=ic+nslips(1)
	endif
c-------BCC 48 slip systems
	if(ibcc.eq.1)then

      R2=1.0D0/DSQRT(2.0D0)
      R3=1.0D0/DSQRT(3.0D0)
      R6=1.0D0/DSQRT(6.0D0)
      R26=2.0D0/DSQRT(6.0D0)
      R14=1.0D0/DSQRT(14.0D0)
      R214=2.0D0/DSQRT(14.0D0)
      R314=3.0D0/DSQRT(14.0D0)
C
C   SLIP PLANE
	DO  I=1,2
	   CMT(1,I)= 0.0
	   CMT(2,I)= R2
	   CMT(3,I)= R2
	enddo
	DO  I=3,4
	   CMT(1,I)= R2
	   CMT(2,I)= R2
	   CMT(3,I)= 0.0
	enddo
	DO  I=5,6
	   CMT(1,I)= R2
	   CMT(2,I)= 0.0
	   CMT(3,I)= R2
	enddo
	DO  I=7,8
	   CMT(1,I)= 0.0
	   CMT(2,I)= R2
	   CMT(3,I)=-R2
	enddo
	DO  I=9,10
	   CMT(1,I)=-R2
	   CMT(2,I)= R2
	   CMT(3,I)= 0.0
	enddo
	DO  I=11,12
	   CMT(1,I)= R2
	   CMT(2,I)= 0.0
	   CMT(3,I)=-R2
	enddo
C  number:13
        CMT(1,13)= R26
        CMT(2,13)= R6
        CMT(3,13)= R6
C  number:14
        CMT(1,14)= R6
        CMT(2,14)= R6
        CMT(3,14)= R26
C  number:15
        CMT(1,15)= R6
        CMT(2,15)= R26
        CMT(3,15)= R6
C  number:16
        CMT(1,16)=-R26
        CMT(2,16)= R6
        CMT(3,16)= R6
C  number:17
        CMT(1,17)=-R6
        CMT(2,17)= R6
        CMT(3,17)= R26
C  number:18
        CMT(1,18)=-R6
        CMT(2,18)= R26
        CMT(3,18)= R6
C  number:19
        CMT(1,19)= R26
        CMT(2,19)= R6
        CMT(3,19)=-R6
C  number:20
        CMT(1,20)= R6
        CMT(2,20)= R6
        CMT(3,20)=-R26
C  number:21
        CMT(1,21)= R6
        CMT(2,21)= R26
        CMT(3,21)=-R6
C  number:22
        CMT(1,22)= R26
        CMT(2,22)=-R6
        CMT(3,22)= R6
C  number:23
        CMT(1,23)= R6
        CMT(2,23)=-R6
        CMT(3,23)= R26
C  number:24
        CMT(1,24)= R6
        CMT(2,24)=-R26
        CMT(3,24)= R6
C  number:25
        CMT(1,25)= R314
        CMT(2,25)= R14
        CMT(3,25)= R214
C  number:26
        CMT(1,26)= R14
        CMT(2,26)= R214
        CMT(3,26)= R314
C  number:27
        CMT(1,27)= R214
        CMT(2,27)= R314
        CMT(3,27)= R14
C  number:28
        CMT(1,28)= R214
        CMT(2,28)= R14
        CMT(3,28)= R314
C  number:29
        CMT(1,29)= R14
        CMT(2,29)= R314
        CMT(3,29)= R214
C  number:30
        CMT(1,30)= R314
        CMT(2,30)= R214
        CMT(3,30)= R14
C  number:31
        CMT(1,31)=-R314
        CMT(2,31)= R14
        CMT(3,31)= R214
C  number:32
        CMT(1,32)=-R14
        CMT(2,32)= R214
        CMT(3,32)= R314
C  number:33
        CMT(1,33)=-R214
        CMT(2,33)= R314
        CMT(3,33)= R14
C  number:34
        CMT(1,34)=-R214
        CMT(2,34)= R14
        CMT(3,34)= R314
C  number:35
        CMT(1,35)=-R14
        CMT(2,35)= R314
        CMT(3,35)= R214
C  number:36
        CMT(1,36)=-R314
        CMT(2,36)= R214
        CMT(3,36)= R14
C  number:37
        CMT(1,37)= R314
        CMT(2,37)= R14
        CMT(3,37)=-R214
C  number:38
        CMT(1,38)= R14
        CMT(2,38)= R214
        CMT(3,38)=-R314
C  number:39
        CMT(1,39)= R214
        CMT(2,39)= R314
        CMT(3,39)=-R14
C  number:40
        CMT(1,40)= R214
        CMT(2,40)= R14
        CMT(3,40)=-R314
C  number:41
        CMT(1,41)= R14
        CMT(2,41)= R314
        CMT(3,41)=-R214
C  number:42
        CMT(1,42)= R314
        CMT(2,42)= R214
        CMT(3,42)=-R14
C  number:43
        CMT(1,43)= R314
        CMT(2,43)=-R14
        CMT(3,43)= R214
C  number:44
        CMT(1,44)= R14
        CMT(2,44)=-R214
        CMT(3,44)= R314
C  number:45
        CMT(1,45)= R214
        CMT(2,45)=-R314
        CMT(3,45)= R14
C  number:46
        CMT(1,46)= R214
        CMT(2,46)=-R14
        CMT(3,46)= R314
C  number:47
        CMT(1,47)= R14
        CMT(2,47)=-R314
        CMT(3,47)= R214
C  number:48
        CMT(1,48)= R314
        CMT(2,48)=-R214
        CMT(3,48)= R14
C
C  SLIP DIRECTION
C  number:1
        CST(1,1)= R3
        CST(2,1)= R3
        CST(3,1)=-R3
C  number:2
        CST(1,2)= R3
        CST(2,2)=-R3
        CST(3,2)= R3
C  number:3
        CST(1,3)=-R3
        CST(2,3)= R3
        CST(3,3)= R3
C  number:4
        CST(1,4)= R3
        CST(2,4)=-R3
        CST(3,4)= R3
C  number:5
        CST(1,5)=-R3
        CST(2,5)= R3
        CST(3,5)= R3
C  number:6
        CST(1,6)= R3
        CST(2,6)= R3
        CST(3,6)=-R3
C  number:7
        CST(1,7)= R3
        CST(2,7)= R3
        CST(3,7)= R3
C  number:8
        CST(1,8)=-R3
        CST(2,8)= R3
        CST(3,8)= R3
C  number:9
        CST(1,9)= R3
        CST(2,9)= R3
        CST(3,9)= R3
C  number:10
        CST(1,10)= R3
        CST(2,10)= R3
        CST(3,10)=-R3
C  number:11
        CST(1,11)= R3
        CST(2,11)= R3
        CST(3,11)= R3
C  number:12
        CST(1,12)= R3
        CST(2,12)=-R3
        CST(3,12)= R3
C  number:13
        CST(1,13)=-R3
        CST(2,13)= R3
        CST(3,13)= R3
C  number:14
        CST(1,14)= R3
        CST(2,14)= R3
        CST(3,14)=-R3
C  number:15
        CST(1,15)= R3
        CST(2,15)=-R3
        CST(3,15)= R3
C  number:16
        CST(1,16)= R3
        CST(2,16)= R3
        CST(3,16)= R3
C  number:17
        CST(1,17)= R3
        CST(2,17)=-R3
        CST(3,17)= R3
C  number:18
        CST(1,18)= R3
        CST(2,18)= R3
        CST(3,18)=-R3
C  number:19
        CST(1,19)= R3
        CST(2,19)=-R3
        CST(3,19)= R3
C  number:20
        CST(1,20)= R3
        CST(2,20)= R3
        CST(3,20)= R3
C  number:21
        CST(1,21)=-R3
        CST(2,21)= R3
        CST(3,21)= R3
C  number:22
        CST(1,22)= R3
        CST(2,22)= R3
        CST(3,22)=-R3
C  number:23
        CST(1,23)=-R3
        CST(2,23)= R3
        CST(3,23)= R3
C  number:24
        CST(1,24)= R3
        CST(2,24)= R3
        CST(3,24)= R3
C  number:25
        CST(1,25)=-R3
        CST(2,25)= R3
        CST(3,25)= R3
C  number:26
        CST(1,26)= R3
        CST(2,26)= R3
        CST(3,26)=-R3
C  number:27
        CST(1,27)= R3
        CST(2,27)=-R3
        CST(3,27)= R3
C  number:28
        CST(1,28)= R3
        CST(2,28)= R3
        CST(3,28)=-R3
C  number:29
        CST(1,29)= R3
        CST(2,29)=-R3
        CST(3,29)= R3
C  number:30
        CST(1,30)=-R3
        CST(2,30)= R3
        CST(3,30)= R3
C  number:31
        CST(1,31)= R3
        CST(2,31)= R3
        CST(3,31)= R3
C  number:32
        CST(1,32)= R3
        CST(2,32)=-R3
        CST(3,32)= R3
C  number:33
        CST(1,33)= R3
        CST(2,33)= R3
        CST(3,33)=-R3
C  number:34
        CST(1,34)= R3
        CST(2,34)=-R3
        CST(3,34)= R3
C  number:35
        CST(1,35)= R3
        CST(2,35)= R3
        CST(3,35)=-R3
C  number:36
        CST(1,36)= R3
        CST(2,36)= R3
        CST(3,36)= R3
C  number:37
        CST(1,37)= R3
        CST(2,37)=-R3
        CST(3,37)= R3
C  number:38
        CST(1,38)= R3
        CST(2,38)= R3
        CST(3,38)= R3
C  number:39
        CST(1,39)=-R3
        CST(2,39)= R3
        CST(3,39)= R3
C  number:40
        CST(1,40)= R3
        CST(2,40)= R3
        CST(3,40)= R3
C  number:41
        CST(1,41)=-R3
        CST(2,41)= R3
        CST(3,41)= R3
C  number:42
        CST(1,42)= R3
        CST(2,42)=-R3
        CST(3,42)= R3
C  number:43
        CST(1,43)= R3
        CST(2,43)= R3
        CST(3,43)=-R3
C  number:44
        CST(1,44)=-R3
        CST(2,44)= R3
        CST(3,44)= R3
C  number:45
        CST(1,45)= R3
        CST(2,45)= R3
        CST(3,45)= R3
C  number:46
        CST(1,46)=-R3
        CST(2,46)= R3
        CST(3,46)= R3
C  number:47
        CST(1,47)= R3
        CST(2,47)= R3
        CST(3,47)= R3
C  number:48
        CST(1,48)= R3
        CST(2,48)= R3
        CST(3,48)=-R3
C   	
C	ic=ic+nslips(6)
	endif
c------------------------end of BCC slip Systems---------

	if(ihcp.eq.1) then
c---    prismatic slip system
	   cmt(1,1+ic)=  0.0
	   cmt(2,1+ic)=  1.0d0 
	   cmt(3,1+ic)=  0.0
	   
	   cmt(1,2+ic)= -r3*r2
	   cmt(2,2+ic)=  r2
	   cmt(3,2+ic)=  0.0
	   
	   cmt(1,3+ic)= -r3*r2
	   cmt(2,3+ic)= -r2 
	   cmt(3,3+ic)=  0.0
	   
	   
	   cst(1,1+ic)=  1.0d0
	   cst(2,1+ic)=  0.0
	   cst(3,1+ic)=  0.0
	   
	   cst(1,2+ic)=  r2
	   cst(2,2+ic)=  r3*r2
	   cst(3,2+ic)=  0.0
	   
	   cst(1,3+ic)= -r2
	   cst(2,3+ic)=  r3*r2
	   cst(3,3+ic)=  0.0
	   ic=ic+nslips(2)
	endif
	if(ihcp.eq.1) then
c       pyramidal a slip
	   bunbo=sqrt(4*(1.594)**2+3)
	   bunshi=1.594
	   cmt(1,1+ic)=0.0
	   cmt(2,1+ic)=-2*bunshi/bunbo
	   cmt(3,1+ic)=r3/bunbo
         
	   cmt(1,2+ic)=r3*bunshi/bunbo
	   cmt(2,2+ic)=-bunshi/bunbo
	   cmt(3,2+ic)=r3/bunbo
	   
	   cmt(1,3+ic)= r3*bunshi/bunbo
	   cmt(2,3+ic)= bunshi/bunbo
	   cmt(3,3+ic)= r3/bunbo 
	   
	   cmt(1,4+ic)= 0.0
	   cmt(2,4+ic)= 2*bunshi/bunbo
	   cmt(3,4+ic)= r3/bunbo
	   
	   cmt(1,5+ic)=-r3*bunshi/bunbo
	   cmt(2,5+ic)= bunshi/bunbo
	   cmt(3,5+ic)= r3/bunbo
         
	   cmt(1,6+ic)=-r3*bunshi/bunbo
	   cmt(2,6+ic)=-bunshi/bunbo
	   cmt(3,6+ic)= r3/bunbo
	   

	   cst(1,1+ic)= 1.0d0
	   cst(2,1+ic)= 0.0
	   cst(3,1+ic)= 0.0 
         
	   cst(1,2+ic)= r2
	   cst(2,2+ic)= r2*r3
	   cst(3,2+ic)= 0.0        
	   
	   cst(1,3+ic)=-r2
	   cst(2,3+ic)= r2*r3
	   cst(3,3+ic)= 0.0 
	   
	   cst(1,4+ic)=-1.0d0
	   cst(2,4+ic)= 0.0
	   cst(3,4+ic)= 0.0
	   
	   cst(1,5+ic)=-r2
	   cst(2,5+ic)=-r2*r3
	   cst(3,5+ic)= 0.0  
         
	   cst(1,6+ic)= r2
	   cst(2,6+ic)=-r2*r3
	   cst(3,6+ic)= 0.0
	   ic=ic+nslips(3)
	endif
	if(ihcp.eq.1) then
C       1st order <c+a> slip
	 bunbo=sqrt(4.0*(1.594)**2+3.0)
         bunshi=1.594
	    
	 cmt(1,1+ic)=  0.0
	 cmt(2,1+ic)=  -2*bunshi/bunbo 
	 cmt(3,1+ic)=  r3/bunbo
	 
	 cmt(1,2+ic)=  r3*bunshi/bunbo
	 cmt(2,2+ic)=  -bunshi/bunbo 
	 cmt(3,2+ic)=  r3/bunbo
	 
	 cmt(1,3+ic)=  r3*bunshi/bunbo
	 cmt(2,3+ic)=  bunshi/bunbo 
	 cmt(3,3+ic)=  r3/bunbo
	
	 cmt(1,4+ic)=  0.0
	 cmt(2,4+ic)=  2*bunshi/bunbo 
	 cmt(3,4+ic)=  r3/bunbo
	 
	 cmt(1,5+ic)=  -r3*bunshi/bunbo
	 cmt(2,5+ic)=  bunshi/bunbo 
	 cmt(3,5+ic)=  r3/bunbo
	 
	 cmt(1,6+ic)=  -r3*bunshi/bunbo
	 cmt(2,6+ic)=  -bunshi/bunbo 
	 cmt(3,6+ic)=  r3/bunbo
	
	 cmt(1,7+ic)=  0.0
	 cmt(2,7+ic)=  -2*bunshi/bunbo 
	 cmt(3,7+ic)=  r3/bunbo
	 
	 cmt(1,8+ic)=  r3*bunshi/bunbo
	 cmt(2,8+ic)=  -bunshi/bunbo 
	 cmt(3,8+ic)=  r3/bunbo
	 
	 cmt(1,9+ic)=  r3*bunshi/bunbo
	 cmt(2,9+ic)=  bunshi/bunbo 
	 cmt(3,9+ic)=  r3/bunbo
	 
	 cmt(1,10+ic)=  0.0
	 cmt(2,10+ic)=  2*bunshi/bunbo 
	 cmt(3,10+ic)=  r3/bunbo
	 
	 cmt(1,11+ic)= - r3*bunshi/bunbo
	 cmt(2,11+ic)=  bunshi/bunbo 
	 cmt(3,11+ic)=  r3/bunbo
	
	 cmt(1,12+ic)=  -r3*bunshi/bunbo
	 cmt(2,12+ic)=  -bunshi/bunbo 
	 cmt(3,12+ic)=  r3/bunbo
	
c-----------slip directions

	 bunb=2.0*(sqrt((1.594)**2+1.0))
	 bush=1.594
	 cst(1,1+ic)=  1.0/bunb
	 cst(2,1+ic)=  r3/bunb 
	 cst(3,1+ic)=  2*bush/bunb

	 cst(1,2+ic)=  -1.0/bunb
	 cst(2,2+ic)=  r3/bunb 
	 cst(3,2+ic)=  2*bush/bunb

	 cst(1,3+ic)=  -2.0/bunb
	 cst(2,3+ic)=  0.0 
	 cst(3,3+ic)=  2*bush/bunb

	 cst(1,4+ic)=  -1.0/bunb
	 cst(2,4+ic)=  -r3/bunb 
	 cst(3,4+ic)=  2*bush/bunb

	 cst(1,5+ic)=  1.0/bunb
	 cst(2,5+ic)=  -r3/bunb 
	 cst(3,5+ic)=  2*bush/bunb

	 cst(1,6+ic)=  2.0/bunb
	 cst(2,6+ic)=  0.0 
	 cst(3,6+ic)=  2*bush/bunb

	 cst(1,7+ic)=  -1.0/bunb
	 cst(2,7+ic)=  r3/bunb 
	 cst(3,7+ic)=  2*bush/bunb

	 cst(1,8+ic)=  -2.0/bunb
	 cst(2,8+ic)=  0.0 
	 cst(3,8+ic)=  2*bush/bunb

	 cst(1,9+ic)=  -1.0/bunb
	 cst(2,9+ic)=  -r3/bunb 
	 cst(3,9+ic)=  2*bush/bunb

	 cst(1,10+ic)=  1.0/bunb
	 cst(2,10+ic)=  -r3/bunb 
	 cst(3,10+ic)=  2*bush/bunb
	
	 cst(1,11+ic)=  2.0/bunb
	 cst(2,11+ic)=  0.0 
	 cst(3,11+ic)=  2*bush/bunb

	 cst(1,12+ic)=  1.0/bunb
	 cst(2,12+ic)=  r3/bunb 
	 cst(3,12+ic)=  2*bush/bunb

	 ic=ic+nslips(4)

	endif
	if(ihcp.eq.1)then
C       2nd order <c+a> slip
	   bunb=2.0*(sqrt((1.594)**2+1.0))
	   bush=1.594
	   cmt(1,1+ic)=  bush/bunb
	   cmt(2,1+ic)=  -r3*bush/bunb 
	   cmt(3,1+ic)=  2.0/bunb
	   
	   cmt(1,2+ic)= 2*bush/bunb
	   cmt(2,2+ic)=  0.0
	   cmt(3,2+ic)=  2.0/bunb
	   
	   cmt(1,3+ic)=  bush/bunb
	   cmt(2,3+ic)=  r3*bush/bunb 
	   cmt(3,3+ic)=  2.0/bunb

	   cmt(1,4+ic)=  -bush/bunb
	   cmt(2,4+ic)=  r3*bush/bunb 
	   cmt(3,4+ic)=  2.0/bunb
	   
	   cmt(1,5+ic)= -2.0*bush/bunb
	   cmt(2,5+ic)= 0.0 
	   cmt(3,5+ic)=  2.0/bunb
	   
	   cmt(1,6+ic)=  -bush/bunb
	   cmt(2,6+ic)=  -r3*bush/bunb 
	   cmt(3,6+ic)=  2.0/bunb
	   
c----------------slip directions------------
	   
	   cst(1,1+ic)=  -1.0/bunb
	   cst(2,1+ic)=  r3/bunb 
	   cst(3,1+ic)=  2.0*bush/bunb
	   
	   cst(1,2+ic)=  -2.0/bunb
	   cst(2,2+ic)=  0.0 
	   cst(3,2+ic)=  2.0*bush/bunb
	   
	   cst(1,3+ic)=  -1.0/bunb
	   cst(2,3+ic)=  -r3/bunb 
	   cst(3,3+ic)=  2.0*bush/bunb
	   
	   cst(1,4+ic)=  1.0/bunb
	   cst(2,4+ic)=  -r3/bunb 
	   cst(3,4+ic)=  2.0*bush/bunb
	   
	   cst(1,5+ic)=  2.0/bunb
	   cst(2,5+ic)=  0.0 
	   cst(3,5+ic)=  2.0*bush/bunb
	   
	   cst(1,6+ic)=  1.0/bunb
	   cst(2,6+ic)=  r3/bunb 
	   cst(3,6+ic)=  2.0*bush/bunb
	   ic=ic+nslips(5)
	endif
	if(ihcp.eq.1)islip=30
	if(ibcc.eq.1)islip=48

	return
	end


C-----------------------------------------------------

C    THIS SUBROUTINE CALCULATES THE TRANSFORMATION MATRIX
c----------------------------------------------------------
c     phi   - euler(1)
c     theta - euler(2)
c     omega - euler(3)
c---------------------------------------------------
      subroutine euler_slip(euler,tlgt)
      implicit double precision (a-h,o-z)  

c      include 'aba_param.inc'

      dimension euler(3),tlg(3,3),tlgt(3,3)

      pi=4.d0*datan(1.d0)

      phi=euler(1)
      theta =euler(2)
      omega  =euler(3)

  
      sp=dsin(phi)                      
      cp=dcos(phi)                     
      st=dsin(theta)                     
      ct=dcos(theta)                    
      so=dsin(omega)                    
      co=dcos(omega)   
      tlg(1,1)=co*cp-so*sp*ct
      tlg(1,2)=co*sp+so*ct*cp   
      tlg(1,3)=so*st   
      tlg(2,1)=-so*cp-sp*co*ct 
      tlg(2,2)=-so*sp+ct*co*cp
      tlg(2,3)=co*st
      tlg(3,1)=sp*st       
      tlg(3,2)=-st*cp       
      tlg(3,3)=ct

	call trans(tlg,tlgt)

      return
      end   

C---------------------------------------------------------

C   REFERING TO THE THESIS THIS SUBROUTINE CALCULATES A,T*tr,B,C
C   AND IT CALLS THE UPDATE IMPLICIT SUBROUTINE WHICH USES 
C   NEWTON RAPHSON FOR CALCULATING T* 
c-----------------------------------------------------------------
c
c         fp_t_inv - inverse of the plastic deformation gradient
c         a_tan    - A (from thesis Pg 175)
c         t_tr_1d  - T*tr
c         b_alpha  - B (Pg 175)
c         c_alpha_1d - C (Pg 175)
c----------------------------------------------------------------

      subroutine ssmatc3(dfgrd1,g_alpha_t,tpk_1d_t,s_tan,dtime,
     & fp_t,de_gg_2d,
     & c_alpha_1d,delta_gamma,tpk_1d_tau,g_alpha_tau,tau,xn,xr,
     & xs_bar,xh0,s_gg_2d,a_dot,rate_exp,pnewdt1,ifirst_check,
     & islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     & a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,ipoly_switch,
     & xn_trb_comp,xr_trb_comp,xh0_trb_comp,xs_trb_comp,
     & xn_pra_comp,xr_pra_comp,xh0_pra_comp,xs_pra_comp,isign,xc,xd,
     & chi_t,chi_tau,epsilon_p_bar,forest,parallel,Act_Energy,
     & gnd_const1,gnd_const2,shear_modulus,bur,passing_gnd,
     & F_THERMAL_TAU,TEMPERATURE,REF_TEMPERATURE,SOFTENING_POWER) !kourosh, KOUROSH_TEMPERATURE

       implicit real*8(a-h,o-z)
      
c      include 'aba_param.inc'

       parameter (itotslips=8,nslip=100)
c	common/slips/islip
       dimension g_alpha_t(nslip),g_alpha_tau(nslip),
     &      delta_gamma(nslip)

       dimension tpk_1d_t(6),tpk_1d_tau(6),s_tan(3,3,nslip),
     &      fp_t(3,3),fp_t_inv(3,3),fp_t_inv_t(3,3),forest(30),
     &      dfgrd1(3,3),dfgrd1_t(3,3),xs_bar(nslip),parallel(30),
     &      cc(3,3),a1_tan(3,3),a_tan(3,3),a_tan_1d(6),xr(nslip),
     &    b_alpha(3,3,nslip),b_alpha_1d(6,nslip),c_alpha_1d(6,nslip),
     &      t_tr_1d(6),ckrone_1d(6),de_gg_2d(6,6),tau(nslip),
     &      xh0(nslip),xn(nslip),s_gg_2d(6,6),passing_gnd(nslip),
     &      xh_0(5),xh_inf(5),xg_0(5),xg_inf(5),tot_gamma(nslip),
     &      a_dot_bcc(5),rate_exp_bcc(5),xn_trb_comp(itotslips),
     &      xr_trb_comp(itotslips),xh0_trb_comp(itotslips),
     &      xs_trb_comp(itotslips),xn_pra_comp(itotslips),
     &      xr_pra_comp(itotslips),xh0_pra_comp(itotslips),
     &      xs_pra_comp(itotslips),chi_t(nslip),chi_tau(nslip),
     &      g_alpha_t_com(nslip)

C************** KOUROSH_TEMPERATURE ********************
       DIMENSION::F_THERMAL_TAU(3,3),F_THERMAL_TAU_INV(3,3),
     &            F_THERMAL_TAU_INV_T(3,3),A_TAN_THERMAL(3,3),
     &            B_ALPHA_THERMAL(3,3,NSLIP)
C************** KOUROSH_TEMPERATURE ********************
       
c	common/crys_const/a_dot,rate_exp
       

      call matinv3(fp_t,fp_t_inv,det_fp_t) 

      if(det_fp_t.eq.0d0) then
        write(*,*) '0 divide'
        stop
      endif    

      call trans(fp_t_inv,fp_t_inv_t)
      call trans(dfgrd1,dfgrd1_t)
      call mat33(cc,dfgrd1_t,dfgrd1,3)
      call mat33(a1_tan,fp_t_inv_t,cc,3)
      call mat33(a_tan,a1_tan,fp_t_inv,3)
      
C************** KOUROSH_TEMPERATURE ********************
       A_TAN_THERMAL=0.D0
       CALL MATINV3(F_THERMAL_TAU,F_THERMAL_TAU_INV,DUMMY)

       IF(DUMMY.EQ.0.D0) THEN
        WRITE(*,*) 'F_THERMAL HAS 0 DETERMINANT'
        STOP
       ENDIF 
      
       CALL TRANS(F_THERMAL_TAU_INV,F_THERMAL_TAU_INV_T)
       A_TAN_THERMAL=MATMUL(MATMUL(F_THERMAL_TAU_INV_T,A_TAN),
     &                      F_THERMAL_TAU_INV) 

C      call tr2to1(a_tan,a_tan_1d)
      call tr2to1(A_TAN_THERMAL,a_tan_1d)
 
C************** KOUROSH_TEMPERATURE ********************

      call cal_ckrone_1d(ckrone_1d)

      do i=1,6
        t_tr_1d(i)=0.d0
        do j=1,6
          t_tr_1d(i)=t_tr_1d(i)+0.5d0*de_gg_2d(i,j)
     &   *(a_tan_1d(j)-ckrone_1d(j))
        enddo
      enddo
      
C************** KOUROSH_TEMPERATURE ********************
      B_ALPHA_THERMAL=0.D0
      
      do isys=1,islip
        do i=1,3
          do j=1,3
            b_alpha(i,j,isys)=0.d0
            do k=1,3
              b_alpha(i,j,isys)=b_alpha(i,j,isys)
     &        +a_tan(i,k)*s_tan(k,j,isys)+s_tan(k,i,isys)*a_tan(k,j)
            enddo
          enddo
        enddo
        
        B_ALPHA_THERMAL(:,:,ISYS)=MATMUL(MATMUL(F_THERMAL_TAU_INV_T,
     &                           B_ALPHA(:,:,ISYS)),F_THERMAL_TAU_INV) 
        
      enddo

       
C       call tr2to1crys(b_alpha,b_alpha_1d,islip)
       call tr2to1crys(B_ALPHA_THERMAL,b_alpha_1d,islip)
C************** KOUROSH_TEMPERATURE ********************

      do isys=1,islip
        do i=1,6
          c_alpha_1d(i,isys)=0.d0
          do j=1,6
            c_alpha_1d(i,isys)=c_alpha_1d(i,isys)
     &      +0.5d0*de_gg_2d(i,j)*b_alpha_1d(j,isys)
          enddo
        enddo
      enddo


	
      call update_implicit(dtime,s_tan,delta_gamma,t_tr_1d,c_alpha_1d,
     &g_alpha_t,tpk_1d_t,tpk_1d_tau,g_alpha_tau,tau,xn,xr,xs_bar,xh0,
     &s_gg_2d,a_dot,rate_exp,pnewdt1,ifirst_check,islip,
     &tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,a_dot_bcc,rate_exp_bcc,
     &a_dot_hcp,rate_exp_hcp,ipoly_switch,xn_trb_comp,xr_trb_comp,
     &xh0_trb_comp,xs_trb_comp,xn_pra_comp,xr_pra_comp,
     &xh0_pra_comp,xs_pra_comp,isign,xc,xd,chi_t,chi_tau,epsilon_p_bar,
     &forest,parallel,fp_t,Act_Energy,gnd_const1,gnd_const2,
     &shear_modulus,bur,passing_gnd,F_THERMAL_TAU,
     &TEMPERATURE,REF_TEMPERATURE,SOFTENING_POWER) !kourosh, KOUROSH_TEMPERATURE



      return
      end




c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine sigmat(x,xx)    
c                                                                     
c  arranges into a symmetric array xx, the six components of a vector x
c                                                                     
       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'
      dimension x(6),xx(3,3)
c
      xx(1,1)=x(1)                                                    
      xx(2,2)=x(2)                                                    
      xx(3,3)=x(3)                                                    
      xx(2,3)=x(6)                                                    
      xx(3,2)=x(6)                                                    
      xx(3,1)=x(5)                                                    
      xx(1,3)=x(5)                                                    
      xx(1,2)=x(4)                                                    
      xx(2,1)=x(4)                                                    
      return
      end




C---------------------------------------------------------

C   THIS USES NEWTON RAPHSON TO CALCULATE T*
c ---------------------------------------------
c      xjn_2d - Jacobian of Newton Raphson for T*
c      g_n    - Gn (Residual) (Pg 175)
c      xjn_gn - Jn_inv*[Gn] (Pg 175)
c--------------------------------------------

      subroutine update_implicit(dtime,s_tan,delta_gamma,t_tr_1d,
     &c_alpha_1d,
     &g_alpha_t,tpk_1d_t,tpk_1d_tau,g_alpha_tau,tau,xn,xr,xs_bar,xh0,
     &s_gg_2d,a_dot,rate_exp,pnewdt1,ifirst_check,islip,
     &tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,a_dot_bcc,rate_exp_bcc,
     &a_dot_hcp,rate_exp_hcp,ipoly_switch,xn_trb_comp,xr_trb_comp,
     &xh0_trb_comp,xs_trb_comp,xn_pra_comp,xr_pra_comp,
     &xh0_pra_comp,xs_pra_comp,isign,xc,xd,chi_t,chi_tau,epsilon_p_bar,
     &forest,parallel,fp_t,Act_Energy,gnd_const1,gnd_const2,
     &shear_modulus,bur,passing_gnd,F_THERMAL_TAU,
     &TEMPERATURE,REF_TEMPERATURE,SOFTENING_POWER) !kourosh, KOUROSH_TEMPERATURE
      
       implicit real*8(a-h,o-z)
c      include 'aba_param.inc'
     
      parameter (itotslips=8,nslip=100,g0=8.0d0)
c	 common/slips/islip

      dimension tpk_1d_tau(6),tpk_1d_t(6),g_alpha_tau(nslip),
     &xs_bar(nslip),g_alpha_t(nslip),s_tan(3,3,nslip),t_tr_1d(6),
     &g_n(6),c_alpha_1d(6,nslip),delta_gamma(nslip),tau(nslip),
     &xh0(nslip),xh_0(5),xh_inf(5),xg_0(5),xg_inf(5),tot_gamma(nslip),
     &xjn_2d(6,6),xjn_2d_inv(6,6),xjn_gn(6),xhard(nslip,nslip),
     &xn(nslip),xr(nslip),chi_t(nslip),chi_tau(nslip),s_gg_2d(6,6),
     &a_dot_bcc(5),rate_exp_bcc(5),xn_trb_comp(itotslips),
     &xr_trb_comp(itotslips),xh0_trb_comp(itotslips),
     &xs_trb_comp(itotslips),xn_pra_comp(itotslips),
     &xr_pra_comp(itotslips),xh0_pra_comp(itotslips),
     &xs_pra_comp(itotslips),forest(30),parallel(30),
     &hgnd(nslip,2),fp_t(3,3),passing_gnd(nslip)

C************** KOUROSH_TEMPERATURE ********************
      DIMENSION::F_THERMAL_TAU(3,3) 
C************** KOUROSH_TEMPERATURE ********************
     
       common/alaki/jelemm
	 
	iatg=0

      tpk_max=0.d0
      do i=1,6
       tpk_1d_tau(i)=tpk_1d_t(i)
       if (tpk_max.lt.dabs(tpk_1d_t(i))) tpk_max=dabs(tpk_1d_t(i)) 
      enddo

      do isys=1,islip
        g_alpha_tau(isys)=g_alpha_t(isys)
	   chi_tau(isys)=chi_t(isys)
      enddo

	g_tol=1.d0
	xtol0=1.d-10; xtol=xtol0*tpk_max; if (xtol.lt.xtol0) xtol=xtol0
	chi_tol=1.d0

	g_max=2*g_tol
	chi_max=2*chi_tol
      
      iter1=0
      iter_max=1000

C     rnorm, g_max defined to eneter the iteration loops

      do while((g_max.gt.g_tol.or.chi_max.gt.chi_tol).and.(iter1.lt.
     &iter_max))
         iter=0
         rnorm=2*xtol
         gamma_tol=0.02d0

         do while(rnorm.gt.xtol.and.iter.le.iter_max)
            
C     Calculating the residual

            xeta=1.d0
	    
            call stress_residue(tpk_1d_tau,s_tan,a_dot,dtime,rate_exp,
     & g_alpha_tau,t_tr_1d,c_alpha_1d,tau,delta_gamma,g_n,xjn_2d,1,
     &s_gg_2d,islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     &a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,isign,xc,xd,chi_t,
     &chi_tau,iatg,epsilon_p_bar,passing_gnd,F_THERMAL_TAU,
     & SOFTENING_POWER,REF_TEMPERATURE,TEMPERATURE) !kourosh, KOUROSH_TEMPERATURE 


     
      call check_det(fp_t,s_tan,delta_gamma,islip,iatg)

	    if(iatg.eq.1)then
	       ifirst_check=1
	       return
	    endif


            do isys=1,islip
               if(dabs(delta_gamma(isys)).gt.gamma_tol) xeta=0.25d0
            enddo

            
            if (iter.eq.0) xeta=0.25d0
            
            do i=1,6
               do j=1,6 
                  xjn_2d_inv(i,j)=xjn_2d(i,j)
               enddo
            enddo
            
            call matinv(xjn_2d_inv,6,6)
            do i=1,6   
               xjn_gn(i)=0.d0
               do k=1,6
                  xjn_gn(i)=xjn_gn(i)+xjn_2d_inv(i,k)*g_n(k)
               enddo
            enddo
            
            do i7=1,6
               tpk_1d_tau(i7)=tpk_1d_tau(i7)-xeta*xjn_gn(i7)     
            enddo   
            
            call stress_residue(tpk_1d_tau,s_tan,a_dot,dtime,rate_exp,
     & g_alpha_tau,t_tr_1d,c_alpha_1d,tau,delta_gamma,g_n,xjn_2d,0,
     & s_gg_2d,islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     & a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,isign,xc,xd,chi_t,
     & chi_tau,iatg,epsilon_p_bar,passing_gnd,F_THERMAL_TAU,
     & SOFTENING_POWER,REF_TEMPERATURE,TEMPERATURE) !kourosh, KOUROSH_TEMPERATURE 
 


	     call check_det(fp_t,s_tan,delta_gamma,islip,iatg)
           

	    if(iatg.eq.1)then
	       ifirst_check=1
	       return
	    endif


            call magn_vect(g_n,rnorm,6)
            
            iter=iter+1
         enddo ! do while(rnorm.gt.xtol.and.iter.le.iter_max)
         

	 ifirst_check=0
         if(rnorm.gt.xtol)then
            write(6,*)'The First Level Iteration did not converge'
	    write(6,*)'rnorm=',rnorm
            ifirst_check=1
            return
         endif
      
   
   
C     Hardness Iteration
	 call make_hard(xh0,g_alpha_tau,xs_bar,delta_gamma,
     &   dtime,a_dot,xr,xn,xhard,tau,islip,tot_gamma,xh_0,xh_inf,xg_0,
     &   xg_inf,itag_el,a_dot_hcp,rate_exp_hcp,ipoly_switch,
     &   xn_trb_comp,xr_trb_comp,xh0_trb_comp,xs_trb_comp,
     &   xn_pra_comp,xr_pra_comp,xh0_pra_comp,xs_pra_comp,isign,
     &	 epsilon_p_bar) ! kourosh
	 


	    call hard_GND(forest,parallel,hgnd,Act_Energy,
     &                gnd_const1,gnd_const2,shear_modulus,bur)	

    
     
	 g_max=0.d0
	 do isys1=1,islip
	    g_prev=g_alpha_tau(isys1)
	    g_alpha_tau(isys1)=g_alpha_t(isys1)
	    do isys2=1,islip
	       g_alpha_tau(isys1)=g_alpha_tau(isys1)+
     &         xhard(isys1,isys2)*dabs(delta_gamma(isys2))
	    enddo
	    g_alpha_tau(isys1)=g_alpha_tau(isys1)+hgnd(isys1,1)
	    
	    if(dabs(g_alpha_tau(isys1)-g_prev).gt.g_max)g_max=
     &      dabs(g_alpha_tau(isys1)-g_prev)
	 enddo
	 

C       Back Stress Iteration

	 chi_max=0.d0
	 do isys=1,islip
	    chi_prev=chi_tau(isys)
	    chi_tau(isys)=chi_t(isys)+xc*delta_gamma(isys)
	    chi_tau(isys)=chi_tau(isys)/(1.d0+xd*
     &      dabs(delta_gamma(isys)))   
	    if(dabs(chi_tau(isys)-chi_prev).gt.chi_max)chi_max=
     &      dabs(chi_tau(isys)-chi_prev)
	 enddo
	 
	
	 

	 iter1=iter1+1


	enddo ! do while((g_max.gt.g_tol.or.chi_max.gt....

	call stress_residue(tpk_1d_tau,s_tan,a_dot,dtime,rate_exp,
     &g_alpha_tau,t_tr_1d,c_alpha_1d,tau,delta_gamma,g_n,xjn_2d,0,
     &s_gg_2d,islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     &a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,isign,xc,xd,chi_t,
     &chi_tau,iatg,epsilon_p_bar,passing_gnd,F_THERMAL_TAU,
     & SOFTENING_POWER,REF_TEMPERATURE,TEMPERATURE) !kourosh, KOUROSH_TEMPERATURE 

    

      call check_det(fp_t,s_tan,delta_gamma,islip,iatg)
	

      if(iatg.eq.1)then
         ifirst_check=1
         return
      endif
      
! deniz's      
      if(iatg.eq.2)then
         ifirst_check=1
         write(*,*) 'tau/g exceed 2 @ Element',jelemm
         return
      endif
! deniz's

	gamma_tol=0.02d0
	ni=0	
	do isys=1,islip
	   if(dabs(delta_gamma(isys)).gt.gamma_tol)pnewdt1=0.5d0
	   if(dabs(delta_gamma(isys)).le.0.0001*gamma_tol)ni=ni+1
	enddo
	if(ni.eq.islip)pnewdt1=2.0d0 	
	
	if(g_max.gt.g_tol)then
	   write(6,*)'Second Level Iteration did not converge'
	   write(6,*)'g_max=',g_max
	   ifirst_check=1
	   return 
	endif


	if(chi_max.gt.chi_tol)then
	   write(6,*)'Back Stress iteration did not converge'
	   write(6,*)'chi_max=',chi_max
	   ifirst_check=1
	   return
	endif
	   
	
	
	return
	end  

C-----------------------------------------------------------------------


C-----------------------------------------------------------------------

	subroutine make_hard(xh0,g_alpha_tau,xs_bar,delta_gamma,
     &  dtime,a_dot,xr,xn,xhard,tau,islip,
     &  tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,a_dot_hcp,
     &  rate_exp_hcp,ipoly_switch,xn_trb_comp,xr_trb_comp,xh0_trb_comp,
     &  xs_trb_comp,xn_pra_comp,xr_pra_comp,xh0_pra_comp,
     &  xs_pra_comp,isign,epsilon_p_bar) ! kourosh
	implicit real*8(a-h,o-z)
c       include 'aba_param.inc'
		intrinsic:: dtanh   !kourosh
	parameter(nslip=100)
	parameter (itotslips=8)	
c	common/slips/islip
	dimension g_alpha_tau(nslip),delta_gamma(nslip),
     &  xhard(nslip,nslip)
	dimension qab(nslip,nslip),gamma_dot(nslip),xh0(nslip),
     &  xs_bar(nslip),
     &  xn(nslip),xr(nslip),tot_gamma(nslip),xh_0(5),xh_inf(5),
     &  xg_0(5),xg_inf(5),tau(nslip),xn_used(nslip),xr_used(nslip),
     &  xh0_used(nslip),xs_bar_used(nslip),xn_trb_comp(itotslips),
     &  xr_trb_comp(itotslips),xh0_trb_comp(itotslips),
     &  xs_trb_comp(itotslips),
     &  xn_pra_comp(itotslips),xr_pra_comp(itotslips),
     &  xh0_pra_comp(itotslips),
     &  xs_pra_comp(itotslips)


	dimension xh_0_bccj(nslip),xh_inf_bccj(nslip),xg_0_bccj(nslip),
     &  xg_inf_bccj(nslip),xsbarj(nslip),xh0j(nslip),xnj(nslip),
     &  xrj(nslip),hbeta(nslip)
	 
	
	common/soften1/gamma_dot_0_basal,gamma_dot_0_prism,
     &gamma_dot_0_pyram,gamma_dot_0_capyr
	common/soften2/xk_star_basal,xk_star_prism,xk_star_pyram,
     &xk_star_capyr
	common/soften3/xlp_basal,xlp_prism,xlp_pyram,xlp_capyr
	common/soften4/xk_basal,xk_prism,xk_pyram,xk_capyr
	common/soften5/gamma_dot_ini_basal,gamma_dot_ini_prism,
     &gamma_dot_ini_pyram,gamma_dot_ini_capyr
	common/soften6/rate_basal,rate_prism,rate_pyram,rate_capyr	


	do isys_a=1,islip
	   do isys_b=1,islip
	      xhard(isys_a,isys_b)=0.d0
	   enddo
	enddo

	if(dabs(dtime).gt.1d-10)then	
	do isys_a=1,islip
	   do isys_b=1,islip
	      if(isys_a.eq.isys_b)then
		 qab(isys_a,isys_b)=1.0d0
	      else
		 qab(isys_a,isys_b)=1.0d0
!		 qab(isys_a,isys_b)=0.0d0
	      endif
	   enddo
	enddo
	
	if(itag_el.eq.2)then
	   tg=0.d0
	   
	   do i=1,48
	      tg=tg+tot_gamma(i)+dabs(delta_gamma(i))
	   enddo

	   
	   do isys_b=1,48
	      if(isys_b.le.12)then
		 xh_0v=xh_0(1)
		 xh_infv=xh_inf(1)
		 xg_0v=xg_0(1)
		 xg_infv=xg_inf(1)
	      endif
	      
	      if(isys_b.ge.13.and.isys_b.le.24)then
		 if(isign.eq.1)then
		    xh_0v=xh_0(2)
		    xh_infv=xh_inf(2)
		    xg_0v=xg_0(2)
		    xg_infv=xg_inf(2)
		 else
		    xh_0v=xh_0(3)
		    xh_infv=xh_inf(3)
		    xg_0v=xg_0(3)
		    xg_infv=xg_inf(3)
		 endif
	      endif
	      
	      if(isys_b.gt.24)then
		 if(isign.eq.1)then
		    xh_0v=xh_0(4)
		    xh_infv=xh_inf(4)
		    xg_0v=xg_0(4)
		    xg_infv=xg_inf(4)
		 else
		    xh_0v=xh_0(5)
		    xh_infv=xh_inf(5)
		    xg_0v=xg_0(5)
		    xg_infv=xg_inf(5)
		 endif
	      endif

	      xh_0_bccj(isys_b)=xh_0v
	      xh_inf_bccj(isys_b)=xh_infv
	      xg_0_bccj(isys_b)=xg_0v
	      xg_inf_bccj(isys_b)=xg_infv
	      xhtemp1=(xh_0v-xh_infv)/(xg_infv-xg_0v)*tg
	      xhtemp2=0.5d0*(dexp(xhtemp1)+dexp(-xhtemp1))
	      xhtemp3=1.d0/xhtemp2**2.d0		 
	      hbeta(isys_b)=xh_infv+(xh_0v-xh_infv)*xhtemp3

		 
	   enddo ! isys_b=1,48




	   do isys_a=1,48
	      do isys_b=1,48
		 
		 if(isys_b.le.12)then
		    xh_0v=xh_0(1)
		    xh_infv=xh_inf(1)
		    xg_0v=xg_0(1)
		    xg_infv=xg_inf(1)
		 endif
		 
		 if(isys_b.ge.13.and.isys_b.le.24)then
		    if(isign.eq.1)then
		       xh_0v=xh_0(2)
		       xh_infv=xh_inf(2)
		       xg_0v=xg_0(2)
		       xg_infv=xg_inf(2)
		    else
		       xh_0v=xh_0(3)
		       xh_infv=xh_inf(3)
		       xg_0v=xg_0(3)
		       xg_infv=xg_inf(3)
		    endif
		 endif
		 
		 if(isys_b.gt.24)then
		    if(isign.eq.1)then
		       xh_0v=xh_0(4)
		       xh_infv=xh_inf(4)
		       xg_0v=xg_0(4)
		       xg_infv=xg_inf(4)
		    else
		       xh_0v=xh_0(5)
		       xh_infv=xh_inf(5)
		       xg_0v=xg_0(5)
		       xg_infv=xg_inf(5)
		    endif
		 endif
		 		 
		 xhtemp1=(xh_0v-xh_infv)/(xg_infv-xg_0v)*tg
		 xhtemp2=0.5d0*(dexp(xhtemp1)+dexp(-xhtemp1))
		 xhtemp3=1.d0/xhtemp2**2.d0		 
		 xhtemp=xh_infv+(xh_0v-xh_infv)*xhtemp3		 
		 xhard(isys_a,isys_b)=xhtemp*qab(isys_a,isys_b)
	      enddo
	   enddo	   	   
	endif ! (itag_el.eq.2)
	
	if(itag_el.eq.1)then
	   a_dot=a_dot_hcp
	   do isys=1,islip
	      gamma_dot(isys)=delta_gamma(isys)/dtime
	   enddo
       
	   do index=1,islip
	      xs_bar_used(index)=xs_bar(index)
	      xh0_used(index)=xh0(index)
	      xn_used(index)=xn(index)
	      xr_used(index)=xr(index)
	      
	      
c**********************TRANSFORMED BETA******************************
	      if(isign.eq.-1)then
		 if(ipoly_switch.eq.2)then
		    if(index.le.6)then
		       xs_bar_used(index)=xs_trb_comp(index)
		       xh0_used(index)=xh0_trb_comp(index)
		       xn_used(index)=xn_trb_comp(index)
		       xr_used(index)=xr_trb_comp(index)
		    endif

		    if(index.gt.6.and.index.le.12)then
		       xs_bar_used(index)=xs_trb_comp(7)
		       xh0_used(index)=xh0_trb_comp(7)
		       xn_used(index)=xn_trb_comp(7)
		       xr_used(index)=xr_trb_comp(7)   
		    endif
		    
		    if(index.gt.12.and.index.le.30)then
		       xs_bar_used(index)=xs_trb_comp(8)
		       xh0_used(index)=xh0_trb_comp(8)
		       xn_used(index)=xn_trb_comp(8)
		       xr_used(index)=xr_trb_comp(8)  
		    endif
		 endif ! (ipoly_switch.eq.2)
	      

c**********************PRIMARY ALPHA******************************
	      
		 if(ipoly_switch.eq.1)then
		    if(index.le.6)then
		       xs_bar_used(index)=xs_pra_comp(index)
		       xh0_used(index)=xh0_pra_comp(index)
		       xn_used(index)=xn_pra_comp(index)
		       xr_used(index)=xr_pra_comp(index)
		    endif
		    
		    if(index.gt.6.and.index.le.12)then
		       xs_bar_used(index)=xs_pra_comp(7)
		       xh0_used(index)=xh0_pra_comp(7)
		       xn_used(index)=xn_pra_comp(7)
		       xr_used(index)=xr_pra_comp(7)   
		    endif
		    
		    if(index.gt.12.and.index.le.30)then
		       xs_bar_used(index)=xs_pra_comp(8)
		       xh0_used(index)=xh0_pra_comp(8)
		       xn_used(index)=xn_pra_comp(8)
		       xr_used(index)=xr_pra_comp(8)  
		    endif
		 endif ! (ipoly_switch.eq.1)
	      endif ! (isign.eq.-1)
	   enddo ! index=1,islip
	   
	   xsbarj(1:islip)=xs_bar_used(1:islip)
	   xh0j(1:islip)=xh0_used(1:islip)
	   xnj(1:islip)=xn_used(1:islip)
	   xrj(1:islip)=xr_used(1:islip)
	   
	   do isys_b=1,islip
	      hbeta(isys_b)=0.d0
	      if(gamma_dot(isys_b).ne.0.d0)then
		   xs=xs_bar_used(isys_b)*(dabs(gamma_dot(isys_b)/a_dot))**
     &         xn_used(isys_b)
		   xtemp=1.d0-g_alpha_tau(isys_b)/xs
		   hbeta(isys_b)=xh0_used(isys_b)*((dabs(xtemp))**
     &                    xr_used(isys_b))*dsignf(xtemp)

	      endif ! (gamma_dot(isys_b).ne.0.d0)
	   enddo ! isys_b=1,islip


c**********************************************************************	   
	   do isys_a=1,islip
	      do isys_b=1,islip
		 if(dabs(gamma_dot(isys_b)).gt.0.d0)then
		 
	   
c********* kourosh start ****************		
		if (isys_b.ge.1.and.isys_b.le.3) then
			a_dot=gamma_dot_0_basal
c     & +*(dtanh(xk_star_basal*(epsilon_p_bar-xlp_basal))+
c     &  dtanh(xk_basal*xlp_basal))/
c     & (dtanh(xk_basal)+dtanh(xk_basal*xlp_basal))
c     &  
	 
		elseif (isys_b.ge.4.and.isys_b.le.6) then
			a_dot=gamma_dot_0_prism
c     & *gamma_dot_0_prism*(dtanh(xk_star_prism*(epsilon_p_bar-xlp_prism))+
c     &  dtanh(xk_prism*xlp_prism))/
c     & (dtanh(xk_prism)+dtanh(xk_prism*xlp_prism))
c     &  
	 
		elseif (isys_b.ge.7.and.isys_b.le.12) then
			a_dot=gamma_dot_0_pyram
c     & +gamma_dot_0_pyram*(dtanh(xk_star_pyram*(epsilon_p_bar-xlp_pyram))+
c     &  dtanh(xk_pyram*xlp_pyram))/
c     & (dtanh(xk_pyram)+dtanh(xk_pyram*xlp_pyram))
c     &  

		elseif (isys_b.ge.13.and.isys_b.le.30) then
			a_dot=gamma_dot_0_capyr
c     & +gamma_dot_0_capyr*(dtanh(xk_star_capyr*(epsilon_p_bar-xlp_capyr))+
c     &  dtanh(xk_capyr*xlp_capyr))/
c     & (dtanh(xk_capyr)+dtanh(xk_capyr*xlp_capyr))
c     &  
		
		endif	
c********* kourosh end ****************			 
		 
		 
		    xs=xs_bar_used(isys_b)*(dabs(gamma_dot(isys_b)/
     &               a_dot))**xn_used(isys_b)
		    xtemp=1.d0-g_alpha_tau(isys_b)/xs
		    xhard(isys_a,isys_b)=qab(isys_a,isys_b)*
     &              xh0_used(isys_b)*
     &	    ((dabs(xtemp))**xr_used(isys_b))*dsignf(xtemp)

		 endif ! (dabs(gamma_dot(isys_b)).gt.0.d0)
	      enddo
	   enddo
	endif ! (itag_el.eq.1)

	endif ! (dabs(dtime).gt.1d-10)
	return
	end



      subroutine rclear (a,max)                                       
c                                                                     
c  fills a real array a with zeros
c                                                                     
       implicit double precision (a-h,o-z)                             

      dimension a(max)
c
      do i=1,max                                                 
      a(i) = 0.d0                                                     
      enddo
      return
      end         
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine prod(a,b,c)                                          
c                                                                     
c  computes the matrix product c=a*b  all matricies are 3x3
c                                                                     
       implicit double precision (a-h,o-z)                             

      dimension a(3,3),b(3,3),c(3,3)                                  
c                                                                     
      do 200 j=1,3                                                    
      do 200 i=1,3                                                    
        s=0.0                                                           
      do 100 k=1,3                                                    
        s=s+a(i,k)*b(k,j)                                             
 100  continue
        c(i,j)=s                                                        
 200  continue
      return
      end   


C---------------------------------------------------------------------
C      THIS SUBROUTINE CALCULATES THE VALUE OF Fp,NORMALIZES IT AND
C      CALCULATES THE CAUCHY STRESS AND THE NEW TEXTURE
c-----------------------------------------------------------------
c      gamma_s_2d - delta_gamma*So (used in calculation of Fp_tau)
c      Fe_tau     - elastic part of the deformation gradient
c      dsrot      - new texture
c      dmrot      - new texture
c----------------------------------------------------------------

       subroutine dstress_fp_fe(dfgrd1,fp_t,s_tan,delta_gamma,tpk_1d,
     & gst,gmt,fp_tau,fe_tau,stress,dsrot,dmrot,ntens,e_plastic,
     & cst_1d,tr,cauchy_2d,fe_tau_inv_t,w_p,islip,strain_1d,
     & F_THERMAL_TAU,PLASTIC_WORK,DTIME) ! KOUROSH_TEMPERATURE      

      implicit real*8 (a-h,o-z)   

      parameter (nslip=100)
	

        dimension dfgrd1(3,3),fp_t(3,3),s_tan(3,3,nslip),
     &     fp_tau(3,3),gst(3,nslip),gmt(3,nslip),delta_gamma(nslip),
     &     tpk_1d(6),gamma_s_2d(3,3),gamma_s_fp(3,3),fe_tau_inv_t(3,3),
     &     fp_tau_inv(3,3),fe_tau(3,3),fe_tau_t(3,3),fe_tau_inv(3,3),
     &     fe_tpk_2d(3,3),fe_tpk_2d_fe_t(3,3),cauchy_2d(3,3),
     &     dmrot(3,nslip),dsrot(3,nslip),tpk_2d_g(3,3),stress(ntens),
     &     up_tau(3,3),rp_tau(3,3),cstrain(3,3),cst_1d(6),
     &     fp_tau_t(3,3),fp_t_fp(3,3),tr(3,3),fe_tr(3,3),vect(3),
     &     vect_0001(3),vect_0001n(3),strain_2d(3,3),strain_1d(6),
     &     f_t_f(3,3),dfgrd1_t(3,3),ckrone_2d(3,3),eigvalue(3),
     &     eigvector(3,3)	 
 
C************** KOUROSH_TEMPERATURE ********************
       DIMENSION::F_THERMAL_TAU(3,3),F_THERMAL_TAU_INV(3,3),FET(3,3),
     &            FET_INV(3,3),PLASTIC_VELOC_GRAD(3,3)
C************** KOUROSH_TEMPERATURE ********************
 
	do i=1,3
	   do j=1,3
	      up_tau(i,j)=0.0d0
	      rp_tau(i,j)=0.0d0
	   enddo
	enddo



      do i=1,3
        do j=1,3
          gamma_s_2d(i,j)=0.d0
          do isys=1,islip
            gamma_s_2d(i,j)=gamma_s_2d(i,j)
     &    +delta_gamma(isys)*s_tan(i,j,isys)
          enddo
        enddo 
      enddo

  
      call mat33(gamma_s_fp,gamma_s_2d,fp_t,3)


      
      do i=1,3
        do j=1,3
          fp_tau(i,j)=fp_t(i,j)+gamma_s_fp(i,j)
        enddo
      enddo

      call matinv3(fp_tau,fp_tau_inv,det_fp_tau)
      if(det_fp_tau.eq.0.d0) then
        write(*,*) '0 divide'        
        stop
      endif


      if(det_fp_tau.ne.1.d0) then
        do i=1,3
          do j=1,3
            a=-1.d0/3.d0
            fp_tau(i,j)=(det_fp_tau**a)*fp_tau(i,j)

          enddo
        enddo
         call matinv3(fp_tau,fp_tau_inv,det_fp_tau)
      endif
     
     
       call sigmat(tpk_1d,tpk_2d_g)
      
C************** KOUROSH_TEMPERATURE ********************      
       CALL MATINV3(F_THERMAL_TAU,F_THERMAL_TAU_INV,DET_F_THERMAL)

       IF(DET_F_THERMAL.EQ.0.D0) THEN
        WRITE(*,*) 'F_THERMAL HAS 0 DETERMINANT'
        STOP
       ENDIF 

C       call mat33(fe_tau,dfgrd1,fp_tau_inv,3)
       
       FE_TAU=MATMUL(MATMUL(DFGRD1,FP_TAU_INV),F_THERMAL_TAU_INV)       
C************** KOUROSH_TEMPERATURE ********************

      
      
      call trans(fe_tau,fe_tau_t)      
      call matinv3(fe_tau,fe_tau_inv,det_fe_tau)
      call trans(fe_tau_inv,fe_tau_inv_t)

      if(det_fe_tau.eq.0.d0) then
        write(*,*) '0 divide'
        stop
      endif

C************** KOUROSH_TEMPERATURE ********************
      FET=MATMUL(FE_TAU,F_THERMAL_TAU)
      CALL MATINV3(FET,FET_INV,DUMMY)
      CAUCHY_2D=MATMUL(MATMUL(FET,TPK_2D_G),TRANSPOSE(FET)) 
      CAUCHY_2D=CAUCHY_2D/(DET_FE_TAU*DET_F_THERMAL)
      
      PLASTIC_VELOC_GRAD=MATMUL(MATMUL(FET,GAMMA_S_2D),FET_INV)
      PLASTIC_WORK=0.D0
      
      DO I=1,3
       DO J=1,3
             PLASTIC_WORK=PLASTIC_WORK+CAUCHY_2D(I,J)*
     &                                 PLASTIC_VELOC_GRAD(I,J)/DTIME
       ENDDO
      ENDDO
      
C      call mat33(fe_tpk_2d,fe_tau,tpk_2d_g,3)
C      call mat33(fe_tpk_2d_fe_t,fe_tpk_2d,fe_tau_t,3)
C
C      do i=1,3
C        do j=1,3 
C          cauchy_2d(i,j)=(1/det_fe_tau)*fe_tpk_2d_fe_t(i,j)
C        enddo
C      enddo   
C************** KOUROSH_TEMPERATURE ********************
      
      stress(1)=cauchy_2d(1,1)
      stress(2)=cauchy_2d(2,2)
      stress(3)=cauchy_2d(3,3)
      stress(4)=0.5d0*(cauchy_2d(1,2)+cauchy_2d(2,1))
      stress(5)=0.5d0*(cauchy_2d(1,3)+cauchy_2d(3,1))
      stress(6)=0.5d0*(cauchy_2d(2,3)+cauchy_2d(3,2))


C************** KOUROSH_TEMPERATURE ********************      
      do i=1,3
        do isys=1,islip
          dmrot(i,isys)=0.d0
          dsrot(i,isys)=0.d0
          do j=1,3
C            dsrot(i,isys)=dsrot(i,isys)+fe_tau(i,j)*gst(j,isys)
            DSROT(I,ISYS)=DSROT(I,ISYS)+FET(I,J)*GST(J,ISYS)
C            dmrot(i,isys)=dmrot(i,isys)+fe_tau_inv_t(i,j)*gmt(j,isys)
            DMROT(I,ISYS)=DMROT(I,ISYS)+GMT(J,ISYS)*FET_INV(J,I)
          enddo
        enddo
      enddo
C************** KOUROSH_TEMPERATURE ********************    

c---------------creep strain-------------------------
	call trans(fp_tau,fp_tau_t)
	call mat33(fp_t_fp,fp_tau_t,fp_tau,3)
	call cal_ckrone_2d(ckrone_2d)
	do i=1,3
	   do j=1,3
	      cstrain(i,j)=0.5d0*(fp_t_fp(i,j)-ckrone_2d(i,j))
	   enddo
	enddo
	cst_1d(1)=cstrain(1,1)
	cst_1d(2)=cstrain(2,2)
	cst_1d(3)=cstrain(3,3)
	cst_1d(4)=cstrain(1,2)
	cst_1d(5)=cstrain(1,3)
	cst_1d(6)=cstrain(2,3)


c----- Kourosh ----------
c----- Strain  ----------
	call trans(dfgrd1,dfgrd1_t)
	call mat33(f_t_f,dfgrd1_t,dfgrd1,3)

	call jacobi(f_t_f,3,3,eigvalue,eigvector,nrot)
	
	do i=1,3
		do j=1,3
			strain_2d(i,j)=0.d0
			do k=1,3
				
				if (eigvalue(k).le.0.d0) then
					write(*,*) 'Negative Eigenvalue @ Strain Cal.'
					stop
				endif
			
				strain_2d(i,j)=strain_2d(i,j)+0.5d0*dlog(eigvalue(k))*
     &                         eigvector(i,k)*eigvector(j,k)
			enddo
		enddo
	enddo
	
	call tr2to1(strain_2d,strain_1d)	
	
c----------------------------------------------------
c--------Plastic work-------------------------------
        do i=1,3
           do j=1,3
              w_p=w_p+0.5d0*cauchy_2d(i,j)*
     $                (gamma_s_2d(i,j)+gamma_s_2d(j,i))
           enddo
        enddo    !Masoud_crack(End)
c---------------------------------------------------
C-------------Final Pole Figure---------------


	call mat33(fe_tr,fe_tau,tr,3)
	vect(1)=0.0d0
	vect(2)=0.0d0
	vect(3)=1.0d0
	do i=1,3
	   vect_0001(i)=0.0d0
	   do j=1,3
	      vect_0001(i)=vect_0001(i)+fe_tr(i,j)*vect(j)
	   enddo
	enddo
	
	xnormfac=sqrt((vect_0001(1))**2+(vect_0001(2))**
     &  2+(vect_0001(3))**2)
	do i=1,3
	   vect_0001n(i)=vect_0001(i)/xnormfac
	enddo
	
	call stereograph(vect_0001n(1),vect_0001n(2),
     &  vect_0001n(3),xpole1,ypole1)
	      

C----------------------------------------------
      
      return
      end  



c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C    MULTIPLICATION OF TWO 3*3 MATRICES

      subroutine mat33(x,y,z,l)
c       
       implicit double precision(a-h,o-z)
c       include 'aba_param.inc'
       dimension x(3,l),y(3,3),z(3,l)
c
        do i=1,3
          do j=1,l
             x(i,j)=0d0
             do  k=1,3
                x(i,j)=x(i,j)+y(i,k)*z(k,j)
             enddo
          enddo
        enddo
        return
        end
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--               
      subroutine rotate(tch,r,t)                                      
c                                                                     
c  computes the matrix t=r*tch*(rtrans)  (all matrices are 3x3)
c                                                                     
       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'
      dimension t(3,3),r(3,3),tch(3,3)                                
c                                                                     
      do 200 j=1,3                                                    
      do 200 i=1,3                                                    
      s=0.0                                                           
      do 100 l=1,3                                                    
        rjl=r(j,l)                                                    
        do 100 k=1,3                                                  
        s=s+r(i,k)*rjl*tch(k,l)                                       
 100  continue                                                      
      t(i,j)=s                                                        
 200  continue                                                        
      return
      end 


c------------------------------------------------------------------------

c    CALCULATES THE JACOBIAN FOR UMAT
C-------------------------------------------------------------------------
C      REFERING TO THESIS Pg 185
c      cl_4d - Lijkl
c      de_g  - Cijkl
c      d_tang_4d -Dijkl
c      g_tan_4d -Gmnkl
c      cj_tan_4d - Jijkl
c      b_tan_2d  - B
c      ck_tan_4d - Kijkl
c      q_tan     -Qijkl
c      r_tan     -Rij
c      Sijkl_tan - Sijkl
c      w_tan     - Wijkl 
C------------------------------------------------------------------------

      subroutine jacobian(dfgrd0,dfgrd1,fp_t,fp_tau,s_tan,c_alpha_1d,
     &tau,delta_gamma,dtime,rate_exp,a_dot,de_g,tpk_1d_tau,g_alpha_tau,
     &ddsdde,ntens,islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     &a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,epsilon_p_bar,
     &passing_gnd) ! kourosh
      
      implicit real*8(a-h,o-z)
c     include 'aba_param.inc'
      
      parameter (nslip=100)
c      common/slips/islip	
      dimension fe_t_t(3,3),fe_tau_inv(3,3),
     &     cl1(3,3),cl2(3,3),fe_gamma_s(3,3),de_g(3,3,3,3),
     &     cl_4d(3,3,3,3),d_tang1_4d(3,3,3,3),defe_1(nslip),
     &     d_tang_4d(3,3,3,3),g_tan_4d(3,3,3,3,nslip),tpk_2d_g(3,3),
     &     cj_tan_4d(3,3,3,3,nslip),cj1_tan_4d(3,3,3,3,nslip), 
     &     b_tan_2d(3,3,nslip),ck_tan_4d(3,3,3,3),ck_tan_2d(6,6),
     &     ckrone(3,3,3,3),cb_tan(3,3,3,3),c_alpha(3,3,nslip),
     &     gamma_j(3,3,3,3),q_tan(3,3,3,3),r_tan(3,3,nslip),
     &     ck_tan_2d_inv(6,6),ck_tan_4d_inv(3,3,3,3),q1_tan(3,3,3,3),
     &     sijkl_tan_1st(3,3,3,3),sijkl_tan_2nd(3,3,3,3),r_s(3,3,3,3),
     &   t_fe_t(3,3),w_tan_1st(3,3,3,3),fe_q(3,3,3,3),dfgrd0_inv(3,3),
     &     w_tan_2nd(3,3,3,3),w_tan(3,3,3,3),w_tan_3rd(3,3,3,3),
     &     w_tan_4th(3,3,3,3),fe_stress_2d(3,3),fe_stress_2d_fe_t(3,3),
     &   sijkl_tan_3rd(3,3,3,3),sijkl_tan(3,3,3,3),g_alpha_tau(nslip),
     &     sijkl_tan_fe_inv(3,3),rt_u_fe_t(3,3),dfgrd0(3,3),
     &     w_tan_2d(6,6),ddsdde(ntens,ntens),tau(nslip),dfgrd1(3,3)

      dimension q2_tan(3,3,3,3) 
      dimension c_alpha_1d(6,nslip),fe_t(3,3),fe_tau(3,3),
     &     fe_tau_t(3,3),
     &     s_tan(3,3,nslip),
     &     fp_t(3,3),fp_t_inv(3,3),
     &     delta_gamma(islip),fp_tau(3,3),fp_tau_inv(3,3),
     &     gamma_s_2d(3,3),ckrone_2d(3,3),
     &     tpk_1d_tau(6),
     &     dfgrdt(3,3),rt_tau(3,3),ut_tau(3,3)    

	dimension xh_0(5),xh_inf(5),xg_0(5),xg_inf(5),tot_gamma(nslip)

	dimension a_dot_bcc(5),rate_exp_bcc(5),passing_gnd(nslip)

!   kourosh		
	common/soften1/gamma_dot_0_basal,gamma_dot_0_prism,
     &gamma_dot_0_pyram,gamma_dot_0_capyr
	common/soften2/xk_star_basal,xk_star_prism,xk_star_pyram,
     &xk_star_capyr
	common/soften3/xlp_basal,xlp_prism,xlp_pyram,xlp_capyr
	common/soften4/xk_basal,xk_prism,xk_pyram,xk_capyr
	common/soften5/gamma_dot_ini_basal,gamma_dot_ini_prism,
     &gamma_dot_ini_pyram,gamma_dot_ini_capyr
	common/soften6/rate_basal,rate_prism,rate_pyram,rate_capyr
!   kourosh


	isign=1


      call cal_ckrone_2d(ckrone_2d)


      call matinv3(dfgrd0,dfgrd0_inv,det)      


      call mat33(dfgrdt,dfgrd1,dfgrd0_inv,3)

      call rudcmp(dfgrdt,rt_tau,ut_tau)


      call matinv3(fp_t,fp_t_inv,det)




      call mat33(fe_t,dfgrd0,fp_t_inv,3)
      call mat33(cl1,ut_tau,fe_t,3)
      call trans(fe_t,fe_t_t)
      call mat33(cl2,fe_t_t,ut_tau,3)
      call sigmat(tpk_1d_tau,tpk_2d_g)


      call matinv3(fp_tau,fp_tau_inv,det)



      call mat33(fe_tau,dfgrd1,fp_tau_inv,3)
      call trans(fe_tau,fe_tau_t)
      call matinv3(fe_tau,fe_tau_inv,det_fe_tau)





      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              cl_4d(i,j,k,l)=fe_t_t(i,k)*cl1(l,j)+cl2(i,k)*fe_t(l,j)
            enddo
          enddo
        enddo
      enddo

    

      call mat44(d_tang1_4d,de_g,cl_4d) 




      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              d_tang_4d(i,j,k,l)=0.5d0*d_tang1_4d(i,j,k,l)
            enddo
          enddo
        enddo
      enddo
      

      do isys=1,islip
        do m=1,3
          do n=1,3
            do k=1,3
              do l=1,3
                g_tan_4d(m,n,k,l,isys)=0d0
                do ip=1,3
                  g_tan_4d(m,n,k,l,isys)=g_tan_4d(m,n,k,l,isys)
     &    +cl_4d(m,ip,k,l)*s_tan(ip,n,isys)
     &    +s_tan(ip,m,isys)*cl_4d(ip,n,k,l)
                enddo
              enddo
            enddo
          enddo
        enddo 
      enddo



      call mat44crys(cj1_tan_4d,de_g,g_tan_4d,islip)
      do isys=1,islip
        do i=1,3
          do j=1,3
            do k=1,3
              do l=1,3
                cj_tan_4d(i,j,k,l,isys)=0.5d0
     & *cj1_tan_4d(i,j,k,l,isys)
              enddo
            enddo
          enddo
        enddo
      enddo
 

      do isys=1,islip



	if(itag_el.eq.1)then
	   a_dot=a_dot_hcp
	   rate_exp=rate_exp_hcp
	endif

	if(itag_el.eq.2)then
	   if (isys.le.12)then
	      a_dot=a_dot_bcc(1)
	      rate_exp=rate_exp_bcc(1)
	   endif
	   if(isys.ge.13.and.isys.le.48)then
	      if(isign.eq.1)then
		 if(isys.le.24)then
		    a_dot=a_dot_bcc(2)
		    rate_exp=rate_exp_bcc(2)
		 else
		    a_dot=a_dot_bcc(4)
		    rate_exp=rate_exp_bcc(4)
		 endif
	      else
		 if(isys.le.24)then
		    a_dot=a_dot_bcc(3)
		    rate_exp=rate_exp_bcc(3)
		 else
		    a_dot=a_dot_bcc(5)
		    rate_exp=rate_exp_bcc(5)
		 endif
	      endif
	   endif
	    endif

				
c********* kourosh start ****************		
		if (isys.ge.1.and.isys.le.3) then
			a_dot=gamma_dot_0_basal
c     & *(dtanh(xk_star_basal*(epsilon_p_bar-xlp_basal))+
c     &  dtanh(xk_basal*xlp_basal))/
c     & (dtanh(xk_basal)+dtanh(xk_basal*xlp_basal))+
c     &  gamma_dot_ini_basal
	 
			rate_exp=rate_basal
	 
		elseif (isys.ge.4.and.isys.le.6) then
			a_dot=gamma_dot_0_prism
c     & *(dtanh(xk_star_prism*(epsilon_p_bar-xlp_prism))+
c     &  dtanh(xk_prism*xlp_prism))/
c     & (dtanh(xk_prism)+dtanh(xk_prism*xlp_prism))+
c     &  gamma_dot_ini_prism
	 
			rate_exp=rate_prism
	 
		elseif (isys.ge.7.and.isys.le.12) then
			a_dot=gamma_dot_0_pyram
c     & *(dtanh(xk_star_pyram*(epsilon_p_bar-xlp_pyram))+
c     &  dtanh(xk_pyram*xlp_pyram))/
c     & (dtanh(xk_pyram)+dtanh(xk_pyram*xlp_pyram))+
c     &  gamma_dot_ini_pyram
	 
		rate_exp=rate_pyram

		elseif (isys.ge.13.and.isys.le.30) then
			a_dot=gamma_dot_0_capyr
c     & *(dtanh(xk_star_capyr*(epsilon_p_bar-xlp_capyr))+
c     &  dtanh(xk_capyr*xlp_capyr))/
c     & (dtanh(xk_capyr)+dtanh(xk_capyr*xlp_capyr))+
c     &  gamma_dot_ini_capyr
	 
		rate_exp=rate_capyr
		
		endif	
c********* kourosh end ****************	


        g_inv=1d0/dabs(g_alpha_tau(isys))
        rate_inv=1d0/rate_exp
        sc=(dabs(tau(isys))-passing_gnd(isys))/g_alpha_tau(isys)
        atg=dabs(sc)		
c********* kourosh end ****************		


        defe_1(isys)=a_dot*dtime*g_inv*atg**(rate_inv-1)
     &  *rate_inv
     
	  if (sc.lt.0.d0) 	defe_1(isys)=0.d0

        do i=1,3
          do j=1,3
            b_tan_2d(i,j,isys)=0.5d0*defe_1(isys)
     &        *(s_tan(i,j,isys)+s_tan(j,i,isys))
          enddo
        enddo
      enddo




      call sigmat_crys(c_alpha_1d,c_alpha,islip)  
      call mat33crys(cb_tan,c_alpha,b_tan_2d,islip)
      call cal_ckrone(ckrone)
c
      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              ck_tan_4d(i,j,k,l)=ckrone(i,j,k,l)+cb_tan(i,j,k,l)
            enddo
          enddo
        enddo
      enddo          

c--- (b) qijkl=
      call tr4to2(ck_tan_4d,ck_tan_2d)

      do i=1,6
        do j=1,6 
          ck_tan_2d_inv(i,j)=ck_tan_2d(i,j)
        enddo
      enddo

      call matinv(ck_tan_2d_inv,6,6)
      call tr2to4(ck_tan_2d_inv,ck_tan_4d_inv,islip)

      do m=1,3
        do n=1,3
          do k=1,3
            do l=1,3
              gamma_j(m,n,k,l)=0d0
              do isys=1,islip 
                gamma_j(m,n,k,l)=gamma_j(m,n,k,l)
     &      +delta_gamma(isys)*cj_tan_4d(m,n,k,l,isys)
              enddo
            enddo
          enddo
        enddo
      enddo

      call mat44(q1_tan,ck_tan_4d_inv,d_tang_4d)
      call mat44(q2_tan,ck_tan_4d_inv,gamma_j)
      
      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              q_tan(i,j,k,l)=q1_tan(i,j,k,l)-q2_tan(i,j,k,l)
            enddo
          enddo
        enddo
      enddo


      do isys=1,islip
        do i=1,3
          do j=1,3
            r_tan(i,j,isys)=0d0
            do k=1,3 
              do l=1,3
                r_tan(i,j,isys)=r_tan(i,j,isys)
     &    +b_tan_2d(k,l,isys)*q_tan(k,l,i,j)      
              enddo
            enddo
          enddo
        enddo
      enddo 


      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              sijkl_tan_1st(i,j,k,l)=rt_tau(i,k)*fe_t(l,j)
            enddo
          enddo
        enddo
      enddo
      
      
      do i=1,3
         do j=1,3
            gamma_s_2d(i,j)=0.d0
            do isys=1,islip
               
               gamma_s_2d(i,j)=gamma_s_2d(i,j)+delta_gamma(isys)
     &              *s_tan(i,j,isys)
            enddo
         enddo
      enddo

      call mat33(fe_gamma_s,fe_t,gamma_s_2d,3)


      do i=1,3 
        do j=1,3
          do k=1,3
            do l=1,3
              sijkl_tan_2nd(i,j,k,l)=rt_tau(i,k)*fe_gamma_s(l,j)
            enddo
          enddo
        enddo
      enddo

      call mat33crys(r_s,r_tan,s_tan,islip)
      call mat33(rt_u_fe_t,dfgrdt,fe_t,3)

      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              sijkl_tan_3rd(i,j,k,l)=0d0
              do ip=1,3
                sijkl_tan_3rd(i,j,k,l)=sijkl_tan_3rd(i,j,k,l)
     &    +rt_u_fe_t(i,ip)*r_s(k,l,ip,j) 
              enddo
            enddo
          enddo
        enddo
      enddo

      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              sijkl_tan(i,j,k,l)=sijkl_tan_1st(i,j,k,l)
     &  -sijkl_tan_2nd(i,j,k,l)-sijkl_tan_3rd(i,j,k,l)
            enddo
          enddo
        enddo
      enddo

      call mat33(t_fe_t,tpk_2d_g,fe_tau_t,3) 
      call mat42(w_tan_1st,sijkl_tan,t_fe_t)


      do i=1,3
        do n=1,3
          do k=1,3
            do l=1,3
              fe_q(i,n,k,l)=0d0
              do m=1,3
                fe_q(i,n,k,l)=fe_q(i,n,k,l)+fe_tau(i,m)
     &    *q_tan(m,n,k,l)
              enddo
            enddo
          enddo
        enddo
      enddo

      call mat42(w_tan_2nd,fe_q,fe_tau_t)


      call mat33(fe_stress_2d,fe_tau,tpk_2d_g,3)

      

      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              w_tan_3rd(i,j,k,l)=0d0
              do n=1,3
                w_tan_3rd(i,j,k,l)=w_tan_3rd(i,j,k,l)
     &  +fe_stress_2d(i,n)*sijkl_tan(j,n,k,l)
              enddo
            enddo
          enddo
        enddo
      enddo


      call mat33(fe_stress_2d_fe_t,fe_stress_2d,fe_tau_t,3)
      do k=1,3
        do l=1,3
          sijkl_tan_fe_inv(k,l)=0d0 
          do ip=1,3
            do iq=1,3
              sijkl_tan_fe_inv(k,l)=sijkl_tan_fe_inv(k,l)
     &  +sijkl_tan(ip,iq,k,l)*fe_tau_inv(iq,ip)   
            enddo
          enddo
        enddo
      enddo

      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              w_tan_4th(i,j,k,l)=fe_stress_2d_fe_t(i,j)
     &  *sijkl_tan_fe_inv(k,l)
            enddo
          enddo
        enddo
      enddo

      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              w_tan(i,j,k,l)=1/det_fe_tau*(w_tan_1st(i,j,k,l)
     &  +w_tan_2nd(i,j,k,l)
     &  +w_tan_3rd(i,j,k,l)-w_tan_4th(i,j,k,l))
            enddo
          enddo
        enddo
      enddo

      call tr4to2(w_tan,w_tan_2d)

       do i=1,6
	 do j=1,3
	    ddsdde(i,j)=w_tan_2d(i,j)
	 enddo
         do j=4,6
            ddsdde(i,j)=w_tan_2d(i,j)/2
         enddo
      enddo



    
      return
      end


c
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine mat44crys(x,y,z,islip)
c
       implicit double precision(a-h,o-z)
c      include 'aba_param.inc'
      parameter (nslip=100)
c      common/slips/islip	
      dimension x(3,3,3,3,nslip),y(3,3,3,3),z(3,3,3,3,nslip) 
c
      do isys=1,islip
        do i=1,3
          do j=1,3
            do k=1,3
              do l=1,3
                x(i,j,k,l,isys)=0d0
                do m=1,3
                  do n=1,3
                    x(i,j,k,l,isys)=x(i,j,k,l,isys)
     &                +y(i,j,m,n)*z(m,n,k,l,isys)
                  enddo
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
c
      return
      end
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C    CONVERTS FOURTH ORDER TENSOR TO EQUIVALENT SECOND ORDER TENSOR

      subroutine tr4to2(a_4d,a_2d)
      implicit real*8(a-h,o-z)
c     include 'aba_param.inc'
      dimension a_4d(3,3,3,3),a_2d(6,6)

      call rclear66(a_2d)

      do i=1,3
        a_2d(i,1)=a_4d(i,i,1,1)
        a_2d(i,2)=a_4d(i,i,2,2)
        a_2d(i,3)=a_4d(i,i,3,3)
        a_2d(i,4)=a_4d(i,i,1,2)+a_4d(i,i,2,1)
        a_2d(i,5)=a_4d(i,i,1,3)+a_4d(i,i,3,1)
        a_2d(i,6)=a_4d(i,i,2,3)+a_4d(i,i,3,2)
      enddo

      do i=1,3
        a_2d(4,i)=a_4d(1,2,i,i)
        a_2d(5,i)=a_4d(1,3,i,i)
        a_2d(6,i)=a_4d(2,3,i,i)
      enddo

      a_2d(4,4)=a_4d(1,2,1,2)+a_4d(1,2,2,1)
      a_2d(4,5)=a_4d(1,2,1,3)+a_4d(1,2,3,1)
      a_2d(4,6)=a_4d(1,2,2,3)+a_4d(1,2,3,2)

      a_2d(5,4)=a_4d(1,3,1,2)+a_4d(1,3,2,1)
      a_2d(5,5)=a_4d(1,3,1,3)+a_4d(1,3,3,1)
      a_2d(5,6)=a_4d(1,3,2,3)+a_4d(1,3,3,2)

      a_2d(6,4)=a_4d(2,3,1,2)+a_4d(2,3,2,1)
      a_2d(6,5)=a_4d(2,3,1,3)+a_4d(2,3,3,1)
      a_2d(6,6)=a_4d(2,3,2,3)+a_4d(2,3,3,2)

      return
      end


c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CONVERTS A SECOND ORDER TENSOR TO A EQUIVALENT FOURTH ORDER TENSOR

      subroutine tr2to4(a_2d,a_4d,islip)
      implicit real*8(a-h,o-z)
c     include 'aba_param.inc'
      parameter (nslip=100)
c      common/slips/islip	
      dimension a_4d(3,3,3,3),a_2d(6,6)

      do i=1,3
         do j=1,3
            do k=1,3
               do l=1,3
                  a_4d(i,j,k,l)=0d0
               enddo
            enddo
         enddo
      enddo
c
c---1-3
      do i=1,3
         a_4d(i,i,1,1)=a_2d(i,1)
         a_4d(i,i,2,2)=a_2d(i,2)
         a_4d(i,i,3,3)=a_2d(i,3)
         a_4d(i,i,1,2)=a_2d(i,4)
         a_4d(i,i,1,3)=a_2d(i,5)
         a_4d(i,i,2,3)=a_2d(i,6)
      enddo

      a_4d(1,2,1,1)=a_2d(4,1)
      a_4d(1,2,2,2)=a_2d(4,2)
      a_4d(1,2,3,3)=a_2d(4,3)
      a_4d(1,2,1,2)=a_2d(4,4)
      a_4d(1,2,1,3)=a_2d(4,5)
      a_4d(1,2,2,3)=a_2d(4,6)

      a_4d(1,3,1,1)=a_2d(5,1)
      a_4d(1,3,2,2)=a_2d(5,2)
      a_4d(1,3,3,3)=a_2d(5,3)
      a_4d(1,3,1,2)=a_2d(5,4)
      a_4d(1,3,1,3)=a_2d(5,5)
      a_4d(1,3,2,3)=a_2d(5,6)

      a_4d(2,3,1,1)=a_2d(6,1)
      a_4d(2,3,2,2)=a_2d(6,2)
      a_4d(2,3,3,3)=a_2d(6,3)
      a_4d(2,3,1,2)=a_2d(6,4)
      a_4d(2,3,1,3)=a_2d(6,5)
      a_4d(2,3,2,3)=a_2d(6,6)

      a_4d(2,1,1,1)=a_2d(4,1)
      a_4d(2,1,2,2)=a_2d(4,2)
      a_4d(2,1,3,3)=a_2d(4,3)
      a_4d(2,1,1,2)=a_2d(4,4)
      a_4d(2,1,1,3)=a_2d(4,5)
      a_4d(2,1,2,3)=a_2d(4,6)

      a_4d(3,1,1,1)=a_2d(5,1)
      a_4d(3,1,2,2)=a_2d(5,2)
      a_4d(3,1,3,3)=a_2d(5,3)
      a_4d(3,1,1,2)=a_2d(5,4)
      a_4d(3,1,1,3)=a_2d(5,5)
      a_4d(3,1,2,3)=a_2d(5,6)

      a_4d(3,2,1,1)=a_2d(6,1)
      a_4d(3,2,2,2)=a_2d(6,2)
      a_4d(3,2,3,3)=a_2d(6,3)
      a_4d(3,2,1,2)=a_2d(6,4)
      a_4d(3,2,1,3)=a_2d(6,5)
      a_4d(3,2,2,3)=a_2d(6,6)
      
      return
      end

c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--       
      subroutine matinv(a,n,id)                                         
c     ******************************************************************
c     this subroutine computes the inverse and determinant of matrix a *
c     of order n,by the gauss-jordan method, a-inverse replaces a ,and *
c     the determinant of a is placed in determ. if m=1 the vector b    *
c     contans the constant vector when matinv is called,and this is    *
c     replaced with the solution vector if m=0,no simultaneous         *
c     equation solution is called for, and b is not pertinent. n is not*
c     to exceed 100.                                                   *
c      a--is the matrix of coefficients or the matrix to be inverted.  *
c      a contans a-inverse after execution.                            *
c      n-- is the order of the square matrix, a.                       *
c      b--is the matrix contaning column vectors of constants (each    *
c         column vector is associated with a in the following          *
c         manner--ax=b.).                                              *
c      m--is the number of criterion vectors (column vectors of        *
c         simultaneous solutions) to be calculated and stored in b.    *
c      m=0 results in the computation of only the inverse and          *
c          determinant of a.                                           *
c      determ--contans the value of the determinant after execution.   *
c     ******************************************************************
c     
       implicit double precision(a-h,o-z)
c      include 'aba_param.inc'
c
      dimension ipivot(1000),index(1000,2),pivot(1000)                        
      dimension a(id,id),b(500,1)                                     

c     initialization                                                    
c      determ=1.0e0                                                      
c     search for pivot element                                          
      do 30 j=1,n                                                       
   30 ipivot (j)=0                                                      
      do 470 i=1,n                                                      
      amax=0.0e0                                                        
      do 150 j=1,n                                                      
      if(ipivot(j).eq.1) go to 150                                      
      do 140 k=1,n                                                      
      if(ipivot(k).gt.1) go to 590                                      
      if(ipivot(k).eq.1) go to 140                                      
      if(dabs(amax).ge.dabs(a(j,k))) go to 140                          
      irow=j                                                            
      icolum=k                                                          
      amax=a(j,k)                                                       
  140 continue                                                          
  150 continue                                                          
      ipivot(icolum)=ipivot(icolum)+1                                   
c     interchange rows to put pivot element on diagonal                 
      if(irow.eq.icolum) go to 280                                      
c      determ=-determ                                                    
      do 220 l=1,n                                                      
      swap=a(irow,l)                                                    
      a(irow,l)=a(icolum,l)                                             
  220 a(icolum,l)=swap                                                  
c      do 270 l=1,m                                                      
c      swap=b(irow,l)                                                    
c      b(irow,l)=b(icolum,l)                                             
c  270 b(icolum,l)=swap                                                  
  280 index(i,1)=irow                                                   
      index(i,2)=icolum                                                 
      pivot(i)=a(icolum,icolum)                                         
c      determ=determ*pivot(i)                                            
c     divide pivot row by pivot element                                 
      a(icolum,icolum)=1.0e0                                            
      do 340 l=1,n                                                      
  340 a(icolum,l)=a(icolum,l)/pivot(i)                                  
c      if(m.le.0) go to 380                                              
c      do 370 l=1,m                                                      
c  370 b(icolum,l)=b(icolum,l)/pivot(i)                                  
c     reduce non-pivot rows                                             
  380 do 470 l1=1,n                                                     
      if(l1.eq.icolum) go to 470                                        
      t=a(l1,icolum)                                                    
      a(l1,icolum)=0.0e0                                                
      do 430 l=1,n                                                      
  430 a(l1,l)=a(l1,l)-a(icolum,l)*t                                     
c      if(m.le.0) go to 470                                              
c      do 460 l=1,m                                                      
c  460 b(l1,l)=b(l1,l)-b(icolum,l)*t                                     
  470 continue                                                          
c     interchange columns                                               
      do 580 i=1,n                                                      
      l=n+1-i                                                           
      if(index(l,1).eq.index(l,2)) go to 580                            
      jrow=index(l,1)                                                   
      jcolum=index(l,2)                                                 
      do 570 k=1,n                                                      
      swap=a(k,jrow)                                                    
      a(k,jrow)=a(k,jcolum)                                             
      a(k,jcolum)=swap   
c       write(115,*) k,jcolum,a(k,jcolum)                                               
  570 continue                                                          
  580 continue                                                          
  590 return                                                            
      end 

c
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine mat44(x,y,z)
c
       implicit double precision(a-h,o-z)
c      include 'aba_param.inc'
      dimension x(3,3,3,3),y(3,3,3,3),z(3,3,3,3) 
c
      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              x(i,j,k,l)=0d0
              do m=1,3
                do n=1,3
                  x(i,j,k,l)=x(i,j,k,l)+y(i,j,m,n)*z(m,n,k,l)
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
c
      return
      end           
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C    CALCULATES THE TRANSPOSE OF A MATRIX

       subroutine trans(x,x_t)
        implicit double precision(a-h,o-z)
c       include 'aba_param.inc'
       dimension x(3,3),x_t(3,3) 
c
       do i=1,3
         do j=1,3
           x_t(j,i)=x(i,j)
         enddo
       enddo
       return
       end
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine mat33crys(x,y,z,islip)

       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'
      parameter (nslip=100)
c      common/slips/islip	
      dimension x(3,3,3,3),y(3,3,islip),z(3,3,nslip)
c
      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              x(i,j,k,l)=0d0
              do isys=1,islip
                x(i,j,k,l)=x(i,j,k,l)
     &    +y(i,j,isys)*z(k,l,isys)
              enddo
            enddo
          enddo
        enddo
      enddo
c
      return
      end





      subroutine mat42(x,y,z)
       implicit real*8 (a-h,o-z)
c      include 'aba_param.inc'
      dimension x(3,3,3,3),y(3,3,3,3),z(3,3)
c
      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              x(i,j,k,l)=0d0
                do n=1,3
                  x(i,j,k,l)=x(i,j,k,l)
     &          +y(i,n,k,l)*z(n,j)
                enddo
              enddo
            enddo
          enddo
        enddo
c
        return
        end


c---+----1----+----2----+----3----+----4----+----5----+----6----+----7-
C     FORMS A FOURTH ORDER IDENTITY TENSOR

      subroutine cal_ckrone(ckrone)

       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'
      dimension ckrone(3,3,3,3)

      do i=1,3
        do j=1,3
          do k=1,3
            do l=1,3
              ckrone(i,j,k,l)=0.d0
            enddo
           enddo
         enddo
      enddo
c  
      ckrone(1,1,1,1)=1.d0
      ckrone(2,2,2,2)=1.d0
      ckrone(3,3,3,3)=1.d0
      ckrone(1,2,1,2)=1.d0
      ckrone(1,3,1,3)=1.d0
      ckrone(2,3,2,3)=1.d0
      ckrone(2,1,2,1)=1.d0
      ckrone(3,1,3,1)=1.d0
      ckrone(3,2,3,2)=1.d0
c
c
      return
      end
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C      FORMS A 1-D IDENTITY VECTOR

      subroutine cal_ckrone_1d(ckrone_1d)
       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'    
      dimension ckrone_1d(6)
c
      do i=1,6
        if(i.eq.1.or.i.eq.2.or.i.eq.3) then
          ckrone_1d(i)=1.d0
        else
          ckrone_1d(i)=0.d0
        endif
      enddo

      return
      end
c
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C    FORMS A SECOND ORDER IDENTITY TENSOR

      subroutine cal_ckrone_2d(ckrone_2d)

       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'
       dimension ckrone_2d(3,3)
c
       do i=1,3
          do j=1,3
             if(i.eq.j) then
                ckrone_2d(i,j)=1.0d0
             else
                ckrone_2d(i,j)=0.0d0
             endif
          enddo
       enddo
c     
       return
       end
c---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     SETS ALL THE COMPONENTS OF A 6*6 MATRIX TO ZERO

      subroutine rclear66(x)

       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'
      dimension x(6,6)
c
      do i=1,6
        do j=1,6
          x(i,j)=0.d0
        enddo
      enddo
c
      return
      end
c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine sigmat_crys(x,xx,islip)    
c                                                                     
c  arranges into a symmetric array xx, the six commponents of a vector x
c                                                                     
       implicit double precision (a-h,o-z)
c      include 'aba_param.inc'
      dimension x(6,islip),xx(3,3,islip)
c
      do isys=1,islip
        xx(1,1,isys)=x(1,isys)
        xx(2,2,isys)=x(2,isys)
        xx(3,3,isys)=x(3,isys)
        xx(2,3,isys)=x(6,isys)
        xx(3,2,isys)=x(6,isys)
        xx(3,1,isys)=x(5,isys)
        xx(1,3,isys)=x(5,isys)
        xx(1,2,isys)=x(4,isys)
        xx(2,1,isys)=x(4,isys)
      enddo

      return
      end

c******************************************************

        
      subroutine rotate_crys(tch,r,t)                                      
c                                                                     
c  computes the matrix t=r*tch*(rtrans)  (all matrices are 3x3)
c                                                                     
      implicit double precision (a-h,o-z)
      parameter(nslip=100)

      dimension t(3,3,nslip),r(3,3),tch(3,3,nslip)                               
      
      do isys=1,islip
        do j=1,3                                                    
          do i=1,3                                                    
            s=0.0                                                           
            do l=1,3                                                    
              rjl=r(j,l)                                                    
              do k=1,3                                                  
                s=s+r(i,k)*rjl*tch(k,l,isys)                                              
              enddo
            enddo                                                      
            t(i,j,isys)=s
          enddo
        enddo
      enddo      
                                    
      return
      end 
	
c----------------------------------------------------
C     CALCULATES THE SIGN 
             
      double precision function dsignf(x)
      double precision x
      if(x.ge.0)then
         dsignf=+1.d0
      else
         dsignf=-1.d0
      endif
      return
      end
      
c-----------------------------------------------------
      subroutine rotate_crys_vector(a,q,a_prime,islip)
      implicit real*8(a-h,o-z)
      parameter(nslip=100)
c      common/slips/islip	
      dimension a(3,nslip),a_prime(3,nslip),q(3,3)
      do isys=1,islip
         do i=1,3
            a_prime(i,isys)=0.d0
            do j=1,3
               a_prime(i,isys)=a_prime(i,isys)+q(i,j)*a(j,isys)
            enddo
         enddo
      enddo
      return
      end


c------------------------------------------------------------
C   TRANSFORMS THE FOURTH ORDER ELASTICITY TENSOR FROM 
C   CRYSTAL SYSTEM TO GLOBAL 

      subroutine rotate4d(ddsdde_4d,tr,de_gg)
      implicit real*8(a-h,o-z)
C     transforms a fourth order tensor
      dimension ddsdde_4d(3,3,3,3),de_gg(3,3,3,3),tr(3,3)
      dimension de_g11(3,3,3,3),de_g22(3,3,3,3),de_g33(3,3,3,3)

      do i=1,3
         do n=1,3
            do io=1,3
               do ip=1,3
                  de_g11(i,n,io,ip)=0d0
                  do m=1,3
                     de_g11(i,n,io,ip)=de_g11(i,n,io,ip)
     &                    +tr(i,m)*ddsdde_4d(m,n,io,ip)
                  enddo
               enddo
            enddo
         enddo
      enddo
      
      do i=1,3
         do j=1,3
            do io=1,3
               do ip=1,3
              de_g22(i,j,io,ip)=0d0
              do n=1,3
                 de_g22(i,j,io,ip)=de_g22(i,j,io,ip)
     &                +tr(j,n)*de_g11(i,n,io,ip)
              enddo
           enddo
          enddo
       enddo
      enddo
      
      do i=1,3
         do j=1,3
            do k=1,3
               do ip=1,3
                  de_g33(i,j,k,ip)=0d0
                  do io=1,3
                     de_g33(i,j,k,ip)=de_g33(i,j,k,ip)
     &                    +tr(k,io)*de_g22(i,j,io,ip)
                  enddo
               enddo
            enddo
         enddo
      enddo
      
      do i=1,3
         do j=1,3
          do k=1,3
             do l=1,3
              de_gg(i,j,k,l)=0d0
              do ip=1,3
                 de_gg(i,j,k,l)=de_gg(i,j,k,l)
     &                +tr(l,ip)*de_g33(i,j,k,ip)
              enddo
           enddo
        enddo
      enddo
      enddo
      
      return
      end


      subroutine tr2to1(a_2d,a_1d)
      implicit real*8(a-h,o-z)
      dimension a_2d(3,3),a_1d(6)
      a_1d(1)=a_2d(1,1)
      a_1d(2)=a_2d(2,2)
      a_1d(3)=a_2d(3,3)
      a_1d(4)=0.5d0*(a_2d(1,2)+a_2d(2,1))
      a_1d(5)=0.5d0*(a_2d(1,3)+a_2d(3,1))
      a_1d(6)=0.5d0*(a_2d(2,3)+a_2d(3,2))
      return
      end

      subroutine tr2to1crys(a_2d,a_1d,islip)
      implicit real*8(a-h,o-z)
      parameter(nslip=100)
c      common/slips/islip	
      dimension a_2d(3,3,nslip),a_1d(6,nslip)
      do isys=1,islip
         a_1d(1,isys)=a_2d(1,1,isys)
         a_1d(2,isys)=a_2d(2,2,isys)
         a_1d(3,isys)=a_2d(3,3,isys)
         a_1d(4,isys)=0.5d0*(a_2d(1,2,isys)+a_2d(2,1,isys))
         a_1d(5,isys)=0.5d0*(a_2d(1,3,isys)+a_2d(3,1,isys))
         a_1d(6,isys)=0.5d0*(a_2d(2,3,isys)+a_2d(3,2,isys))
	enddo
	return
	end
	
c----------------------------------------------------------------
C       CALCULATES THE RESIDUE FOR NEWTON RAPHSON FOR T*
C------------------------------------------------------
C       g_n    - Residual
c       tau    - resolved shear stress
c       xjn_2d - Jacobain for Newton Raphson
c----------------------------------------------------------------
	
	subroutine stress_residue(tpk_1d_tau,s_tan,a_dot,dtime,
     & rate_exp,g_alpha_tau,
     & t_tr_1d,c_alpha_1d,tau,delta_gamma,g_n,xjn_2d,iflag,
     & s_gg_2d,islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     & a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,isign,xc,xd,
     & chi_t,chi_tau,iatg,epsilon_p_bar,passing_gnd,F_THERMAL_TAU,
     & SOFTENING_POWER,REF_TEMPERATURE,TEMPERATURE) !kourosh, KOUROSH_TEMPERATURE 

	implicit real*8(a-h,o-z)
		intrinsic:: dtanh   !kourosh
	parameter(nslip=100)
c	common/slips/islip	
	dimension tpk_1d_tau(6),s_tan(3,3,nslip),g_alpha_tau(nslip)
	dimension c_alpha_1d(6,nslip),tau(nslip),delta_gamma(nslip)
	dimension tpk_2d(3,3),gamma_c_alpha(6),t_tr_1d(6),g_n(6)
	dimension defe_11(nslip),b_alpha_der(3,3,nslip),xjn(3,3,3,3)
	dimension xjn_2d(6,6),c_alpha_2dd(3,3,nslip)
	dimension a_dot_bcc(5),rate_exp_bcc(5),chi_t(nslip),
     &  chi_tau(nslip),s_gg_2d(6,6),e_strain(6,1),e_strain_2d(3,3),
     &     fe_t_fe_tpk(3,3),fe_t_fe(3,3),g_alpha(nslip)
	dimension xh_0(5),xh_inf(5),xg_0(5),xg_inf(5),tot_gamma(nslip)
	dimension a_dotj(nslip),rate_expj(nslip),passing_gnd(nslip)

C************** KOUROSH_TEMPERATURE ********************
       DIMENSION::F_THERMAL_TAU(3,3),F_THERMAL_TAU_T(3,3),
     &            C_E_THERMAL(3,3)
C************** KOUROSH_TEMPERATURE ********************       
       
!	kourosh	
	common/soften1/gamma_dot_0_basal,gamma_dot_0_prism,
     &gamma_dot_0_pyram,gamma_dot_0_capyr
	common/soften2/xk_star_basal,xk_star_prism,xk_star_pyram,
     &xk_star_capyr
	common/soften3/xlp_basal,xlp_prism,xlp_pyram,xlp_capyr
	common/soften4/xk_basal,xk_prism,xk_pyram,xk_capyr
	common/soften5/gamma_dot_ini_basal,gamma_dot_ini_prism,
     &gamma_dot_ini_pyram,gamma_dot_ini_capyr
	common/soften6/rate_basal,rate_prism,rate_pyram,rate_capyr	 
	 
    
      real(8), parameter :: TAU_LIMIT = 1.5d0  ! deniz's    
    
    
    
    
      iatg = 0
    
	if(iflag.eq.0.or.iflag.eq.1)then

c----------------------------------------------------------
c
c       for feTfe
c
         call matmulx(s_gg_2d,tpk_1d_tau,e_strain,6,6,1)
         call sigmat(e_strain,e_strain_2d)
         do i=1,3
            do j=1,3
               if(i.eq.j)then
                  fe_t_fe(i,j)=1.d0+(2.d0*e_strain_2d(i,j))
               else
                  fe_t_fe(i,j)=2.d0*e_strain_2d(i,j)
               endif
            enddo
         enddo        
         call sigmat(tpk_1d_tau,tpk_2d)
         
C************** KOUROSH_TEMPERATURE ********************          
         CALL TRANS(F_THERMAL_TAU,F_THERMAL_TAU_T)
         C_E_THERMAL=MATMUL(MATMUL(F_THERMAL_TAU_T,FE_T_FE),
     &                      F_THERMAL_TAU)
C         call mat33(fe_t_fe_tpk,fe_t_fe,tpk_2d,3)
         CALL MAT33(FE_T_FE_TPK,C_E_THERMAL,TPK_2D,3)
C************** KOUROSH_TEMPERATURE ******************** 


c-------------------------------------------------------------

	   do isys=1,islip
	      tau(isys)=0.0d0
	      do i=1,3
		 do j=1,3

		    tau(isys)=tau(isys)+fe_t_fe_tpk(i,j)*s_tan(i,j,isys)
		 enddo
	      enddo
		  

	      tau(isys)=tau(isys)-chi_tau(isys)
		  
		  
	   enddo
 

   
C************** KOUROSH_TEMPERATURE ********************
       TEMP_SCALE=(TEMPERATURE/REF_TEMPERATURE)**SOFTENING_POWER
       g_alpha=g_alpha_tau
       g_alpha=g_alpha*TEMP_SCALE
C************** KOUROSH_TEMPERATURE ********************
 
         do isys=1,islip
	    
	    
	    sc=(dabs(tau(isys))-passing_gnd(isys))/(g_alpha(isys))
           atg=dabs(sc)

	    
	    
	    if(itag_el.eq.1)then
	       a_dot=a_dot_hcp
	       rate_exp=rate_exp_hcp
	    endif
	    
	    
	    if(itag_el.eq.2) then
	       if (isys.le.12)then
		  a_dot=a_dot_bcc(1)
		  rate_exp=rate_exp_bcc(1)
	       endif
	       if(isys.ge.13.and.isys.le.48) then
		  if(isign.eq.1) then
		     if(isys.le.24)then
			a_dot=a_dot_bcc(2)
			rate_exp=rate_exp_bcc(2)
		     else
			a_dot=a_dot_bcc(4)
			rate_exp=rate_exp_bcc(4)
		     endif
		  else
		     if(isys.le.24)then
			a_dot=a_dot_bcc(3)
			rate_exp=rate_exp_bcc(3)
		     else
			a_dot=a_dot_bcc(5)
			rate_exp=rate_exp_bcc(5)
		     endif
		  endif ! if(isign.eq.1)
	       endif ! if(isys.ge.13.and.isys.le.48)

	    endif ! if(itag_el.eq.2)
 	    
	    a_dotj(isys)=a_dot
	    rate_expj(isys)=rate_exp

       
       
!   deniz's       

!	    if(atg.gt.2.d0)then
!	       iatg=1
!		  write(*,*) 'isys=', isys
!		  write(*,*) 'tau=',tau(isys)
!		  write(*,*) 'g_alpha_tau=',g_alpha(isys)			
!	       return
!	    endif	

      if (atg.gt.TAU_LIMIT) then
         iatg = 2
      endif
!
		
c********* kourosh start ****************		
		if (isys.ge.1.and.isys.le.3) then
			a_dot=gamma_dot_0_basal
c     & *(dtanh(xk_star_basal*(epsilon_p_bar-xlp_basal))+
c     &  dtanh(xk_basal*xlp_basal))/
c     & (dtanh(xk_basal)+dtanh(xk_basal*xlp_basal))+
c     &  gamma_dot_ini_basal
	 
			rate_exp=rate_basal
	 
		elseif (isys.ge.4.and.isys.le.6) then
			a_dot=gamma_dot_0_prism
c     & *(dtanh(xk_star_prism*(epsilon_p_bar-xlp_prism))+
c     &  dtanh(xk_prism*xlp_prism))/
c     & (dtanh(xk_prism)+dtanh(xk_prism*xlp_prism))+
c     &  gamma_dot_ini_prism
	 
			rate_exp=rate_prism
	 
		elseif (isys.ge.7.and.isys.le.12) then
			a_dot=gamma_dot_0_pyram
c     & *(dtanh(xk_star_pyram*(epsilon_p_bar-xlp_pyram))+
c     &  dtanh(xk_pyram*xlp_pyram))/
c     & (dtanh(xk_pyram)+dtanh(xk_pyram*xlp_pyram))+
c     &  gamma_dot_ini_pyram
	 
		rate_exp=rate_pyram

		elseif (isys.ge.13.and.isys.le.30) then
			a_dot=gamma_dot_0_capyr
c     & *(dtanh(xk_star_capyr*(epsilon_p_bar-xlp_capyr))+
c     &  dtanh(xk_capyr*xlp_capyr))/
c     & (dtanh(xk_capyr)+dtanh(xk_capyr*xlp_capyr))+
c     &  gamma_dot_ini_capyr
	 
		rate_exp=rate_capyr
		
		endif	
c********* kourosh end ****************				



! deniz's		
!            delta_gamma(isys)=a_dot*dtime*atg**(1.d0/rate_exp)*
!     &      dsignf(tau(isys))

            if (atg < TAU_LIMIT) then
               delta_gamma(isys)=a_dot*dtime*  
     &                           atg**(1.d0/rate_exp)*
     &                           dsignf(tau(isys))
            else  ! stabilize power law (only during iterations)
               delta_gamma(isys)=a_dot*dtime*  
     &                  (TAU_LIMIT**(1.d0/rate_exp)+ 
     & (1.d0/rate_exp)*TAU_LIMIT**(1.d0/rate_exp-1.d0)* 
     &    (atg-TAU_LIMIT))*dsignf(tau(isys))            
            endif
! deniz's
     
	     if (sc.lt.0.d0) delta_gamma(isys)=0.d0   ! added by kourosh
	 
         enddo
         
         do it=1,6
            gamma_c_alpha(it)=0.0d0
            do isys=1,islip
               gamma_c_alpha(it)=gamma_c_alpha(it)
     &              +delta_gamma(isys)*c_alpha_1d(it,isys)
            enddo
         enddo
         
         do i2=1,6
            g_n(i2)=tpk_1d_tau(i2)-t_tr_1d(i2)+gamma_c_alpha(i2)
         enddo


      endif ! if(iflag.eq.0.or.iflag.eq.1)
         
C     Calculating the jacobian for nr iteration
	if(iflag.eq.1)then
	   do isys=1,islip
	      
	      if(itag_el.eq.1)then
		 a_dot=a_dot_hcp
		 rate_exp=rate_exp_hcp
	      endif
	      
	      
	      if(itag_el.eq.2)then
		 if (isys.le.12)then
		    a_dot=a_dot_bcc(1)
		    rate_exp=rate_exp_bcc(1)
		 endif
		 if(isys.ge.13.and.isys.le.48)then
		    if(isign.eq.1)then
		       if(isys.le.24)then
			  a_dot=a_dot_bcc(2)
			  rate_exp=rate_exp_bcc(2)
		       else
			  a_dot=a_dot_bcc(4)
			  rate_exp=rate_exp_bcc(4)
		       endif
		    else
		       if(isys.le.24)then
			  a_dot=a_dot_bcc(3)
			  rate_exp=rate_exp_bcc(3)
		       else
			  a_dot=a_dot_bcc(5)
			  rate_exp=rate_exp_bcc(5)
		       endif
		    endif
		 endif
	      endif
	      

c********* kourosh start ****************		
		if (isys.ge.1.and.isys.le.3) then
			a_dot=gamma_dot_0_basal
c     & *(dtanh(xk_star_basal*(epsilon_p_bar-xlp_basal))+
c     &  dtanh(xk_basal*xlp_basal))/
c     & (dtanh(xk_basal)+dtanh(xk_basal*xlp_basal))+
c     &  gamma_dot_ini_basal
	 
			rate_exp=rate_basal
	 
		elseif (isys.ge.4.and.isys.le.6) then
			a_dot=gamma_dot_0_prism
c     & *(dtanh(xk_star_prism*(epsilon_p_bar-xlp_prism))+
c     &  dtanh(xk_prism*xlp_prism))/
c     & (dtanh(xk_prism)+dtanh(xk_prism*xlp_prism))+
c     &  gamma_dot_ini_prism
	 
			rate_exp=rate_prism
	 
		elseif (isys.ge.7.and.isys.le.12) then
			a_dot=gamma_dot_0_pyram
c     & *(dtanh(xk_star_pyram*(epsilon_p_bar-xlp_pyram))+
c     &  dtanh(xk_pyram*xlp_pyram))/
c     & (dtanh(xk_pyram)+dtanh(xk_pyram*xlp_pyram))+
c     &  gamma_dot_ini_pyram
	 
		rate_exp=rate_pyram

		elseif (isys.ge.13.and.isys.le.30) then
			a_dot=gamma_dot_0_capyr
c     & *(dtanh(xk_star_capyr*(epsilon_p_bar-xlp_capyr))+
c     &  dtanh(xk_capyr*xlp_capyr))/
c     & (dtanh(xk_capyr)+dtanh(xk_capyr*xlp_capyr))+
c     &  gamma_dot_ini_capyr
	 
		rate_exp=rate_capyr
		
		endif	
c********* kourosh end ****************			      
	      
	      g_inv=1.d0/dabs(g_alpha(isys))
	      rate_inv=1.d0/rate_exp
	      sc=(dabs(tau(isys))-passing_gnd(isys))/(g_alpha(isys))
	      atg=dabs(sc)

         
! deniz's         
         if(atg.gt.TAU_LIMIT) then
            iatg= 2
         endif 
         
!	      defe_11(isys)=a_dot*dtime*g_inv*atg**(rate_inv-1)
!     &           *rate_inv

         if (atg < TAU_LIMIT) then
         defe_11(isys)=a_dot*dtime*rate_inv*atg**(rate_inv-1.d0)  
     &             *g_inv
         else ! stabilize during convergence
         defe_11(isys)=a_dot*dtime*rate_inv*
     &      TAU_LIMIT**(rate_inv-1.d0)*g_inv
         endif     
     
     
! deniz's
     
		if (sc.lt.0.d0)defe_11(isys)=0.d0   ! added by kourosh		
		
		
	      do i=1,3
		 do j=1,3
		    b_alpha_der(i,j,isys)=0.5d0*defe_11(isys)
     &                 *(s_tan(i,j,isys)+s_tan(j,i,isys))
		 enddo
	      enddo

C************** KOUROSH_TEMPERATURE ********************
             B_ALPHA_DER(:,:,ISYS)=MATMUL(C_E_THERMAL,
     &                                    B_ALPHA_DER(:,:,ISYS)) 
C************** KOUROSH_TEMPERATURE ********************             
	   enddo
	   
	   
	   call sigmat_crys(c_alpha_1d,c_alpha_2dd,islip)
	   
	   do i=1,3
	      do j=1,3
		 do k=1,3
		    do l=1,3
		       xjn(i,j,k,l)=0.0d0
		       do isys=1,islip
			  xjn(i,j,k,l)=xjn(i,j,k,l)
     &                    +c_alpha_2dd(i,j,isys)*b_alpha_der(k,l,isys)
		       enddo
		       
		    enddo
		 enddo
	      enddo
	   enddo
	   
	   
	   
	   call tr4to2(xjn,xjn_2d)      
	   
	   do i=1,6
	      do j=1,6
		 if(i.eq.j)xjn_2d(i,j)=xjn_2d(i,j)+1.d0
	      enddo
	   enddo
	endif
	

	return
	end


c------------------------------------------
C    CALCULATES THE NORM OF THE RESIDUAL
      subroutine magn_vect(a,r,n)
      implicit real*8(a-h,o-z)
      dimension a(n)
      r=0.d0
      do i=1,n
         r=r+a(i)**2
      enddo
      r=dsqrt(r)
      return
      end
c-------------------------------------------
C    RU DECOMPOSITON          

	subroutine rudcmp(f,rd,ud) 
      implicit real*8 (a-h,o-z)                                        
               
      dimension rd(3,3),f(3,3),ud(3,3) 

c    
C	write(6,*)'entering ru'
	o3=1.0d0/3.0
      root3=dsqrt(3.d0)
      c11=f(1,1)*f(1,1)+f(2,1)*f(2,1)+f(3,1)*f(3,1)
      c12=f(1,1)*f(1,2)+f(2,1)*f(2,2)+f(3,1)*f(3,2)
      c13=f(1,1)*f(1,3)+f(2,1)*f(2,3)+f(3,1)*f(3,3)
      c23=f(1,2)*f(1,3)+f(2,2)*f(2,3)+f(3,2)*f(3,3)
      c22=f(1,2)*f(1,2)+f(2,2)*f(2,2)+f(3,2)*f(3,2)
      c33=f(1,3)*f(1,3)+f(2,3)*f(2,3)+f(3,3)*f(3,3)
      c1212=c12*c12
      c1313=c13*c13
      c2323=c23*c23
      c2313=c23*c13
      c1223=c12*c23
      c1213=c12*c13
      s11=c22*c33-c2323
      ui1=o3*(c11+c22+c33)
      ui2=s11+c11*c22+c33*c11-c1212-c1313
      ui3=c11*s11+c12*(c2313-c12*c33)
     1      +c13*(c1223-c22*c13)
      ui1s=ui1*ui1
      q    =dsqrt(-dmin1(o3*ui2-ui1s,0.d0))                     
      r    =0.5*(ui3-ui1*ui2)+ui1*ui1s
      xmod =q*q*q
      scl1 =.5d0+dsign(.5d0,xmod-1.d-30)                          
      scl2 =.5d0+dsign(.5d0,xmod-dabs(r))                   
      scl0 =dmin1(scl1,scl2)                               
      scl1 =1.-scl0

      if(scl1.eq.0)then
        xmodscl1=xmod
      else
        xmodscl1=xmod+scl1
      endif

      sdetm=dacos(r/(xmodscl1))*o3

      q  =scl0*q
      ct3=q*dcos(sdetm)
      st3=q*root3*dsin(sdetm)
      sdetm=scl1*dsqrt(dmax1(0.0d0,r))
      aa=2.000*(ct3+sdetm)+ui1 
      bb=-ct3+st3-sdetm+ui1
      cc=-ct3-st3-sdetm+ui1                         
      xlamda1=dsqrt(dmax1(aa,0.d0))
      xlamda2=dsqrt(dmax1(bb,0.d0))
      xlamda3=dsqrt(dmax1(cc,0.d0))
      sdetm=xlamda1*xlamda2
      xli1=xlamda1+xlamda2+xlamda3
      xli2= sdetm+xlamda2*xlamda3+xlamda3*xlamda1
      xli3= sdetm*xlamda3/xli1
      s11= c11+xli3
      s22= c22+xli3
      s33= c33+xli3
      s12= c2313-c12*s33
      s13= c1223-s22*c13
      s23=-c2323+s22*s33
      sdetm=1./(xli1*(s11*s23+c12*s12+c13*s13))
      c11=c11+xli2
      c22=c22+xli2
      c33=c33+xli2
      si11=sdetm*s23
      si12=sdetm*s12
      si13=sdetm*s13
      si22=sdetm*( s11*s33-c1313)
      si23=sdetm*(-s11*c23+c1213)
      si33=sdetm*( s11*s22-c1212)
      s12=c12*si12
      s13=c13*si13
      s23=c23*si23
      ui11=c11*si11+s12+s13
      ui22=s12+c22*si22+s23
      ui33=s13+s23+c33*si33
      ui12=c11*si12+c12*si22+c13*si23
      ui13=c11*si13+c12*si23+c13*si33
      ui23=c12*si13+c22*si23+c23*si33
      rd(1,1)=f(1,1)*ui11+f(1,2)*ui12+f(1,3)*ui13
      rd(1,2)=f(1,1)*ui12+f(1,2)*ui22+f(1,3)*ui23
      rd(1,3)=f(1,1)*ui13+f(1,2)*ui23+f(1,3)*ui33
      rd(2,1)=f(2,1)*ui11+f(2,2)*ui12+f(2,3)*ui13
      rd(2,2)=f(2,1)*ui12+f(2,2)*ui22+f(2,3)*ui23
      rd(2,3)=f(2,1)*ui13+f(2,2)*ui23+f(2,3)*ui33
      rd(3,1)=f(3,1)*ui11+f(3,2)*ui12+f(3,3)*ui13
      rd(3,2)=f(3,1)*ui12+f(3,2)*ui22+f(3,3)*ui23
      rd(3,3)=f(3,1)*ui13+f(3,2)*ui23+f(3,3)*ui33 



      do i=1,3
         do j=1,3
            ud(i,j)=0.d0
            do k=1,3
               ud(i,j)=ud(i,j)+rd(k,i)*f(k,j)
            enddo
         enddo
      enddo
C	write(6,*)'exiting ru'
      return
      end


	subroutine stereograph(xl,xm,xn,x,y)
	implicit real*8 (a-h,o-z)
	x=dsignf(xn)*xl/(xn*dsignf(xn)+1.d0)
	y=dsignf(xn)*xm/(xn*dsignf(xn)+1.d0)
	return
	end

	integer function ngrain(nel,m,n)
	implicit real*8 (a-h,o-z)
	n3=(nel-1)/(m*n)**2+1
	nz=(n3-1)/m+1
	nel_2d=nel-(n3-1)*(m*n)**2
	nt1=nel_2d-1
	nt2=m*n
	n1=nt1-(nt1/nt2)*nt2+1
	nx=(n1-1)/m+1
	n2=(nel_2d-n1)/(m*n)+1
	ny=(n2-1)/m+1
	ngrain=n**2*(nz-1)+n*(ny-1)+nx
	return
	end

c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C    MATRIX MULTIPLICATION SUBROUTINE 

      subroutine matmulx(aa,bb,cc,n,m,l)
      
c      include 'aba_param.inc'
       implicit double precision(a-h,o-z)

c   this subroutine returns martix [cc] = matrix [aa] * matrix [bb]
c   n=no. of rows of [aa]      =no. of rows of [cc]
c   m=no. of columns of [aa]   =no. of rows of [bb]
c   l=no. of columns of [bb]   =no. of columns of [cc]

       dimension aa(n,m),bb(m,l),cc(n,l)

       do 10 i=1,n
       do 10 j=1,l
         cc(i,j)=0.0
       do 10 k=1,m
   10    cc(i,j)=cc(i,j)+aa(i,k)*bb(k,j)
       return 
       end  

c------------------------------------------------------------------------
C    CALCULATE GND HARDNESS

       subroutine hard_GND(forest,parallel,hgnd,Act_Energy,
     &                gnd_const1,gnd_const2,shear_modulus,bur)	    
      
       implicit double precision(a-h,o-z)
	   
	parameter(nslip=100)

       dimension forest(30),hgnd(nslip,2),parallel(30)
	
	hgnd=0.d0
	
	do i=1,30
		hgnd(i,1)=Act_Energy/gnd_const1/bur**2.0*dsqrt(forest(i)/bur)
		hgnd(i,2)=gnd_const2*shear_modulus*bur*dsqrt(parallel(i)/bur)
	enddo
            
       return 
       end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	subroutine check_det(fp_t,s_tan,delta_gamma,islip,iiatg)
	implicit double precision (a-h,o-z)
	parameter(nslip=100)
	dimension fp_t(3,3),fp_tau(3,3),fp_tau_inv(3,3),
     &           s_tan(3,3,nslip),delta_gamma(nslip)
       common/alaki/jelemm
     
	fp_tau=0.d0
	
	 do i=1,3
	    do j=1,3

	       tmp=0.d0

	       do isys=1,islip
		  do k=1,3

		     tmp=tmp+delta_gamma(isys)*s_tan(i,k,isys)*fp_t(k,j)

		  enddo
	       enddo

	      
	       fp_tau(i,j)=fp_t(i,j)+tmp

	    enddo
	 enddo

	 call matinv3(fp_tau,fp_tau_inv,det_fp_tau)

c	 if (det_fp_tau.lt.0.2.or.det_fp_tau.gt.2.d0.or.
c     &   det_fp_tau.ne.det_fp_tau) then
	 if (det_fp_tau.ne.det_fp_tau) then
		write(*,*) 'det_fp_tau',det_fp_tau,'element:',jelemm
		iiatg=1
		return
	 endif
	 
	 return
	 end
	  
	  
	
c-------------------------------------------------------------------
	subroutine disp(u,time)
	implicit real*8 (a-h,o-z)
	dimension u(3),time(2)
	common/load_dwell/tperiod,tramp,t_nodwell,t_dwell,samp,smin,
     *                uamp,umin
	common/load_mono/sload,st_rate
	common/icyc_flag/icyc

	u(3)=0.0
	u(2)=0.0


	if(icyc.eq.1)then

	   umax=uamp+umin
	   
	   if(time(1).le.tramp)then
	      u(1)=umin*time(1)/tramp
	   else
	      tcyc=time(1)-tramp
	      icyc1=tcyc/tperiod
	      t1=tcyc-icyc1*tperiod
	      
	      if(t1.ge.0.d0.and.t1.lt.t_nodwell)then
		 u(1)=umin+t1/t_nodwell*uamp
	      elseif(t1.ge.t_nodwell.and.t1.lt.(t_nodwell+t_dwell))then
		 u(1)=umax
	      else
		 t2=tperiod-t1
		 u1=t2/t_nodwell*uamp
		 u(1)=umin+u1
	      endif
	      
	   endif
	   
	else

c	   u(1)=800.d0*(dexp(st_rate*time(1))-1.d0)
c	   u(1)=350.d0*(st_rate*time(1))
c	   u(1)=200.d0*(st_rate*time(1))   ! 14 grain
  
c          u(1)=24.d0*((st_rate*time(1))) ! 1  grain
c          u(1)=36.d0*(st_rate*time(1))    ! 2  grains

          u(1)=10.d0*((st_rate*time(1))) ! 540 grain
	endif


	return
	end

!------------------------------------------------------------------
	subroutine dload(jelem,pr,time)
	implicit real*8 (a-h,o-z)
	dimension time(2)
	common/load_dwell/tperiod,tramp,t_nodwell,t_dwell,samp,smin,
     &                uamp,umin
	common/load_mono/sload,st_rate
	common/icyc_flag/icyc

	if(icyc.eq.1)then
	
	   smax=samp+smin
	   
	   if(time(1).le.tramp)then
	      pr=smin*time(1)/tramp
	   else
	      tcyc=time(1)-tramp
	      icyc1=tcyc/tperiod
	      t1=tcyc-icyc1*tperiod
	      
	      if(t1.ge.0.d0.and.t1.lt.t_nodwell)then
		 pr=smin+t1/t_nodwell*samp
	      elseif(t1.ge.t_nodwell.and.t1.lt.(t_nodwell+t_dwell))then
		 pr=smax
	      else
		 t2=tperiod-t1
		 s1=t2/t_nodwell*samp
		 pr=smin+s1
	      endif
	      
	   endif
	   
	else

	   if(time(1).le.tramp)then

	      pr=sload*time(1)/tramp

	   else

	      pr=sload

	   endif
	      
	endif

	return
	end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc	
      subroutine jacobian_new(dfgrd0,dfgrd1,fp_t,fp_tau,s_tan,
     & c_alpha_1d,tau,
     & delta_gamma,dtime,rate_exp,a_dot,de_g,tpk_1d_tau,g_alpha_tau,
     & ddsdde,ntens,islip,tot_gamma,xh_0,xh_inf,xg_0,xg_inf,itag_el,
     & a_dot_bcc,rate_exp_bcc,a_dot_hcp,rate_exp_hcp,epsilon_p_bar,
     & passing_gnd,TEMPERATURE,REF_TEMPERATURE,SOFTENING_POWER) 

      
      implicit real*8(a-h,o-z)
c     include 'aba_param.inc'
      
      parameter (nslip=100)
      dimension de_g(3,3,3,3),defe_1(nslip),tpk_2d_g(3,3),
     &     ckrone(3,3,3,3),c_alpha(3,3,nslip),g_alpha_tau(nslip),
     &     dfgrd0(3,3),ddsdde(6,6),tau(nslip),dfgrd1(3,3)


      dimension c_alpha_1d(6,nslip),fe_t(3,3),fe_tau(3,3),
     &     fe_tau_t(3,3),tot_gamma(nslip),
     &     s_tan(3,3,nslip),
     &     fp_t(3,3),fp_t_inv(3,3),
     &     delta_gamma(islip),fp_tau(3,3),fp_tau_inv(3,3),
     &     gamma_s_2d(3,3),ckrone_2d(3,3),
     &     tpk_1d_tau(6),
     &     dfgrdt(3,3),fp_tau_t(3,3)   


      dimension a_dot_m(30),a_dot_bcc(5),rate_exp_bcc(5)
      dimension  passing_gnd(nslip),g_alpha(nslip)
      dimension xh_0(5),xh_inf(5),xg_0(5),xg_inf(5)
      
      
      real(8):: sqr_1(3,3,islip)
      real(8)::sqr_2(3,3,3,3),sqr_tmp(3,3,3,3)
      real(8)::sqr_3(3,3,3,3,islip),sqr_tmp1(3,3,3,3)
      real(8)::sqr_tmp2(3,3,3,3)
      real(8)::sqr_3_tmp(3,3),sqr_3_tmp_t(3,3)
      real(8):: fp_tau_inv_t(3,3)
      real(8)::cir_tmp1(3,3),cir_tmp2(3,3)
      real(8):: cir_1(3,3)
        real(8):: cir_2_tmp0(3,3,3,3),cir_2_tmp2_2d_inv(9,9)
        real(8):: cir_2_tmp1(3,3,3,3),cir_2_tmp2(3,3,3,3)
        real(8):: cir_2_tmp2_inv(3,3,3,3),cir_2_tmp3(3,3,3,3)
        real(8):: cir_2_tmp4(3,3,3,3),cir_2_tmp4_2d(9,9)
        real(8):: cir_2(3,3,3,3), cir_2_2d(9,9)
      real(8):: cir_3_tmp0(3,3),cir_3_tmp1(3,3)
      real(8):: cir_3(3,3,3,3)
      
      real(8):: final_0(3,3,3,3), final_0_2d_inv(9,9)
      real(8):: final_1(3,3,3,3),final_2(3,3,3,3)
      real(8):: final_3(3,3,3,3),final_4(3,3,3,3)
      real(8):: final_3_tmp(3,3),final_2_tmp(3,3)
      real(8):: final_5(3,3,3,3),final_6(3,3,3,3)
      real(8):: final_7(3,3,3,3),final_7_2d(9,9)
      real(8):: final_4d(3,3,3,3),final_2d_tmp(6,6)
      real(8):: final_2d(9,9),ddsdde_SE(6,6)
      real(8):: TS_ddsdde(3,3,3,3),dfgrd1_inv(3,3)
      real(8):: ddsdde_TK(6,6)
      
      integer:: a,b,m,n,i,j,k,l
      



!   kourosh		
	common/soften1/gamma_dot_0_basal,gamma_dot_0_prism,
     &gamma_dot_0_pyram,gamma_dot_0_capyr
	common/soften2/xk_star_basal,xk_star_prism,xk_star_pyram,
     &xk_star_capyr
	common/soften3/xlp_basal,xlp_prism,xlp_pyram,xlp_capyr
	common/soften4/xk_basal,xk_prism,xk_pyram,xk_capyr
	common/soften5/gamma_dot_ini_basal,gamma_dot_ini_prism,
     &gamma_dot_ini_pyram,gamma_dot_ini_capyr
	common/soften6/rate_basal,rate_prism,rate_pyram,rate_capyr
!   kourosh



	isign=1

   
C************** KOUROSH_TEMPERATURE ********************
       TEMP_SCALE=(TEMPERATURE/REF_TEMPERATURE)**SOFTENING_POWER
       g_alpha=g_alpha_tau
       g_alpha=g_alpha*TEMP_SCALE
C************** KOUROSH_TEMPERATURE ********************
!__________________________________________________________
! initialization
!__________________________________________________________

      sqr_2=0.0
      sqr_3_tmp=0.0
      sqr_3=0.0
      cir_2_tmp1=0.0
      cir_2_tmp3=0.0
      cir_2_2d=0.0
      cir_3_tmp0=0.0
      cir_3_tmp1=0.0
      cir_3=0.0
      final_2_tmp=0.0
      final_3_tmp=0.0
      final_6=0.0
      final_2d=0.0

!      call DGETRI( N, A, LDA, IPIV, WORK, LWORK, INFO )


!square_1 
!___________________________________________________________  





      do isys=1,islip

      if(itag_el.eq.1)then
      a_dot=a_dot_hcp
      rate_exp=rate_exp_hcp
      endif

      if(itag_el.eq.2)then
      if (isys.le.12)then
      a_dot=a_dot_bcc(1)
      rate_exp=rate_exp_bcc(1)
      endif
      if(isys.ge.13.and.isys.le.48)then
      if(isign.eq.1)then
      if(isys.le.24)then
      a_dot=a_dot_bcc(2)
      rate_exp=rate_exp_bcc(2)
      else
      a_dot=a_dot_bcc(4)
      rate_exp=rate_exp_bcc(4)
      endif
      else
      if(isys.le.24)then
      a_dot=a_dot_bcc(3)
      rate_exp=rate_exp_bcc(3)
      else
      a_dot=a_dot_bcc(5)
      rate_exp=rate_exp_bcc(5)
      endif
      endif
      endif
      endif

				
c********* kourosh start ****************		
      if (isys.ge.1.and.isys.le.3) then
      a_dot=gamma_dot_0_basal
      rate_exp=rate_basal

      elseif (isys.ge.4.and.isys.le.6) then
      a_dot=gamma_dot_0_prism
      rate_exp=rate_prism

      elseif (isys.ge.7.and.isys.le.12) then
      a_dot=gamma_dot_0_pyram
      rate_exp=rate_pyram

      elseif (isys.ge.13.and.isys.le.30) then
      a_dot=gamma_dot_0_capyr
      rate_exp=rate_capyr

      endif	
c********* kourosh end ****************	
      g_inv=1d0/dabs(g_alpha(isys))
      rate_inv=1d0/rate_exp
      sc=(dabs(tau(isys))-passing_gnd(isys))/g_alpha(isys)
      atg=dabs(sc)		
c********* kourosh end ****************		


      defe_1(isys)=a_dot*dtime*g_inv*atg**(rate_inv-1)
     &  *rate_inv
     
         if (sc.lt.0.d0) 	defe_1(isys)=0.d0

        do i=1,3
          do j=1,3
            sqr_1(i,j,isys)=0.5d0*defe_1(isys)
     &        *(s_tan(i,j,isys)+s_tan(j,i,isys))
          enddo
        enddo
      enddo
      
      
      
      
 !___________________________________________________________  
 !square_2
 !___________________________________________________________  

      
      
      call matinv3(fp_tau,fp_tau_inv,det_fp)
      do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3
      sqr_tmp(i,j,k,l)=fp_tau_inv(i,k)*fp_tau_inv(j,l)
                enddo
            enddo
         enddo
      enddo
      
      
      do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3  
                    do m=1,3
                        do n=1,3  
      sqr_2(i,j,k,l)=sqr_2(i,j,k,l)+de_g(i,j,m,n)*sqr_tmp(m,n,k,l)   
                        enddo
                    enddo
                enddo
            enddo
         enddo
      enddo
      
      
      
      
!___________________________________________________________       
!sqr_3
!___________________________________________________________ 

    
      
      do isys=1,islip   

      sqr_3_tmp=0.0
       do i=1,3
        do j=1,3
            do k=1,3
      
        sqr_3_tmp(i,j)=sqr_3_tmp(i,j)+fp_tau_inv(i,k)*s_tan(k,j,isys)
      
            enddo
        enddo
      enddo
      
      call trans(sqr_3_tmp,sqr_3_tmp_t)
      call trans(fp_tau_inv,fp_tau_inv_t)
      
       do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3
      sqr_tmp1(i,j,k,l)=fp_tau_inv_t(i,k)*sqr_3_tmp_t(j,l)
      sqr_tmp2(i,j,k,l)=sqr_3_tmp_t(i,k)*fp_tau_inv_t(j,l)
      
                enddo
            enddo
         enddo
      enddo
      
      
      
       do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3  
                    do m=1,3
                        do n=1,3 
      sqr_3(i,j,k,l,isys)=sqr_3(i,j,k,l,isys)+
     & de_g(i,j,m,n)*(sqr_tmp1(m,n,k,l)+sqr_tmp2(m,n,k,l)) 
                        enddo
                    enddo
                enddo
            enddo
         enddo
        enddo
      
      
      
      
      enddo
      
      
      
!___________________________________________________________        
! cir_1      
!___________________________________________________________       

      
      call sigmat(tpk_1d_tau,tpk_2d_g)
      call trans(fp_tau,fp_tau_t)
      
        call mat33(cir_tmp1,tpk_2d_g,fp_tau_t,3)
        call mat33(cir_tmp2,fp_tau,cir_tmp1,3)
      
        do i=1,3
        do j=1,3
        cir_1(i,j)=1.0/det_fp*cir_tmp2(i,j)
        enddo
        enddo
        
        
        
      
!___________________________________________________________        
! cir_2
!___________________________________________________________  

      
      
      call cal_ckrone_2d(ckrone_2d)  
      
      do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3 
      cir_2_tmp0(i,j,k,l)=ckrone_2d(i,k)*ckrone_2d(j,l)
                enddo
            enddo
          enddo
        enddo

      call sigmat_crys(c_alpha_1d,c_alpha,islip)


      do isys=1,islip
        
       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3   
        cir_2_tmp1(i,j,k,l)=cir_2_tmp1(i,j,k,l)+
     & c_alpha(i,j,isys)*sqr_1(k,l,isys)
                enddo
             enddo
         enddo
       enddo
       
       enddo
       
       
     
       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3    
                
        cir_2_tmp2(i,j,k,l)= cir_2_tmp0(i,j,k,l)+cir_2_tmp1(i,j,k,l)   
        
                enddo
             enddo
         enddo
       enddo
      
       call tr4dto2d(cir_2_tmp2,cir_2_tmp2_2d_inv) 
     
       call matinv(cir_2_tmp2_2d_inv,9,9)
       
       
       
       
       
      do isys=1,islip

       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3  
           cir_2_tmp3(i,j,k,l)=cir_2_tmp3(i,j,k,l)+
     & delta_gamma(isys)*sqr_3(i,j,k,l,isys)    
                enddo
             enddo
         enddo
       enddo
       
      enddo 
       
       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3
       cir_2_tmp4(i,j,k,l)=sqr_2(i,j,k,l)-cir_2_tmp3(i,j,k,l)
                enddo
             enddo
         enddo
       enddo


      call tr4dto2d(cir_2_tmp4,cir_2_tmp4_2d)
      
      do i=1,9
      do j=1,9
      do k=1,9
      
      cir_2_2d(i,j)=cir_2_2d(i,j)+
     &  cir_2_tmp2_2d_inv(i,k)*cir_2_tmp4_2d(k,j)
     
      enddo
      enddo
      enddo

      call tr2dto4d(cir_2_2d, cir_2)
      
      
      

!___________________________________________________________       
!cir_3
!___________________________________________________________  

      
      
      do isys=1,islip

      cir_3_tmp0=0.0
      do i=1,3
      do j=1,3
      do k=1,3
      cir_3_tmp0(i,j)=cir_3_tmp0(i,j)+s_tan(i,k,isys)*fp_t(k,j)
      enddo
      enddo
      enddo
      
      cir_3_tmp1=0.0
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      cir_3_tmp1(i,j)=cir_3_tmp1(i,j)+
     &sqr_1(k,l,isys)*cir_2(i,j,k,l)
      enddo
      enddo
      enddo
      enddo
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      cir_3(i,j,k,l)=cir_3(i,j,k,l)+
     &cir_3_tmp0(i,j)*cir_3_tmp1(k,l)
      enddo  
      enddo
      enddo
      enddo
      
      enddo
      
      
!___________________________________________________________      
! final jacobian
!___________________________________________________________



      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      
      final_0(i,j,k,l)=fp_tau(i,k)*fp_tau(j,l)
      
      enddo  
      enddo
      enddo
      enddo   
      
      
      call tr4dto2d(final_0, final_0_2d_inv)
      call matinv(final_0_2d_inv,9,9)
      
      
      
      do i=1,9
      do j=1,9 
      
       final_0_2d_inv(i,j)=det_fp*final_0_2d_inv(i,j)     
      enddo  
      enddo
      
      
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
       final_1(i,j,k,l)=cir_1(i,j)*fp_tau_inv_t(k,l)       
      enddo  
      enddo
      enddo
      enddo 
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      final_2_tmp(j,i)=final_2_tmp(j,i)+
     &tpk_2d_g(i,k)*fp_tau_t(k,j) 
      enddo  
      enddo
      enddo
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_2(i,j,k,l)=ckrone_2d(i,k)*final_2_tmp(j,l)
      enddo  
      enddo
      enddo
      enddo
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      final_3_tmp(i,j)=final_3_tmp(i,j)+
     &fp_tau(i,k)*tpk_2d_g(k,j) 
      enddo  
      enddo
      enddo
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_3(i,j,k,l)=final_3_tmp(i,l)*ckrone_2d(j,k)
      enddo  
      enddo
      enddo
      enddo
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3

      final_4(i,j,k,l)=1.0/det_fp*(final_2(i,j,k,l)+final_3(i,j,k,l))
     
      enddo
      enddo
      enddo
      enddo
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_5(i,j,k,l)=final_1(i,j,k,l)-final_4(i,j,k,l)
      enddo  
      enddo
      enddo
      enddo
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      do m=1,3
      do n=1,3
      final_6(i,j,k,l)=final_6(i,j,k,l)+final_5(i,j,m,n)*cir_3(m,n,k,l)
      enddo
      enddo
      enddo  
      enddo
      enddo
      enddo      
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_7(i,j,k,l)=cir_2(i,j,k,l)+final_6(i,j,k,l)
      enddo  
      enddo
      enddo
      enddo
      
      
      call tr4dto2d(final_7,final_7_2d)
      
      do i=1,9
      do j=1,9
      do k=1,9
      final_2d(i,j)=final_2d(i,j)+final_0_2d_inv(i,k)*
     & final_7_2d(k,j) 
      enddo
      enddo
      enddo




      call tr2dto4d(final_2d,final_4d)
      ! final_4d is for:  T_ij=final_4d_ijkl*E_kl
      
      call tr4to2(final_4d,ddsdde)
       

    
      return
      end
      
      
      
      
      
      
      
      
      
      
      subroutine cvrt(a,b,c)
      
      implicit none
      integer:: a,b,c
      
      if (a==1) then
      b=1
      c=1
      elseif (a==2) then
      b=2
      c=2
      elseif (a==3) then
      b=3
      c=3
      elseif (a==4) then
      b=1
      c=2
      elseif (a==5) then
      b=1
      c=3
      elseif (a==6) then
      b=2
      c=3
      elseif (a==7) then
      b=2
      c=1
      elseif (a==8) then
      b=3
      c=1
      elseif (a==9) then
      b=3
      c=2
      endif
      
      return 
      end
      
      
      subroutine cvrt_inv(b,c,a)
      
      implicit none
      integer:: a,b,c
      
      if ((b==1).and.(c==1)) then
      a=1
      elseif ((b==2).and.(c==2)) then
      a=2
      elseif ((b==3).and.(c==3)) then
      a=3
      elseif ((b==1).and.(c==2)) then
      a=4
      elseif ((b==1).and.(c==3)) then
      a=5
      elseif ((b==2).and.(c==3))then
      a=6
      elseif ((b==2).and.(c==1)) then
      a=7
      elseif ((b==3).and.(c==1)) then
      a=8
      elseif ((b==3).and.(c==2)) then
      a=9
      endif
      
      return 
      end     
      
      
      
      
      subroutine tr4dto2d(A_4d, A_2d)
      
      implicit none
      real(8):: A_4d(3,3,3,3), A_2d(9,9)
      integer:: i,j,k,l,m,n
      
      
      do i=1,3
      do j=1,3
      call cvrt_inv(i,j,m)
      
      do k=1,3
      do l=1,3
      call cvrt_inv(k,l,n)
      
      A_2d(m,n)=A_4d(i,j,k,l)
      enddo
      enddo
      enddo
      enddo
      
      end
      
      
      
      
      
      
      subroutine tr2dto4d(A_2d, A_4d)
      
      implicit none
      real(8):: A_4d(3,3,3,3), A_2d(9,9)
      integer:: i,j,k,l,m,n
      
      
      do m=1,9
      call cvrt(m,i,j)
      do n=1,9
      call cvrt(n,k,l)
      
      A_4d(i,j,k,l)=A_2d(m,n)
      enddo
      enddo
      
      end 
      
      
      
      
      
      
      
      
      
      


