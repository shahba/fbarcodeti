
/*
 * -- SuperLU MT routine (version 1.0) --
 * Univ. of California Berkeley, Xerox Palo Alto Research Center,
 * and Lawrence Berkeley National Lab.
 * August 15, 1997
 *
 */
typedef long int fptr; 


#include <stdlib.h>
#include <stdio.h>
#include "pdsp_defs.h"
#include "slu_mt_util.h"
#include "slu_mt_Cnames.h"

typedef struct {
    SuperMatrix *L;
    SuperMatrix *U;
    int *perm_c;
    int *perm_r;
} factors_t;



void
c_bridge_pdgssv_(int *nprocs, int *n, int *nnz, int *nrhs, double *values,
		 int *rowind, int *colptr, double *b, int *ldb, int *info,int*inum,fptr*f_factors)
{
    SuperMatrix A, B, *L, *U;
    SCformat *Lstore;
    NCformat *Ustore;
    int      *perm_r; /* row permutations from partial pivoting */
    int      *perm_c; /* column permutation vector */
    int      permc_spec, i,xx;
    trans_t  trans;
    Gstat_t stat;
    factors_t *LUfactors;
    int panel_size,relax;
    trans = NOTRANS;

    /* Adjust to 0-based indexing */



    if(*inum==1){
      for (i = 0; i < *nnz; ++i) --rowind[i];
      for (i = 0; i <= *n; ++i) --colptr[i];

      dCreate_CompCol_Matrix(&A, *n, *n, *nnz, values, rowind, colptr, 
			   SLU_NC, SLU_D, SLU_GE);
      dCreate_Dense_Matrix(&B, *n, *nrhs, b, *ldb, SLU_DN, SLU_D, SLU_GE);

      if ( !(perm_r = intMalloc(*n)) ) SUPERLU_ABORT("Malloc fails for perm_r[].");
      if ( !(perm_c = intMalloc(*n)) ) SUPERLU_ABORT("Malloc fails for perm_c[].");
      L = (SuperMatrix *) SUPERLU_MALLOC( sizeof(SuperMatrix) );
      U = (SuperMatrix *) SUPERLU_MALLOC( sizeof(SuperMatrix) );

      permc_spec = 1;
      get_perm_c(permc_spec, &A, perm_c);

      panel_size = sp_ienv(1);
    
      pdgssv(*nprocs, &A, perm_c, perm_r, L, U, &B, info);
      Destroy_SuperMatrix_Store(&B);
      Destroy_SuperMatrix_Store(&A);


      if(*info!=0){
	printf("Inversion Failed");
	return;
      }
      LUfactors = (factors_t*) SUPERLU_MALLOC(sizeof(factors_t));
      LUfactors->L = L;
      LUfactors->U = U;
      LUfactors->perm_c = perm_c;
      LUfactors->perm_r = perm_r;
      *f_factors = (fptr) LUfactors;
      for (i = 0; i < *nnz; ++i) ++rowind[i];
      for (i = 0; i <= *n; ++i) ++colptr[i];

    }

    if(*inum>1){

      panel_size = sp_ienv(1);
      relax = sp_ienv(2);
      StatAlloc(*n, *nprocs, panel_size, relax, &stat);
      StatInit(*n,*nprocs,&stat);

      LUfactors = (factors_t*) *f_factors;
      L = LUfactors->L;
      U = LUfactors->U;
      perm_c = LUfactors->perm_c;
      perm_r = LUfactors->perm_r;

      dCreate_Dense_Matrix(&B, *n, *nrhs, b, *ldb, SLU_DN, SLU_D, SLU_GE);

      dgstrs (trans, L, U, perm_r, perm_c, &B, &stat, info);
      
      Destroy_SuperMatrix_Store(&B);
      StatFree(&stat);

    }

    if(*inum==*n){
      printf("Stuff Destroyed\n");
      LUfactors = (factors_t*) *f_factors;
      SUPERLU_FREE (LUfactors->perm_r);
      SUPERLU_FREE (LUfactors->perm_c);
      Destroy_SuperNode_Matrix(LUfactors->L);
      Destroy_CompCol_Matrix(LUfactors->U);
      SUPERLU_FREE (LUfactors->L);
      SUPERLU_FREE (LUfactors->U);
      SUPERLU_FREE (LUfactors);
    }

}

