
      module nodalRecovery
      
      private :: calcJacTET4, READSTR_l
      
      ! major mesh structures. 
      ! these are initialized with the nodalRecovery module at the start of the program.
      integer :: nPatches      
      integer, private :: NX_l,NELX_l,nGrains_l,NODE_l
      integer, private, allocatable :: IJK_l(:)
      integer, private, allocatable :: IJK_patchNodes(:,:)
      real(8), private, allocatable :: nodalPositions_l(:,:)
      real(8), private, allocatable :: xElemCenter(:,:)
      
      ! field/grain segmentation of elements
      integer, private, allocatable :: nGrainElements(:)
      integer, private, allocatable :: listGrainElements(:)
      integer, private, allocatable :: grainElementIndx(:)
      
      ! non-local patches
      integer, allocatable:: listPatchElementsNonlocal(:,:)
      integer, allocatable:: nPatchElementsNonLocal(:)
      integer, allocatable:: listPatchNodesNonLocal(:,:)
      integer, allocatable:: listPatchGlobalNodesNonLocal(:,:)
      integer, allocatable:: nPatchNodesNonLocal(:)
      real(8), allocatable:: distPatchElementsNonLocal(:,:)
      
      ! local patches
      integer, allocatable:: listPatchElementsLocal(:,:)
      integer, allocatable:: nPatchElementsLocal(:)
      integer, allocatable:: listPatchNodesLocal(:,:)
      integer, allocatable:: listPatchGlobalNodesLocal(:,:)
      integer, allocatable:: nPatchNodesLocal(:)
      real(8), allocatable:: distPatchElementsLocal(:,:)
      
      integer :: nMaxPatchElementsLocal, nMaxPatchElementsNonLocal
      integer :: nMaxPatchNodesLocal, nMaxPatchNodesNonLocal
      integer :: nMaxPatchNodes, nMaxPatchElements
      integer :: nMaxPatchesPerGrain

      ! patchID-nodeID mappings
      integer, allocatable :: listPatchesInGrain(:,:)
      integer, allocatable :: nPatchesInGrain(:)
      integer, allocatable :: patchGrainID(:)
      integer, allocatable :: nodeIDFromPatchID(:)
      integer, allocatable :: patchIDFromNodeID(:)
      
      ! boundary nodes
      logical, allocatable :: isBoundaryNode(:)
      
      ! settings
      logical, parameter :: normalize_SPR = .true.
      
      ! state
      logical :: patchesInitialized

      
      contains
      
      SUBROUTINE initialize_Patches(strPatchFile, &
                         NX_i,NELX_i,IJK_i,G0XYZ)
      
      implicit none
      
      character(len=*), intent(in) :: strPatchFile
      integer, intent(in) :: NX_i,NELX_i,IJK_i(:)
      real(8), intent(in) :: G0XYZ(:)

      !locals
      integer :: I,J,N,ISTART,IEND,INC,error,iLocNode
      integer :: lastIndex, iIndex, staIndex, endIndex, nodeIdx, elemIdx
      integer :: elemID, iGrain, patchID, nodeID
      real(8) :: dist(3)
      logical :: fileExists
      logical, allocatable :: elementAdded(:)
      character(len=150) :: lineStr
      
      patchesInitialized = .false.
      
      inquire(file=strPatchFile,exist=fileExists)
      if(.NOT.fileExists) RETURN

      open(1105,file=trim(strPatchFile))

      open(unit=1105,file='listPatches.dat')
      read(1105,*) nPatches
      call READSTR_l(1105,lineStr,error) ! 'patchIDs'
      read(1105,*) NX_l,NELX_l,nGrains_l,NODE_l
      if (NX_i /= NX_l .or. NELX_i /= NELX_l) then
         write(*,*) 'imported mesh and patch data file are inconsistent' 
         write(*,*) NX_i,NELX_i, NX_l,NELX_l
         return
      endif
      read(1105,*) nMaxPatchesPerGrain
      read(1105,*) nMaxPatchElementsLocal
      read(1105,*) nMaxPatchNodesLocal
      read(1105,*) nMaxPatchElementsNonLocal
      read(1105,*) nMaxPatchNodesNonLocal
      
      nMaxPatchElements = MAX(nMaxPatchElementsLocal,nMaxPatchElementsNonLocal)
      nMaxPatchNodes = MAX(nMaxPatchNodesLocal,nMaxPatchNodesNonLocal)
      
      allocate(listPatchesInGrain(nMaxPatchesPerGrain,nGrains_l))
      allocate(nodeIDFromPatchID(nPatches))
      allocate(nPatchesInGrain(nGrains_l))
      do iGrain=1,nGrains_l
         read(1105,*) nPatchesInGrain(iGrain), (listPatchesInGrain(nodeIdx,iGrain),nodeIdx=1,nPatchesInGrain(iGrain))
         read(1105,*) (nodeIDFromPatchID(listPatchesInGrain(nodeIdx,iGrain)),nodeIdx=1,nPatchesInGrain(iGrain))
      enddo
      
      call READSTR_l(1105,lineStr,error) ! 'listPatchElementsLocal'
      allocate(listPatchElementsLocal(nMaxPatchElementsLocal,nPatches))
      allocate(distPatchElementsLocal(nMaxPatchElementsLocal,nPatches))
      allocate(listPatchNodesLocal(nMaxPatchNodesLocal,nPatches))
      allocate(listPatchGlobalNodesLocal(nMaxPatchNodesLocal,nPatches))
      allocate(nPatchElementsLocal(nPatches))
      allocate(nPatchNodesLocal(nPatches))
      do patchID=1,nPatches
         read(1105,*) nPatchElementsLocal(patchID)
         read(1105,*) (listPatchElementsLocal(elemIdx,patchID),elemIdx=1,nPatchElementsLocal(patchID))
         read(1105,*) nPatchNodesLocal(patchID)
         read(1105,*) (listPatchNodesLocal(nodeIdx,patchID),nodeIdx=1,nPatchNodesLocal(patchID))
         read(1105,*) (listPatchGlobalNodesLocal(nodeIdx,patchID),nodeIdx=1,nPatchNodesLocal(patchID))
      enddo
      
      
      call READSTR_l(1105,lineStr,error) ! 'listPatchElementsNonLocal'
      allocate(listPatchElementsNonLocal(nMaxPatchElementsNonLocal,nPatches))
      allocate(distPatchElementsNonLocal(nMaxPatchElementsNonLocal,nPatches))
      allocate(listPatchNodesNonLocal(nMaxPatchNodesNonLocal,nPatches))
      allocate(listPatchGlobalNodesNonLocal(nMaxPatchNodesNonLocal,nPatches))
      allocate(nPatchElementsNonLocal(nPatches))
      allocate(nPatchNodesNonLocal(nPatches))
      do patchID=1,nPatches
         read(1105,*) nPatchElementsNonLocal(patchID)
         read(1105,*) (listPatchElementsNonLocal(elemIdx,patchID),elemIdx=1,nPatchElementsNonLocal(patchID))
         read(1105,*) nPatchNodesNonLocal(patchID)
         read(1105,*) (listPatchNodesNonLocal(nodeIdx,patchID),nodeIdx=1,nPatchNodesNonLocal(patchID))
         read(1105,*) (listPatchGlobalNodesNonLocal(nodeIdx,patchID),nodeIdx=1,nPatchNodesNonLocal(patchID))
      enddo
      
      call READSTR_l(1105,lineStr,error) ! 'isBoundaryNode'
      allocate(isBoundaryNode(nPatches))
      do patchID=1,nPatches
         read(1105,*) isBoundaryNode(patchID)
      enddo            
      
      close(1105)
      
      allocate(patchGrainID(nPatches))
      do iGrain=1,nGrains_l
         do nodeIdx = 1,nPatchesInGrain(iGrain)
            patchID = listPatchesInGrain(nodeIdx,iGrain)
            patchGrainID(patchID) = iGrain
         enddo
      enddo
      
      ! import nodal positions as a matrix
      allocate(nodalPositions_l(3,NX_l))
      nodalPositions_l = 0.d0
      do nodeID=1,NX_l
         nodalPositions_l(1:3,nodeID)=G0XYZ((nodeID-1)*3+1:nodeID*3)
      enddo
      
      ! import connectivity matrix.
      allocate(IJK_l(NODE_l*NELX_l))
      IJK_l(1:NODE_l*NELX_l) = IJK_i(1:NODE_l*NELX_l)
      
      ! construct connectivity matrix for patchNodes (element -> 4 patchNodes)
      allocate(IJK_patchNodes(NODE_l,NELX_l))
      IJK_patchNodes = 0
      allocate(patchIDFromNodeID(NX_l))
      do iGrain=1,nGrains_l
               
         ! initialize the temporary inverse mapping array
         patchIDFromNodeID = 0
         
         do nodeIdx = 1,nPatchesInGrain(iGrain)
         
            patchID = listPatchesInGrain(nodeIdx,iGrain)
            nodeID = listPatchGlobalNodesLocal(1,patchID)
                  
            ! add to the temporary array for inverse mapping: global Node ID --> patchID (only valid for this grain)
            patchIDFromNodeID(nodeID) = patchID
         enddo
      
         do nodeIdx = 1,nPatchesInGrain(iGrain)
            patchID = listPatchesInGrain(nodeIdx,iGrain)
            do elemIdx=1,nPatchElementsLocal(patchID)
               elemID = listPatchElementsLocal(elemIdx,patchID)
               if (IJK_patchNodes(1,elemID)==0) then
                  IJK_patchNodes(:,elemID) = patchIDFromNodeID(IJK_l((elemID-1)*4+1:elemID*4))
               endif
            enddo
         enddo
      enddo
      deallocate(patchIDFromNodeID)

      ! calculate and save gauss point positions (TET geometric center)
      allocate(xElemCenter(3,NELX_l))
      xElemCenter = 0.d0      
      do elemID=1,NELX_l
         do iLocNode=1,NODE_l
            nodeID=IJK_l((elemID-1)*NODE_l+iLocNode)
            xElemCenter(:,elemID) = xElemCenter(:,elemID) &
               + nodalPositions_l(:,nodeID)
         enddo
         xElemCenter(:,elemID)=xElemCenter(:,elemID)/NODE_l
      enddo
      
      ! construct grain element lists
      
      allocate(nGrainElements(nGrains_l))
      allocate(listGrainElements(NELX_l))
      allocate(grainElementIndx(nGrains_l+1))
      allocate(elementAdded(NELX_l))
      elementAdded = .false.
      nGrainElements = 0
      patchesInitialized = .true.
      grainElementIndx = 0
      grainElementIndx(1) = 1
      lastIndex = 0
      do iGrain=1,nGrains_l
         grainElementIndx(iGrain) = lastIndex + 1
         do nodeIdx = 1,nPatchesInGrain(iGrain)
            patchID = listPatchesInGrain(nodeIdx,iGrain)
            do elemIdx=1,nPatchElementsLocal(patchID)
               elemID = listPatchElementsLocal(elemIdx,patchID)
               if(.not.elementAdded(elemID)) then  ! add element to the list of elements in this grain/field
                  nGrainElements(iGrain) = nGrainElements(iGrain) + 1
                  lastIndex = lastIndex + 1
                  listGrainElements(lastIndex) = elemID
                  elementAdded(elemID) = .true.
               endif
            enddo
         enddo
      enddo
      grainElementIndx(iGrain) = lastIndex + 1
      deallocate(elementAdded)
      
      ! calculate gauss point distances to patch nodes
      do patchID=1,nPatches
         ! local patches
         do elemIdx=1,nPatchElementsLocal(patchID)
            elemID = listPatchElementsLocal(elemIdx,patchID)
            nodeID = listPatchGlobalNodesLocal(1,patchID)
            dist(:) = xElemCenter(:,elemID) - nodalPositions_l(:,nodeID)
            distPatchElementsLocal(elemIdx,patchID) = dSqrt(dot_product(dist,dist))
         enddo
         ! nonlocal patches
         do elemIdx=1,nPatchElementsNonLocal(patchID)
            elemID = listPatchElementsNonLocal(elemIdx,patchID)
            nodeID = listPatchGlobalNodesNonLocal(1,patchID)
            dist(:) = xElemCenter(:,elemID) - nodalPositions_l(:,nodeID)
            distPatchElementsNonLocal(elemIdx,patchID) = dSqrt(dot_product(dist,dist))
         enddo
      enddo
     
      
      END SUBROUTINE
      
      SUBROUTINE getElemNodalValues(elemID,elemNodalValues, &
                                               nodalValues, &
                                                 nQuantDOF)
      implicit none 
      integer, intent(in) :: elemID
      real(8), intent(out):: elemNodalValues(nQuantDOF,NODE_l)
      real(8), intent(in) :: nodalValues(nQuantDOF,nPatches)
      integer, intent(in) :: nQuantDOF
      
      ! locals
      integer :: iNode,iGloNode,patchID

      do iNode=1,NODE_l
         patchID = IJK_patchNodes(iNode,elemID)
         elemNodalValues(:,iNode)=nodalValues(:,patchID)
      enddo
      
      END SUBROUTINE

      
      ! nodalValues(:,:,:) : nodal stresses obtained by weighted averaging over surrounding elements
      ! WARNING! -- this does not initialize the whole array
      ! only the nodal values for the grain of interest are initialized
      SUBROUTINE nodalAveraging(nodalValues,gaussValues,nQuantDOF,debug)
      implicit none
      real(8), intent(out):: nodalValues(nQuantDOF,nPatches)
      real(8), intent(in) :: gaussValues(nQuantDOF,NELX_l)
      integer, intent(in) :: nQuantDOF
      logical, intent(in) :: debug
      
      ! local variables
      integer :: iElem,nElem,patchID,elemIdx

      do patchID = 1, nPatches
         
         ! initialize values for this patch-node
         nodalValues(:,patchID) = 0.D0
         nElem = 0
         
         do elemIdx = 1,nPatchElementsLocal(patchID)
            iElem = listPatchElementsLocal(elemIdx,patchID)

            nodalValues(:,patchID) =  &
               nodalValues(:,patchID) + gaussValues(:,iElem)
               nElem = nElem + 1
               
         enddo

         if (nElem.EQ.0) then 
            nodalValues(:,patchID)=0.D0
         else
            nodalValues(:,patchID)= &
               nodalValues(:,patchID) / nElem
         endif
         
      enddo
      END SUBROUTINE

      SUBROUTINE recoverySPR(nodalValues,gaussValues,nQuantDOF, &
                         p_deg,regularizeBoundaries,localNonlocal, &
                                      nFailedPatches,info,debug)
      implicit none
      !arguments
      integer, intent(in) :: nQuantDOF
      integer, intent(in) :: p_deg   ! requested degree of the patching polynomials
      logical, intent(in) :: regularizeBoundaries  ! always take patch averages for the boundary nodes
      integer, intent(in) :: localNonlocal   ! use local or non-local patches
      real(8), intent(out):: nodalValues(nQuantDOF,nPatches)
      real(8), intent(in) :: gaussValues(nQuantDOF,NELX_l)
      integer, intent(out) :: nFailedPatches
      integer, intent(out):: info
      logical, intent(in) :: debug
      ! local variables
      real(8) :: nodalValues_NonLocal(nQuantDOF,nPatches)
      real(8) :: nodalValuesPatch(nQuantDOF,nMaxPatchNodes)
      integer :: nContributionsOnNodalValues(nPatches)
      logical :: patchSuccessful(nPatches)
      integer :: iEl,iNode,jNode,jNodeIdx,iLocNode,nElem, &
                  elemIdx,staElemIdx,endElemIdx,nodeID, &
                  iQuantDOF,iGloNode,iDOF,i,j,nodeIdx,patchID,patchIDnode
      integer :: nPolyTerms
      real(8) :: condN

      nodalValues(:,:) = 0.D0          ! nodal values calculated using SPR. this is the main output
      nodalValues_NonLocal(:,:) = 0.d0 ! nodal values calculated using SPR with patches 
                                       !     contributing to the non-central nodes as well.
                                       !     we need this in case patching fails for a boundary node.
                                       !     in those cases we use contributions coming from surrounding patches
      nContributionsOnNodalValues(:) = 0 ! # of non-local contributions made on each node from patches. 
                                         !      this count is used for calculating the average after summing the contributions
      patchSuccessful(:) = .false.
      
      do patchID=1,nPatches
      
         nPolyTerms=(p_deg+3)*(p_deg+2)*(p_deg+1)/6

         CALL calculatePatchSPR(patchID,p_deg, &
                   nQuantDOF,gaussValues(:,:), &
                        nodalValuesPatch(:,:), &
               localNonlocal,condN,INFO,debug)

         if(info.NE.0) then
           write(131,*)  'ERROR in calculatePatchSPR'
           patchSuccessful(patchID) = .false.
           cycle
         endif
         
         patchSuccessful(patchID) = .true.
         
         ! add the contribution from the patch to its central node
         nodalValues(:,patchID) = nodalValuesPatch(:,1)
         
         ! add the contribution from the patch to all patch nodes (non-local contrib.)
         if (localNonlocal == 1) then
            do nodeIdx = 1,nPatchNodesLocal(patchID)
               patchIDnode = listPatchNodesLocal(nodeIdx,patchID)
               nodalValues_NonLocal(:,patchIDnode) = &
                  nodalValues_NonLocal(:,patchIDnode) + nodalValuesPatch(:,nodeIdx)
               nContributionsOnNodalValues(patchIDnode) = nContributionsOnNodalValues(patchIDnode) + 1
            enddo
         else
            do nodeIdx = 1,nPatchNodesNonLocal(patchID)
               patchIDnode = listPatchNodesNonLocal(nodeIdx,patchID)
               nodalValues_NonLocal(:,patchIDnode) = &
                  nodalValues_NonLocal(:,patchIDnode) + nodalValuesPatch(:,nodeIdx)
               nContributionsOnNodalValues(patchIDnode) = nContributionsOnNodalValues(patchIDnode) + 1
            enddo
         endif
         
      enddo
      
      ! take the average of accummulated nodal values
      do patchID=1,nPatches
         if (nContributionsOnNodalValues(patchID)==0) then
            write(*,*) 'no contributions done to patch node:', patchID
            info = -1
            !return
            !stop
         endif
         nodalValues_NonLocal(:,patchID) = nodalValues_NonLocal(:,patchID) / nContributionsOnNodalValues(patchID)       
      enddo

      ! for the patches that have failed, use contributions from neigboring patches
      nFailedPatches = 0
      do patchID=1,nPatches
         if (.not.patchSuccessful(patchID)) then
            nFailedPatches = nFailedPatches + 1
            nodalValues(:,patchID) = nodalValues_NonLocal(:,patchID)
         endif
         ! also, if required, for nodes on the grain/domain boundary do an averaging to alleviate overshooting
         if (regularizeBoundaries.and.isBoundaryNode(patchID)) then
            nodalValues(:,patchID) = nodalValues_NonLocal(:,patchID)         
         endif
      enddo

      END SUBROUTINE

      SUBROUTINE calculatePatchSPR(patchID,p_deg, &
                                       nQuantDOF, &
                                     gaussValues, &
                                nodalValuesPatch, &
                  localNonlocal,condN,INFO,debug)
                                  
      implicit none
      !arguments
      integer, intent(in) :: patchID
      integer, intent(in) :: p_deg       ! degree of the patching polynomial requested/applied
      integer, intent(in) :: localNonlocal   ! use local or non-local patches
      integer, intent(in) :: nQuantDOF
      real(8), intent(in) :: gaussValues(nQuantDOF,NELX_l)
      real(8), intent(out):: nodalValuesPatch(nQuantDOF,nMaxPatchNodes)
      real(8), intent(out):: condN
      integer, intent(out) :: info
      logical, intent(in) :: debug

      logical, parameter :: debug1 = .false.
      logical, parameter :: debug2 = .false.
      
      ! local variables
      real(8) :: quantThisNode(nQuantDOF)
      integer :: iElem,iNode,nodeID,iLocNode,elemIdx,staElemIdx,MDIM, &
                  endElemIdx,iNodeIdx,iQuantDOF,iGloNode,iDOF,i,j
      real(8) :: xNode(3),xPatchCenter(3),iNodeX(3)
      real(8) :: xPatchRange(3,2)
      real(8) :: xPatchGauss(3,nMaxPatchElements)
      integer :: nPatchElements, nPatchNodes
      integer :: listPatchElements(nMaxPatchElements)
      real(8) :: distPatchElements(nMaxPatchElements)
      integer :: listPatchGlobalNodes(nMaxPatchNodes)
      ! patch calculation
      real(8), allocatable :: P(:)
      real(8), allocatable :: A(:,:)
      real(8), allocatable :: b(:,:)
      integer :: nPolyTerms


      nPolyTerms=(p_deg+3)*(p_deg+2)*(p_deg+1)/6
      
      MDIM = 3
      
      condN = 1.d0
      
      ! now we have enough elements to calculate coefficients
!****** initialize coefficient arrays
      ! get the coordinates of the central node
      nodeID = nodeIDFromPatchID(patchID)
      xNode(:) = nodalPositions_l(:,nodeID)
      
      ! loop over the patch elements
      if (localNonlocal == 1) then
         nPatchElements = nPatchElementsLocal(patchID)
         nPatchNodes = nPatchNodesLocal(patchID)
         listPatchElements(1:nPatchElements) = listPatchElementsLocal(1:nPatchElements,patchID)
         listPatchGlobalNodes(1:nPatchNodes) = listPatchGlobalNodesLocal(1:nPatchNodes,patchID)
         distPatchElements(1:nPatchElements) = distPatchElementsLocal(1:nPatchElements,patchID)
      else
         nPatchElements = nPatchElementsNonLocal(patchID)
         nPatchNodes = nPatchNodesNonLocal(patchID)
         listPatchElements(1:nPatchElements) = listPatchElementsNonLocal(1:nPatchElements,patchID)
         listPatchGlobalNodes(1:nPatchNodes) = listPatchGlobalNodesNonLocal(1:nPatchNodes,patchID)
         distPatchElements(1:nPatchElements) = distPatchElementsNonLocal(1:nPatchElements,patchID)
      endif


      xPatchGauss(:,:) = 0.d0
      xPatchCenter(:) = 0.d0
      do elemIdx = 1,nPatchElements
         iElem = listPatchElements(elemIdx)
         xPatchGauss(:,elemIdx) = xElemCenter(:,iElem)
         xPatchCenter(:) = xPatchCenter(:) + xElemCenter(:,iElem)
      enddo
      xPatchCenter = xPatchCenter / nPatchElements
      
      ! translate to origin to improve the conditioning of the A-matrix
      if (normalize_SPR) &
         xPatchGauss(:,elemIdx)=xPatchGauss(:,elemIdx)-xPatchCenter(:)
      
      !calculate the spatial range of the patch
      !small range results in ill-conditioned matrix
      xPatchRange(:,1) = xPatchGauss(:,1)
      xPatchRange(:,2) = xPatchGauss(:,1)
      do elemIdx = 1,nPatchElements
         xPatchRange(:,1)= &
               MIN(xPatchGauss(:,elemIdx),xPatchRange(:,1))
         xPatchRange(:,2)= &
               MAX(xPatchGauss(:,elemIdx),xPatchRange(:,2))
      enddo
      
      ! NORMALIZE all gauss point positions
      if (normalize_SPR) then
         do elemIdx =1,nPatchElements
            xPatchGauss(:,elemIdx) = &
             -1 + 2*(xPatchGauss(:,elemIdx)-xPatchRange(:,1)) &
                     / (xPatchRange(:,2)-xPatchRange(:,1))
         enddo
         if (debug1) write(997,*) 'xPatchGauss normalized'
      endif
      
      
      !form the A and b matrices of the patch
      allocate(P(nPolyTerms))
      allocate(A(nPolyTerms,nPolyTerms))
      allocate(b(nPolyTerms,nQuantDOF))      
      CALL createPatchMatrices(A,b,P,nPolyTerms,nQuantDOF,p_deg, &
                            listPatchElements(:),nPatchElements, &
                                  xPatchGauss,distPatchElements, &
                                 gaussValues,NX_l,NELX_l,debug2)

!****** solve for the coeff.s of the patch polynomial *******
      
      ! LAPACK subroutines assume that matrices have
      ! FORTRAN-type (column major) memory layout.
      if (debug1) then
         write(997,*) 'Node: ',nodeID
         write(997,*) '# patch elements: ',nPatchElements
         write(997,*) '# patch nodes: ', nPatchNodes
         CALL printMatrix(A,nPolyTerms,nPolyTerms,'A matrix',997)
         CALL printMatrix(b,nPolyTerms,nQuantDOF,'b vector',997)
         CALL printMatrix(xPatchRange(:,2)-xPatchRange(:,1),1,3, &
                                          'Patch Range',997)
      end if
!     hypothesis: if the gauss pts are positioned in a cubic structure
!     you wont be able to fit a 3D quad polynomial even if N_points>10
!     we need a cental node to 'feel' the curves of the quad bases.
!     test below showed that:
!     inserting a node in the origin of the patch increased the reciprocal conditioning
!     from e-17 to e-5.
!      if(normalize_SPR.AND.p_deg.EQ.2.AND.nNeighDeg.EQ.1
!     &      .AND.nPatchElements.EQ.12) then
!         A(1,1)=A(1,1)+1
!         write(*,'(A,1I5)')'Recovered A singularity! at node',nodeID
!      endif
      CALL DPOSV('U',nPolyTerms,nQuantDOF,A,nPolyTerms,b,nPolyTerms,info)
      if (info.NE.0) then
         !write(*,*)'DPOSV() call at RecoverySPR() returned an error. &
         !                            Node: ', nodeID, &
         !                          'Patch: ', patchID, &
         !                          'nPatchElems:', nPatchElements
         !write(*,*) info
         write(131,*)'DPOSV() call at RecoverSPR() returned an error. &
                                     Node: ', nodeID, &
                                   'Patch: ', patchID, &
                                   'nPatchElems:', nPatchElements
         write(131,*) info
         return
      endif

      ! EVALUATE THE PATCH POLYNOMIAL
      ! ON THE PATCH NODES
      do iNodeIdx=1,nPatchNodes
         ! calculate node position
         iNode = listPatchGlobalNodes(iNodeIdx)
         iNodeX(:) = nodalPositions_l(:,iNode)
         ! transform to patch coordinate system
         if (normalize_SPR) &
            iNodeX(:)=iNodeX(:)-xPatchCenter(:)
         
         ! NORMALIZE according to patch range
         if (normalize_SPR) then
            iNodeX(:) = &
               -1 + 2*(iNodeX(:)-xPatchRange(:,1)) &
                     / (xPatchRange(:,2)-xPatchRange(:,1))
         endif

         CALL calcPolynomialTET(iNodeX(:),p_deg,P(:),nPolyTerms)
         ! evaluate polynomial at node for each DOF of the quantity
         do iQuantDOF=1,nQuantDOF
            quantThisNode(iQuantDOF)= &
                  DOT_PRODUCT(P(:),b(:,iQuantDOF))
         enddo
         nodalValuesPatch(:,iNodeIdx) = quantThisNode(:)
      enddo
   
      if (debug.and..false.) then
      if (nodalValuesPatch(3,1) > 6000.d0) then
         write(997,*) 'overshoot:', nodalValuesPatch(:,1)
         write(997,*) 'central nodeID',nodeIDFromPatchID(patchID)
         write(997,*) 'at:',nodalPositions_l(:,nodeIDFromPatchID(patchID))
         write(997,*) 'patchID',patchID
         write(997,*) 'localnonlocal',localNonlocal
         write(997,*) 'nPatchElements',nPatchElements
         write(997,*) 'listPatchElements',listPatchElements(1:nPatchElements)
         do elemIdx = 1,nPatchElements
            iElem = listPatchElements(elemIdx)
            write(997,*) xElemCenter(:,iElem), gaussValues(:,iElem)
         enddo
         
         CALL printMatrix(A,nPolyTerms,nPolyTerms,'A matrix',997)
         CALL printMatrix(b,nPolyTerms,nQuantDOF,'b vector',997)
         CALL printMatrix(xPatchRange(:,2)-xPatchRange(:,1),1,3, &
                                          'Patch Range',997)
      endif
      endif
  
      ! deallocate
      deallocate(P)
      deallocate(A)
      deallocate(b)
     
      END SUBROUTINE

      SUBROUTINE getTET4UnitMatrix(mat)
         ! returns the matrix [I]_ab=integral[N_a(x)*N_b(x)*(dVOL/VOL_e)]
         ! where N_a(x) is the shape function of node a
         ! note: integral is over the 'master' element
         real(8), intent(out):: mat(4,4)
         mat(:,:)= 0.05D0 / 6
         mat(1,1)= 0.1D0 / 6
         mat(2,2)= 0.1D0 / 6
         mat(3,3)= 0.1D0 / 6
         mat(4,4)= 0.1D0 / 6
      END SUBROUTINE
      
      
      ! Used in forming a polynomial of degree p:
      ! a + b*x + c*y + d*z + e*xy + f*yz + g*xz + h*xx + i*yy + ...
      ! k*xxy + l*xyy + m*yyy + .. etc 
      ! given the argument x,y,z, this subroutine returns the terms;
      ! 1, x, y, z, xx, xy, xz, yx ... etc.
      ! dot-product terms(:) with an array of coefficients 
      ! to calculate the value of the polynomial
      SUBROUTINE calcPolynomialTET(x, p_deg, terms, nterms)
      implicit none
      ! the number of terms must be: (p+3)!/(p!*3!)
      ! nterms = (p+1)*(p+2)*(p+3)/6
         real(8), intent(in) :: x(3)
         integer, intent(in) :: p_deg
         real(8), intent(out):: terms(nterms)
         integer, intent(in):: nterms
         !local vars
         integer :: i,j,k,n
         ! fill in the terms
         n=0
         do k=0,p_deg
            do j=0,p_deg-k
               do i=0,p_deg-k-j
                  n=n+1
                  terms(n)=(x(1)**i)*(x(2)**j)*(x(3)**k)
               enddo
            enddo
         enddo

      END SUBROUTINE

      SUBROUTINE getGrainIdxForNode(iGloNode,iGrain,grainIdx, &
                       grainListsForNodes,NX_l,MAX_GR_PER_NODE)
      implicit none
      integer, intent(in) :: iGloNode, iGrain, MAX_GR_PER_NODE,NX_l
      integer, intent(out):: grainIdx
      integer, intent(in) :: grainListsForNodes(NX_l,MAX_GR_PER_NODE)
      
      logical :: found
      found = .FALSE.
      do grainIdx=1,MAX_GR_PER_NODE
         if(grainListsForNodes(iGloNode,grainIdx)==iGrain) then
            found = .TRUE.
            exit
         endif
      enddo
      
      if(.NOT.found) then
         write(*,*) 'grain not found in grainListsForNodes. &
       nodeID:',iGloNode,'grainID:',iGrain
         stop
      endif


      END SUBROUTINE

      SUBROUTINE createPatchMatrices(A,b,P,nPolyTerms,nQuantDOF,p_deg, &
                                    patchElements,nPatchElements,xPatchGauss, &
                                    distPatchElements, &
                                    gaussValues,NX_i,NELX_i,debug2)
      implicit none
      real(8), intent(inout) :: P(nPolyTerms)
      real(8), intent(out) :: A(nPolyTerms,nPolyTerms)
      real(8), intent(out) :: b(nPolyTerms,nQuantDOF)
      integer, intent(in) :: p_deg,nPolyTerms,nQuantDOF,nPatchElements
      integer, intent(in) :: NELX_i,NX_i
      integer, intent(in) :: patchElements(nPatchElements)
      real(8), intent(in) :: xPatchGauss(3,nPatchElements)
      real(8), intent(in) :: distPatchElements(nPatchElements)
      real(8), intent(in) :: gaussValues(nQuantDOF,NELX_i)
      logical, intent(in) :: debug2
      
      ! local
      integer :: iPatchGauss, elemIdx, iEl, i, j, iQuantDOF
      real(8) :: weightGauss
      
      if (nPolyTerms /= (p_deg+3)*(p_deg+2)*(p_deg+1)/6) then
         write(*,*) 'requested degree of patching polynomical:',p_deg
         write(*,*) 'required number of polynomial coefficients: ', &
                     (p_deg+3)*(p_deg+2)*(p_deg+1)/6
         write(*,*) 'nPolyTerms argument passed:',nPolyTerms
         stop
      endif
      P(:) = 0.D0
      A(:,:) = 0.D0
      b(:,:) = 0.D0
      iPatchGauss=0
      do elemIdx = 1,nPatchElements
         iEl = patchElements(elemIdx)
         weightGauss = 1.d0/distPatchElements(elemIdx)
         iPatchGauss=iPatchGauss+1
         CALL calcPolynomialTET(xPatchGauss(:,iPatchGauss), &
                                    p_deg,P(:),nPolyTerms)
         if (debug2) then
            write(997,*) 'Element ', iEl
            CALL printMatrix(xPatchGauss(:,iPatchGauss),1,3, &
                                          'gauss point',997)
         endif
         if (debug2) then
            CALL printMatrix(P(:),1,nPolyTerms,'P vector',997)
         endif
         
         do i=1,nPolyTerms
            do j=1,nPolyTerms
               A(i,j)=A(i,j)+P(i)*P(j)*weightGauss*weightGauss
            enddo
         enddo
         ! set the RHS vector
         do iQuantDOF=1,nQuantDOF
            b(:,iQuantDOF)=b(:,iQuantDOF)+ &
                        P(:)*gaussValues(iQuantDOF,iEl)*weightGauss*weightGauss
         enddo
      enddo
      
      END SUBROUTINE
      
      SUBROUTINE recoveryLSQ(nodalValues,gaussValues,nQuantDOF, &
                             info,debug,staGrainIdx,endGrainIdx)

      ! WARNING: ASSUMES nQuantDOF=6 ! !
      ! WARNING: ASSUMES nQuantDOF=6 ! !
      !use meshtools
      implicit none
      
      !arguments
      real(8), intent(out):: nodalValues(nQuantDOF,nPatches)
      real(8), intent(in) :: gaussValues(nQuantDOF,NELX_l)
      integer, intent(in) :: nQuantDOF
      integer, intent(out):: info
      logical, intent(in) :: debug
      integer, intent(in), optional :: staGrainIdx,endGrainIdx
      ! local variables
      integer :: elemID,elemIdx,staElemIdx,endElemIdx,iGrain
      integer :: iLocNode,jLocNode,iGloNode,jGloNode,iDOF,nEqnsMax,nEqns
      integer :: iPatchNode,jPatchNode,iEqnID,jEqnID,staNodeIdx
      integer :: MDIM
      integer :: firstGrain, lastGrain
      real(8) :: elemJacMat(3,3)
      real(8) :: elemJac, elemVol
      real(8) :: gaussLoading(nQuantDOF)
      real(8) :: TETUnitMatrix(4,4)
      real(8) :: xElemNodes(3,4)
      real(8), allocatable :: FEMUnitMatrix(:,:)
      real(8), allocatable :: nodalValues_Grain(:,:)

      
      MDIM=3
      
      nodalValues(:,:) = 0.D0
      gaussLoading(:) = 0.D0
      CALL getTET4UnitMatrix(TETUnitMatrix)
      !if(debug) &
      !   CALL printMatrix(TETUnitMatrix,4,4,'Tet Unit Matrix',997)

      ! allocate LSQ matrix and RHS, s.t. largest eqn. system will fit in
      
      if (present(staGrainIdx) .and. present(endGrainIdx)) then
         nEqnsMax = MAXVAL(nPatchesInGrain(staGrainIdx:endGrainIdx))
         firstGrain = staGrainIdx
         lastGrain = endGrainIdx
      else
         nEqnsMax = MAXVAL(nPatchesInGrain(1:nGrains_l)) 
         firstGrain = 1
         lastGrain = nGrains_l         
      endif
      

      allocate(FEMUnitMatrix(nEqnsMax,nEqnsMax))
      allocate(nodalValues_Grain(nEqnsMax,nQuantDOF))
         
      do iGrain=firstGrain,lastGrain
      
         ! initialize LSQ matrix and RHS for the nodal values within this grain
         nEqns = nPatchesInGrain(iGrain)
         FEMUnitMatrix(:,:)  = 0.D0
         nodalValues_Grain(:,:) = 0.d0
         
         staNodeIdx = listPatchesInGrain(1,iGrain) ! starting node index within this patch
      
         staElemIdx = grainElementIndx(iGrain)
         endElemIdx = grainElementIndx(iGrain+1)-1
         do elemIdx=staElemIdx,endElemIdx
            elemID=listGrainElements(elemIdx)
            ! get the coordinates of the element
            do iLocNode=1,NODE_l
               iGloNode=IJK_l((elemID-1)*NODE_l+iLocNode)
               xElemNodes(:,iLocNode) = nodalPositions_l(:,iGloNode)
            enddo
            !calculate element Jacobian
            CALL calcJacTET4(xElemNodes, elemJacMat, elemJac, elemVol)
            !get element gauss point values
            gaussLoading(:)=gaussValues(:,elemID)
            ! assemble element matrix and forces into the global matrix
            do iLocNode=1,NODE_l
               iPatchNode=IJK_patchNodes(iLocNode,elemID)
               iEqnID = iPatchNode-staNodeIdx+1

               do jLocNode=1,NODE_l
                  jPatchNode=IJK_patchNodes(jLocNode,elemID)
                  jEqnID = jPatchNode-staNodeIdx+1
                  FEMUnitMatrix(iEqnID,jEqnID) = &
                     FEMUnitMatrix(iEqnID,jEqnID) &
                   + TETUnitMatrix(iLocNode,jLocNode)*elemJac
               enddo
               
               ! assemble the forces into the solution vector.
               ! DPOSV() will store the solution here
               nodalValues_Grain(iEqnID,:) = &
                     nodalValues_Grain(iEqnID,:) &
                   + gaussLoading(:)*elemJac/24
   !            write(997,*) 'elem ',elemID,' Jac ', elemJac
               if(debug) then
            CALL printMatrix(gaussLoading(:)*elemJac/24,1, &
                        nQuantDOF,'LSQ load vector',997)
               endif
                                 
            enddo
         enddo
         
         ! solve for the nodal values within this grain
         if (debug) then
            CALL printMatrix(FEMUnitMatrix(1:nEqns,1:nEqns),nEqns,nEqns,'FemUnitMatrix',997)
            CALL printMatrix(nodalValues_Grain(1:nEqns,1:nQuantDOF),nEqns,nQuantDOF,'gaussForcing',997)
         end if
         CALL DPOSV('U',nEqns,nQuantDOF,FEMUnitMatrix,nEqnsMax, &
                                    nodalValues_Grain,nEqnsMax,info)
         if (info.NE.0) then
            write(*,*)'DPOSV() call at recoveryLSQ() returned an error.'
            stop
         endif
         
         ! save the nodal values of the grain into the array for the whole domain
         do iDOF = 1,nQuantDOF
            nodalValues(iDOF,staNodeIdx:staNodeIdx+nEqns-1) = nodalValues_Grain(1:nEqns,iDOF)
         enddo
         
      enddo
      
      deallocate(FEMUnitMatrix)
      deallocate(nodalValues_Grain)
      
      END SUBROUTINE
      
      SUBROUTINE calcJacTET4(xElemNodes, elemJacMat, elemJac, elemVol)
      implicit none
      real(8), intent(in) :: xElemNodes(3,4)
      real(8), intent(out):: elemJacMat(3,3), elemJac, elemVol
      real(8):: XX(3),YY(3),ZZ(3)
      integer :: iCoord, iDir

      do iCoord=1,3
         do iDir=1,3
            elemJacMat(iCoord,iDir) =     &
               xElemNodes(iCoord,iDir)-xElemNodes(iCoord,4)
         enddo
      enddo
      
      elemJac= &
          elemJacMat(1,1)*elemJacMat(2,2)*elemJacMat(3,3)   &
         -elemJacMat(1,1)*elemJacMat(2,3)*elemJacMat(3,2)   &
         -elemJacMat(2,1)*elemJacMat(1,2)*elemJacMat(3,3)   &
         +elemJacMat(2,1)*elemJacMat(1,3)*elemJacMat(3,2)   &
         +elemJacMat(3,1)*elemJacMat(1,2)*elemJacMat(2,3)   &
         -elemJacMat(3,1)*elemJacMat(1,3)*elemJacMat(2,2)
     
      elemVol = elemJac/6
      
      END SUBROUTINE
      
      SUBROUTINE READSTR_l(fileNum,lineStr,error)
      IMPLICIT REAL*8(A-H,O-Z)
      ! this code skips 1 line of text in the input file.
      integer, intent(in) :: fileNum
      character(150), intent(out) :: lineStr
      integer, intent(out) :: error
      read(fileNum,'(A)',iostat=error) lineStr
      RETURN
      END SUBROUTINE
      
      end module
      
