      PROGRAM DISLOC
      
      USE SUPERLU_MOD   
      use nodalRecovery
      IMPLICIT NONE

      INCLUDE 'PARDIS.H'
      INCLUDE 'mpif.h'

      PARAMETER(MAXNRHS = 1)


!*********WITHOUT CONSTRAINT************************************
      REAL(8):: VTAU(MDOFX),G0XYZ(MAXCRD),VBC1(MBOUND1),VBC3(MBOUND3)
      REAL(8):: VTAU0(MDOFX),F(MDOFX),VT(MDOFX)
      REAL(8):: VBCS1(MBOUND1),VBCS2(MBOUND2),XUND(MAXCRD) 
!      REAL(8):: SVARS1(MAXSV),SVARS2(MAXSV)   !****************F-BAR METHOD*********************  
      REAL(8):: TRAC(MDOFX)
      REAL(8):: PROMAT2D(MAXPROPS,MAXGRP),TIME1(2)
      REAL(8):: VBC2(MBOUND2),GPXYZ(MAXCRD)
     
       
      INTEGER:: IMID(3*MAXEL),NBC1(MBOUND1)
      INTEGER:: NBC2(MBOUND2),IDF(MBOUND1)
      INTEGER:: IJK(MNELX),NBC3(MBOUND3)
      INTEGER:: IBCT1(MDOFX),NPROPSG(MAXGRP)
      INTEGER:: IBC1(MDOFX),IBCT(MDOFX)
      INTEGER:: IBC(MDOFX)
      
      ! deniz - thermal loading
      REAL(8):: TempAmbient(2), TempAmbient0
      COMMON/THERM/TempAmbient,TempAmbient0
      ! deniz - thermal loading/control information      
      integer :: temp_loading_type ! 1: temperature time history given
                                   ! 2: cyclic thermal loading around initial temp
                                   ! 3: cyclic thermal loading ABOVE initial temp
      real(8) :: temp_stress_free        ! initial/stress-free temperature
      real(8) :: temp_cyclic_amplitude   ! in Kelvins
      real(8) :: temp_cyclic_period      ! in seconds
      real(8) :: temp_history_values(MAX_HISTORY_N) ! provide temp-time points. 
      real(8) :: temp_history_times(MAX_HISTORY_N)  ! program will interpolate
                                                         ! at integration time steps
      integer :: temp_history_N           ! number of temp-time points

      
c      INTEGER:: IELEMNO(MAXNODELEM*MAXNODE),NINDX(MAXNODE)
      INTEGER:: IBELEM(MAXEL),ILF(MAXEL),IFACE_TET(12)
      INTEGER:: IFACE_BRICK(24)
      INTEGER:: NBOUD1,NBOUD2,NBOUD3,NUPNCH,IELEMNO_SIZE
      INTEGER::NX,NELX,NEQ,NPLANE,NODE
      INTEGER::NSVARS1,NDOFEL1,MDLOAD1,NPREDF1,MCRD1 
      INTEGER::NELST,NELEND,NELNOX
      INTEGER:: IERROR
      INTEGER::IPROC,NPROC,NPROW,NPCOL,MPIINFSIZE
      INTEGER::MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      INTEGER::ISENDORRECV(MAXPROC)
      INTEGER::NPROCS,ICYC,LEN,NGAUSS,NNODE
      INTEGER::NDF,MDIM, NOSTP,NGROUP
      INTEGER::NSTATE,I1,I2,IA,IB,ITOD
      INTEGER::NSEG,I,J,ICUT
      INTEGER:: N,ICOMP,JJJ,nflush1,IEL,IPATCH

      INTEGER::II,N1,N_UPDATE,IN_ST,ISIZE,NSTEP
      INTEGER::IFLAG_OVER
      INTEGER::MAXREF,NITER_REF
      INTEGER::JACFLAG,IBROYFLAG,NCYC
      INTEGER::ITE,ITR,ITEREF,NUMUPD
      INTEGER:: NUMRHS,KINC,IFLAG,IAM
      INTEGER:: IGA_ID,M
      INTEGER:: NNZ_LOC,IS,IE
      INTEGER:: NRHS,NROW_LOC
      INTEGER:: III,NMPD1,INIT,NROW,NCOL
      INTEGER:: NITER,NPOST1,JJ,ISTV_START3,KK
      INTEGER::  NSHIFT,ITAG,ISTV_START
      INTEGER:: ISTV_START1,ISTV_START2,ISLIP,IST1
      INTEGER:: IKIN_STATE,ISHIFT1,ISHIFT,ISHIFT2
      INTEGER:: INDX2,INDX,INFO,KSTEP
      
      REAL(8):: PRESSB(MAXEL)
      REAL(8):: SOLVEINITIALIZED
      REAL(8)::T1,TOTME,XIDT,XMXDT,XMINDT,TIME_PRINT
      REAL(8)::T_NODWELL,T_DWELL,TT1,TT2
      REAL(8)::SAMP,SMIN,UAMP,UMIN,TRAMP
      REAL(8):: TPERIOD,SLOAD,ST_RATE
      REAL(8)::DELTME,TMP,FINTME,deltme0
      REAL(8):: TWRITE,PNEWDT
      REAL(8)::SVOL,EVOL,VOLTOT
      REAL(8):: STEP,T2,TELAPSED,TT1SLU,TT2SLU
      REAL(8):: XMAXV,W2,W1
      REAL(8)::COLPERM,ROWPERM
      REAL(8):: PNEWDT1, B1, B2
      REAL(8):: B3, C1, C2, C3, DGAMMADT
      REAL(8):: DCHIDT,DFP2,DFP1,DELTME_PREV
      REAL(8):: DFP3,DGDT,ECT1,FTOL,VAL_MAX
      
      
C************* KOUROSH_LINESEARCH *******************************
       REAL(8):: ENERGY_NORM0,ENERGY_NORM,DOTPROD
       INTEGER:: IFLAG_LINE_SEARCH
C************* KOUROSH_LINESEARCH *******************************



C************ KOUROSH_CONVERGENCE_CRITERION*********************
       REAL(8)::DISP0_NORM,DISP1_NORM,DISP_TOL,DISP_TOL_REF,UNM,Q1,Q2,
     &          Q,OLUNM,DINORM,SQQ,SQTOL,ECTL,ENERGY_NORM_REF,
     &          R_NORM_REF,RES_TOL
       real(8) :: RES_TOL_REF
       real(8), parameter :: DISP_sq_THRESHOLD = 0.00000001d0
       real(8), parameter :: RES_THRESHOLD = 0.0001d0
       LOGICAL::CONVERGENCE
       REAL(8)::F_EXT(MDOFX)
C************ KOUROSH_CONVERGENCE_CRITERION*********************

C********* KOUROSH_BFGS *********
       REAL(8)::FOLD(MDOFX)
       INTEGER::NUPD
C********* KOUROSH_BFGS *********       

C************  KOUROSH_INT  ************
       REAL(8)::T2STEPBACK_T3STEPBACK,T1STEPBACK_T3STEPBACK,
     &          TCURRENT_T3STEPBACK
       INTEGER:: I_REPOSITORY,NSTEP_AFTER_CONTINUE_DEFORM
       REAL(8)::TIME_REPOSITORY(3),U_REPOSITORY(MDOFX,3)
C************  KOUROSH_INT  ************

C************ KOUROSH-PRESSURE ****************
       INTEGER::IDIR_PRESSURE
       COMMON/PRESSURE/IDIR_PRESSURE
C************ KOUROSH-PRESSURE ****************


C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************
       INTEGER::I_ADDAP_FACT
       INTEGER::NITER_STORE(4)
       LOGICAL::ADDAPTIVE_FACTOR
C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************


C****************** KOUROSH_LOADING_UNLOADING *************************
       LOGICAL::SHARP_POINT
       INTEGER:: N_TPOINTS_AFTER_SHARP
C****************** KOUROSH_LOADING_UNLOADING *************************


C***************** KOUROSH_TIME CONTROL *******************************
       INTEGER::N_TIME_STEP
       REAL(8),ALLOCATABLE::TIME_LIST(:)
C***************** KOUROSH_TIME CONTROL *******************************

C*************** KOUROSH-OPTIONS ************************************
       INTEGER::CONDITION_INIT,I_CONVRGNC_CRITERION,BROY_BFGS,N_SPLIT,
     &          FLOW_RULE 
       LOGICAL::CONTINUE_DEFORM,SCALE_DISP_NORM,CONDITION_LINE_SEARCH,
     &          TIME_CONTROL_FROM_FILE,GND_CAL,GND_END_OF_CYCLE,
     &          GND_CYCLIC,QUASI_NR,FACTORIZE_FREQUENTLY,FIRST_TIME,
     &          FACTORIZED,EXTENSIVE_PRINT
C*************** KOUROSH-OPTIONS ************************************

 



!****************F-BAR METHOD*********************   
      INTEGER::NPATCH,MaxElemInPatch,MinElemInPatch,MAXSV,
     &         NElementsInProcessor
      INTEGER,ALLOCATABLE::NElementsInPatch(:),ElementsInPatch(:,:)
      REAL(8),ALLOCATABLE::SVARS1(:),SVARS2(:)
!****************F-BAR METHOD*********************   



C************* KOUROSH***************
      REAL(8):: AVE_STRESS_RECV(6),AVE_STRAIN_RECV(6),RESULTOUT(18),
     &      VOL_TOTAL_RECV(1),VOL_TOTAL(1),AVE_STRESS(6),
     &      AVE_STRAIN(6),AVE_PSTRAIN(6),AVE_PSTRAIN_RECV(6),
     &      AVE_EFFECTIVEPSTRAIN(1),AVE_EFFECTIVEPSTRAIN_RECV(1)
	  
	  
      INTEGER:: IPERM(3,3,3),IELGR(MAXEL),NNODGR(NNODEX),
     & INODGR(NNODEX,MAXADJGRAIN)
	 
      REAL(8)::WEIGHT,WEIGHT_TOT,XNORM
      INTEGER::IELEM,LL,KOUNT,IDIME,JDIME,IELST,NODE_FIRST,
     &         NODE_IN_PROCESSOR,NODE_LAST,NTMP,SPR_NUMBER_SIZE,
     &         SPR_PATCH_SIZE,FP_NODAL_SIZE,ICREEP
	 

      INTEGER::NOGND,NGND_SIZE,NEL,ISTV_START_LOC
      
      REAL(8),ALLOCATABLE::GND_TOT(:),GND_TOT_RECV(:),EUL(:,:)
      REAL(8),ALLOCATABLE::GND_FOREST(:,:),GND_PARALLEL(:,:)
      INTEGER, ALLOCATABLE:: IELEMNO(:),NINDX(:),IPHASE(:)
      INTEGER, ALLOCATABLE:: NATEL(:),IATEL(:,:)
      REAL(8),ALLOCATABLE::FPEL(:,:),FP_NODAL(:,:)
      REAL(8),ALLOCATABLE::CARTD(:,:,:),EULER(:,:),GRAIN_DIAMETER(:) 
      REAL(8)::DUMMY
c************* kourosh***************      
      
      
      COMMON/BOUNDA/NBOUD1,NBOUD2,NBOUD3,NUPNCH
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
!      COMMON/STATEUPDATE/SVARS2   !****************F-BAR METHOD*********************   
!      COMMON/ABQ/TIME1,NSVARS1,NDOFEL1,MDLOAD1,NPREDF1,SVARS1,MCRD1  !****************F-BAR METHOD*********************   
      COMMON/ABQ/TIME1,NSVARS1,NDOFEL1,MDLOAD1,NPREDF1,MCRD1
      COMMON/PRESS/PRESSB,IBELEM,ILF,IFACE_TET,IFACE_BRICK
      COMMON/EL_PAR/NELST,NELEND,NELNOX
******************************************************************
C     ADDED BY PRITAM
      INTEGER,ALLOCATABLE:: IBINDX(:)
      INTEGER,ALLOCATABLE:: IROWPTR(:)
      INTEGER,ALLOCATABLE:: IBINDX_LOC(:)
      INTEGER,ALLOCATABLE:: IROWPTR_LOC(:)
      REAL(8),ALLOCATABLE:: RESID(:)
      REAL(8),ALLOCATABLE:: VAL(:)
      REAL(8),ALLOCATABLE:: RESID_LOC(:)
      REAL(8),ALLOCATABLE:: VAR(:), WAR(:)
      REAL(8),ALLOCATABLE:: DVTAU(:)
      REAL(8),ALLOCATABLE:: DVTAU_RECV(:), DVTAU0(:)
      REAL(8),ALLOCATABLE::VAL_STORE(:),DVTAU0_RECV(:) ! KOUROSH_INT


      
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV
******************************************************************  



!!!!!!!!!! nodal recovery
      integer :: p_deg 
      logical::  regularizeBoundaries 
      integer::  recoveryMethod
      integer :: localNonlocal
      logical :: DEBUG_nodalValues
      integer :: staGrainIdx, endGrainIdx, nGrainIdx
      integer :: iPatchID, grainID, LOCATION, iNode
      integer :: nfailedpatches, grainIdx, nGrains
      real(8), allocatable :: FpNodal(:)
!!!!!!!!!! nodal recovery

      COMMON/LOAD_DWELL/TPERIOD,TRAMP,T_NODWELL,T_DWELL,SAMP,SMIN,
     &                  UAMP,UMIN
      COMMON/LOAD_MONO/SLOAD,ST_RATE
      COMMON/ICYC_FLAG/ICYC
      COMMON/SPLIT_COMMUN/N_SPLIT  
      CHARACTER*30 FNAME
      CHARACTER*50 FILNAME
      REAL(8),ALLOCATABLE::TSEG(:)
      REAL(8):: BERR(MAXNRHS)

!------------SUPER LU------------------
      INTEGER(SUPERLU_PTR) :: GRID
      INTEGER(SUPERLU_PTR) :: OPTIONS
      INTEGER(SUPERLU_PTR) :: SCALEPERMSTRUCT
      INTEGER(SUPERLU_PTR) :: LUSTRUCT
      INTEGER(SUPERLU_PTR) :: SOLVESTRUCT
      INTEGER(SUPERLU_PTR) :: A
      INTEGER(SUPERLU_PTR) :: STAT
!--------------------------------------

      CALL MPI_INIT(IERROR)

      CALL MPI_COMM_RANK(MPI_COMM_WORLD,IPROC,IERROR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS,IERROR)
      
!-------FOR SLU DIST--------------------------------
      OPEN(102,FILE='slu_proc_distrib.inp')
      READ(102,*)NPROW,NPCOL
      CLOSE(102)

      IF(NPROW*NPCOL.NE.NPROCS)THEN
         WRITE(*,*)'NPROW*NPCOL NOT EQUAL TO NPROCS'
         STOP
      ENDIF
!--------------------------------------------------

C*************** KOUROSH-OPTIONS ************************************
       OPEN(102,FILE='options.inp')
       CALL READSTR(102)
       READ(102,*) GND_CAL
       if (GND_CAL) read(102,*) p_deg,regularizeBoundaries,
     &                          recoveryMethod,localNonlocal,
     &                          DEBUG_nodalValues
       
       CALL READSTR(102)
       READ(102,*) GND_END_OF_CYCLE
       IF (GND_END_OF_CYCLE) READ(102,*) TIME_PRINT
       CALL READSTR(102)
       READ(102,*) CONTINUE_DEFORM
       CALL READSTR(102)
       READ(102,*) TIME_CONTROL_FROM_FILE
       CALL READSTR(102)
       READ(102,*) CONDITION_INIT
       CALL READSTR(102)
       READ(102,*) CONDITION_LINE_SEARCH       
       CALL READSTR(102)
       READ(102,*) I_CONVRGNC_CRITERION
       CALL READSTR(102)
       READ(102,*) SCALE_DISP_NORM       
       CALL READSTR(102)
       READ(102,*) BROY_BFGS
       CALL READSTR(102)
       READ(102,*) FACTORIZE_FREQUENTLY    
       CALL READSTR(102)
       READ(102,*) QUASI_NR  
       CALL READSTR(102)
       READ(102,*) N_SPLIT    
       CALL READSTR(102)
       READ(102,*) FLOW_RULE
       IF (FLOW_RULE.EQ.2) READ(102,*) EXTENSIVE_PRINT
       CLOSE(102)       
C*************** KOUROSH-OPTIONS ************************************




      T1=MPI_WTIME()

      IF(NPROCS.LT.MINPROC)THEN
         WRITE(*,*)'NPROCS LESS THAN MINPROC',NPROCS,MINPROC
         STOP
      ENDIF

      MPIINFSIZE=0

      CALL PROC_COMB(NPROCS,MPIINF,MPIINFSIZE)

      CALL ARRANGE_PROC(IPROCINFO,ISENDORRECV,MPIINF,MPIINFSIZE,IPROC)
     


      OPEN(201,FILE='loadtime.inp')
      
      CALL READSTR(201)
      READ(201,*)ICYC

      CALL READSTR(201)
      READ(201,*)FNAME

      CLOSE(201)
            
      LEN=LEN_TRIM(FNAME)
      FILNAME=FNAME(1:LEN)//'.inp'

      OPEN(UNIT=LR,FILE=FILNAME)

      IF(IPROC.EQ.0)THEN
         FILNAME=FNAME(1:LEN)//'.out'
         OPEN(UNIT=131,FILE=FILNAME)
         OPEN(UNIT=100,FILE='disp.out')
      ENDIF

      CALL GET_SUBSCRIPT(IPROC,FNAME)
      
      FILNAME='stress'//FNAME
      OPEN(508,FILE=FILNAME)

      FILNAME='fp'//FNAME
      OPEN(506,FILE=FILNAME)

      IF (GND_CAL) THEN 	
         FILNAME='curlfp'//FNAME
         open(509,file=FILNAME)
      ENDIF
      FILNAME='sevol.out'
      OPEN(505,FILE=FILNAME)	

      FILNAME='time.out'
      OPEN(132,FILE=FILNAME)
	

   

      IF (FLOW_RULE.EQ.2.AND.EXTENSIVE_PRINT) THEN
      
         FILNAME='VELOCITY'//FNAME
         OPEN(1001,FILE=FILNAME)

         FILNAME='TIME_WAIT'//FNAME
         OPEN(1002,FILE=FILNAME)

         FILNAME='TIME_RUN'//FNAME
         OPEN(1003,FILE=FILNAME)

!       FILNAME='PASSING_STRESS'//FNAME
!       OPEN(1004,FILE=FILNAME)
              
         FILNAME='MOB_DENSITY'//FNAME
         OPEN(1005,FILE=FILNAME)
                     
         FILNAME='DELTA_GAMMA'//FNAME
         OPEN(1006,FILE=FILNAME)
                     
!       FILNAME='CUT_STRESS'//FNAME
!       OPEN(1007,FILE=FILNAME) 

!       FILNAME='FOREST_DISLOCATION'//FNAME
!       OPEN(1008,FILE=FILNAME) 

!       FILNAME='PARALLEL_DISLOCATION'//FNAME
!       OPEN(1009,FILE=FILNAME)

!       FILNAME='RESOLVED_SHEAR'//FNAME
!       OPEN(1010,FILE=FILNAME)

         FILNAME='TEMPERATURE_WP'//FNAME
         OPEN(1011,FILE=FILNAME) 
       
      ENDIF
       
   
   
   
   
   

      IF(IBRICKORTET.EQ.1)THEN

         NGAUSS=8
         NNODE=8
         NODE=8

      ELSEIF(IBRICKORTET.EQ.2)THEN

         NGAUSS=1
         NNODE=4
         NODE=4

      ENDIF


  

      PRESSB=0.D0
      IBELEM=0
      ILF=0

      
      CALL READGM(NDF,G0XYZ,IMID,IJK,MDIM)
      
      IF(IPROC.EQ.0)THEN
      WRITE(*,*)'#DOF #NODES #ELEMENTS', NDF ,NX, NELX
      ENDIF
      
      CALL READNSET()
      CALL READELSET()
      CALL TMSTEP(TOTME,XIDT,XMXDT,XMINDT,NOSTP)
      
      IF(IPROC.EQ.0)THEN
      WRITE(*,*)'TOTME XIDT XMXDT XMINDT NOSTP', 
     & TOTME, XIDT,XMXDT,XMINDT,NOSTP
      ENDIF
      
      CALL READMT(NGROUP,PROMAT2D,NPROPSG,NSTATE)


      
      
!****************F-BAR METHOD*********************      
      OPEN(201,FILE='PATCH.inp')
      READ(201,*) NPATCH,MaxElemInPatch,MinElemInPatch
      
      ALLOCATE(NElementsInPatch(NPATCH))
      ALLOCATE(ElementsInPatch(NPATCH,MaxElemInPatch))
      
      NElementsInPatch=0
      ElementsInPatch=0
      
      DO I=1,NPATCH
         READ(201,*) NElementsInPatch(I),
     &               (ElementsInPatch(I,J),J=1,NElementsInPatch(I))      
      ENDDO
      
      CLOSE(201)
      
      
C      I1=NELX/NPROCS+1
C      I2=NELX/NPROCS
      
C      IA=NELX-I2*NPROCS
C      IB=NPROCS-IA
      
C      IF((IPROC+1).LE.IA)THEN
C         NELST=IPROC*I1+1
C         NELNOX=I1
C      ELSE
C         NELST=IA*I1+(IPROC-IA)*I2+1
C         NELNOX=I2
C      ENDIF     
C      NELEND=NELST+NELNOX-1      
      
      I1=NPATCH/NPROCS+1
      I2=NPATCH/NPROCS
      
      IA=NPATCH-I2*NPROCS
      IB=NPROCS-IA
      
      IF((IPROC+1).LE.IA)THEN
         NELST=IPROC*I1+1
         NELNOX=I1
      ELSE
         NELST=IA*I1+(IPROC-IA)*I2+1
         NELNOX=I2
      ENDIF
      
      NELEND=NELST+NELNOX-1

      NElementsInProcessor=0
      DO I=NELST,NELEND
         NElementsInProcessor=NElementsInProcessor+NElementsInPatch(I)
      ENDDO


      IF (I2.LT.N_SPLIT) THEN
         WRITE(*,*) '# PATCHES PER PROCESSORS IS LESS THAT N_SPLIT'
         CALL MPI_FINALIZE(IERROR)
         STOP
      ENDIF
      
      I2=0
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
      CALL MPI_ALLREDUCE(NElementsInProcessor,I2,1,
     &     MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,IERROR)
     
      IF (I2.NE.NELX) THEN
         WRITE(*,*) 'SOME ELEMENTS ARE NOT IN ANY OF THE PATCHES'
         CALL MPI_FINALIZE(IERROR)
         STOP
      ENDIF
            
      
      MAXSV=NElementsInProcessor*msv
      ALLOCATE(SVARS1(MAXSV),SVARS2(MAXSV))
      
!****************F-BAR METHOD*********************            

      ALLOCATE(EUL(3,MAXEL),GRAIN_DIAMETER(MAXEL),IPHASE(MAXEL))
      OPEN(UNIT=201,FILE='texture.dat')
      DO I=1,NELX
         READ(201,*) EUL(1,I),EUL(2,I),EUL(3,I),IPHASE(I)
      ENDDO
      CLOSE(201)
   
      OPEN(UNIT=201,FILE='size.dat')
      DO I=1,NELX
         READ(201,*) J,GRAIN_DIAMETER(I)
      ENDDO
      CLOSE(201)
 

      OPEN(UNIT=201,FILE='ielgr.inp')
      DO I=1,MAXEL
          READ(201,*)J,IELGR(I)          
      ENDDO
      CLOSE(201)
 

      CALL INITABQ(NSTATE,MDIM,KINC,NSTEP,NCYC,CONTINUE_DEFORM,MAXSV,
     &             SVARS1,SVARS2,EUL,IPHASE,GRAIN_DIAMETER,FLOW_RULE,
     &             NPATCH,MaxElemInPatch,NElementsInPatch,
     &             ElementsInPatch)
     
      DEALLOCATE(EUL,GRAIN_DIAMETER,IPHASE)
     
      CALL READBC(NBC1,VBC1,IDF,NBC2,VBC2,NBC3,VBC3,NDF,IBC1,IBCT1)
      
      ! deniz - read thermal loading info
      CALL READTEMP('thermal_loading.inp',
     &              temp_loading_type, temp_stress_free,
     &              temp_cyclic_amplitude, temp_cyclic_period,
     &              temp_history_values, temp_history_times,
     &              temp_history_N,
     &              INFO)
      if(IPROC.EQ.0) then
         if(INFO.EQ.1) then
            write(*,*) 'Thermal loading data imported.'
         else
            write(*,*) 'WARNING !!! no thermal loading specified 
     &(thermal_loading.inp) -- ASSUMING T=300 K'
         end if
      endif
      

      ALLOCATE(CARTD(NElementsInProcessor,3,4))    !F-BAR METHOD
      CARTD=0.D0


C************* PARTITIONING NODES TO PROCESSORS
      I1=NX/NPROCS+1
      I2=NX/NPROCS
      
      IA=NX-I2*NPROCS

      IF((IPROC+1).LE.IA)THEN
         NODE_FIRST=IPROC*I1+1
         NODE_IN_PROCESSOR=I1
      ELSE
         NODE_FIRST=IA*I1+(IPROC-IA)*I2+1
         NODE_IN_PROCESSOR=I2
      ENDIF

      NODE_LAST=NODE_FIRST+NODE_IN_PROCESSOR-1

      ALLOCATE(NATEL(NODE_IN_PROCESSOR))
      ALLOCATE(IATEL(NODE_IN_PROCESSOR,MAXADJGRAINELEM))

         
C************* END PARTITIONING NODES

      
      
      CALL NODNB(NATEL,IATEL,IJK,G0XYZ,IPERM,IELGR,
     &        CARTD,NELST,NELEND,MNELX,MAXCRD,NODE_IN_PROCESSOR,
     &        NElementsInProcessor,NElementsInPatch,NPATCH,
     &        MaxElemInPatch,ElementsInPatch,maxadjelem,
     &        maxadjgrain,maxadjgrainelem,maxnode,maxel,NNODGR,INODGR,
     &        NODE_FIRST,NODE_LAST)   !F-BAR METHOD

      IF (IPROC.EQ.0) THEN

         OPEN(515,FILE='NNODGR.OUT')
         OPEN(516,FILE='INODGR.OUT')

         DO II=1,NX
            WRITE(515,*) NNODGR(II)
            DO JJ=1,NNODGR(II)
               WRITE(516,*) INODGR(II,JJ)          
            ENDDO
         ENDDO

         CLOSE(515)
         CLOSE(516)	
      ENDIF
	
      IF (GND_CAL) THEN
		
         CALL initialize_Patches('listPatches.dat',
     &                        NX,NELX,IJK,G0XYZ)
         if (IPROC==0) then
            if (patchesInitialized) then
               write(*,*) 'patches imported:',nPatches
            else
               write(*,*) 'error importing listPatches.dat'
               stop
            endif
         endif

         ALLOCATE(FPEL(9,NELX),FP_NODAL(9,nPatches),
     &            FpNodal(9*Nx*MAXADJGRAIN) )
               
      ENDIF
	
	
********************************************************************
C     ADDED BY PRITAM
      CALL NODE_ELEM_CONNEC_SCAN(IJK,NDF,IELEMNO_SIZE,ElementsInPatch,
     &                      NElementsInPatch,MaxElemInPatch,NPATCH)    !F-BAR METHOD
      ALLOCATE(IELEMNO(IELEMNO_SIZE))   !F-BAR METHOD
      ALLOCATE(NINDX(MAXNODE))   
      IELEMNO=0
      NINDX=0
      CALL NODE_ELEM_CONNEC(IJK,NDF,IELEMNO,NINDX,ElementsInPatch,
     &          IELEMNO_SIZE,NElementsInPatch,MaxElemInPatch,NPATCH)    !F-BAR METHOD    
********************************************************************


!--------------READS TRACTION OR DISPLACEMENT FLAG----------------------
     
      IF(ICYC.EQ.1)THEN

         OPEN(201,FILE='loadtime_cyc.inp')
         
         CALL READSTR(201)
         READ(201,*)T_NODWELL,T_DWELL

         CALL READSTR(201)
         READ(201,*)ITOD,IDIR_PRESSURE 

         IF(ITOD.EQ.1)THEN

            CALL READSTR(201)    
            READ(201,*)SAMP,SMIN
            UAMP=0.D0
            UMIN=0.D0

         ELSEIF(ITOD.EQ.2)THEN

            CALL READSTR(201)
            READ(201,*)UAMP,UMIN
            SAMP=0.D0
            SMIN=0.D0

         ENDIF

         CALL READSTR(201)
         READ(201,*)NSEG

         ALLOCATE(TSEG(NSEG+1))
         TSEG=0.D0
         
         READ(201,*)(TSEG(I),I=1,NSEG+1)
         
         CLOSE(201)

            
         IF(ITOD.EQ.1)THEN
            TRAMP=T_NODWELL*SMIN/SAMP
         ELSE
            TRAMP=T_NODWELL*UMIN/UAMP
         ENDIF
         
      
         TPERIOD=2.D0*T_NODWELL+T_DWELL
         SLOAD=0.D0
         ST_RATE=0.D0


      ELSE

         OPEN(201,FILE='loadtime_creep.inp')
      
         CALL READSTR(201)
         READ(201,*)TRAMP

         CALL READSTR(201)
         READ(201,*)ITOD 

         CALL READSTR(201)
         
         IF(ITOD.EQ.1)THEN
            ST_RATE=0.D0
	     ICREEP=1
            READ(201,*)SLOAD,IDIR_PRESSURE   !************ KOUROSH-PRESSURE ****************
         ELSE
	     ICREEP=0	
            SLOAD=0.D0
            IDIR_PRESSURE=0 !************ KOUROSH-PRESSURE ****************
            READ(201,*)ST_RATE
         ENDIF
            
         CLOSE(201)
      
      
         TPERIOD=0.D0
         T_NODWELL=0.D0
         T_DWELL=0.D0
         SAMP=0.D0
         SMIN=0.D0
         UAMP=0.D0
         UMIN=0.D0
         NSEG=0.D0

      ENDIF


C***************** KOUROSH_TIME CONTROL *******************************
       IF (TIME_CONTROL_FROM_FILE) THEN
         OPEN(201,FILE='time_list.inp')
         READ(201,*) N_TIME_STEP
         ALLOCATE(TIME_LIST(N_TIME_STEP))
         TIME_LIST=0.D0
         DO I=1,N_TIME_STEP
               READ(201,*) TIME_LIST(I)
         ENDDO
       ENDIF
C***************** KOUROSH_TIME CONTROL *******************************

      DELTME=XIDT
      DUMMY = XIDT
      ICUT=0

      DO I=1,MCRD1*NX
         GPXYZ(I)=G0XYZ(I)
         XUND(I)=G0XYZ(I)
      ENDDO

      DO I=1,NEQ
         VTAU0(I)=0.D0
         VT(I)=0.D0
         TRAC(I)=0.D0
      ENDDO
                       

!***********************************************                       
!  New feature---Continue from last simulation
 
      IF (CONTINUE_DEFORM) THEN
      
         OPEN(UNIT=336,FILE='LASTDISP.INP')
            
         DO I=1,NEQ
               READ(336,*) VTAU(I)  
               VTAU0(I)=VTAU(I)
               VT(I)=VTAU(I)
         ENDDO
            
            
         DO I=1,MCRD1*NX
            GPXYZ(I)=G0XYZ(I)+VTAU(I)
         ENDDO
            
         CLOSE(336)
      
      ENDIF
!************************************************	

      DO N=1,NBOUD1
         IF(IDF(N).EQ.0)THEN
            DO II=1,NDF
               N1=NDF*(N-1)+II      
               VBCS1(N1)=VBC1(N1)/TOTME
               VBC1(N1)=0.D0
            ENDDO
         ENDIF
      ENDDO

      DO N=1,NBOUD2
         DO II=1,NDF
            N1=NDF*(N-1)+1
            VBCS2(N1)=VBC2(N1)/TOTME
         ENDDO
      ENDDO
     


C     AZTEC ROUTINE PERFORMING A LINEAR PARTITIONING OF THE NODES
      CALL NODE_PARTITION(N_UPDATE,IN_ST,IPROC,NPROCS,NX,NDF)

      ISIZE=0
      CALL INIT_SIZE_SLU(IJK,NDF,IPROC,N_UPDATE,IN_ST,
     &              IELEMNO_SIZE,ISIZE,IELEMNO,NINDX)
      

      ALLOCATE(IROWPTR(N_UPDATE+1))
      ALLOCATE (IBINDX(ISIZE))

      CALL INIT_MSR_SLU(IJK,NDF,IPROC,N_UPDATE,IN_ST,IELEMNO_SIZE,
     &           IROWPTR,IBINDX,ISIZE,IELEMNO,NINDX)

      DEALLOCATE(IELEMNO,NINDX) 
      
      ALLOCATE(RESID(N_UPDATE))
      ALLOCATE (VAL(ISIZE))
      ALLOCATE (VAL_STORE(ISIZE)) ! KOUROSH_INT
      ALLOCATE (DVTAU0_RECV(NEQ)) ! KOUROSH_INT  
      VAL=0.D0
      RESID=0.D0
      VAL_STORE=0.D0 ! KOUROSH_INT
      DVTAU0_RECV=0.D0      



      NITER= 0
      FINTME=0.D0
      IFLAG_OVER=0



      MAXREF=18
      NITER_REF=10
      JACFLAG=0
      IBROYFLAG=0


      ALLOCATE(DVTAU0(NEQ),DVTAU(NEQ),DVTAU_RECV(NEQ))
      
C********* KOUROSH_BFGS *********      
       IF (BROY_BFGS.EQ.1) THEN
         ALLOCATE(VAR(N_UPDATE*MAXREF),WAR(N_UPDATE*MAXREF))
         VAR=0.D0
         WAR=0.D0
       ELSEIF (BROY_BFGS.EQ.2) THEN
         ALLOCATE(VAR(NEQ*MAXREF),WAR(NEQ*MAXREF))
         VAR=0.D0
         WAR=0.D0
       ENDIF       
C********* KOUROSH_BFGS *********  
    
      DVTAU0=0.D0
      DVTAU=0.D0
      DVTAU_RECV=0.D0
      deltme0=XMINDT
      
      IF (CONTINUE_DEFORM) THEN
         FINTME=TIME1(1)
         NSTEP_AFTER_CONTINUE_DEFORM=1
      ENDIF     
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************
       NITER_STORE=0
       I_ADDAP_FACT=1
       ADDAPTIVE_FACTOR=.TRUE.
C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************


C************  KOUROSH_INT  ************
       TIME_REPOSITORY=0.D0
       U_REPOSITORY=0.D0
       I_REPOSITORY=1
C************  KOUROSH_INT  ************

C****************** KOUROSH_LOADING_UNLOADING *************************
       SHARP_POINT = .false.
       N_TPOINTS_AFTER_SHARP=1
C****************** KOUROSH_LOADING_UNLOADING *************************


      IF (FACTORIZE_FREQUENTLY) FIRST_TIME=.TRUE.  !*********NRorQUASINR*********

      DO WHILE(IFLAG_OVER.EQ.0)  
         TT1=MPI_WTIME()  
         IF(IPROC.EQ.0)THEN       
         WRITE(*,*)'START INCREMENT', NSTEP
         ENDIF

C***************** KOUROSH_TIME CONTROL *******************************              
         IF(ICUT.EQ.0.AND.IBROYFLAG.EQ.0)THEN

            IF (.NOT.TIME_CONTROL_FROM_FILE) THEN  
                 IF(ICYC.EQ.1)THEN
                     CALL TDWELL(FINTME,DELTME,XIDT,XMINDT,
     &                           NSEG,TSEG,SHARP_POINT)  !*********** KOUROSH_LOADING_UNLOADING 
                 ELSE
                     CALL T_CREEP_CSTRAINRATE(FINTME,DELTME,XIDT,
     &                           XMINDT,XMXDT)
                ENDIF
            
            ELSE
               IF (NCYC.EQ.0) THEN
                  DUMMY = DELTME
                  CALL TDWELL(FINTME,DUMMY,XIDT,XMINDT,
     &                           NSEG,TSEG,SHARP_POINT)  !*********** KOUROSH_LOADING_UNLOADING             
               ENDIF
                CALL TIME_CONTROL(TIME_LIST,TSEG,NSEG,N_TIME_STEP,
     &                    NCYC,FINTME,DELTME,TRAMP,SHARP_POINT)
     
               
     
            ENDIF  

         ENDIF
C***************** KOUROSH_TIME CONTROL *******************************      
         IF (NCYC.EQ.0.AND.ICYC.EQ.1.and. 
     &       ICUT.EQ.0.AND.IBROYFLAG.EQ.0) DELTME = DUMMY
         FINTME=FINTME+DELTME
         
         IF (FINTME.GT.TOTME) THEN
            FINTME=FINTME-DELTME
            DELTME=TOTME-FINTME
            FINTME=TOTME
         ENDIF
         
         
         TIME1(1)=FINTME
         TIME1(2)=FINTME

 
        IF(IPROC.EQ.0)THEN
            WRITE(*,*)'FINTME TRAMP DELTME',FINTME,TRAMP,DELTME
         ENDIF
            
            
         TWRITE=TIME1(1)-TRAMP
         

         KSTEP=1
         PNEWDT=1.D0

         FTOL=1.0D-2      
         ITE=0
         ITR=0
         ITEREF=0
         NUMUPD=0
         NUMRHS=0 
         STEP=1.D0
         KINC=NSTEP

C************ KOUROSH_CONVERGENCE_CRITERION*********************
         DISP_TOL=2.D-2
         ECTL=1.D-2
         RES_TOL=1.D-2
         Q1=0.D0
         Q2=0.D0
         Q=0.D0
C************ KOUROSH_CONVERGENCE_CRITERION********************* 



C****************** KOUROSH_LOADING_UNLOADING *************************
         IF (SHARP_POINT) THEN
              N_TPOINTS_AFTER_SHARP=1
         ENDIF
C****************** KOUROSH_LOADING_UNLOADING *************************


C************  KOUROSH_INT  ************

 
         IFLAG=1
         IF(JACFLAG.EQ.0.OR.FACTORIZE_FREQUENTLY)THEN
            IFLAG=0
            VAL=0.D0         
            IBROYFLAG=0
         ENDIF


         
         IF (FACTORIZE_FREQUENTLY) FACTORIZED=.FALSE.  !*********NRorQUASINR*********
         
         
         IF ((ICYC.NE.1.AND.CONDITION_INIT.EQ.1).OR.
     &       (ICYC.EQ.1.AND.(CONDITION_INIT.EQ.1.OR.SHARP_POINT))) THEN   !C****************** KOUROSH_LOADING_UNLOADING
              IF (IPROC.EQ.0) WRITE(*,*) '^^^ENTERED K_TANGT GUESS^^^'
              
              F(1:NEQ)=0.D0
              RESID(1:N_UPDATE)=0.D0
  
              VTAU=VT
              DVTAU0=0.D0
              CALL LOADVC(F,VTAU,DVTAU0,XUND,IJK,IMID,NPROPSG,
     &                    PROMAT2D,DELTME,NBC1,IBC1,KINC,NDF,N_UPDATE,
     &                    IN_ST,IROWPTR,IBINDX,VAL,RESID,ISIZE,IFLAG,
     &                    NGAUSS,PNEWDT,VAL_STORE,F_EXT,SVARS1,SVARS2,
     &                    MAXSV,MaxElemInPatch,NElementsInPatch,
     &                    ElementsInPatch,NPATCH,FLOW_RULE,BROY_BFGS)  ! KOUROSH_INT, KOUROSH_CONVERGENCE_CRITERION, F-BAR METHOD
     
              IF(PNEWDT.LT.1.D0) GOTO 20
              IF (IFLAG.EQ.0.AND.BROY_BFGS.NE.0) IFLAG=1
              VTAU0=VT
              CALL DISINC(VTAU0,VBCS1,NBC1,DELTME,IDF,NSTEP,KSTEP,
     &        G0XYZ,TIME1,NDF,MCRD1)
              VTAU=VTAU0
              
              
              
              CALL FIRST_DISPLACEMENT_GUESS(F,VTAU,VT, VAL_STORE,
     &          IROWPTR,IBINDX,IBC1,NEQ,ISIZE,MDOFX,N_UPDATE,IN_ST)
     
              ALLOCATE(RESID_LOC(N_UPDATE))
              RESID_LOC(1:N_UPDATE)=F(IN_ST+1:IN_ST+N_UPDATE)
              

!*********NRorQUASINR*********
!*********NRorQUASINR*********
!*********NRorQUASINR*********
              
            IF (.NOT.FACTORIZE_FREQUENTLY) THEN
               IF(JACFLAG.EQ.0)THEN  ! KOUROSH_CONVERGENCE_CRITERION
    
                     CALL F_CREATE_GRIDINFO_HANDLE(GRID) 
                     CALL F_CREATE_OPTIONS_HANDLE(OPTIONS)  
                     CALL F_CREATE_SCALEPERM_HANDLE(SCALEPERMSTRUCT)
                     CALL F_CREATE_LUSTRUCT_HANDLE(LUSTRUCT)
                     CALL F_CREATE_SOLVESTRUCT_HANDLE(SOLVESTRUCT)
                     CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                     CALL F_CREATE_SUPERLUSTAT_HANDLE(STAT)
            
                     CALL F_SUPERLU_GRIDINIT(MPI_COMM_WORLD,NPROW,
     &                                       NPCOL,GRID)
                     CALL GET_GRIDINFO(GRID, IAM=IAM)
            
                     IF ( IAM >= NPROW * NPCOL ) THEN
               
                            IF(IGA_ID.EQ.0)THEN
                                   WRITE(*,*)'IAM>=NPROW*NPCOL'
                     
                            ENDIF
               
                            CALL MPI_FINALIZE(IERROR)
                            STOP
               
                     ENDIF
            

                     ALLOCATE(IBINDX_LOC(IROWPTR(N_UPDATE+1)))
                     ALLOCATE(IROWPTR_LOC(N_UPDATE+1))
                     IBINDX_LOC=0
                     IROWPTR_LOC=0
            
            
                     N=NEQ
                     M=NEQ
            
                     NNZ_LOC=0
                     DO I=1,N_UPDATE
                            IS=IROWPTR(I)
                            IE=IROWPTR(I+1)-1
                            IF(I.EQ.N_UPDATE)IE=IROWPTR(I+1)
                            DO J=IS,IE
                                   IBINDX_LOC(J)=IBINDX(J)-1
                                   NNZ_LOC=NNZ_LOC+1
                            ENDDO
                     ENDDO
            
            
            
                     DO I=1,N_UPDATE
                            IROWPTR_LOC(I)=IROWPTR(I)-1
                     ENDDO
                     IROWPTR_LOC(N_UPDATE+1)=IROWPTR(N_UPDATE+1)
            
            
                     CALL F_DCREATE_COMPROWLOC_MAT_DIST(A,M,N,NNZ_LOC,
     &                     N_UPDATE,IN_ST,VAL,IBINDX_LOC,IROWPTR_LOC, 
     &                     SLU_NR_LOC,SLU_D,SLU_GE)
            
            
                     NRHS = 1
                        
                     CALL GET_COMPROWLOC_MATRIX(A, NROW_LOC=N_UPDATE)

                     CALL F_SET_DEFAULT_OPTIONS(OPTIONS) 
                     call set_superlu_options(options,PrintStat=NO)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,COLPERM=NATURAL)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,ROWPERM=NOROWPERM)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        COLPERM=MMD_AT_PLUS_A)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        ROWPERM=LARGEDIAG)
                     CALL GET_SUPERMATRIX(A,NROW=M,NCOL=N)
                     CALL F_SCALEPERMSTRUCTINIT(M, N, SCALEPERMSTRUCT)
                     CALL F_LUSTRUCTINIT(M, N, LUSTRUCT)
                     CALL F_PSTATINIT(STAT)

            
               ENDIF              
              
             
               IF(JACFLAG.EQ.0)THEN
                     CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                     TT1SLU=MPI_WTIME()
                     CALL F_PDGSSVX(OPTIONS,A,SCALEPERMSTRUCT,
     &                              RESID_LOC,N_UPDATE, NRHS, GRID, 
     &                              LUSTRUCT,SOLVESTRUCT,BERR,STAT,
     &                              INFO)

                     TT2SLU=MPI_WTIME()
                     IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)  
     
                     JACFLAG=1
               ELSE
               
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,FACT=FACTORED)
                     CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                     TT1SLU=MPI_WTIME()
                     CALL F_PDGSSVX(OPTIONS, A, SCALEPERMSTRUCT, 
     &                              RESID_LOC,N_UPDATE, NRHS, GRID, 
     &                              LUSTRUCT,SOLVESTRUCT, BERR, STAT,
     &                              INFO)
                      TT2SLU=MPI_WTIME()
                     IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)  
               ENDIF
             

            ELSEIF (FACTORIZE_FREQUENTLY) THEN
              
                  IF(JACFLAG.EQ.0)THEN  ! KOUROSH_CONVERGENCE_CRITERION
    
                     IF (FIRST_TIME) THEN
                        CALL F_CREATE_GRIDINFO_HANDLE(GRID) 
                        CALL F_CREATE_OPTIONS_HANDLE(OPTIONS)  
                        CALL F_CREATE_SCALEPERM_HANDLE(SCALEPERMSTRUCT)
                        CALL F_CREATE_LUSTRUCT_HANDLE(LUSTRUCT)
                        CALL F_CREATE_SOLVESTRUCT_HANDLE(SOLVESTRUCT)
                        CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                        CALL F_CREATE_SUPERLUSTAT_HANDLE(STAT)
                        
                        CALL F_SUPERLU_GRIDINIT(MPI_COMM_WORLD,NPROW,
     &                                       NPCOL,GRID)
                        CALL GET_GRIDINFO(GRID, IAM=IAM)
                        
                     ELSE
                        CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                     ENDIF
            
                     IF ( IAM >= NPROW * NPCOL ) THEN
               
                            IF(IPROC.EQ.0)THEN
                                   WRITE(*,*)'IAM>=NPROW*NPCOL'
                     
                            ENDIF
               
                            CALL MPI_FINALIZE(IERROR)
                            STOP
               
                     ENDIF
            

                     ALLOCATE(IBINDX_LOC(IROWPTR(N_UPDATE+1)))
                     ALLOCATE(IROWPTR_LOC(N_UPDATE+1))
                     IBINDX_LOC=0
                     IROWPTR_LOC=0
            
            
                     N=NEQ
                     M=NEQ
            
                     NNZ_LOC=0
                     DO I=1,N_UPDATE
                            IS=IROWPTR(I)
                            IE=IROWPTR(I+1)-1
                            IF(I.EQ.N_UPDATE)IE=IROWPTR(I+1)
                            DO J=IS,IE
                                   IBINDX_LOC(J)=IBINDX(J)-1
                                   NNZ_LOC=NNZ_LOC+1
                            ENDDO
                     ENDDO
            
            
            
                     DO I=1,N_UPDATE
                            IROWPTR_LOC(I)=IROWPTR(I)-1
                     ENDDO
                     IROWPTR_LOC(N_UPDATE+1)=IROWPTR(N_UPDATE+1)
            
            
                     CALL F_DCREATE_COMPROWLOC_MAT_DIST(A,M,N,NNZ_LOC,
     &                     N_UPDATE,IN_ST,VAL,IBINDX_LOC,IROWPTR_LOC, 
     &                     SLU_NR_LOC,SLU_D,SLU_GE)
            
            
                     NRHS = 1
                        
                     CALL GET_COMPROWLOC_MATRIX(A, NROW_LOC=N_UPDATE)

                     CALL F_SET_DEFAULT_OPTIONS(OPTIONS) 
                     call set_superlu_options(options,PrintStat=NO)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,COLPERM=NATURAL)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,ROWPERM=NOROWPERM)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        COLPERM=MMD_AT_PLUS_A)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        ROWPERM=LARGEDIAG)           
                     IF (FIRST_TIME) THEN
                        CALL GET_SUPERMATRIX(A,NROW=M,NCOL=N)
                        CALL F_SCALEPERMSTRUCTINIT(M,N,SCALEPERMSTRUCT)
                        CALL F_LUSTRUCTINIT(M, N, LUSTRUCT)
                        CALL F_PSTATINIT(STAT)
                     ENDIF
  

            
                  ENDIF              
              
             
                  IF (FIRST_TIME) THEN
                     FIRST_TIME = .FALSE.
                  ELSE
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                 FACT=SAMEPATTERN_SAMEROWPERM) 
                  ENDIF

                  CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                  TT1SLU=MPI_WTIME()
                  CALL F_PDGSSVX(OPTIONS,A,SCALEPERMSTRUCT,RESID_LOC,
     &                        N_UPDATE, NRHS, GRID, LUSTRUCT, 
     &                        SOLVESTRUCT, BERR, STAT, INFO)
                  TT2SLU=MPI_WTIME()
                  IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)  
               
                  IF (QUASI_NR) THEN
                     FACTORIZED=.TRUE.              
                  ELSE
                     DEALLOCATE(IBINDX_LOC,IROWPTR_LOC)
                     CALL F_DESTROY_SUPERMAT_STORE_DIST(A)
                     CALL F_DESTROY_SUPERMATRIX_HANDLE(A)                
                  ENDIF
              
            ENDIF

!*********NRorQUASINR*********
!*********NRorQUASINR*********
!*********NRorQUASINR*********

              
              DVTAU0=0.D0
              DVTAU0(IN_ST+1:IN_ST+N_UPDATE)=RESID_LOC(1:N_UPDATE)
              DEALLOCATE(RESID_LOC)
              CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)           
              CALL MPI_ALLREDUCE(DVTAU0,DVTAU0_RECV,NEQ,
     &                           MPI_DOUBLE_PRECISION,
     &                           MPI_SUM,MPI_COMM_WORLD,IERROR)
     
              DVTAU0(1:NEQ)=DVTAU0_RECV(1:NEQ)
              VTAU(1:NEQ)=VT(1:NEQ)+DVTAU0(1:NEQ)

              
         ELSEIF(CONDITION_INIT.EQ.2) THEN
              IF (IPROC.EQ.0) WRITE(*,*) '^^^^ENTERED LINEAR GUESS^^^'
              IF ((.NOT.CONTINUE_DEFORM.AND.NSTEP.NE.1).OR.
     &    (CONTINUE_DEFORM.AND.NSTEP_AFTER_CONTINUE_DEFORM.NE.1)) THEN
                        VTAU0(1:NEQ)=U_REPOSITORY(1:NEQ,3)+
     &                  (FINTME-TIME_REPOSITORY(3))/
     &                  (TIME_REPOSITORY(3)-TIME_REPOSITORY(2))*
     &                  (U_REPOSITORY(1:NEQ,3)-U_REPOSITORY(1:NEQ,2))   ! KOUROSH_INT
              ENDIF
              CALL DISINC(VTAU0,VBCS1,NBC1,DELTME,IDF,NSTEP,KSTEP,
     &        G0XYZ,TIME1,NDF,MCRD1)
              VTAU(1:NEQ)=VTAU0(1:NEQ)     
              DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)
              

              
         ELSEIF(CONDITION_INIT.EQ.3) THEN
              
              IF ((.NOT.CONTINUE_DEFORM.AND.ICYC.NE.1.AND.NSTEP.GE.4)
     &             .OR.(.NOT.CONTINUE_DEFORM.AND.ICYC.EQ.1.AND.
     &             N_TPOINTS_AFTER_SHARP.GE.4).OR.
     &            (CONTINUE_DEFORM.AND.ICYC.NE.1.AND.
     &             NSTEP_AFTER_CONTINUE_DEFORM.GE.4)
     &             .OR.(CONTINUE_DEFORM.AND.ICYC.EQ.1.AND.
     &             N_TPOINTS_AFTER_SHARP.GE.4)) THEN
                  IF (IPROC.EQ.0) WRITE(*,*)'^^^ENTERED QUAD GUESS^^^'
                     T2STEPBACK_T3STEPBACK= TIME_REPOSITORY(2) - 
     &                               TIME_REPOSITORY(1)
                     T1STEPBACK_T3STEPBACK= TIME_REPOSITORY(3) - 
     &                               TIME_REPOSITORY(1)
                     TCURRENT_T3STEPBACK=   FINTME - 
     &                               TIME_REPOSITORY(1)
                     VTAU0(1:NEQ)= U_REPOSITORY(1:NEQ,1)*
     &                   (T2STEPBACK_T3STEPBACK-TCURRENT_T3STEPBACK)*
     &                   (T1STEPBACK_T3STEPBACK-TCURRENT_T3STEPBACK)/
     &                   T2STEPBACK_T3STEPBACK/T1STEPBACK_T3STEPBACK+
     &                   U_REPOSITORY(1:NEQ,2)*TCURRENT_T3STEPBACK*
     &                   (TCURRENT_T3STEPBACK-T1STEPBACK_T3STEPBACK)/
     &                   T2STEPBACK_T3STEPBACK/(T2STEPBACK_T3STEPBACK-
     &                   T1STEPBACK_T3STEPBACK)+U_REPOSITORY(1:NEQ,3)*
     &                   TCURRENT_T3STEPBACK*(T2STEPBACK_T3STEPBACK-
     &                   TCURRENT_T3STEPBACK)/(T2STEPBACK_T3STEPBACK-
     &                   T1STEPBACK_T3STEPBACK)/T1STEPBACK_T3STEPBACK
                     CALL DISINC(VTAU0,VBCS1,NBC1,DELTME,IDF,NSTEP,
     &                           KSTEP,G0XYZ,TIME1,NDF,MCRD1)
                     VTAU(1:NEQ)=VTAU0(1:NEQ)     
                     DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)
          

              ELSE
              
                IF (IPROC.EQ.0) WRITE(*,*)'^^^ENTERED LINEAR GUESS^^^'
                IF ((.NOT.CONTINUE_DEFORM.AND.NSTEP.NE.1).OR.
     &    (CONTINUE_DEFORM.AND.NSTEP_AFTER_CONTINUE_DEFORM.NE.1)) THEN
                        VTAU0(1:NEQ)=U_REPOSITORY(1:NEQ,3)+
     &                  (FINTME-TIME_REPOSITORY(3))/
     &                  (TIME_REPOSITORY(3)-TIME_REPOSITORY(2))*
     &                  (U_REPOSITORY(1:NEQ,3)-U_REPOSITORY(1:NEQ,2))   ! KOUROSH_INT
                     ENDIF
                     CALL DISINC(VTAU0,VBCS1,NBC1,DELTME,IDF,NSTEP,
     &                           KSTEP,G0XYZ,TIME1,NDF,MCRD1)
                     VTAU(1:NEQ)=VTAU0(1:NEQ)     
                     DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)
              ENDIF       
              
         ENDIF		
C************  KOUROSH_INT  ************    

         CALL TRAINC(NBC2,VBC2,VBCS2,DELTME,NDF,TRAC) 
         
         ! deniz - update temperature state variable, 
         ! according to the prescribed thermal loading
         CALL TEMPINC(temp_loading_type, temp_stress_free, 
     &                temp_cyclic_amplitude, temp_cyclic_period, 
     &                temp_history_values, temp_history_times,  
     &                temp_history_N, TIME1(:),
     &                TempAmbient(:), TempAmbient0)
         ! print thermal conditions
         IF(IPROC.EQ.0)THEN
            WRITE(*,*)'AMBIENT TEMPERATURE',TempAmbient(2)
         ENDIF
         
!-----------------------------------------QSINT---------------------------------------------

        

C         DVTAU0(1:NEQ)=0.D0   !KOUROSH_INT

C********* KOUROSH_BFGS *********
C         VAR(1:N_UPDATE*MAXREF)=0.D0
C         WAR(1:N_UPDATE*MAXREF)=0.D0

!*********NRorQUASINR*********
         IF (BROY_BFGS.NE.0) THEN
            VAR=0.D0
            WAR=0.D0    
         ENDIF
!*********NRorQUASINR*********         
         
C********* KOUROSH_BFGS *********

C         VTAU(1:NEQ)=VTAU0(1:NEQ)   !KOUROSH_INT
C         DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)  !KOUROSH_INT
	  
C         IFLAG=1                   !KOUROSH_INT
C
C        IF(JACFLAG.EQ.0)THEN       !KOUROSH_INT
C            IFLAG=0                !KOUROSH_INT
C           VAL=0.D0                !KOUROSH_INT  
C            IBROYFLAG=0            !KOUROSH_INT
C         ENDIF                     !KOUROSH_INT




         F(1:NEQ)=0.D0
         RESID(1:N_UPDATE)=0.D0
  

         CALL LOADVC(F,VTAU,DVTAU0,XUND,IJK,IMID,NPROPSG,
     & PROMAT2D,DELTME,NBC1,IBC1,KINC,NDF,N_UPDATE,IN_ST,IROWPTR,
     & IBINDX,VAL,RESID,ISIZE,IFLAG,NGAUSS,PNEWDT,VAL_STORE,F_EXT,
     & SVARS1,SVARS2,MAXSV,MaxElemInPatch,NElementsInPatch,
     & ElementsInPatch,NPATCH,FLOW_RULE,BROY_BFGS)  ! KOUROSH_INT, KOUROSH_CONVERGENCE_CRITERION, F-BAR METHOD

         IF (IFLAG.EQ.0.AND.PNEWDT.LT.1.D0) GOTO 20


         IF(PNEWDT.GE.1.D0)THEN

            XMAXV=DABS(F(1))
            DO I=2,NEQ
               IF(DABS(F(I)).GT.XMAXV)THEN
                  XMAXV=DABS(F(I))
               ENDIF
            ENDDO
            
            VAL_MAX=DOTPROD(F,F,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
            VAL_MAX=DSQRT(VAL_MAX)
C************ KOUROSH_CONVERGENCE_CRITERION*********************            
            R_NORM_REF=VAL_MAX
C************ KOUROSH_CONVERGENCE_CRITERION*********************             
            IF(IPROC.EQ.0)THEN
               WRITE(*,*)'       RESID:',XMAXV,'R NORM:',VAL_MAX
            ENDIF
            
C********* KOUROSH_BFGS *********            
            FOLD=F
C********* KOUROSH_BFGS *********

         ENDIF



             

!*********NRorQUASINR*********
!*********NRorQUASINR*********
!*********NRorQUASINR*********
              
            IF (.NOT.FACTORIZE_FREQUENTLY) THEN
               IF(JACFLAG.EQ.0)THEN  ! KOUROSH_CONVERGENCE_CRITERION
    
                     CALL F_CREATE_GRIDINFO_HANDLE(GRID) 
                     CALL F_CREATE_OPTIONS_HANDLE(OPTIONS)  
                     CALL F_CREATE_SCALEPERM_HANDLE(SCALEPERMSTRUCT)
                     CALL F_CREATE_LUSTRUCT_HANDLE(LUSTRUCT)
                     CALL F_CREATE_SOLVESTRUCT_HANDLE(SOLVESTRUCT)
                     CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                     CALL F_CREATE_SUPERLUSTAT_HANDLE(STAT)
            
                     CALL F_SUPERLU_GRIDINIT(MPI_COMM_WORLD,NPROW,
     &                                       NPCOL,GRID)
                     CALL GET_GRIDINFO(GRID, IAM=IAM)
            
                     IF ( IAM >= NPROW * NPCOL ) THEN
               
                            IF(IGA_ID.EQ.0)THEN
                                   WRITE(*,*)'IAM>=NPROW*NPCOL'
                     
                            ENDIF
               
                            CALL MPI_FINALIZE(IERROR)
                            STOP
               
                     ENDIF
            

                     ALLOCATE(IBINDX_LOC(IROWPTR(N_UPDATE+1)))
                     ALLOCATE(IROWPTR_LOC(N_UPDATE+1))
                     IBINDX_LOC=0
                     IROWPTR_LOC=0
            
            
                     N=NEQ
                     M=NEQ
            
                     NNZ_LOC=0
                     DO I=1,N_UPDATE
                            IS=IROWPTR(I)
                            IE=IROWPTR(I+1)-1
                            IF(I.EQ.N_UPDATE)IE=IROWPTR(I+1)
                            DO J=IS,IE
                                   IBINDX_LOC(J)=IBINDX(J)-1
                                   NNZ_LOC=NNZ_LOC+1
                            ENDDO
                     ENDDO
            
            
            
                     DO I=1,N_UPDATE
                            IROWPTR_LOC(I)=IROWPTR(I)-1
                     ENDDO
                     IROWPTR_LOC(N_UPDATE+1)=IROWPTR(N_UPDATE+1)
            
            
                     CALL F_DCREATE_COMPROWLOC_MAT_DIST(A,M,N,NNZ_LOC,
     &                     N_UPDATE,IN_ST,VAL,IBINDX_LOC,IROWPTR_LOC, 
     &                     SLU_NR_LOC,SLU_D,SLU_GE)
            
            
                     NRHS = 1
                        
                     CALL GET_COMPROWLOC_MATRIX(A, NROW_LOC=N_UPDATE)

                     CALL F_SET_DEFAULT_OPTIONS(OPTIONS) 
                     call set_superlu_options(options,PrintStat=NO)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,COLPERM=NATURAL)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,ROWPERM=NOROWPERM)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        COLPERM=MMD_AT_PLUS_A)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        ROWPERM=LARGEDIAG)
                     CALL GET_SUPERMATRIX(A,NROW=M,NCOL=N)
                     CALL F_SCALEPERMSTRUCTINIT(M, N, SCALEPERMSTRUCT)
                     CALL F_LUSTRUCTINIT(M, N, LUSTRUCT)
                     CALL F_PSTATINIT(STAT)

            
               ENDIF              
              
 
            ELSEIF (FACTORIZE_FREQUENTLY) THEN
              
                  IF(JACFLAG.EQ.0.AND.((QUASI_NR.AND..NOT.FACTORIZED)
     &                                  .OR.(.NOT.QUASI_NR))) THEN  ! KOUROSH_CONVERGENCE_CRITERION
    
                     IF (FIRST_TIME) THEN
                        CALL F_CREATE_GRIDINFO_HANDLE(GRID) 
                        CALL F_CREATE_OPTIONS_HANDLE(OPTIONS)  
                        CALL F_CREATE_SCALEPERM_HANDLE(SCALEPERMSTRUCT)
                        CALL F_CREATE_LUSTRUCT_HANDLE(LUSTRUCT)
                        CALL F_CREATE_SOLVESTRUCT_HANDLE(SOLVESTRUCT)
                        CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                        CALL F_CREATE_SUPERLUSTAT_HANDLE(STAT)
                        
                        CALL F_SUPERLU_GRIDINIT(MPI_COMM_WORLD,NPROW,
     &                                       NPCOL,GRID)
                        CALL GET_GRIDINFO(GRID, IAM=IAM)
                        
                     ELSE
                        CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                     ENDIF
            
                     IF ( IAM >= NPROW * NPCOL ) THEN
               
                            IF(IPROC.EQ.0)THEN
                                   WRITE(*,*)'IAM>=NPROW*NPCOL'
                     
                            ENDIF
               
                            CALL MPI_FINALIZE(IERROR)
                            STOP
               
                     ENDIF
            

                     ALLOCATE(IBINDX_LOC(IROWPTR(N_UPDATE+1)))
                     ALLOCATE(IROWPTR_LOC(N_UPDATE+1))
                     IBINDX_LOC=0
                     IROWPTR_LOC=0
            
            
                     N=NEQ
                     M=NEQ
            
                     NNZ_LOC=0
                     DO I=1,N_UPDATE
                            IS=IROWPTR(I)
                            IE=IROWPTR(I+1)-1
                            IF(I.EQ.N_UPDATE)IE=IROWPTR(I+1)
                            DO J=IS,IE
                                   IBINDX_LOC(J)=IBINDX(J)-1
                                   NNZ_LOC=NNZ_LOC+1
                            ENDDO
                     ENDDO
            
            
            
                     DO I=1,N_UPDATE
                            IROWPTR_LOC(I)=IROWPTR(I)-1
                     ENDDO
                     IROWPTR_LOC(N_UPDATE+1)=IROWPTR(N_UPDATE+1)
            
            
                     CALL F_DCREATE_COMPROWLOC_MAT_DIST(A,M,N,NNZ_LOC,
     &                     N_UPDATE,IN_ST,VAL,IBINDX_LOC,IROWPTR_LOC, 
     &                     SLU_NR_LOC,SLU_D,SLU_GE)
            
            
                     NRHS = 1
                        
                     CALL GET_COMPROWLOC_MATRIX(A, NROW_LOC=N_UPDATE)

                     CALL F_SET_DEFAULT_OPTIONS(OPTIONS) 
                     call set_superlu_options(options,PrintStat=NO)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,COLPERM=NATURAL)
c                    CALL SET_SUPERLU_OPTIONS(OPTIONS,ROWPERM=NOROWPERM)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        COLPERM=MMD_AT_PLUS_A)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        ROWPERM=LARGEDIAG)           
                     IF (FIRST_TIME) THEN
                       CALL GET_SUPERMATRIX(A,NROW=M,NCOL=N)
                       CALL F_SCALEPERMSTRUCTINIT(M,N,SCALEPERMSTRUCT)
                       CALL F_LUSTRUCTINIT(M, N, LUSTRUCT)
                       CALL F_PSTATINIT(STAT)
                     ENDIF
  
                  ENDIF              
            ENDIF

!*********NRorQUASINR*********
!*********NRorQUASINR*********
!*********NRorQUASINR*********


         
!------------------------------------------------------
         
C         IF(XMAXV.GT.FTOL)THEN   !KOUROSH_CONVERGENCE_CRITERION
         IF(PNEWDT.GE.1.D0)THEN   ! KOUROSH_CONVERGENCE_CRITERION 
            DVTAU(1:NEQ)=0.D0
            
            ALLOCATE(RESID_LOC(N_UPDATE))
            
            RESID_LOC(1:N_UPDATE)=RESID(1:N_UPDATE)
            
            
            IF (.NOT.FACTORIZE_FREQUENTLY) THEN
               IF(JACFLAG.EQ.0)THEN
                  CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                  TT1SLU=MPI_WTIME()
                  CALL F_PDGSSVX(OPTIONS,A,SCALEPERMSTRUCT,RESID_LOC,
     &               N_UPDATE, NRHS, GRID, LUSTRUCT, SOLVESTRUCT, 
     &               BERR,STAT,INFO)
                  TT2SLU=MPI_WTIME()
                  IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)                            
                  JACFLAG=1
           
               ELSE
                  
                  CALL SET_SUPERLU_OPTIONS(OPTIONS,FACT=FACTORED)
                  CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                  TT1SLU=MPI_WTIME()
                  CALL F_PDGSSVX(OPTIONS,A,SCALEPERMSTRUCT,RESID_LOC,
     &                        N_UPDATE, NRHS, GRID, LUSTRUCT, 
     &                        SOLVESTRUCT, BERR, STAT, INFO)
                   TT2SLU=MPI_WTIME()
                  IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)  
               ENDIF
               
               
            ELSEIF (FACTORIZE_FREQUENTLY.AND.QUASI_NR) THEN
            
              IF (FIRST_TIME.AND..NOT.FACTORIZED) THEN
                  FIRST_TIME = .FALSE.
               ELSEIF(FACTORIZED) THEN
                  CALL SET_SUPERLU_OPTIONS(OPTIONS,FACT=FACTORED)
               ELSE
                  CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                 FACT=SAMEPATTERN_SAMEROWPERM) 
               ENDIF

               CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
               TT1SLU=MPI_WTIME()
               CALL F_PDGSSVX(OPTIONS, A, SCALEPERMSTRUCT, RESID_LOC,
     &                        N_UPDATE, NRHS, GRID, LUSTRUCT, 
     &                        SOLVESTRUCT, BERR, STAT, INFO)
               TT2SLU=MPI_WTIME()
               IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)  
               FACTORIZED=.TRUE.            
            

            ELSEIF (FACTORIZE_FREQUENTLY.AND..NOT.QUASI_NR) THEN
               
               IF (FIRST_TIME) THEN
                  FIRST_TIME = .FALSE.
               ELSE
                  CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                 FACT=SAMEPATTERN_SAMEROWPERM) 
               ENDIF

               CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
               TT1SLU=MPI_WTIME()
               CALL F_PDGSSVX(OPTIONS, A, SCALEPERMSTRUCT, RESID_LOC,
     &                        N_UPDATE, NRHS, GRID, LUSTRUCT, 
     &                        SOLVESTRUCT, BERR, STAT, INFO)
               TT2SLU=MPI_WTIME()
               IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)  
               DEALLOCATE(IBINDX_LOC,IROWPTR_LOC)
               CALL F_DESTROY_SUPERMAT_STORE_DIST(A)
               CALL F_DESTROY_SUPERMATRIX_HANDLE(A)               

            ENDIF   
               
               
            DVTAU(1+IN_ST:N_UPDATE+IN_ST)=RESID_LOC(1:N_UPDATE)
            
            DEALLOCATE(RESID_LOC)
         
            CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)           
            CALL MPI_ALLREDUCE(DVTAU,DVTAU_RECV,NEQ,
     &             MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)   
            DVTAU(1:NEQ)=DVTAU_RECV(1:NEQ)
            
         ENDIF
         
         
         
C************* KOUROSH_LINESEARCH *******************************         
         ENERGY_NORM0=DOTPROD(DVTAU,F,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
C************* KOUROSH_LINESEARCH *******************************

         
C************ KOUROSH_CONVERGENCE_CRITERION*********************
         ENERGY_NORM_REF=ENERGY_NORM0
         DISP0_NORM=DOTPROD(DVTAU0,DVTAU0,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
         DISP1_NORM=DOTPROD(DVTAU,DVTAU,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
c         DISP_TOL_REF=DISP_TOL**2.D0*MAX(DISP0_NORM,DISP1_NORM)
         DISP_TOL_REF=DISP_TOL**2.D0*DISP0_NORM
         CONVERGENCE=.FALSE.
C************ KOUROSH_CONVERGENCE_CRITERION*********************


!         DO WHILE(XMAXV.GT.FTOL.AND.PNEWDT.GE.1.D0)  !KOUROSH_CONVERGENCE_CRITERION     !<--------------------
         DO WHILE(.NOT.CONVERGENCE.AND.PNEWDT.GE.1.D0)


            IF (IPROC.EQ.0) THEN
              WRITE(*,*)('@TIME  ITER'), TIME1(1), ITEREF
            ENDIF
        
        
            IF (IFLAG.EQ.0.AND.BROY_BFGS.NE.0) IFLAG=1

              
            ITE=ITE+1
            ITR=ITR+1
            STEP=1.D0
C************* KOUROSH_LINESEARCH *******************************            
            IFLAG_LINE_SEARCH=0
C************* KOUROSH_LINESEARCH *******************************

            VTAU(1:NEQ)=VTAU(1:NEQ)+STEP*DVTAU(1:NEQ)
            DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)

            F(1:NEQ)=0.D0
            RESID(1:N_UPDATE)=0.D0
  
  
            
            CALL LOADVC(F,VTAU,DVTAU0,XUND,IJK,IMID,NPROPSG,
     &      PROMAT2D,DELTME,NBC1,IBC1,KINC,NDF,N_UPDATE,IN_ST,IROWPTR,
     &      IBINDX,VAL,RESID,ISIZE,IFLAG,NGAUSS,PNEWDT,VAL_STORE,
     &      F_EXT,SVARS1,SVARS2,MAXSV,MaxElemInPatch,NElementsInPatch,
     &      ElementsInPatch,NPATCH,FLOW_RULE,BROY_BFGS)  ! KOUROSH_INT, KOUROSH_CONVERGENCE_CRITERION, F-BAR METHOD
            
       
            

            IF(PNEWDT.GE.1.D0)THEN
              
              
C************* KOUROSH_LINESEARCH *******************************
              ENERGY_NORM=DOTPROD(DVTAU,F,NBC1,NEQ,NDF,NBOUD1,MBOUND1)

              
              IF(IPROC.EQ.0) WRITE(*,'(3(A15,ES16.8))') 
     &        '       ENERGY_NORM=',ENERGY_NORM,'ENERGY_NORM0=',
     &        ENERGY_NORM0,'RATIO=',DABS(ENERGY_NORM/ENERGY_NORM0)
     
            

              IF (DABS(ENERGY_NORM).GT.(0.9*DABS(ENERGY_NORM0)).AND.
     &            CONDITION_LINE_SEARCH) THEN
                     VTAU(1:NEQ)=VTAU(1:NEQ)-STEP*DVTAU(1:NEQ)
                     DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)

                     CALL LINE_SEARCH(STEP,ENERGY_NORM,
     &                   ENERGY_NORM0,VTAU,VT,DVTAU,PROMAT2D,XUND,
     &                   VAL_STORE,IJK,NBC1,IBC1,IMID,NPROPSG,IROWPTR,
     &                   IBINDX,MNELX,MAXEL,KINC,NDF,IN_ST,ISIZE,IFLAG,
     &                   NGAUSS,MAXGRP,MAXDIM,MAXPROPS,MBOUND1,MDOFX,
     &                   DELTME,NBOUD1,NEQ,N_UPDATE,MAXCRD,IPROC,
     &                   IFLAG_LINE_SEARCH,F,RESID,SVARS1,SVARS2,
     &                   MAXSV,MaxElemInPatch,NElementsInPatch,
     &                   ElementsInPatch,NPATCH,FLOW_RULE,BROY_BFGS)  ! KOUROSH_INT, KOUROSH_CONVERGENCE_CRITERION, F-BAR METHOD
                     
                     IF (IFLAG_LINE_SEARCH.EQ.1) GOTO 10
                     IF (IPROC.EQ.0) WRITE(*,'(2(A32,ES16.8))') 
     &                               'AFTER LINE SEARCH,ENERGY_NORM=',
     &                                ENERGY_NORM,'REF ENERY NORM=',
     &                                ENERGY_NORM0
                     IF (STEP.LT.0.001D0) STEP=0.D0
                     IF (STEP.LT.0.001D0) ITEREF=MAXREF+1
                     IF (IPROC.EQ.0) WRITE(*,*) 'STEP=',STEP
                     VTAU(1:NEQ)=VTAU(1:NEQ)+STEP*DVTAU(1:NEQ)
                     DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)
     
              ENDIF
C************* KOUROSH_LINESEARCH *******************************            
            
            
               XMAXV=DABS(F(1))
               DO I=2,NEQ
                  IF(DABS(F(I)).GT.XMAXV)THEN
                     XMAXV=DABS(F(I))
                  ENDIF
               ENDDO
               
               VAL_MAX=DOTPROD(F,F,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
               VAL_MAX=DSQRT(VAL_MAX)
               IF(IPROC.EQ.0)THEN
                  WRITE(*,*)'       RESID:',XMAXV,'R NORM:',VAL_MAX
               ENDIF
            
C************ KOUROSH_CONVERGENCE_CRITERION*********************
              UNM=DOTPROD(DVTAU,DVTAU,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
              UNM=UNM*STEP*STEP
              Q2=Q1
              Q1=0.D0
              IF (ITR.NE.1) Q1=UNM/OLUNM
              Q=MAX(Q1,Q2)
              OLUNM=UNM
              if (OLUNM < DISP_sq_THRESHOLD) OLUNM = DISP_sq_THRESHOLD
              DINORM=DSQRT(UNM)
              SQQ=DSQRT(Q)
              SQTOL=DSQRT(DISP_TOL_REF)
              
              RES_TOL_REF = MAXVAL(DABS(F_EXT))
              if (RES_TOL_REF <= RES_THRESHOLD) RES_TOL_REF = RES_THRESHOLD
              
              IF(.NOT.SCALE_DISP_NORM)  SQQ=0.5D0
      
              
              IF (IPROC.EQ.0) THEN
                  WRITE(*,*)'       DISP: SQQ,DINORM,SQTOL',SQQ,
     &                          DINORM,SQTOL
                  WRITE(*,*)'       ENERGY: NORM0,REF NORM',
     &                          ENERGY_NORM0,ENERGY_NORM_REF
                  WRITE(*,*)'       FORCE: MAX RESIDUAL, MAX F_EXT',
     &                          XMAXV,RES_TOL_REF   
              ENDIF
              
              
              IF ( ((SQQ*DINORM).LE.((1.D0-SQQ)*SQTOL)) .AND. 
     &        (DABS(ENERGY_NORM0).LE.ECTL*DABS(ENERGY_NORM_REF)).AND.
     &         ITR.GT.1.AND.I_CONVRGNC_CRITERION.EQ.1) THEN
                     
                     CONVERGENCE=.TRUE.
                     
              ELSEIF(XMAXV.LE.RES_TOL*RES_TOL_REF.AND.I_CONVRGNC_CRITERION.EQ.2) THEN

                     CONVERGENCE=.TRUE.
                     
              ELSEIF ( ((SQQ*DINORM).LE.((1.D0-SQQ)*SQTOL)) .AND. 
     &        (XMAXV.LE.RES_TOL*RES_TOL_REF).AND.ITR.GT.1.AND.
     &        I_CONVRGNC_CRITERION.EQ.3) THEN 

                     CONVERGENCE=.TRUE.
                     
              ENDIF

              IF (CONVERGENCE) GOTO 10

C************ KOUROSH_CONVERGENCE_CRITERION*********************            

               ITEREF=ITEREF+1
               
               IF(ITEREF.GT.MAXREF)THEN 
                        
                  IF(IPROC.EQ.0)THEN
                     WRITE(*,*)'BROY ITR NOT CONVERGING'
                  ENDIF
                  
                  PNEWDT=0.5D0
                  IBROYFLAG=1
     
               ENDIF
                              

C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************
              IF (FINTME.GT.TRAMP.AND.
     &            NITER_STORE(1).GT.(NITER_REF-1).AND.
     &            NITER_STORE(2).GT.(NITER_REF-1).AND.
     &            NITER_STORE(3).GT.(NITER_REF-1).AND.
     &            NITER_STORE(4).GT.(NITER_REF-1).AND.
     &            DELTME.LT.(0.5D0*XMXDT).AND.ADDAPTIVE_FACTOR) THEN
                  
                  IF (IPROC.EQ.0) WRITE(*,*)'FACTORIZATION REQUIRED'
                  PNEWDT=0.5D0
                  IBROYFLAG=1
                  ADDAPTIVE_FACTOR=.FALSE.
              ENDIF

C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************




!*********NRorQUASINR*********
!*********NRorQUASINR*********
!*********NRorQUASINR*********

              IF (BROY_BFGS.EQ.0) THEN  !********* KOUROSH_BFGS *********
               IF(IBROYFLAG.EQ.0)THEN


                  IF(JACFLAG.EQ.0)THEN  ! KOUROSH_CONVERGENCE_CRITERION

                     IF (FIRST_TIME) THEN
                        CALL F_CREATE_GRIDINFO_HANDLE(GRID) 
                        CALL F_CREATE_OPTIONS_HANDLE(OPTIONS)  
                        CALL F_CREATE_SCALEPERM_HANDLE(SCALEPERMSTRUCT)
                        CALL F_CREATE_LUSTRUCT_HANDLE(LUSTRUCT)
                        CALL F_CREATE_SOLVESTRUCT_HANDLE(SOLVESTRUCT)
                        CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                        CALL F_CREATE_SUPERLUSTAT_HANDLE(STAT)
                        
                        CALL F_SUPERLU_GRIDINIT(MPI_COMM_WORLD,NPROW,
     &                                          NPCOL,GRID)
                        CALL GET_GRIDINFO(GRID, IAM=IAM)
                        
                     ELSE
                        CALL F_CREATE_SUPERMATRIX_HANDLE(A)
                     ENDIF

                     
                     IF ( IAM >= NPROW * NPCOL ) THEN
                        
                        IF(IGA_ID.EQ.0)THEN
                           WRITE(*,*)'IAM>=NPROW*NPCOL'
                           
                        ENDIF
                        
                        CALL MPI_FINALIZE(IERROR)
                        STOP
                        
                     ENDIF
            

                     ALLOCATE(IBINDX_LOC(IROWPTR(N_UPDATE+1)))
                     ALLOCATE(IROWPTR_LOC(N_UPDATE+1))
                     IBINDX_LOC=0
                     IROWPTR_LOC=0
                     
                     
                     N=NEQ
                     M=NEQ
                     
                     NNZ_LOC=0
                     DO I=1,N_UPDATE
                        IS=IROWPTR(I)
                        IE=IROWPTR(I+1)-1
                        IF(I.EQ.N_UPDATE)IE=IROWPTR(I+1)
                        DO J=IS,IE
                           IBINDX_LOC(J)=IBINDX(J)-1
                           NNZ_LOC=NNZ_LOC+1
                        ENDDO
                     ENDDO
                     
            
                     
                     DO I=1,N_UPDATE
                        IROWPTR_LOC(I)=IROWPTR(I)-1
                     ENDDO
                     IROWPTR_LOC(N_UPDATE+1)=IROWPTR(N_UPDATE+1)
                     
                     
                     CALL F_DCREATE_COMPROWLOC_MAT_DIST(A,M,N,NNZ_LOC,
     &      N_UPDATE,IN_ST,VAL, IBINDX_LOC, IROWPTR_LOC, SLU_NR_LOC,
     &      SLU_D, SLU_GE)
            
            
                     NRHS = 1
                                 
                     CALL GET_COMPROWLOC_MATRIX(A, NROW_LOC=N_UPDATE)

                     CALL F_SET_DEFAULT_OPTIONS(OPTIONS) 
                     call set_superlu_options(options,PrintStat=NO)
c            CALL SET_SUPERLU_OPTIONS(OPTIONS,COLPERM=NATURAL)
c            CALL SET_SUPERLU_OPTIONS(OPTIONS,ROWPERM=NOROWPERM)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,COLPERM=
     &                                        MMD_AT_PLUS_A)
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                                        ROWPERM=LARGEDIAG)
                     
        
                     IF (FIRST_TIME) THEN
                        CALL GET_SUPERMATRIX(A,NROW=M,NCOL=N)
                        CALL F_SCALEPERMSTRUCTINIT(M,N,SCALEPERMSTRUCT)
                        CALL F_LUSTRUCTINIT(M, N, LUSTRUCT)
                        CALL F_PSTATINIT(STAT)
                     ENDIF

                  ENDIF

                    
                  DVTAU(1:NEQ)=0.D0
                  
                  ALLOCATE(RESID_LOC(N_UPDATE))
                  
                  RESID_LOC(1:N_UPDATE)=RESID(1:N_UPDATE)
            
                  IF (FIRST_TIME) THEN
                     FIRST_TIME = .FALSE.
                  ELSE
                     CALL SET_SUPERLU_OPTIONS(OPTIONS,
     &                 FACT=SAMEPATTERN_SAMEROWPERM) 
                  ENDIF

                  CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                  TT1SLU=MPI_WTIME()
                  CALL F_PDGSSVX(OPTIONS,A,SCALEPERMSTRUCT,RESID_LOC,
     &                        N_UPDATE, NRHS, GRID, LUSTRUCT, 
     &                        SOLVESTRUCT, BERR, STAT, INFO)
                  TT2SLU=MPI_WTIME()
                  IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)  
                  DEALLOCATE(IBINDX_LOC,IROWPTR_LOC)
                  CALL F_DESTROY_SUPERMAT_STORE_DIST(A)
                  CALL F_DESTROY_SUPERMATRIX_HANDLE(A)

                  DVTAU(1:NEQ)=0.D0
                  DVTAU(1+IN_ST:N_UPDATE+IN_ST)=RESID_LOC(1:N_UPDATE)
                  
                  DEALLOCATE(RESID_LOC)
                
                  CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                  CALL MPI_ALLREDUCE(DVTAU,DVTAU_RECV,NEQ,
     &                 MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,
     &                 IERROR)
                  
                  DVTAU(1:NEQ)=DVTAU_RECV(1:NEQ)
                  
C************* KOUROSH_LINESEARCH *******************************
                  ENERGY_NORM0=DOTPROD(DVTAU,F,NBC1,NEQ,NDF,NBOUD1,
     &                                 MBOUND1)   
C************* KOUROSH_LINESEARCH *******************************                
                  NITER=ITEREF+1 
               ENDIF
            ENDIF


!*********NRorQUASINR*********
!*********NRorQUASINR*********
!*********NRorQUASINR*********

            

              IF (BROY_BFGS.EQ.1) THEN  !********* KOUROSH_BFGS *********
               IF(IBROYFLAG.EQ.0)THEN
            
            
                  DVTAU(1:NEQ)=STEP*DVTAU(1:NEQ)
                  NMPD1=NUMUPD*N_UPDATE
                     
                  WAR(NMPD1+1:NMPD1+N_UPDATE)=DVTAU(IN_ST+1:IN_ST+
     &                                              N_UPDATE)
                  
                  ALLOCATE(RESID_LOC(N_UPDATE))
                  
                  RESID_LOC(1:N_UPDATE)=RESID(1:N_UPDATE)
                  
                  CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                  
                  CALL SET_SUPERLU_OPTIONS(OPTIONS,FACT=FACTORED)
     
                  TT1SLU=MPI_WTIME()
                  CALL F_PDGSSVX(OPTIONS,A,SCALEPERMSTRUCT,RESID_LOC,
     &                 N_UPDATE, NRHS, GRID, LUSTRUCT,SOLVESTRUCT,
     &                 BERR,STAT,INFO)
                  
                  TT2SLU=MPI_WTIME()
                  IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)                       
                  
                  DVTAU(1:NEQ)=0.D0
                  DVTAU(1+IN_ST:N_UPDATE+IN_ST)=RESID_LOC(1:N_UPDATE)
                  
                  DEALLOCATE(RESID_LOC)
              
                  CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                  CALL MPI_ALLREDUCE(DVTAU,DVTAU_RECV,NEQ,
     &                 MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,
     &                 IERROR)
                
                  DVTAU(1:NEQ)=DVTAU_RECV(1:NEQ)
                  
                  CALL BROY_PARALLEL(DVTAU,VAR,WAR,NUMUPD,NMPD1,
     &                 MAXREF,IN_ST,N_UPDATE,NEQ,STEP,IPROC)
     
C************* KOUROSH_LINESEARCH *******************************
                  ENERGY_NORM0=DOTPROD(DVTAU,F,NBC1,NEQ,NDF,NBOUD1,
     &                                 MBOUND1)   
C************* KOUROSH_LINESEARCH *******************************                  
       
               ENDIF
              ENDIF  !********* KOUROSH_BFGS *********
              
              
              
              
C********* KOUROSH_BFGS *********
              IF (BROY_BFGS.EQ.2) THEN
               IF(IBROYFLAG.EQ.0)THEN
                     CALL BFGS(DVTAU,FOLD,F,STEP,ENERGY_NORM,
     &                         ENERGY_NORM0,DINORM,VAR,WAR,MDOFX,
     &                         N_UPDATE,MAXREF,NUMUPD,NBC1,NEQ,NDF,
     &                         NBOUD1,MBOUND1,1,IPROC,NUPD)
              
                   ALLOCATE(RESID_LOC(N_UPDATE))
                  
                   RESID_LOC(1:N_UPDATE)=DVTAU(1+IN_ST:N_UPDATE+IN_ST)
                  
                   CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                   CALL SET_SUPERLU_OPTIONS(OPTIONS,FACT=FACTORED)
                   
                  TT1SLU=MPI_WTIME()
                  CALL F_PDGSSVX(OPTIONS,A,SCALEPERMSTRUCT,
     &                           RESID_LOC,N_UPDATE,NRHS,GRID, 
     &                           LUSTRUCT,SOLVESTRUCT,BERR,STAT,INFO)
                  
                  TT2SLU=MPI_WTIME()
                  IF (IPROC.EQ.0) WRITE(*,*) 'SLU TOOK',
     &                                 (TT2SLU-TT1SLU)                      
                  
                     DVTAU(1:NEQ)=0.D0
                     DVTAU(1+IN_ST:N_UPDATE+IN_ST)=
     &                                           RESID_LOC(1:N_UPDATE)
                  
                     DEALLOCATE(RESID_LOC)
    
                     CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
                     CALL MPI_ALLREDUCE(DVTAU,DVTAU_RECV,NEQ,
     &                     MPI_DOUBLE_PRECISION,MPI_SUM,
     &                     MPI_COMM_WORLD,IERROR)
              
                     DVTAU(1:NEQ)=DVTAU_RECV(1:NEQ)
                     
                     NUMUPD=NUMUPD+NUPD
                     
                     CALL BFGS(DVTAU,FOLD,F,STEP,ENERGY_NORM,
     &                         ENERGY_NORM0,DINORM,VAR,WAR,MDOFX,
     &                         N_UPDATE,MAXREF,NUMUPD,NBC1,NEQ,NDF,
     &                         NBOUD1,MBOUND1,2,IPROC,NUPD)
     
C************* KOUROSH_LINESEARCH *******************************
                  ENERGY_NORM0=DOTPROD(DVTAU,F,NBC1,NEQ,NDF,NBOUD1,
     &                                 MBOUND1)   
C************* KOUROSH_LINESEARCH ******************************* 
               ENDIF     
              ENDIF



C********* KOUROSH_BFGS *********              

              
C************ KOUROSH_CONVERGENCE_CRITERION********************* 
              DISP1_NORM=DOTPROD(DVTAU0,DVTAU0,NBC1,NEQ,NDF,NBOUD1,
     &                           MBOUND1)
              DISP_TOL_REF=DISP_TOL**2.D0*DISP1_NORM 
C************ KOUROSH_CONVERGENCE_CRITERION*********************              
              
            ENDIF

C************* KOUROSH_LINESEARCH *******************************            
10          IF (IFLAG_LINE_SEARCH.EQ.1) PNEWDT=0.5D0   
C************* KOUROSH_LINESEARCH *******************************
         ENDDO                                                       !<--------------------
         

!--------------------------------QSINT END-----------------------------------------------------------

20       CONTINUE 
         IF(PNEWDT.LT.1.D0)THEN
            ICUT=ICUT+1
            IF(IPROC.EQ.0) WRITE(*,*)'CUTBACK',ICUT
         ELSE
            ICUT=0
         ENDIF
         
              
C************ KOUROSH_CONVERGENCE_CRITERION*********************  
         IF (ICUT.GT.MAXCUT) THEN
            IF (IPROC.EQ.0) WRITE(*,*) 'MAX CUTBACK ACHIEVED'
            PNEWDT=0.5D0
            IBROYFLAG=1                     
         ENDIF
C************ KOUROSH_CONVERGENCE_CRITERION*********************           
  
         IF (.NOT.FACTORIZE_FREQUENTLY) THEN
            IF(IBROYFLAG.EQ.1.AND.JACFLAG.EQ.1)THEN

               DEALLOCATE(IBINDX_LOC,IROWPTR_LOC)
               CALL F_PSTATFREE(STAT)
               CALL F_DESTROY_SUPERMAT_STORE_DIST(A)
               CALL F_SCALEPERMSTRUCTFREE(SCALEPERMSTRUCT)
               CALL F_DESTROY_LU(N, GRID, LUSTRUCT)
               CALL F_LUSTRUCTFREE(LUSTRUCT)
               CALL GET_SUPERLU_OPTIONS(OPTIONS,SOLVEINITIALIZED=INIT)
               IF (INIT == YES) THEN
                  CALL F_DSOLVEFINALIZE(OPTIONS, SOLVESTRUCT)
               ENDIF

               CALL F_SUPERLU_GRIDEXIT(GRID)

               CALL F_DESTROY_GRIDINFO_HANDLE(GRID)
               CALL F_DESTROY_OPTIONS_HANDLE(OPTIONS)
               CALL F_DESTROY_SCALEPERM_HANDLE(SCALEPERMSTRUCT)
               CALL F_DESTROY_LUSTRUCT_HANDLE(LUSTRUCT)
               CALL F_DESTROY_SOLVESTRUCT_HANDLE(SOLVESTRUCT)
               CALL F_DESTROY_SUPERMATRIX_HANDLE(A)
               CALL F_DESTROY_SUPERLUSTAT_HANDLE(STAT)

            ELSE
            
               NITER=ITEREF+1
         
            ENDIF
            
            
         ELSEIF (FACTORIZE_FREQUENTLY.AND.FACTORIZED.AND.
     &           BROY_BFGS.NE.0) THEN
         
            DEALLOCATE(IBINDX_LOC,IROWPTR_LOC)
            CALL F_DESTROY_SUPERMAT_STORE_DIST(A)
            CALL F_DESTROY_SUPERMATRIX_HANDLE(A)
            NITER=ITEREF+1         
         ENDIF

         

         IF(ICUT.NE.0)THEN

            FINTME=FINTME-DELTME            
            DELTME=PNEWDT*DELTME

            IF(DELTME.LT.XMINDT)THEN
               IF(IPROC.EQ.0)THEN
                  WRITE(131,*)'TOO SMALL INCREMENT REQUIRED',DELTME
               ENDIF
               STOP
            ENDIF

            IF(IPROC.EQ.0)THEN
               WRITE(131,*)'USER ROUTINE REQUESTS SMALLER INCREMENT'
            ENDIF
            
         ENDIF
         
         

         IF(IBROYFLAG.EQ.1)THEN
            JACFLAG=0
C************* KOUROSH_LINESEARCH *******************************
            ICUT=0
C************* KOUROSH_LINESEARCH *******************************            
         ENDIF         

         
         IF(ICUT.EQ.0.AND.IBROYFLAG.EQ.0)THEN
            
            TT2=MPI_WTIME()
            IF (IPROC.EQ.0) WRITE(*,*) 'THIS TIME STEP TOOK',(TT2-TT1)

            DELTME_PREV=DELTME
            nflush1=NSTEP-NSTEP/nflush*nflush
            IF(IPROC.EQ.0)THEN
             WRITE(131,*)'STEP=',NSTEP,'TIME=',FINTME
             WRITE(131,*)'FRAC COMPL=',FINTME/TOTME,'TIME INC=',DELTME
             WRITE(131,*)'NITER=',NITER
             IF (NFLUSH1.EQ.0) CALL FLUSH(131)
            ENDIF
			
			
C************  KOUROSH_INT  ************			
            DELTME0=DELTME
               
            U_REPOSITORY(1:NEQ,1)=U_REPOSITORY(1:NEQ,2)
            U_REPOSITORY(1:NEQ,2)=U_REPOSITORY(1:NEQ,3)
            U_REPOSITORY(1:NEQ,3)=VTAU(1:NEQ)


            TIME_REPOSITORY(1)=TIME_REPOSITORY(2)
            TIME_REPOSITORY(2)=TIME_REPOSITORY(3)
            TIME_REPOSITORY(3)=FINTME

C************  KOUROSH_INT  ************      

            DO III=1,NEQ
               GPXYZ(III)=G0XYZ(III)+VTAU(III)
               VT(III)=VTAU(III)
               VTAU0(III)=VTAU(III)
            ENDDO

!            DO II=1,NSVARS1*NELNOX*NGAUSS
            DO II=1,NSVARS1*NElementsInProcessor*NGAUSS
               SVARS1(II)=SVARS2(II)
            ENDDO

C***************** KOUROSH_TIME CONTROL            
            IF (DABS((NCYC+1)*TPERIOD-TWRITE).LT.1D-10) NCYC=NCYC+1 
C***************** KOUROSH_TIME CONTROL


C****************** KOUROSH_LOADING_UNLOADING *************************
            N_TPOINTS_AFTER_SHARP=N_TPOINTS_AFTER_SHARP+1
C****************** KOUROSH_LOADING_UNLOADING ************************* 
            PNEWDT1=(1.D0*NITER_REF/(1.D0*NITER))**0.5D0

            IF (PNEWDT1.GT.PNEWDT) PNEWDT=PNEWDT1
            IF (PNEWDT.GT.1.5D0) PNEWDT=1.5D0
            IF(IPROC.EQ.0)  WRITE(*,*)'PNEWDT',PNEWDT

            DELTME=PNEWDT*DELTME
            
C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************
            IF (FINTME.GT.TRAMP) THEN
              NITER_STORE(I_ADDAP_FACT)=NITER
              IF (.NOT.ADDAPTIVE_FACTOR) ADDAPTIVE_FACTOR=.TRUE.
              IF (I_ADDAP_FACT.EQ.1) THEN
                     I_ADDAP_FACT=2
              ELSEIF (I_ADDAP_FACT.EQ.2) THEN
                     I_ADDAP_FACT=3
              ELSEIF (I_ADDAP_FACT.EQ.3) THEN
                     I_ADDAP_FACT=4
              ELSEIF (I_ADDAP_FACT.EQ.4) THEN
                     I_ADDAP_FACT=1
              ENDIF              
            ENDIF
C************ KOUROSH-ADDAPTIVE-FACTORIZATION ****************  

            IF(IPROC.EQ.0)THEN
               NPOST1=NSTEP-NSTEP/NPOST*NPOST
               nflush1=NSTEP-NSTEP/nflush*nflush
               IF (NPOST1.EQ.0) THEN
                  IF(ICYC.NE.1)THEN
                     WRITE(132,*)FINTME
                  ENDIF
               ENDIF               

             ENDIF


		  
	     CALL POST_PROC(NX,NELX,NPROCS,IJK,G0XYZ,MAXCRD,
     &       VTAU,MDOFX,SVARS1,MAXSV,NELST,NELEND,NSVARS1,MNELX,
     &       AVE_STRESS,AVE_STRAIN,VOL_TOTAL,NPATCH,MaxElemInPatch,
     &       NElementsInPatch,ElementsInPatch,FLOW_RULE,AVE_PSTRAIN,
     &       AVE_EFFECTIVEPSTRAIN)
     
     
     	
            
         II=6
         CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
         CALL MPI_ALLREDUCE(AVE_STRESS,AVE_STRESS_RECV,II,
     &     MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)
     

         CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)            
         CALL MPI_ALLREDUCE(AVE_STRAIN,AVE_STRAIN_RECV,II,
     &     MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)

         CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)            
         CALL MPI_ALLREDUCE(AVE_PSTRAIN,AVE_PSTRAIN_RECV,II,
     &     MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)  
     
         JJ=1
         CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
         CALL MPI_ALLREDUCE(VOL_TOTAL,VOL_TOTAL_RECV,JJ,
     &     MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)
      

         CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
         CALL MPI_ALLREDUCE(AVE_EFFECTIVEPSTRAIN,
     &     AVE_EFFECTIVEPSTRAIN_RECV,JJ,
     &     MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)
     
     
         AVE_STRESS_RECV(:)=AVE_STRESS_RECV(:)/VOL_TOTAL_RECV(1)
         AVE_STRAIN_RECV(:)=AVE_STRAIN_RECV(:)/VOL_TOTAL_RECV(1)	
         AVE_PSTRAIN_RECV(:)=AVE_PSTRAIN_RECV(:)/VOL_TOTAL_RECV(1)
         AVE_EFFECTIVEPSTRAIN_RECV(1)=AVE_EFFECTIVEPSTRAIN_RECV(1)/
     &                                VOL_TOTAL_RECV(1)

     
         IF (IPROC.EQ.0) THEN
           DO II=1,6
                  RESULTOUT((II-1)*3+1)=AVE_STRAIN_RECV(II)
                  RESULTOUT((II-1)*3+2)=AVE_PSTRAIN_RECV(II)
                  RESULTOUT((II-1)*3+3)=AVE_STRESS_RECV(II)
           ENDDO
           WRITE(505,'(20ES18.8)') FINTME,(RESULTOUT(II),II=1,18),
     &               AVE_EFFECTIVEPSTRAIN_RECV(1)

           NFLUSH1=NSTEP-NSTEP/NFLUSH*NFLUSH
           IF (NFLUSH1.EQ.0) CALL FLUSH (505)

         ENDIF




         IF (IRESTART_WRITE.EQ.1) THEN
            IF(ICYC.EQ.1) THEN
               NPOST1=1
               IF(DABS(NCYC*TPERIOD-TWRITE).LT.1D-10.AND.
     &    (NCYC-NCYC/N_RESTART_WRITE*N_RESTART_WRITE).EQ.0) NPOST1=0
            ELSE
               NPOST1=NSTEP-NSTEP/N_RESTART_WRITE*N_RESTART_WRITE
            ENDIF
            IF (NPOST1.EQ.0) THEN
               FILNAME='RESTART'//FNAME
               OPEN(335,FILE=FILNAME,STATUS='REPLACE')
!				DO II=1,NELNOX   !F-BAR METHOD 
!					DO JJ=1,NGAUSS    !F-BAR METHOD 

               KOUNT=0
               DO II=NELST,NELEND
                  DO JJ=1,NElementsInPatch(II)
                     KOUNT=KOUNT+1
                     
                     DO KK=1,MAXSTATEV
                      ISHIFT=(KOUNT-1)*NSVARS1+KK
                      WRITE(335,*) SVARS1(ISHIFT)
                     ENDDO
                  ENDDO
               ENDDO
               WRITE(335,*) TIME1(1),TIME1(2),KINC,NSTEP,NCYC
               CLOSE(335)
               
               IF (IPROC.EQ.0) THEN
                  OPEN(338, FILE='LASTDISP.INP',STATUS='REPLACE')
                  KK=0
                  DO II=1,NX
                     DO JJ=1,3
                        KK=KK+1
                        WRITE(338,*) VTAU(KK)
                     ENDDO
                  ENDDO
                 CLOSE(338)
                                      
               ENDIF	
            ENDIF
         ENDIF


         GND_CYCLIC=.TRUE.	
         IF (ICYC.EQ.1.AND.GND_END_OF_CYCLE) THEN
           GND_CYCLIC=(DABS((NCYC)*TPERIOD+TIME_PRINT-TWRITE).LT.1D-10)
         ENDIF
  

      IF (GND_CAL) THEN  
         IF ((ICYC.NE.1).OR.(ICYC.EQ.1.AND.GND_CYCLIC)) THEN 

            TT1=MPI_WTIME()	
            NOGND=9
            NGND_SIZE=NELX*NOGND
            ALLOCATE(GND_TOT(NGND_SIZE))           ! WE DON'T NEED ENTIRE STATE VARIABLES, WE JUST TAKE OUT THE GND RELATED PART (FP)
            ALLOCATE(GND_TOT_RECV(NGND_SIZE))
            ALLOCATE(GND_FOREST(30,NELEMENTSINPROCESSOR))
            ALLOCATE(GND_PARALLEL(30,NELEMENTSINPROCESSOR))
            ALLOCATE(EULER(NELEMENTSINPROCESSOR,3))

            GND_TOT=0.D0
            GND_TOT_RECV=0.D0
            GND_FOREST=0.D0
            GND_PARALLEL=0.D0
            FPEL=0.D0
            EULER=0.D0
            
 	 
            KOUNT=0
            DO IPATCH=NELST,NELEND
            DO JJ=1,NElementsInPatch(IPATCH)
               KOUNT=KOUNT+1
               IF (FLOW_RULE.EQ.1) ISTV_START2=(KOUNT-1)*MAXSTATEV+66
               IF (FLOW_RULE.EQ.2) ISTV_START2=(KOUNT-1)*MAXSTATEV+101
               NEL=ElementsInPatch(IPATCH,JJ)
               
               DO II=1,NOGND                      ! 9 COMPONENTS OF FP
                    GND_TOT((NEL-1)*NOGND+II)=SVARS1(ISTV_START2+II)
               ENDDO
		     
               IF (FLOW_RULE.EQ.1) ISTV_START2=(KOUNT-1)*MAXSTATEV+300
               IF (FLOW_RULE.EQ.2) ISTV_START2=(KOUNT-1)*MAXSTATEV+1
               EULER(KOUNT,1)=SVARS1(ISTV_START2+1)
               EULER(KOUNT,2)=SVARS1(ISTV_START2+2)
               EULER(KOUNT,3)=SVARS1(ISTV_START2+3)
            ENDDO
            ENDDO
 

	
            CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR) 
            CALL MPI_ALLREDUCE(GND_TOT,GND_TOT_RECV,NGND_SIZE,
     &       MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)		 
           
			
               
            DO II=1,NELX
               ISHIFT=(II-1)*NOGND
               DO IDIME=1,9
                  FPEL(IDIME,II)=GND_TOT_RECV(ISHIFT+IDIME)
               ENDDO
            ENDDO

            DEALLOCATE(GND_TOT)
            DEALLOCATE(GND_TOT_RECV)


            FP_NODAL=0.D0

            
            if(recoveryMethod==1)then ! nodal averaging

               CALL nodalAveraging(FP_NODAL,
     &                  FPEL,9,DEBUG_nodalValues)
                                   
            elseif(recoveryMethod==2)then ! SPR

               CALL recoverySPR(FP_NODAL,FPEL,
     &                  9,p_deg,regularizeBoundaries, 
     &                  localNonlocal,nFailedPatches,
     &                  info,DEBUG_nodalValues)

            elseif (recoveryMethod==3) then  ! LSQ
            
               nGrains = size (nPatchesInGrain)
               CALL PARTITION_CALCULATOR(staGrainIdx,endGrainIdx,
     &                  nGrainIdx,nGrains,NPROCS,IPROC) 
            
            
               CALL recoveryLSQ(FP_NODAL,FPEL,9,info, 
     &                  DEBUG_nodalValues,staGrainIdx,endGrainIdx)
                                

               CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)      
               call MPI_ALLREDUCE(MPI_IN_PLACE,FP_NODAL,
     &                  9*nPatches,MPI_DOUBLE_PRECISION,
     &                  MPI_SUM,MPI_COMM_WORLD,IERROR)                                  

            endif            
            
            FpNodal = 0.d0
            do iPatchID = 1,nPatches
               iNode = nodeIDFromPatchID(iPatchID)
               grainID = patchGrainID(iPatchID)
               do grainIdx=1,nnodgr(iNode)
                  if (inodgr(iNode,grainIdx)==grainID) then
                     LOCATION = (iNode-1)*MAXADJGRAIN*9+(grainIdx-1)*9
                     FpNodal(LOCATION+1:LOCATION+9)= 
     &                  FP_NODAL(1:9,iPatchID)
                     exit
                  endif
               enddo
            enddo
            
            
      IF (ICYC.NE.1) THEN
         NPOST1=NSTEP-NSTEP/NPOST*NPOST
      else 
         npost1=mod(ncyc+1,npost)
      ENDIF
            
            
            CALL CURLFP(FpNodal,NELST,NELEND,NELNOX,IJK,IPERM,IELGR,
     &     CARTD,EULER,GND_FOREST,GND_PARALLEL,NPOST1,MAXEL,MNELX,
     &     MAXNODE,maxadjgrain,NELEMENTSINPROCESSOR,NElementsInPatch,
     &     ElementsInPatch,NPATCH,MaxElemInPatch,NNODGR,INODGR)

  
            DEALLOCATE(EULER)


                        
            KOUNT=0
            DO IPATCH=NELST,NELEND
            DO IEL=1,NElementsInPatch(IPATCH)
               KOUNT=KOUNT+1
               IF (FLOW_RULE.EQ.1) ISTV_START2=(KOUNT-1)*maxstatev+351
               IF (FLOW_RULE.EQ.2) ISTV_START2=(KOUNT-1)*maxstatev+516
               IF (FLOW_RULE.EQ.1) ISTV_START3=(KOUNT-1)*maxstatev+381
               IF (FLOW_RULE.EQ.2) ISTV_START3=(KOUNT-1)*maxstatev+546
               
               DO II=1,30
                 SVARS1(ISTV_START2+II)=GND_FOREST(II,KOUNT)
                 SVARS1(ISTV_START3+II)=GND_PARALLEL(II,KOUNT)
               ENDDO
               
            ENDDO
            ENDDO

		

            DEALLOCATE(GND_FOREST)
            DEALLOCATE(GND_PARALLEL)
            
            TT2=MPI_WTIME()
            IF (IPROC.EQ.0) WRITE(*,*) 'GND TOOK:',(TT2-TT1)              
         ENDIF
      ENDIF            
            

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)


      
      IF (FLOW_RULE.EQ.2.AND.EXTENSIVE_PRINT) THEN
         NFLUSH1=NSTEP-NSTEP/nflush*nflush
         kount=0
         DO II=NELST,NELEND
            DO JJ=1,NElementsInPatch(II)
               kount=kount+1
               ISTV_START3=(kount-1)*NSVARS1


              WRITE(1001,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+456:
     & ISTV_START3+485)
              WRITE(1002,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+396:
     & ISTV_START3+425)
              WRITE(1003,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+426:
     & ISTV_START3+455)
!              WRITE(1004,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+186:
!     & ISTV_START3+215)
              WRITE(1005,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+6:
     & ISTV_START3+35)
              WRITE(1006,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+66:
     & ISTV_START3+95)
!              WRITE(1007,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+156:
!     & ISTV_START3+185)
!              WRITE(1008,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+487:
!     & ISTV_START3+516)
!              WRITE(1009,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+276:
!     & ISTV_START3+305)
!              WRITE(1010,'(31ES18.8)') FINTME,SVARS1(ISTV_START3+113:
!     & ISTV_START3+142)
              WRITE(1011,'(3ES18.8)') FINTME,SVARS1(ISTV_START3+111:
     & ISTV_START3+112)

            ENDDO
         ENDDO   

         IF (NFLUSH1.EQ.0) THEN
            CALL FLUSH(1001)
            CALL FLUSH(1002)
            CALL FLUSH(1003)
            CALL FLUSH(1005)
            CALL FLUSH(1006)
            CALL FLUSH(1011)
         ENDIF
      
      ENDIF
      
      


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

            
      IF (ICYC.NE.1) THEN
         NPOST1=NSTEP-NSTEP/NPOST*NPOST
      else 
         npost1=mod(ncyc+1,npost)
      ENDIF
         
      IF (NPOST1.EQ.0) then  
         
      IF(DABS((NCYC)*TPERIOD+TIME_PRINT-TWRITE).LT.1.D-10.AND.
     &         ICYC.EQ.1)THEN

         
         NFLUSH1=NSTEP-NSTEP/nflush*nflush
         IF (IPROC.EQ.0) WRITE(132,*)FINTME-TRAMP

         kount=0
         DO II=NELST,NELEND
            DO JJ=1,NElementsInPatch(II)
               kount=kount+1
               IF (FLOW_RULE.EQ.1) ISTV_START3=(kount-1)*NSVARS1+210
               IF (FLOW_RULE.EQ.2) ISTV_START3=(kount-1)*NSVARS1+95
               IF (FLOW_RULE.EQ.1) ISTV_START2=(kount-1)*NSVARS1+66
               IF (FLOW_RULE.EQ.2) ISTV_START2=(kount-1)*NSVARS1+101
 
               DO KK=1,6
                  WRITE(508,*)SVARS1(ISTV_START3+KK)
               ENDDO
                        			   
               DO KK=1,9
               WRITE(506,*)SVARS1(ISTV_START2+KK)
               ENDDO
                        
            ENDDO
         ENDDO

         
         IF (IPROC.EQ.0) THEN
            DO II=1,NX
               IS=(II-1)*3
               WRITE(100,*)VTAU(IS+1),VTAU(IS+2),VTAU(IS+3)
            ENDDO
         ENDIF
		    
         IF (NFLUSH1.EQ.0) then
            CALL FLUSH(132)
            CALL FLUSH(508)
            CALL FLUSH(506)
            CALL FLUSH(100)
         endif
		


c               NCYC=NCYC+1  !***************** KOUROSH_TIME CONTROL

         IF (IPROC.EQ.0) WRITE(*,*)'#####WRITE OUTPUT#####'
		
		
      ELSEif (icyc.ne.1) then
         
    
 
         nflush1=NSTEP-NSTEP/nflush*nflush

 
         kount=0
         DO II=NELST,NELEND
            DO JJ=1,NElementsInPatch(II)
               kount=kount+1
               IF (FLOW_RULE.EQ.1) ISTV_START3=(kount-1)*NSVARS1+210
               IF (FLOW_RULE.EQ.2) ISTV_START3=(kount-1)*NSVARS1+95
               IF (FLOW_RULE.EQ.1) ISTV_START2=(kount-1)*NSVARS1+66
               IF (FLOW_RULE.EQ.2) ISTV_START2=(kount-1)*NSVARS1+101
 
               DO KK=1,6
                  WRITE(508,*)SVARS1(ISTV_START3+KK)
               ENDDO
                        			   
               DO KK=1,9
               WRITE(506,*)SVARS1(ISTV_START2+KK)
               ENDDO
                        
            ENDDO
         ENDDO

         
         IF (IPROC.EQ.0) THEN
            DO II=1,NX
               IS=(II-1)*3
               WRITE(100,*)VTAU(IS+1),VTAU(IS+2),VTAU(IS+3)
            ENDDO
         ENDIF
		    
         
         IF (NFLUSH1.EQ.0) then
            CALL FLUSH(132)
            CALL FLUSH(508)
            CALL FLUSH(506)
            CALL FLUSH(100)
         endif


	                  
      ENDIF                ! IF WRITE_FLAG =1
      endif
            
         
!-------------------------------------------------------------------------------------------            

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)

      NSTEP=NSTEP+1
      
      IF (CONTINUE_DEFORM) NSTEP_AFTER_CONTINUE_DEFORM=
     &                            NSTEP_AFTER_CONTINUE_DEFORM+1  ! KOUROSH_INT
            
      IF(NSTEP.GT.NOSTP)IFLAG_OVER=1
      IF(DABS(FINTME-TOTME).LT.1D-8.OR.FINTME.GT.TOTME) IFLAG_OVER=2
      IF(IPROC.EQ.0) WRITE(*,*)'********************************'
       
      ENDIF
      
      ENDDO                                
       
      
      

      T2=MPI_WTIME()
      
      TELAPSED=(T2-T1)

      DEALLOCATE(VAL,RESID,IBINDX,IROWPTR)
      DEALLOCATE(CARTD)
      

      IF(ICYC.EQ.1)THEN
         DEALLOCATE(TSEG)
      ENDIF


      DEALLOCATE(DVTAU0,DVTAU,DVTAU_RECV)
      IF (BROY_BFGS.NE.0) DEALLOCATE(VAR,WAR)


      IF(JACFLAG.EQ.1)THEN

         DEALLOCATE(IBINDX_LOC,IROWPTR_LOC)
         
                                ! DEALLOCATE THE STORAGE ALLOCATED BY SUPERLU_DIST
         CALL F_PSTATFREE(STAT)
         CALL F_DESTROY_SUPERMAT_STORE_DIST(A)
         CALL F_SCALEPERMSTRUCTFREE(SCALEPERMSTRUCT)
         CALL F_DESTROY_LU(N, GRID, LUSTRUCT)
         CALL F_LUSTRUCTFREE(LUSTRUCT)
         CALL GET_SUPERLU_OPTIONS(OPTIONS, SOLVEINITIALIZED=INIT)
         IF (INIT == YES) THEN
            CALL F_DSOLVEFINALIZE(OPTIONS, SOLVESTRUCT)
         ENDIF
         
         
         
         CALL F_SUPERLU_GRIDEXIT(GRID)
         
!     DEALLOCATE THE C STRUCTURES POINTED TO BY THE FORTRAN HANDLES
         CALL F_DESTROY_GRIDINFO_HANDLE(GRID)
         CALL F_DESTROY_OPTIONS_HANDLE(OPTIONS)
         CALL F_DESTROY_SCALEPERM_HANDLE(SCALEPERMSTRUCT)
         CALL F_DESTROY_LUSTRUCT_HANDLE(LUSTRUCT)
         CALL F_DESTROY_SOLVESTRUCT_HANDLE(SOLVESTRUCT)
         CALL F_DESTROY_SUPERMATRIX_HANDLE(A)
         CALL F_DESTROY_SUPERLUSTAT_HANDLE(STAT)
         
         
      ENDIF

      IF(IPROC.EQ.0)THEN
         WRITE(131,*)'TIME OF OPERATION=',TELAPSED,'SECS'
         WRITE(131,*)'PROGRAM ENDS'
         CLOSE(131)
         CLOSE(100)         
      ENDIF

      IF(ICYC.EQ.1)THEN

         CLOSE(500)
         CLOSE(501)
         CLOSE(502)
         CLOSE(503)
         CLOSE(504)
         CLOSE(505)
         CLOSE(506)
         CLOSE(507)
         CLOSE(508)

      ELSE
         CLOSE(504)
         CLOSE(505)
         CLOSE(506)
         close(507)
         CLOSE(508)
         close(4444)
      ENDIF

      

      IF(IPROC.EQ.0)THEN
         CLOSE(132)
      ENDIF

      CALL MPI_FINALIZE(IERROR)

      END  

*********************************************************

      SUBROUTINE READGM(NDF,GXYZ,IMID,IJK,MDIM)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION GXYZ(MAXCRD),IMID(3*MAXEL)
      DIMENSION IJK(MNELX)    
      DIMENSION IJKNEL(MAXELN),XC(3)
      INTEGER::MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      INTEGER::ISENDORRECV(MAXPROC)
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV

      CALL READSTR(LR)
      READ(LR,*)MDIM,NDFADD
      NDF=MDIM+NDFADD     
      IF(NDF.GT.NDOFELX)THEN
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'INSUFFICIENT MEM-NDOFELX'
         ENDIF
         STOP
      ENDIF
          
      CALL READSTR(LR)
      READ(LR,*) NX
      NEQ=NDF*NX
      IF(NDF.GT.MDOFX)THEN
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'INSUFFICIENT MEMORY-MDOFX'
         ENDIF
         STOP
      ENDIF

      IF(NX*MDIM.GT.MAXCRD)THEN
         WRITE(131,*)"INSUFFICIENT MEM -MAXCRD"
         STOP
      ENDIF

      IF(MDIM.GT.MAXDIM)THEN
         WRITE(131,*)'INSUFFICIENT MEM-MAXDIM'
         STOP
      ENDIF
      
      DO I=1,NX
         READ(LR,*) NNUM,(XC(II),II=1,MDIM)
         DO II=1,MDIM
            GXYZ(MDIM*(NNUM-1)+II)=XC(II)
         ENDDO
      ENDDO

C     ( READ ELEMENT CONNECTIVITY )

      CALL READSTR(LR)

      IF(IBRICKORTET.EQ.1)THEN

         NNODE=8
         NODE=8
         NGAUSS=8

      ELSE
         
         NGAUSS=1
         NNODE=4
         NODE=4

      ENDIF


      MPE=1
      IEP=1
      READ(LR,*) NELX
      IF(NELX.GT.MAXEL)THEN
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'INSUFFICIENT MEMORY -MNEL'
         ENDIF
         STOP
      ENDIF
      JJ=0
      DO N=1,NELX
         READ(LR,*) NEL,(IJKNEL(J),J=1,NNODE)
         IMID(NEL)=NNODE
         IMID(NELX+NEL)=MPE
         IMID(2*NELX+NEL)=IEP
         DO J=1,NNODE
            JJ=JJ+1
            IJK(JJ)=IJKNEL(J)
         ENDDO
      ENDDO
      RETURN
      END

************************************************************      

      SUBROUTINE READMT(NGROUP,PROMAT2D,NPROPSG,NSTATE)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION PROMAT2D(MAXPROPS,MAXGRP)
      DIMENSION NPROPSG(MAXGRP)
      INTEGER::MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      INTEGER::ISENDORRECV(MAXPROC)
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV

      CALL READSTR(LR)
      READ(LR,*)NGROUP
      IF(NGROUP.GT.MAXGRP)THEN
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'INSUFFICIENT MEM -MAXGRP'
         ENDIF
         STOP
      ENDIF
      
      READ(LR,*)(NPROPSG(N),N=1,NGROUP)
      DO I=1,NGROUP
         IF(NPROPSG(I).GT.MAXPROPS)THEN
            IF(IPROC.EQ.0)THEN
               WRITE(131,*)'INSUFFICIENT MEM - MAXPROPS'
            ENDIF
            STOP
         ENDIF
      ENDDO
      
      DO N=1,NGROUP
         MATGRP=NPROPSG(N)
         NCOUNT=MATGRP/8
         NREMAIN=MOD(MATGRP,8)
         NSHIFT=0
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'NUM OF PROPS: ',MATGRP
         ENDIF
         DO J=1,NCOUNT
            READ(LR,*)(PROMAT2D(NSHIFT+I,N),I=1,8)
            NSHIFT=NSHIFT+8
         ENDDO
         IF (NREMAIN.NE.0) READ(LR,*)(PROMAT2D(NSHIFT+I,N),I=1,NREMAIN) 
      ENDDO
      READ(LR,*)NSTATE
      IF(NSTATE.GT.MSV)THEN
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'INSUFFICIENT MEMORY -MSV'
         ENDIF
         STOP
      ENDIF
      RETURN
      END

*******************************************************************

      SUBROUTINE DISINC(V,VBCS1,NBC1,DELT,IDF,KINC,KSTEP,
     &             G0XYZ,TIME,NDF,MCRD)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION V(MDOFX),VBCS1(MBOUND1),NBC1(MBOUND1),U(3),TIME(2)
      DIMENSION COORDS(3),IDF(MBOUND1),G0XYZ(MAXCRD)
      INTEGER::MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      INTEGER::ISENDORRECV(MAXPROC)
      COMMON/BOUNDA/NBOUD1,NBOUD2,NBOUD3,NUPNCH
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV

      NDF1=NDF+1
      DO N=1,NBOUD1
         NODE1=NBC1(NDF1*(N-1)+1) 
         DO  I=1,NDF
            INDEX=NBC1(NDF1*(N-1)+1+I)
            IF(INDEX.EQ.1) THEN
               NN=NDF*(NODE1-1)+I
               IF(IDF(N).EQ.0)THEN
                  N1=NDF*(N-1)+I
                  V(NN)=V(NN)+VBCS1(N1)*DELT
               ENDIF
               IF(IDF(N).EQ.1)THEN
                  ICN=MCRD*(NODE1-1)
                  DO II=1,MCRD
                     COORDS(II)=G0XYZ(ICN+II)
                  ENDDO
                  CALL DISP(U,TIME)
                  V(NN)=U(1)
               ENDIF
            ENDIF
         ENDDO
      ENDDO
      RETURN
      END

*************************************************

      SUBROUTINE READSTR(LR)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION FLAG(20)
      READ(LR,505) (FLAG(I),I=1,20)
505   FORMAT(20A4)
      RETURN
      END
*************************************************

      SUBROUTINE INITABQ(NSTATE,MDIM,KINC,NSTEP,NCYC,CONTINUE_DEFORM,
     &        MAXSV,SVARS1,SVARS2,EUL,IPHASE,GRAIN_DIAMETER,FLOW_RULE,
     &        NPATCH,MaxElemInPatch,NElementsInPatch,ElementsInPatch)
     
      IMPLICIT REAL*8 (A-H,O-Z)
      INCLUDE 'PARDIS.H'
      INTEGER:: FLOW_RULE,NPATCH,MaxElemInPatch,STATEV_OFFSET
      DIMENSION TIME1(2),SVARS1(MAXSV),SVARS2(MAXSV),Hard(47)
      INTEGER:: IPHASE(MAXEL),NElementsInPatch(NPATCH),
     &          ElementsInPatch(NPATCH,MaxElemInPatch)
      CHARACTER*30 FNAME
      CHARACTER*50 FILNAME
      COMMON/ABQ/TIME1,NSVARS1,NDOFEL1,MDLOAD1,NPREDF1,MCRD1     ! F-BAR METHOD
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
      COMMON/DOF/NDOF
      COMMON/EL_PAR/NELST,NELEND,NELNOX
!      COMMON/STATEUPDATE/SVARS2    ! F-BAR METHOD
      DIMENSION  MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      DIMENSION ISENDORRECV(MAXPROC),EUL(3,MAXEL),GRAIN_DIAMETER(MAXEL)
      LOGICAL::CONTINUE_DEFORM
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV
      
      
      MCRD1=MDIM
      NDOF=NEQ/NX
      NUMN=4
      IF(MCRD1.EQ.2)NUMN=4
      NDOFEL1=NDOF*NUMN
      MDLOAD1=1
      NPREDF1=1
      TIME1(1)=0.D0
      TIME1(2)=0.D0
      NSVARS1=NSTATE
                           
!  NEW FEATURE---CONTINUE FROM LAST SIMULATION      
      IF (CONTINUE_DEFORM) THEN

         CALL GET_SUBSCRIPT(IPROC,FNAME)
         FILNAME='RESTART'//FNAME


         OPEN(UNIT=334,FILE=FILNAME)
      
!	DO I=1,NELNOX*MSV    ! F-BAR METHOD
         DO I=1,MAXSV
            READ(334,*) SVARS1(I)     
            SVARS2(I)=SVARS1(I)
         ENDDO
 
! F-BAR METHOD 
!	DO I=NELNOX*MSV+1, MAXSV
!          SVARS1(I)=0.D0
!          SVARS2(I)=0.D0
!	ENDDO
! F-BAR METHOD
      
         READ(334,*) TIME1(1), TIME1(2), KINC, NSTEP, NCYC
            
         CLOSE(334)
            
         KINC=KINC+1
         NSTEP=NSTEP+1

      
      ELSE

         KOUNT=0
         DO IPATCH=NELST,NELEND
            DO IEL=1,NElementsInPatch(IPATCH)
               KOUNT=KOUNT+1
               IELEM=ElementsInPatch(IPATCH,IEL)
               STATEV_OFFSET=(KOUNT-1)*NSVARS1
               
               IF (FLOW_RULE.EQ.1) THEN
               
                  CALL HALLPETCH(IELEM,Hard,GRAIN_DIAMETER,MAXEL)
               
                  SVARS1(STATEV_OFFSET+301)=EUL(1,IELEM)
                  SVARS1(STATEV_OFFSET+302)=EUL(2,IELEM)
                  SVARS1(STATEV_OFFSET+303)=EUL(3,IELEM)
                  SVARS1(STATEV_OFFSET+304)=IPHASE(IELEM)  
                  DO II=1,47
                     SVARS1(STATEV_OFFSET+304+II)=HARD(II)
                  ENDDO	
                  
               ELSEIF (FLOW_RULE.EQ.2) THEN
                  
                  SVARS1(STATEV_OFFSET+1)=IELEM
                  SVARS1(STATEV_OFFSET+2)=EUL(1,IELEM)
                  SVARS1(STATEV_OFFSET+3)=EUL(2,IELEM)
                  SVARS1(STATEV_OFFSET+4)=EUL(3,IELEM)
                  SVARS1(STATEV_OFFSET+5)=IPHASE(IELEM)
                  SVARS1(STATEV_OFFSET+486)=GRAIN_DIAMETER(IELEM)                                     
               ENDIF

               
            ENDDO
         ENDDO
      
         SVARS2=0.D0
         NSTEP=1
         KINC=1
         NCYC=0
      ENDIF

      RETURN
      END
      
      
******************************************************

      SUBROUTINE TMSTEP(TOTME,XIDT,XMXDT,XMINDT,NOSTP)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION FLAG(20)
      READ(LR,505) (FLAG(I),I=1,20)
      READ(LR,*) TOTME,XIDT,XMXDT,XMINDT,NOSTP
505   FORMAT(20A4)     
      RETURN
      END

*****************************************************************


      SUBROUTINE READBC(NBC1,VBC1,IDF,NBC2,VBC2,NBC3,VBC3,NDF,IBC,
     &                  IBCT)
      IMPLICIT NONE
      
      INCLUDE 'PARDIS.H'
      
      REAL(8):: VBC1(MBOUND1),VBC2(MBOUND2)
      REAL(8):: VBC3(MBOUND3)
      REAL(8):: DK(MAXDIM),PRESSB(MAXEL)
      INTEGER:: NBC3(MBOUND3),IK(MAXDIM)
      INTEGER:: NBC1(MBOUND1),NBC2(MBOUND2)
      INTEGER:: NNSET(MAXNSET),NLIST(MAXNS,MAXNSET)
      INTEGER:: IDF(MBOUND1), IBC(MDOFX),IBCT(MDOFX)
      INTEGER:: IPRESS_SET(MAXPRESS),IPRESS_SF(MAXPRESS)
      INTEGER:: ILF(MAXEL),IFACE_TET(12),IFACE_BRICK(24)
      INTEGER:: IBELEM(MAXEL)
      INTEGER:: NELSET(MAXELSET),NELLIST(MAXELS,MAXELSET)
      INTEGER:: MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      INTEGER:: ISENDORRECV(MAXPROC)
      INTEGER:: NDF,I,IFUNC,IEND,II
      INTEGER:: IPROC,INDEX,IPF, J
      INTEGER:: ISTART,ISET,MPIINFSIZE,NCONSTR
      INTEGER:: N,NBOU2,NBN,NBOU1
      INTEGER:: NBOUD1,NBOUD2,NBOUD3
      INTEGER:: NCONST, NPLANE, NODE1,NN,NESET
      INTEGER:: NDF1,NEL, NEQ,NELX,NODE
      INTEGER:: NNOD, NODES,NX,NUPNCH,NSET
      INTEGER:: NPROCS,NPRESS
      REAL(8):: PRESS_VAL
      
      COMMON/CBELSET/NESET,NELSET,NELLIST
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
      COMMON/BOUNDA/NBOUD1,NBOUD2,NBOUD3,NUPNCH
      COMMON/CBNSET/NSET,NNSET,NLIST
      COMMON/PRESS/PRESSB,IBELEM,ILF,IFACE_TET,IFACE_BRICK
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV
C
C     ( READ THE 1-ST TYPE BOUNDARY CONDITION
      
      CALL READSTR(LR)

      READ(LR,*) NBOU1
      
      NDF1=NDF+1
      NBOUD1=0
      DO N=1,NBOU1
         READ(LR,*) NBN,ISET,IFUNC,ISTART,IEND
         DO I=1,NDF
            IK(I)=0
            DK(I)=0.D0
         ENDDO
         
         IF(IFUNC.EQ.0)THEN
            READ(LR,*)(DK(I),I=ISTART,IEND)
         ENDIF
         DO I=ISTART,IEND
            IK(I)=1
         ENDDO
         NNOD=1
         IF(ISET.EQ.1)NNOD=NNSET(NBN)
        
         DO I=1,NNOD
            NBOUD1=NBOUD1+1
            IF(NBOUD1.GT.MBOUND1)THEN
               IF(IPROC.EQ.0)THEN
                  WRITE(131,*)'INSUFFICIENT MEMORY-MBOUND1'
               ENDIF
               STOP
            ENDIF
            NODE1=NBN
            IF(ISET.EQ.1)NODE1=NLIST(I,NBN)
            IDF(NBOUD1)=IFUNC
            DO II=1,NDF1        
               IF(II.EQ.1)THEN
                  NBC1(NDF1*(NBOUD1-1)+II)=NODE1
               ELSE
                  IF(NDF1*(NBOUD1-1)+II.GT.MBOUND1)THEN
                     IF(IPROC.EQ.0)THEN
                        WRITE(131,*)'INSUFFICIENT MEMEORY-MBOUND1'
                     ENDIF
                     STOP
                  ENDIF
                  NBC1(NDF1*(NBOUD1-1)+II)=IK(II-1)
                  IF(IFUNC.EQ.0)VBC1(NDF*(NBOUD1-1)+II-1)=DK(II-1)
               ENDIF
            ENDDO
         ENDDO
      ENDDO
      DO N=1,NEQ
         IBC(N)=0
      ENDDO
      DO N=1,NBOUD1
         NODES=NBC1(NDF1*(N-1)+1) 
         DO I=1,NDF
            INDEX=NBC1(NDF1*(N-1)+1+I)
            IF(INDEX.EQ.1)THEN
               NN=NDF*(NODES-1)+I
               IBC(NN)=1
            ENDIF
         ENDDO         
      ENDDO
      
C
C     ( READ THE 2-ND TYPE BOUNDARY CONDITION : POINT FORCES )
C
      CALL READSTR(LR)      
      READ(LR,*) NBOU2
      NBOUD2=0
      DO N=1,NBOU2
         READ(LR,*) NBN,ISET,IFUNC,ISTART,IEND
         DO I=1,NDF
            IK(I)=0
            DK(I)=0.D0
         ENDDO

         IF(IFUNC.EQ.0)THEN
            READ(LR,*)(DK(I),I=ISTART,IEND)
         ENDIF
         DO I=ISTART,IEND
            IK(I)=1
         ENDDO
         NNOD=1
         IF(ISET.EQ.1)NNOD=NNSET(NBN)
        
         DO I=1,NNOD
            NBOUD2=NBOUD2+1
            IF(NBOUD2.GT.MBOUND2)THEN
               IF(IPROC.EQ.0)THEN
                  WRITE(131,*)'INSUFFICIENT MEMORY-MBOUND2'
               ENDIF
               STOP
            ENDIF
            NODE=NBN
            IF(ISET.EQ.1) THEN
            NODE1=NLIST(I,NBN)
            ENDIF
            IDF(NBOUD1)=IFUNC
            DO II=1,NDF1        
               IF(II.EQ.1)THEN
                   NBC2(NDF1*(NBOUD1-1)+II)=0 
               ELSE
                  IF(NDF1*(NBOUD2-1)+II.GT.MBOUND2)THEN
                     IF(IPROC.EQ.0)THEN
                        WRITE(131,*)'INSUFFICIENT MEMEORY-MBOUND1'
                     ENDIF
                     STOP
                  ENDIF
                  NBC2(NDF1*(NBOUD2-1)+II)=0
                  IF(IFUNC.EQ.0)VBC2(NDF*(NBOUD2-1)+II-1)=0.0D0
               ENDIF
            ENDDO
         ENDDO
      ENDDO
      DO N=1,NEQ
         IBCT(N)=0
      ENDDO
      DO N=1,NBOUD2
         NODES=NBC2(NDF1*(N-1)+1) 
         DO I=1,NDF
            INDEX=NBC2(NDF1*(N-1)+1+I)
            IF(INDEX.EQ.1)THEN
               NN=NDF*(NODES-1)+I
               IBCT(NN)=1
            ENDIF
         ENDDO         
      ENDDO
C     
C      READ THE THIRD TYPE BOUNDARY CONDITION (PRESSURE)
C
      CALL READSTR(LR)
      READ(LR,*) NBOUD3

!-----------------------------------
      IFACE_TET(1)=1
      IFACE_TET(2)=2
      IFACE_TET(3)=3

      IFACE_TET(4)=1
      IFACE_TET(5)=2
      IFACE_TET(6)=4

      IFACE_TET(7)=2
      IFACE_TET(8)=3
      IFACE_TET(9)=4
     
      IFACE_TET(10)=1
      IFACE_TET(11)=3
      IFACE_TET(12)=4

!------------------------------------
      
      IFACE_BRICK(1)=1
      IFACE_BRICK(2)=2
      IFACE_BRICK(3)=3
      IFACE_BRICK(4)=4

      IFACE_BRICK(5)=5
      IFACE_BRICK(6)=6
      IFACE_BRICK(7)=7
      IFACE_BRICK(8)=8

      IFACE_BRICK(9)=2
      IFACE_BRICK(10)=3
      IFACE_BRICK(11)=7
      IFACE_BRICK(12)=6

      IFACE_BRICK(13)=1
      IFACE_BRICK(14)=4
      IFACE_BRICK(15)=8
      IFACE_BRICK(16)=5

      IFACE_BRICK(17)=4
      IFACE_BRICK(18)=3
      IFACE_BRICK(19)=7
      IFACE_BRICK(20)=8

      IFACE_BRICK(21)=1
      IFACE_BRICK(22)=2
      IFACE_BRICK(23)=6
      IFACE_BRICK(24)=5


!------------------------------------
      DO N=1,NBOUD3
         READ(LR,*)NPRESS,IPF,(IPRESS_SET(I),I=1,NPRESS),
     &        (IPRESS_SF(I),I=1,NPRESS)
         
         IF(IPF.EQ.0)THEN
            READ(LR,*)PRESS_VAL
         ENDIF
         
         DO I=1,NPRESS
            ISET=IPRESS_SET(I)
            DO J=1,NELSET(ISET)
               NEL=NELLIST(J,ISET)
               IBELEM(NEL)=IPRESS_SF(I)
               IF(IPF.EQ.0)THEN
                  ILF(NEL)=0
                  PRESSB(NEL)=PRESS_VAL
               ELSE
                  ILF(NEL)=1
               ENDIF

            ENDDO
         ENDDO
      ENDDO

      CALL READSTR(LR)
C     READ CONSTRAINTS
      READ(LR,*)NCONST
      NCONSTR=0

      IF(NCONST.GT.0)THEN
         WRITE(*,*)'CONSTRAINTS NOT IN CODE'
         STOP
      ENDIF
      
      
      DO I=1,MBOUND2
      NBC2(I)=0
      VBC2(I)=0.0D0
      ENDDO
      
      DO I=1,MBOUND3
      NBC3(I)=0
      VBC3(I)=0.0D0
      ENDDO
      
!-------------------------------------------------------------      

      RETURN
      END 

************************************************************

      ! deniz - update temperature state variable, 
      ! according to the prescribed thermal loading
      SUBROUTINE TEMPINC(temp_loading_type, temp_stress_free, 
     &                temp_cyclic_amplitude, temp_cyclic_period, 
     &                temp_history_values, temp_history_times, 
     &                temp_history_N, TIME, TEMP, TEMP_0)
      implicit none
      INCLUDE 'PARDIS.H'
      real(8) :: PI
      parameter(PI=3.14159265359D0)
      
      ! INPUTS
      integer, intent(in) :: temp_loading_type ! 1: temperature time history given
                                               ! 2: cyclic thermal loading around initial temp
                                               ! 3: cyclic thermal loading ABOVE initial temp
      real(8), intent(in) :: temp_stress_free  ! initial/stress-free temperature
      real(8), intent(in) :: temp_cyclic_amplitude   ! in Kelvins
      real(8), intent(in) :: temp_cyclic_period      ! in seconds
      real(8), intent(in) :: temp_history_values(MAX_HISTORY_N) ! provide temp-time points. 
      real(8), intent(in) :: temp_history_times(MAX_HISTORY_N)  ! program will interpolate
      integer, intent(in) :: temp_history_N                          ! for integration time steps
      real(8), intent(in) :: TIME(2)
      
      ! OUTPUTS
      real(8), intent(out) :: TEMP(2), TEMP_0
      
      !locals
      integer :: iTemp, t_or_tau
      real(8) :: w1,w2
      logical :: found
      
      !statements
      
      TEMP_0 = temp_stress_free
      TEMP(:)=TEMP_0 ! init. to default

      if(temp_loading_type.EQ.2) then  ! thermal cycling, +/- around initial temp
         TEMP(1) = TEMP_0+   
     &       temp_cyclic_amplitude*SIN(2*PI*TIME(1)/temp_cyclic_period)  ! temp. at time t
         TEMP(2) = TEMP_0+   
     &       temp_cyclic_amplitude*SIN(2*PI*TIME(2)/temp_cyclic_period)  ! temp. at time tau
      elseif (temp_loading_type.EQ.3) then  ! thermal cycling, above initial temp
         TEMP(1) = TEMP_0+   
     &     temp_cyclic_amplitude*SIN(PI*TIME(1)/temp_cyclic_period)**2.0  ! temp. at time t
         TEMP(2) = TEMP_0+   
     &     temp_cyclic_amplitude*SIN(PI*TIME(2)/temp_cyclic_period)**2.0  ! temp. at time tau
      elseif(temp_loading_type.EQ.1) then
         do t_or_tau=1,2
            ! interpolate temperature at integration time steps
            ! from the provided temp-time points
            
            found=.FALSE.
            do iTemp=1,temp_history_N
               if(TIME(t_or_tau).LT.temp_history_times(iTemp))then
                  found=.TRUE.
                  exit
               endif
            enddo
            if(found)then
               if(iTemp.EQ.1)then
                  TEMP(t_or_tau)=temp_history_values(1)
               else
                  w1 = (temp_history_times(iTemp)-TIME(t_or_tau)) /   
     &         (temp_history_times(iTemp)-temp_history_times(iTemp-1))
                  w2 = (TIME(t_or_tau)-temp_history_times(iTemp-1)) /   
     &         (temp_history_times(iTemp)-temp_history_times(iTemp-1))
                  TEMP(t_or_tau)=w1*temp_history_values(iTemp-1)   
     &                         + w2*temp_history_values(iTemp)
               endif 
            else
               ! out of provided data range. assign the last provided value
               TEMP(t_or_tau)=temp_history_values(temp_history_N)
            endif
         enddo
      endif

      END SUBROUTINE

      ! deniz -- read thermal loading information
      SUBROUTINE READTEMP(strFileName,
     &                    temp_loading_type,temp_stress_free,  
     &                    temp_cyclic_amplitude, temp_cyclic_period,  
     &                    temp_history_values, temp_history_times,  
     &                    temp_history_N,  
     &                    info)
     
      IMPLICIT NONE

      INCLUDE 'PARDIS.H'
      
      character(len=*), intent(in) :: strFileName
      integer, intent(out) :: temp_loading_type ! 1: temperature time history given
                                                ! 2: cyclic thermal loading around initial temp
                                                ! 3: cyclic thermal loading ABOVE initial temp
      real(8), intent(out) :: temp_stress_free  ! initial/stress-free temperature
      real(8), intent(out) :: temp_cyclic_amplitude   ! in Kelvins
      real(8), intent(out) :: temp_cyclic_period      ! in seconds
      real(8), intent(out) :: temp_history_values(MAX_HISTORY_N) ! provide temp-time points. 
      real(8), intent(out) :: temp_history_times(MAX_HISTORY_N)  ! program will interpolate
      integer, intent(out) :: temp_history_N                          ! for integration time steps
      integer, intent(out) :: info 
      ! info=1: temp loading imported, info=0: error, or no temp. loading
      
      ! locals
      integer :: iTemp
      logical :: fileExists
      
      info = 0
            
      INQUIRE(file=trim(strFileName),exist=fileExists)
      if (.NOT.fileExists) then
         ! no thermal loading info. default conditions:
         ! no temperature change throughout simulation
         temp_loading_type=1
         temp_stress_free=300.d0
         temp_history_values(1)=300.d0
         temp_history_times(1)=0
         temp_history_N=1
         return
      end if
      
      OPEN(201,file=trim(strFileName))

      CALL READSTR(201)
      READ(201,*) temp_loading_type
      
      CALL READSTR(201)
      READ(201,*) temp_stress_free

      CALL READSTR(201)
      CALL READSTR(201)
      if (temp_loading_type.EQ.2.OR.  
     &    temp_loading_type.EQ.3) then       ! CYCLIC THERMAL LOADING
      
         READ(201,*) temp_cyclic_amplitude, temp_cyclic_period
         
      elseif (temp_loading_type.EQ.1) then   ! TEMP. HISTORY PROVIDED
      
         READ(201,*) temp_history_N
         
         if(temp_history_N.GT.MAX_HISTORY_N)then
            write(*,*) 'Increase MAX_HISTORY_N to ',temp_history_N
            STOP
         endif
         
         do iTemp = 1,temp_history_N
            READ(201,*) temp_history_times(iTemp),  
     &                  temp_history_values(iTemp)
         enddo
         if(temp_history_N.EQ.0)then
            ! default: T = T0 (const)
            temp_history_N = 1
            temp_history_times(1) = 0.0
            temp_history_values(1) = temp_stress_free
         end if

      else
      
         ! unexpected input. adopt default conditions:
         ! no temperature change throughout simulation
         temp_loading_type=1
         temp_stress_free=273.0
         temp_history_values(1)=273.0
         temp_history_times(1)=0.0
         temp_history_N=1
         
      endif
      
      ! success
      info = 1
      CLOSE(201)
      
      END SUBROUTINE

      SUBROUTINE TRAINC(NBC2,VBC2,VBCS2,DELT,NDF,TRAC)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      INTEGER::MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      INTEGER::ISENDORRECV(MAXPROC)
      COMMON/BOUNDA/NBOUD1,NBOUD2,NBOUD3,NUPNCH
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV
      DIMENSION VBC2(MBOUND2),VBCS2(MBOUND2),NBC2(MBOUND2),
     &     TRAC(MDOFX)

      NDF1=NDF+1
      DO N=1,NBOUD2
         NODE=NBC2(NDF1*(N-1)+1)
         DO I=1,NDF
            INDEX=NBC2(NDF1*(N-1)+I)
            IF(INDEX.EQ.1)THEN
               NN=NDF*(NODE-1)+I
!               NN=IEQNO(NN)
               N1=NDF*(N-1)+I
               TRAC(NN)=TRAC(NN)+VBCS2(N1)*DELT
            ENDIF
         ENDDO
      ENDDO
      RETURN
      END

*******************************************************************


      SUBROUTINE LOADVC(F,VTAU,DVTAU0,XUND,IJK,IMID,NPROPSG,
     &     PROMAT2D,DELTME,NBC1,IBC,KINC,NDF,N_UPDATE,IN_ST,IROWPTR,
     &     IBINDX,VAL,RESID,ISIZE,IFLAG,NGAUSS,PNEWDT,VAL_STORE,F_EXT,
     &     SVARS1,SVARS2,MAXSV,MaxElemInPatch,NElementsInPatch,
     &     ElementsInPatch,NPATCH,FLOW_RULE,BROY_BFGS)  ! KOUROSH_INT, KOUROSH_CONVERGENCE_CRITERION, F-BAR METHOD
     

     

      IMPLICIT NONE
      INCLUDE 'PARDIS.H'
      INCLUDE 'mpif.h'
      
      INTEGER:: N_UPDATE,MAXSV,NPATCH,MaxElemInPatch,IPATCH,IPT,
     &          NOFFSET,TotalElmsInPatch,JINDX_RHS,NROWS_RHS,
     &          NRECVROWS_RHS,BROY_BFGS    !F-BAR METHOD
      REAL(8):: DET_FTAU,DET_FT,TotalVolTAU,TotalVol0,TotalVolT  !F-BAR METHOD
      INTEGER::NElementsInPatch(NPATCH),IROWARR_RHS(MDOFPROC),
     &         IELDOF_NonLocal(12,MaxElemInPatch),
     &         NUMROWSPROC_RHS(MAXPROC),IROWNO_RHS(MDOFPROC,MAXPROC),
     &         IRECVROW_RHS(MDOFPROC)    !F-BAR METHOD
      INTEGER::ElementsInPatch(NPATCH,MaxElemInPatch)  !F-BAR METHOD
      REAL(8)::XBAR_ST(6,12,1),XG_ST(9,12,1),DVOLTAU_ST(1),
     &         DFGRD0(3,3),DFGRD1(3,3),DVOL0_ST(1),DVOLT_ST(1),
     &         GMATX_REPOSITORY(9,12,MaxElemInPatch),
     &         DFGRD0MAT_REPOSITORY(3,3,MaxElemInPatch),
     &         DFGRD1MAT_REPOSITORY(3,3,MaxElemInPatch),
     &         DVOLMAT_REPOSITORY(MaxElemInPatch),
     &         DVOL0MAT_REPOSITORY(MaxElemInPatch),
     &         DVOLTMAT_REPOSITORY(MaxElemInPatch),     
     &         DETFtau_REPOSITORY(MaxElemInPatch),
     &         DETFt_REPOSITORY(MaxElemInPatch),Fbar_TAU(3,3),
     &         Fbar_T(3,3),BMATX_REPOSITORY(6,12,MaxElemInPatch),
     &         SHPTAU_ST(3,4,1),SKE_NonLocal(12,12,MaxElemInPatch), 
     &         SHPtauMAT_REPOSITORY(3,4,MaxElemInPatch)  !F-BAR METHOD
         
      
      REAL(8)::F(MDOFX),SKE(MDOFE,MDOFE),FE(MDOFE)
      REAL(8)::XEUN(MAXDIM,MAXELN),XUND(MAXCRD)   !,SKE1(MDOFE,MDOFE)    !F-BAR METHOD
      REAL(8)::F_RECV(MDOFX),VTAU(MDOFX),DVTAU0(MDOFX)
      REAL(8)::PROMAT2D(MAXPROPS,MAXGRP),VTAU_E(MAXELN*NDOFELX)
      REAL(8)::PROPS1(MAXPROPS),TIME1(2),SVARS1(MAXSV),TIME(2)
      REAL(8)::DVTAU0_E(MAXELN*NDOFELX)
      REAL(8)::EXTRNVAL(MAXDIM*4,MDOFPROC),EXTRNRHS(MDOFPROC,MAXPROC)
      REAL(8)::RHSARR(MDOFPROC),EXTRNMPI(MDOFPROC*MAXDIM*MAXELN)
      REAL(8)::RECVVAL(MDOFPROC*4*MAXDIM),RECVRHS(MDOFPROC)
      REAL(8)::RESID(N_UPDATE),VAL(ISIZE)
      REAL(8)::DELTME,PNEWDT,PNEWDT_LOC,PNEWDT_RECV
      REAL(8)::SVARS2(MAXSV)
      
      
      INTEGER:: IMID(MAXDIM*MAXEL),IJK(MNELX),NBC1(MBOUND1)
      INTEGER:: NPROPSG(MAXGRP),IBC(MDOFX)
      INTEGER:: NUMROWSPROC(MAXPROC),IROWNO(MDOFPROC,MAXPROC)
      INTEGER:: IELNO(MDOFPROC,MAXPROC),INDXCOLM(MDOFPROC,MAXPROC)
      INTEGER:: IROWINDX(MDOFPROC,MAXPROC),IELDOF(MDOFX*MAXELN)
      INTEGER:: IROWARR(MDOFPROC),IELARR(MDOFPROC),IRINDX(MDOFPROC)
      INTEGER:: KINC,NDF,IN_ST,ISIZE
      INTEGER:: NGAUSS,IFLAG,IROW,IIB,IACOLM
      INTEGER:: IE,ICOLM,IEL,IERROR,IEND
      INTEGER:: IEXTRN,IIA,II,III,FLOW_RULE
      
      INTEGER:: IRECVROW(MDOFPROC),IRECVELNO(MDOFPROC)
      INTEGER:: IRECVRINDX(MDOFPROC),ISTATUS(MPI_STATUS_SIZE)
      INTEGER:: NELX,NEQ,NPLANE,NODE
      INTEGER:: NX,NSVARS1,NDOFEL1,MDLOAD1
      INTEGER:: NPREDF1,MCRD1,MPIINF(MPIINFSIZEMAX)
      INTEGER:: IPROCINFO(MAXPROC), ISENDORRECV(MAXPROC)
      INTEGER:: NELST,NELEND,NELNOX
      INTEGER:: IBINDX(ISIZE)
      INTEGER:: IROWPTR(N_UPDATE+1)
      INTEGER:: INDX1,I,J,IRNO,ISEND
      INTEGER:: IS,JELEM,ITEMP,JKIA,JJ
      INTEGER:: JINDX,JKIA1,K,LL,KK
      INTEGER:: MCRD,MDLOAD,MLVARX,MPIPROCNO
      INTEGER:: NDOFEL, NGROUP,NNO,NN
      INTEGER:: NODEINPROC,NPREDF,IPROC
      INTEGER:: MPIINFSIZE, NEL, NROWS, IELEM
      INTEGER:: NPROCS,NRECVROWS,NPROPS,NSVARS
      
      INTEGER::N_SPLIT,I_FK,nelnox_FK,NELST_FK,NELEND_FK
  
      
      REAL(8)::VAL_STORE(ISIZE) ! KOUROSH_INT
      REAL(8)::F_EXT(MDOFX),F_APPL(MDOFX),FE_APPL(MDOFE) ! KOUROSH_CONVERGENCE_CRITERION

      INTEGER::IFS,IST,IDIR_PRESSURE
      REAL(8)::TEMP,SUM_CURRENT_ELEMENT_AREA,SUM_REF_ELEMENT_AREA,
     &         PRESSURE_SCALE
      INTEGER::IBNODE(3)
      REAL(8)::UE(MAXDIM,MAXELN),XE_TAU(MAXDIM,MAXELN),UV2(3),UV3(3),
     &         UV4(3),UV4_NORMAL(3)      
      INTEGER::IBELEM(MAXEL),ILF(MAXEL),IFACE_TET(12),IFACE_BRICK(24)
      REAL(8)::PRESSB(MAXEL)
      COMMON/PRESS/PRESSB,IBELEM,ILF,IFACE_TET,IFACE_BRICK
      COMMON/PRESSURE/IDIR_PRESSURE      
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
      COMMON/ABQ/TIME1,NSVARS1,NDOFEL1,MDLOAD1,NPREDF1,MCRD1  !F-BAR METHOD
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV
      COMMON/EL_PAR/NELST,NELEND,NELNOX
      COMMON/SPLIT_COMMUN/N_SPLIT
 
      DO I=1,N_UPDATE
         RESID(I)=0.0D0
      ENDDO
      
      IF (BROY_BFGS.EQ.0) VAL=0.D0
      
      F_APPL=0.D0  ! KOUROSH_CONVERGENCE_CRITERION


      TIME(1)=TIME1(1)
      TIME(2)=TIME1(2)
      NSVARS=NSVARS1
      NDOFEL=NDOFEL1
      MDLOAD=MDLOAD1
      NPREDF=NPREDF1
      MCRD=MCRD1
      MLVARX=NDOFEL
      PNEWDT_LOC=10.D0
      NOFFSET=0


      SUM_CURRENT_ELEMENT_AREA=0.D0
      SUM_REF_ELEMENT_AREA=0.D0
      
      DO IPATCH=NELST,NELEND
      DO IELEM=1,NElementsInPatch(IPATCH)
         NEL=ElementsInPatch(IPATCH,IELEM)
         IF (IBELEM(NEL).NE.0) THEN

            NODE=IMID(NEL)

            IIB=0
            DO IIA=1,NODE
               JKIA=IJK(NODE*(NEL-1)+IIA) 
               JKIA1=MCRD*(JKIA-1) 
               DO III=1,MCRD
                  XEUN(III,IIA)=XUND(JKIA1+III)
               ENDDO
               DO II=1,NDF
                  NN=NDF*(JKIA-1)+II
                  IIB=IIB+1
                  VTAU_E(IIB)=VTAU(NN)
                  DVTAU0_E(IIB)=DVTAU0(NN)
               ENDDO     
            ENDDO
            
            UE(1:MAXDIM,1:MAXELN)=RESHAPE(VTAU_E,(/MAXDIM,MAXELN/))
            IFS=IBELEM(NEL)
            IST=3*(IFS-1)
            IBNODE(1:3)=IFACE_TET(IST+1:IST+3)
            
            XE_TAU=XEUN+UE
            UV2(1:3)=XE_TAU(1:3,IBNODE(2))-XE_TAU(1:3,IBNODE(1))
            UV3(1:3)=XE_TAU(1:3,IBNODE(3))-XE_TAU(1:3,IBNODE(1))
            CALL CROSS(UV4,UV3,UV2)
            CALL DOT3(TEMP,UV4,UV4)
            UV4_NORMAL=UV4/DSQRT(TEMP)
            SUM_CURRENT_ELEMENT_AREA=SUM_CURRENT_ELEMENT_AREA+
     &               DSQRT(TEMP)*DABS(UV4_NORMAL(IDIR_PRESSURE))/2.D0
     
     
            XE_TAU=XEUN
            UV2(1:3)=XE_TAU(1:3,IBNODE(2))-XE_TAU(1:3,IBNODE(1))
            UV3(1:3)=XE_TAU(1:3,IBNODE(3))-XE_TAU(1:3,IBNODE(1))
            CALL CROSS(UV4,UV3,UV2)
            CALL DOT3(TEMP,UV4,UV4)
            UV4_NORMAL=UV4/DSQRT(TEMP)
            SUM_REF_ELEMENT_AREA=SUM_REF_ELEMENT_AREA+
     &              DSQRT(TEMP)*DABS(UV4_NORMAL(IDIR_PRESSURE))/2.D0     
            
         ENDIF
      ENDDO
      ENDDO
      
      
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,SUM_CURRENT_ELEMENT_AREA,1, 
     &      MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR) 

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,SUM_REF_ELEMENT_AREA,1, 
     &      MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)      
      
      IF (SUM_CURRENT_ELEMENT_AREA.GT.1D-9) THEN
         PRESSURE_SCALE=SUM_REF_ELEMENT_AREA/SUM_CURRENT_ELEMENT_AREA
         IF (IPROC.EQ.0)WRITE(*,*)'@@@ PRESSURE IS BEING SCALED BY:',
     &                             1.D0/PRESSURE_SCALE
         PRESSURE_SCALE=1.D0
      ENDIF
      
      
      nelnox_FK=NELNOX/N_SPLIT     

      DO I_FK = 1,N_SPLIT

         IF (I_FK.EQ.1) THEN
            NELST_FK=NELST
         ELSE
            NELST_FK=NELEND_FK + 1
         ENDIF

         NELEND_FK=NELST_FK + NELNOX_FK - 1

         IF (I_FK.EQ.N_SPLIT) NELEND_FK = NELEND
         
         DO I=1,NPROCS-1
            NUMROWSPROC(I)=0
            NUMROWSPROC_RHS=0    !F-BAR METHOD
         ENDDO
         
         IROWNO_RHS=0
         IROWNO=0
         IELNO=0
         INDXCOLM=0
         IROWINDX=0
         EXTRNRHS=0.D0

         IEXTRN=1
         EXTRNVAL=0.D0  !F-BAR METHOD
      
      
      
      
      DO IPATCH=NELST_FK,NELEND_FK
      
         GMATX_REPOSITORY=0.D0
         BMATX_REPOSITORY=0.D0
         DFGRD0MAT_REPOSITORY=0.D0
         DFGRD1MAT_REPOSITORY=0.D0
         DVOLMAT_REPOSITORY=0.D0
         DVOL0MAT_REPOSITORY=0.D0
         DVOLTMAT_REPOSITORY=0.D0
         DETFtau_REPOSITORY=0.D0
         DETFt_REPOSITORY=0.D0
         SHPtauMAT_REPOSITORY=0.D0
         
         DO IELEM=1,NElementsInPatch(IPATCH)
      
            NEL=ElementsInPatch(IPATCH,IELEM)

            NODE=IMID(NEL)

            IIB=0
            DO IIA=1,NODE
               JKIA=IJK(NODE*(NEL-1)+IIA) 
               JKIA1=MCRD*(JKIA-1) 
               DO III=1,MCRD
                  XEUN(III,IIA)=XUND(JKIA1+III)
               ENDDO
               DO II=1,NDF
                  NN=NDF*(JKIA-1)+II
                  IIB=IIB+1
                  VTAU_E(IIB)=VTAU(NN)
                  DVTAU0_E(IIB)=DVTAU0(NN)
               ENDDO     
            ENDDO

            NDOFEL=NODE*MCRD
            

            IPT=1
            CALL ELSTNEW(XBAR_ST,XG_ST,DVOLTAU_ST,MCRD,NDOFEL,
     &           NODE,XEUN,VTAU_E,DVTAU0_E,DFGRD0,DFGRD1,TIME,KINC,
     &           NEL,IPT,DVOL0_ST,DVOLT_ST,DET_FTAU,DET_FT,SHPTAU_ST,
     &           PNEWDT)  ! KOUROSH_CONVERGENCE_CRITERION       


            IF(PNEWDT.LT.PNEWDT_LOC)PNEWDT_LOC=PNEWDT
            
            
      
            GMATX_REPOSITORY(1:9,1:12,IELEM)=XG_ST(1:9,1:12,1) 
            BMATX_REPOSITORY(1:6,1:12,IELEM)=XBAR_ST(1:6,1:12,1) 
            DFGRD0MAT_REPOSITORY(1:3,1:3,IELEM)=DFGRD0(1:3,1:3)
            DFGRD1MAT_REPOSITORY(1:3,1:3,IELEM)=DFGRD1(1:3,1:3) 
            DVOLMAT_REPOSITORY(IELEM)=DVOLTAU_ST(1)
            DVOL0MAT_REPOSITORY(IELEM)=DVOL0_ST(1) 
            DVOLTMAT_REPOSITORY(IELEM)=DVOL0_ST(1) 
            DETFtau_REPOSITORY(IELEM)=DET_FTAU
            DETFt_REPOSITORY(IELEM)=DET_FT
            SHPtauMAT_REPOSITORY(1:3,1:4,IELEM)=SHPTAU_ST(1:3,1:4,1)

         ENDDO


         TotalVolTAU=sum(DVOLMAT_REPOSITORY)
         TotalVol0=sum(DVOL0MAT_REPOSITORY)
         TotalVolT=sum(DVOLTMAT_REPOSITORY)
         
         DO IELEM=1,NElementsInPatch(IPATCH)

            Fbar_TAU(1:3,1:3)=(TotalVolTAU/TotalVol0/
     &                        DETFtau_REPOSITORY(IELEM))**(1.D0/3.D0)*
     &                        DFGRD1MAT_REPOSITORY(1:3,1:3,IELEM)

            Fbar_T(1:3,1:3)=(TotalVolT/TotalVol0/
     &                        DETFt_REPOSITORY(IELEM))**(1.D0/3.D0)*
     &                        DFGRD0MAT_REPOSITORY(1:3,1:3,IELEM)
     
     
            XBAR_ST(1:6,1:12,1)=BMATX_REPOSITORY(1:6,1:12,IELEM) 
            JELEM=ElementsInPatch(IPATCH,IELEM)
            TotalElmsInPatch=NElementsInPatch(IPATCH)
            
            NODE=IMID(JELEM)
            NGROUP=IMID(NELX+JELEM)
            NPROPS=NPROPSG(NGROUP)

            DO II=1,NPROPS
               PROPS1(II)=PROMAT2D(II,NGROUP)
            ENDDO
                        
            IIB=0
            DO IIA=1,NODE
               JKIA=IJK(NODE*(JELEM-1)+IIA) 
               JKIA1=MCRD*(JKIA-1) 
               DO III=1,MCRD
                  XEUN(III,IIA)=XUND(JKIA1+III)
               ENDDO
               DO II=1,NDF
                  NN=NDF*(JKIA-1)+II
                  IIB=IIB+1
                  VTAU_E(IIB)=VTAU(NN)
               ENDDO     
            ENDDO
                        
            
            CALL ELST1(JELEM,NOFFSET,SKE_NonLocal,Fbar_TAU,Fbar_T,
     &        XBAR_ST,DVOLMAT_REPOSITORY,MaxElemInPatch,IELEM,
     &        SHPtauMAT_REPOSITORY,TotalElmsInPatch,NODE,XEUN,SKE,FE,
     &        TIME,DELTME,PROPS1,NPROPS,NSVARS,NDOFEL,MDLOAD,MLVARX,
     &        NPREDF,MCRD,VTAU_E,KINC,IFLAG,NGAUSS,PNEWDT,FE_APPL,
     &        SVARS1,SVARS2,MAXSV,FLOW_RULE,PRESSURE_SCALE)   ! F-BAR METHOD

            NOFFSET=NOFFSET+NSVARS
            
            
            IF(PNEWDT.LT.PNEWDT_LOC)PNEWDT_LOC=PNEWDT
            
            
            NEL=JELEM
           
C************ KOUROSH_CONVERGENCE_CRITERION*********************
            DO J=1,NODE
               NNO=IJK((NEL-1)*NODE + J)
               F_APPL((NNO-1)*NDF+1:(NNO-1)*NDF+NDF) =   
     &             F_APPL((NNO-1)*NDF+1:(NNO-1)*NDF+NDF)  +
     &            FE_APPL((J-1)*NDF+1:(J-1)*NDF+NDF)
            ENDDO
C************ KOUROSH_CONVERGENCE_CRITERION*********************

            
            DO II=1,NODE
               NNO=IJK((NEL-1)*NODE+II)
               DO JJ=1,NDF
                  IELDOF((II-1)*NDF+JJ)=(NNO-1)*NDF+JJ
               ENDDO
            ENDDO
            
            
            DO IEL=1,NElementsInPatch(IPATCH)
               IIB=ElementsInPatch(IPATCH,IEL)
               
               IF (IIB.NE.JELEM) THEN
                  DO II=1,NODE
                     NNO=IJK((IIB-1)*NODE+II)
                     DO JJ=1,NDF
                     IELDOF_NonLocal((II-1)*NDF+JJ,IEL)=(NNO-1)*NDF+JJ
                     ENDDO
                  ENDDO                  
               ENDIF

            ENDDO


            DO J=1,NODE
               NNO=IJK((NEL-1)*NODE + J)
               CALL FINDPROC(NNO,NPROCS,NODEINPROC,NDF)

               DO K=1,NDF

                  IROW=(NNO-1)*NDF+K
                  
     
                  IF(NODEINPROC.EQ.IPROC)THEN
                     
                     INDX1=IROW-IN_ST
                     IS=IROWPTR(INDX1)
                     IE=IROWPTR(INDX1+1)-1

                     IF(INDX1.EQ.N_UPDATE)IE=IROWPTR(N_UPDATE+1)



                     IF(IFLAG.EQ.0)THEN


                        DO KK=1,NODE*NDF
                           IACOLM=IELDOF(KK)          
                           DO LL=IS,IE
                              IF(IBINDX(LL).EQ.IACOLM)THEN
                                 VAL(LL)=VAL(LL)+SKE((J-1)*NDF+K,KK)
                                 GOTO 110
                              ENDIF
                           ENDDO
110                     ENDDO



                        DO IEL=1,NElementsInPatch(IPATCH) 
                           IF (IEL.NE.IELEM) THEN
                              DO KK=1,NODE*NDF
                                 IACOLM=IELDOF_NonLocal(KK,IEL)          
                                 DO LL=IS,IE
                                    IF(IBINDX(LL).EQ.IACOLM)THEN
                                      VAL(LL)=VAL(LL)+
     &                                SKE_NonLocal((J-1)*NDF+K,KK,IEL)
                                      GOTO 120
                                    ENDIF
                                 ENDDO
120                           ENDDO
                           ENDIF
                        ENDDO

    
                     ENDIF

       
               
                     RESID(INDX1)=RESID(INDX1) + FE((J-1)*NDF + K)

                  ELSE
                  
      

                     IF(NODEINPROC.LT.IPROC)THEN
                        INDX1 = NODEINPROC+1
                     ELSE
                        INDX1 = NODEINPROC
                     ENDIF

                     
 
                     JINDX_RHS=NUMROWSPROC_RHS(INDX1)+1
                     NUMROWSPROC_RHS(INDX1)=JINDX_RHS
                     IROWNO_RHS(JINDX_RHS,INDX1)=IROW
                     EXTRNRHS(JINDX_RHS,INDX1)=FE((J-1)*NDF + K)  


                     
                     DO IEL=1,NElementsInPatch(IPATCH)
                        IIB=ElementsInPatch(IPATCH,IEL)
                        JINDX=NUMROWSPROC(INDX1)+1
                        NUMROWSPROC(INDX1)=JINDX
                        IROWNO(JINDX,INDX1)=IROW
                        IELNO(JINDX,INDX1)=IIB
                        INDXCOLM(JINDX,INDX1)=IEXTRN
                        IROWINDX(JINDX,INDX1)=(J-1)*NDF + K
                        
                        IF(IFLAG.EQ.0)THEN
                           IF (IEL.NE.IELEM) THEN
                        
                           DO JJ=1,NDOFEL
                              EXTRNVAL(JJ,IEXTRN)=
     &                             SKE_NonLocal((J-1)*NDF + K,JJ,IEL)
                           ENDDO
                           IEXTRN=IEXTRN+1
                           
                           ELSE
                           
                           DO JJ=1,NDOFEL
                              EXTRNVAL(JJ,IEXTRN)=SKE((J-1)*NDF+K,JJ)
                           ENDDO
                           
                           IEXTRN=IEXTRN+1                           
                           
                           ENDIF
                           
                        ENDIF
                     
                     ENDDO

                     IF(JINDX.GT.MDOFPROC.OR.IEXTRN.GT.MDOFPROC) THEN
                        WRITE(*,*)'EXTERNAL COMMUNICATION VARIABLE 
     &                             MDOFPROC SHOULD BE INCREASED'
                        WRITE(*,*)JINDX,IEXTRN,MDOFPROC
                        STOP
                     ENDIF     

                  ENDIF
           
               ENDDO
            ENDDO

         ENDDO

      ENDDO


      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)

      
      CALL MPI_ALLREDUCE(PNEWDT_LOC,PNEWDT_RECV,1,
     & MPI_DOUBLE_PRECISION,MPI_MIN,MPI_COMM_WORLD,IERROR)
   
      PNEWDT=PNEWDT_RECV

      IF(PNEWDT.LT.1.D0) RETURN
      

      DO I=1,NPROCS-1
      
         
   
         MPIPROCNO=IPROCINFO(I)


         ISEND=ISENDORRECV(I)

         IF(IPROC.GT.MPIPROCNO) THEN 
            ITEMP=MPIPROCNO+1
         ELSE 
            ITEMP=MPIPROCNO
         ENDIF

         IF(ISEND.EQ.1)THEN

            
         
            NROWS=NUMROWSPROC(ITEMP)
            NROWS_RHS=NUMROWSPROC_RHS(ITEMP)   ! F-BAR METHOD


            CALL MPI_SSEND(NROWS,1,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)
     
            CALL MPI_SSEND(NROWS_RHS,1,MPI_INTEGER,MPIPROCNO,100,
     &           MPI_COMM_WORLD,IERROR)      ! F-BAR METHOD


            DO J=1,NROWS_RHS
               IRNO=IROWNO_RHS(J,ITEMP)
               IROWARR_RHS(J)=IRNO
               RHSARR(J)=EXTRNRHS(J,ITEMP)
            ENDDO

            DO J=1,NROWS
               IRNO=IROWNO(J,ITEMP)
               IROWARR(J)=IRNO
               ICOLM=INDXCOLM(J,ITEMP)
               IEL=IELNO(J,ITEMP)
               IELARR(J)=IEL
               IRINDX(J)=IROWINDX(J,ITEMP)
!               RHSARR(J)=EXTRNRHS(J,ITEMP)   ! F-BAR METHOD

               IF(IFLAG.EQ.0)THEN
                  DO K=1,NODE*NDF
                     EXTRNMPI((J-1)*NODE*NDF+K)=EXTRNVAL(K,ICOLM)
                  ENDDO
               ENDIF
            ENDDO

            
            IF(NROWS.GT.0)THEN

               CALL MPI_SSEND(IROWARR,NROWS,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)

               CALL MPI_SSEND(IELARR,NROWS,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)

               CALL MPI_SSEND(IRINDX,NROWS,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)

               CALL MPI_SSEND(IROWARR_RHS,NROWS_RHS,MPI_INTEGER,
     &              MPIPROCNO,100,MPI_COMM_WORLD,IERROR)

               CALL MPI_SSEND(RHSARR,NROWS_RHS,MPI_DOUBLE_PRECISION,
     &                   MPIPROCNO,100,MPI_COMM_WORLD,IERROR)  ! F-BAR METHOD

               IF(IFLAG.EQ.0)THEN
                 CALL MPI_SSEND(EXTRNMPI,NROWS*NODE*NDF,
     &           MPI_DOUBLE_PRECISION,MPIPROCNO,100,MPI_COMM_WORLD,
     &           IERROR)
               ENDIF
            ENDIF

            CALL MPI_RECV(NRECVROWS,1,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,ISTATUS,IERROR)
     
            CALL MPI_RECV(NRECVROWS_RHS,1,MPI_INTEGER,MPIPROCNO,100,
     &           MPI_COMM_WORLD,ISTATUS,IERROR)     

            IF(NRECVROWS.GT.0)THEN



             CALL MPI_RECV(IRECVROW,NRECVROWS,MPI_INTEGER,MPIPROCNO,
     &                   100,MPI_COMM_WORLD,ISTATUS,IERROR)

             CALL MPI_RECV(IRECVELNO,NRECVROWS,MPI_INTEGER,MPIPROCNO,
     &                  100,MPI_COMM_WORLD,ISTATUS,IERROR)

             CALL MPI_RECV(IRECVRINDX,NRECVROWS,MPI_INTEGER,MPIPROCNO,
     &                  100,MPI_COMM_WORLD,ISTATUS,IERROR)
     
             CALL MPI_RECV(IRECVROW_RHS,NRECVROWS_RHS,MPI_INTEGER,
     &              MPIPROCNO,100,MPI_COMM_WORLD,ISTATUS,IERROR)
     

             CALL MPI_RECV(RECVRHS,NRECVROWS,MPI_DOUBLE_PRECISION,
     &                  MPIPROCNO,100,MPI_COMM_WORLD,ISTATUS,IERROR)
               
               IF(IFLAG.EQ.0)THEN
             CALL MPI_RECV(RECVVAL,NRECVROWS*NDF*NODE,
     &             MPI_DOUBLE_PRECISION,MPIPROCNO,100,MPI_COMM_WORLD,
     &             ISTATUS,IERROR)
               ENDIF

         CALL UPDATE_LOCAL_ARRAY(IJK,NRECVROWS,RECVVAL,RECVRHS,
     &        IRECVROW,IRECVELNO,IRECVRINDX,NDF,IPROC,N_UPDATE,IN_ST,
     &        IROWPTR,IBINDX,VAL,RESID,ISIZE,IFLAG,
     &        IRECVROW_RHS,NRECVROWS_RHS)
     
 

            ENDIF
            


         ELSE

            CALL MPI_RECV(NRECVROWS,1,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,ISTATUS,IERROR)
     
            CALL MPI_RECV(NRECVROWS_RHS,1,MPI_INTEGER,MPIPROCNO,100,
     &           MPI_COMM_WORLD,ISTATUS,IERROR)     


            IF(NRECVROWS.GT.0)THEN


             CALL MPI_RECV(IRECVROW,NRECVROWS,MPI_INTEGER,MPIPROCNO,
     &                   100,MPI_COMM_WORLD,ISTATUS,IERROR)

             CALL MPI_RECV(IRECVELNO,NRECVROWS,MPI_INTEGER,MPIPROCNO,
     &                  100,MPI_COMM_WORLD,ISTATUS,IERROR)

             CALL MPI_RECV(IRECVRINDX,NRECVROWS,MPI_INTEGER,MPIPROCNO,
     &                  100,MPI_COMM_WORLD,ISTATUS,IERROR)

             CALL MPI_RECV(IRECVROW_RHS,NRECVROWS_RHS,MPI_INTEGER,
     &                  MPIPROCNO,100,MPI_COMM_WORLD,ISTATUS,IERROR)
     
             CALL MPI_RECV(RECVRHS,NRECVROWS,MPI_DOUBLE_PRECISION,
     &                  MPIPROCNO,100,MPI_COMM_WORLD,ISTATUS,IERROR)

             IF(IFLAG.EQ.0)THEN
             CALL MPI_RECV(RECVVAL,NRECVROWS*NODE*NDF,
     &                   MPI_DOUBLE_PRECISION,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,ISTATUS,IERROR)
             ENDIF

             CALL UPDATE_LOCAL_ARRAY(IJK,NRECVROWS,RECVVAL,RECVRHS,
     &               IRECVROW,IRECVELNO,IRECVRINDX,NDF,IPROC,N_UPDATE,
     &               IN_ST,IROWPTR,IBINDX,VAL,RESID,ISIZE,IFLAG,
     &               IRECVROW_RHS,NRECVROWS_RHS)
     
     


            ENDIF
            
            NROWS=NUMROWSPROC(ITEMP)
            NROWS_RHS=NUMROWSPROC_RHS(ITEMP)

            CALL MPI_SSEND(NROWS,1,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)
         
            CALL MPI_SSEND(NROWS_RHS,1,MPI_INTEGER,MPIPROCNO,100,
     &           MPI_COMM_WORLD,IERROR)         
 

            DO J=1,NROWS_RHS
               IRNO=IROWNO_RHS(J,ITEMP)
               IROWARR_RHS(J)=IRNO
               RHSARR(J)=EXTRNRHS(J,ITEMP)
            ENDDO
         
            DO J=1,NROWS
               IRNO=IROWNO(J,ITEMP)
               IROWARR(J)=IRNO
               ICOLM=INDXCOLM(J,ITEMP)
               IEL=IELNO(J,ITEMP)
               IELARR(J)=IEL
!               RHSARR(J)=EXTRNRHS(J,ITEMP)   ! f-bar method
               IRINDX(J)=IROWINDX(J,ITEMP)
               IF(IFLAG.EQ.0)THEN
                  DO K=1,NODE*NDF
                     EXTRNMPI((J-1)*NODE*NDF+K)=EXTRNVAL(K,ICOLM)
                  ENDDO
               ENDIF
            ENDDO


            IF(NROWS.GT.0)THEN



               CALL MPI_SSEND(IROWARR,NROWS,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)

               CALL MPI_SSEND(IELARR,NROWS,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)

               CALL MPI_SSEND(IRINDX,NROWS,MPI_INTEGER,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)
     
               CALL MPI_SSEND(IROWARR_RHS,NROWS_RHS,MPI_INTEGER,
     &              MPIPROCNO,100,MPI_COMM_WORLD,IERROR)     

               CALL MPI_SSEND(RHSARR,NROWS,MPI_DOUBLE_PRECISION,
     &                   MPIPROCNO,100,MPI_COMM_WORLD,IERROR)

               IF(IFLAG.EQ.0)THEN
                  CALL MPI_SSEND(EXTRNMPI,NROWS*NODE*NDF,
     &                   MPI_DOUBLE_PRECISION,MPIPROCNO,100,
     &                   MPI_COMM_WORLD,IERROR)
               ENDIF

            ENDIF

         ENDIF


      ENDDO
      ENDDO


C*********** KOUROSH_INT ****************
      IF (IFLAG.EQ.0) VAL_STORE=VAL	
C*********** KOUROSH_INT ****************	
       
C************ KOUROSH_CONVERGENCE_CRITERION*********************
       F_EXT=0.D0
       DO I=1,N_UPDATE
              IRNO=IN_ST+I
              IF (IBC(IRNO).EQ.1) F_EXT(IRNO)=RESID(I)
       ENDDO
       
       F_EXT=F_EXT+F_APPL

       CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
       CALL MPI_ALLREDUCE(MPI_IN_PLACE,F_EXT,MDOFX, 
     &      MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)       
    
       
C************ KOUROSH_CONVERGENCE_CRITERION*********************       
	
      CALL REMOVE_DISPDOF(IBC,N_UPDATE,IN_ST,IROWPTR,IBINDX,VAL,
     &                     RESID,ISIZE,IFLAG)


      DO I=1,N_UPDATE
         F(I+IN_ST)=RESID(I)
      ENDDO

          

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
      CALL MPI_ALLREDUCE(F,F_RECV,NEQ,
     & MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)
    

      DO I=1,NEQ
         F(I)=F_RECV(I)
      ENDDO
      
 
             
      RETURN
      END 

************************************************
      SUBROUTINE EXCHN(A,B,N)   
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(1),B(1)
      DO 10 I=1,N
10    B(I)=A(I)
      RETURN
      END

**************************************************
      
      SUBROUTINE READNSET()
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      INTEGER:: NNSET(MAXNSET),NLIST(MAXNS,MAXNSET)
      INTEGER::MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      INTEGER::ISENDORRECV(MAXPROC)
      INTEGER:: N
      COMMON/CBNSET/NSET,NNSET,NLIST
      COMMON/PROCESSORINFO/IPROC,NPROCS,MPIINFSIZE,MPIINF,
     &                     IPROCINFO,ISENDORRECV

      CALL READSTR(LR)
      READ(LR,*)NSET
      
      IF(NSET.GT.MAXNSET)THEN
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'INSUFFICIENT MEMORY-MAXNSET'
         ENDIF
         STOP
      ENDIF
     
      DO I=1,NSET
         READ(LR,*)NSNO,NGEN
         IF(NGEN.EQ.0)THEN
            READ(LR,*)NUMN,(NLIST(J,NSNO),J=1,NUMN)
         ENDIF
                  
         IF(NGEN.EQ.1)THEN
            READ(LR,*)ISTART,IEND,INC
            NUMN=0
            DO N=ISTART,IEND,INC
               NUMN=NUMN+1
               NLIST(NUMN,NSNO)=N
            ENDDO
         ENDIF

         
         IF (NUMN.GT.MAXNS) THEN
         IF(IPROC.EQ.0)THEN
            WRITE(131,*)'INSUFFICIENT MEMORY-MAXNS'
         ENDIF
         STOP            
         ENDIF
         
         NNSET(NSNO)=NUMN
      ENDDO
      RETURN
      END

*******************************************************
      SUBROUTINE READELSET()
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION NELSET(MAXELSET),NELLIST(MAXELS,MAXELSET)
      COMMON/CBELSET/NESET,NELSET,NELLIST
      CALL READSTR(LR)
      READ(LR,*)NESET
      
      IF(NESET.GT.MAXELSET)THEN
         WRITE(131,*)'INSUFFICIENT MEMORY-MAXELSET'
         STOP
      ENDIF
     
      DO I=1,NESET
         READ(LR,*)NSNO,NGEN
         IF(NGEN.EQ.0)THEN
            READ(LR,*)NUMN,(NELLIST(J,NSNO),J=1,NUMN)
         ENDIF
         IF(NGEN.EQ.1)THEN
            READ(LR,*)ISTART,IEND,INC
            NUMN=0
            DO N=ISTART,IEND,INC
               NUMN=NUMN+1
               NELLIST(NUMN,NSNO)=N
            ENDDO
         ENDIF
         NELSET(NSNO)=NUMN
      ENDDO
      RETURN
      END

!**********************************************************
      FUNCTION DOTPRD(V1,V2,NBC1,IEQNO)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'     
      DIMENSION V1(1),V2(1),NBC1(1),NFAC(1),IEQNO(1)
      COMMON/BOUNDA/NBOUD1,NBOUD2,NBOUD3,NUPNCH
       COMMON/DOF/NDOF

      NDF=NDOF
C
      DOTPRD=FDOT(V1,V2)
      NDF1=NDF+1
      IF(NBOUD1.EQ.0)GO TO 15
      DO 10 N=1,NBOUD1
      NODE=NBC1(NDF1*(N-1)+1)
      DO 12 I=1,NDF
      INDEX=NBC1(NDF1*(N-1)+1+I)
      IF(INDEX.NE.1)GO TO 12
      NN=NDF*(NODE-1)+I
      NN=IEQNO(NN)
      DOTPRD=DOTPRD-V1(NN)*V2(NN)
12    CONTINUE
10    CONTINUE   
15    CONTINUE
      RETURN
      END

***************************************************************      
      SUBROUTINE ELST1(JELEM,NOFFSET,SKE_NonLocal,Fbar_TAU,Fbar_T,
     &   XBAR_ST,DVOLMAT_REPOSITORY,MaxElemInPatch,IELEM,
     &   SHPtauMAT_REPOSITORY,TotalElmsInPatch,NNODE,XEUN,SKE,FE,
     &   TIME,DELTME,PROPS1,NPROPS,NSVARS,NDOFEL,MDLOAD,MLVARX,
     &   NPREDF,MCRD,VTAU_E,KINC,IFLAG,NGAUSS,PNEWDT,FE_APPL,SVARS1,
     &   SVARS2,MAXSV,FLOW_RULE,PRESSURE_SCALE)   ! F-BAR METHOD

      IMPLICIT NONE
      INCLUDE 'PARDIS.H'

      INTEGER,PARAMETER:: NTENS=6,NSTR=6
      INTEGER::  I,J,K,L,JELEM,NNODE,IELEM,IEL        
      INTEGER::  MAXSV,NOFFSET,MaxElemInPatch,KRON,MM,NN,FLOW_RULE,
     &           RR,SS,NODEA,NODEB,DOFA,DOFB,TotalElmsInPatch   ! F-BAR METHOD
      REAL(8):: DVOLMAT_REPOSITORY(MaxElemInPatch),DDSDDE4D(3,3,3,3),
     &          Fbar_T_INV(3,3),dE_dF(3,3,3,3),CAUCHY(3,3),
     &          dFrel_dF(3,3,3,3),W(3,3,3,3),Fbar_TAU_INV(3,3),
     &          DKIRCHHOFF_DF(3,3,3,3),FIRST_TERM(3,3,3,3),
     &          SECOND_TERM(3,3,3,3),FIRST_ELASTICITY(3,3,3,3),
     &          FIRST_SPATIAL_ELASTICITY(3,3,3,3),Q4D(3,3,3,3),
     &          MAT1(3,1),MAT2(3,1),C_ELASTIC(3,3),Q2D(3,3),
     &          SHPtauMAT_REPOSITORY(3,4,MaxElemInPatch),TPK(3,3),
     &          SKE_NonLocal(12,12,MaxElemInPatch),TMP(1,1)     ! F-BAR METHOD
      REAL(8):: DET_FTAU,TOTALTIME  ! F-BAR METHOD 
      
      REAL(8)::  FE(MDOFE),SKE(MDOFE,MDOFE)
      REAL(8)::  XEUN(MAXDIM,MAXELN)
      REAL(8)::  SVARS(MSV)
      REAL(8)::  TIME(2)
      REAL(8)::  PROPS(NPROPS),PROPS1(MAXPROPS)
      REAL(8)::  SVARS1(MAXSV),COORDS(MCRD,NNODE),TIME1(2)
      REAL(8)::  SVARS2(MAXSV)
      REAL(8)::  VTAU_E(MDOFE)    
!-----------------------------------------------------------------------
      REAL(8):: STRESS(6),DDSDDE(NTENS,NTENS)
      
      REAL(8):: UE(MCRD,NNODE),Fbar_T(3,3),Fbar_TAU(3,3)                  
      REAL(8):: F1(MDOFE)

      REAL(8):: XE_TAU(MCRD,NNODE)                                                 


      REAL(8)::  XBAR_ST(2*MAXDIM,MDOFE,MAXNP)           

  
      REAL(8)::  DFGRDT(3,3),FTEMP(MDOFE) 
      REAL(8)::  XBAR(NTENS,MDOFE)
      REAL(8)::  STATEV(MAXSTATEV),PRESSB(MAXEL)

      INTEGER::  IBELEM(MAXEL),ILF(MAXEL),IFACE_TET(12)     
      INTEGER::  IFACE_BRICK(24),IBNODE(4)
      REAL(8):: XCG(3),UV1(3),UV2(3),UV3(3),UV4(3)
      REAL(8):: FLOAD(12)             
      REAL(8):: SKE_LOAD(12,12)

      REAL(8):: DELTME,PNEWDT,DET,XULR1,XULR2,XULR3 !,grainsize
      INTEGER:: NPROPS,NSVARS,NDOFEL,MDLOAD
      INTEGER:: MLVARX,NPREDF,MCRD,KINC,IFLAG
      INTEGER:: NGAUSS,ISTA
      INTEGER:: IFS,II
      REAL(8):: DTIME 
      INTEGER:: IPT,IN
      INTEGER:: IST,IS,ISTV_START
      INTEGER:: NELST,KSTEP,MCRD1,MDLOAD1
      INTEGER:: NDOFEL1,NELEND,NELNOX,NEQ
      INTEGER:: NELX,NODE,NPLANE,NPREDF1
      INTEGER:: NSTATV,NSV,NSVARS1
      INTEGER:: NX
      REAL(8):: PR,TEMP

!----------------------------------------------------------------------      
      COMMON/PRESS/PRESSB,IBELEM,ILF,IFACE_TET,IFACE_BRICK      
      COMMON/ABQ/TIME1,NSVARS1,NDOFEL1,MDLOAD1,NPREDF1,MCRD1  ! F-BAR METHOD   
!      COMMON/STATEUPDATE/SVARS2    ! F-BAR METHOD
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
      COMMON/EL_PAR/NELST,NELEND,NELNOX


C************ KOUROSH-PRESSURE ****************     
       REAL(8)::UV4_NORMAL(3)
       INTEGER::IDIR_PRESSURE
       REAL(8)::Q,ELEMENT_AREA,PRESSURE_SCALE
       COMMON/PRESSURE/IDIR_PRESSURE
C************ KOUROSH-PRESSURE ****************
       
       
       
C************ KOUROSH_CONVERGENCE_CRITERION*********************       
       REAL(8)::FE_APPL(MDOFE)
       
       
         FE_APPL=0.D0
C************ KOUROSH_CONVERGENCE_CRITERION*********************       

         DO I=1,NPROPS
            PROPS(I)=PROPS1(I)
         ENDDO

!      NSV=NSVARS*NGAUSS*(JELEM-1)-NSVARS*NGAUSS*(NELST-1)  ! F-BAR METHOD
         NSV=NOFFSET
         
         DO I=1,NSVARS*NGAUSS
            SVARS(I)=SVARS1(NSV+I)
         ENDDO


         
         DTIME=DELTME



         KSTEP=1
         PNEWDT=1.D0 
         
      
    
         SKE(1:MDOFE,1:MDOFE)=0.D0
         SKE_NonLocal=0.D0
         F1(1:MDOFE)=0.D0
         NGAUSS=1
      
      
         NSTATV=NSVARS
         DO IPT=1,NGAUSS

         ISTV_START=NSVARS*(IPT-1)
         DO ISTA=1,NSTATV
            STATEV(ISTA)=SVARS(ISTV_START+ISTA)
         ENDDO

         IF (FLOW_RULE.EQ.1) THEN
            CALL UMAT_CPFEM(STRESS,STATEV,PROPS,DDSDDE,NTENS,NSTATV,
     .                NPROPS,PNEWDT,Fbar_T,Fbar_TAU,KINC,DTIME,JELEM) ! KOUROSH
     
         ELSEIF (FLOW_RULE.EQ.2) THEN
         
            CALL UMAT_CPFEM(STRESS,STATEV,PROPS,DDSDDE,NSTATV,NPROPS,
     1                   PNEWDT,Fbar_T,Fbar_TAU,TOTALTIME,DTIME,KINC)        
         ENDIF
     

         IF(PNEWDT.LT.1.D0)RETURN
      
     

         DO ISTA=1,NSTATV
            SVARS(ISTV_START+ISTA)=STATEV(ISTA)
         ENDDO


         XBAR(1:NSTR,1:NDOFEL)=XBAR_ST(1:NSTR,1:NDOFEL,IPT)                
     &     *DVOLMAT_REPOSITORY(IELEM)
         FTEMP=MATMUL(TRANSPOSE(XBAR(1:NSTR,1:NDOFEL)),STRESS(1:NSTR))
         F1(1:NDOFEL)=F1(1:NDOFEL)+FTEMP(1:NDOFEL)


         CALL SIGMAT(STRESS,CAUCHY)
         CALL MATINV3_UEL(Fbar_TAU,Fbar_TAU_INV,DET_FTAU)
         CALL TR2TO4(DDSDDE,DDSDDE4D,1)
         
         TPK=DET_FTAU*MATMUL(MATMUL(Fbar_TAU_INV,CAUCHY),
     &                       TRANSPOSE(Fbar_TAU_INV))
         
         dE_dF=0.D0
         DO I=1,3
         DO J=1,3
         DO K=1,3
         DO L=1,3
            dE_dF(I,J,K,L)=0.5*(KRON(I,L)*Fbar_TAU(K,J)+
     &                                KRON(L,J)*Fbar_TAU(K,I))
         ENDDO
         ENDDO
         ENDDO
         ENDDO        
         
         
         FIRST_TERM=0.D0
         SECOND_TERM=0.D0


         DO I=1,3
         DO J=1,3
         DO K=1,3
         DO L=1,3
         
            FIRST_TERM(I,J,K,L)=KRON(I,K)*TPK(L,J)
            
            DO MM=1,3
            DO NN=1,3
            DO RR=1,3
            
               SECOND_TERM(I,J,K,L)=SECOND_TERM(I,J,K,L)+
     &            Fbar_TAU(I,MM)*DDSDDE4D(MM,J,NN,RR)*dE_dF(NN,RR,K,L)
            
            ENDDO
            ENDDO
            ENDDO
         ENDDO
         ENDDO
         ENDDO
         ENDDO
         


         FIRST_ELASTICITY=FIRST_TERM+SECOND_TERM         
         
 
         FIRST_SPATIAL_ELASTICITY=0.D0
         DO I=1,3
         DO J=1,3
         DO K=1,3
         DO L=1,3
            DO MM=1,3
            DO NN=1,3
               FIRST_SPATIAL_ELASTICITY(I,J,K,L)=
     &                       FIRST_SPATIAL_ELASTICITY(I,J,K,L)+1.D0/
     &                       DET_FTAU*Fbar_TAU(J,MM)*Fbar_TAU(L,NN)*
     &                       FIRST_ELASTICITY(I,MM,K,NN)
            ENDDO
            ENDDO
         ENDDO
         ENDDO
         ENDDO
         ENDDO


         FIRST_TERM=0.D0
         SECOND_TERM=0.D0
         DO I=1,3
         DO J=1,3
         DO K=1,3
         DO L=1,3

            DO MM=1,3
               FIRST_TERM(I,J,K,L)=FIRST_TERM(I,J,K,L)+
     &               FIRST_SPATIAL_ELASTICITY(I,J,MM,MM)*KRON(K,L)
            ENDDO
            SECOND_TERM(I,J,K,L)=CAUCHY(I,J)*KRON(K,L)
            
         ENDDO
         ENDDO
         ENDDO
         ENDDO
         Q4D=1.D0/3.D0*FIRST_TERM-2.D0/3.D0*SECOND_TERM



         DO NODEA=1,4
         MAT1(1:3,1)=SHPtauMAT_REPOSITORY(1:3,NODEA,IELEM)
         DO DOFA=1,3
         
         
            DO NODEB=1,4
            MAT2(1:3,1)=SHPtauMAT_REPOSITORY(1:3,NODEB,IELEM)
            DO DOFB=1,3
                       
               C_ELASTIC(1,1)=FIRST_SPATIAL_ELASTICITY(DOFA,1,DOFB,1)
               C_ELASTIC(1,2)=FIRST_SPATIAL_ELASTICITY(DOFA,1,DOFB,2)
               C_ELASTIC(1,3)=FIRST_SPATIAL_ELASTICITY(DOFA,1,DOFB,3)
               C_ELASTIC(2,1)=FIRST_SPATIAL_ELASTICITY(DOFA,2,DOFB,1)
               C_ELASTIC(2,2)=FIRST_SPATIAL_ELASTICITY(DOFA,2,DOFB,2)
               C_ELASTIC(2,3)=FIRST_SPATIAL_ELASTICITY(DOFA,2,DOFB,3)
               C_ELASTIC(3,1)=FIRST_SPATIAL_ELASTICITY(DOFA,3,DOFB,1)
               C_ELASTIC(3,2)=FIRST_SPATIAL_ELASTICITY(DOFA,3,DOFB,2)
               C_ELASTIC(3,3)=FIRST_SPATIAL_ELASTICITY(DOFA,3,DOFB,3)

               Q2D(1,1)=Q4D(DOFA,1,DOFB,1)
               Q2D(1,2)=Q4D(DOFA,1,DOFB,2)
               Q2D(1,3)=Q4D(DOFA,1,DOFB,3)
               Q2D(2,1)=Q4D(DOFA,2,DOFB,1)
               Q2D(2,2)=Q4D(DOFA,2,DOFB,2)
               Q2D(2,3)=Q4D(DOFA,2,DOFB,3)
               Q2D(3,1)=Q4D(DOFA,3,DOFB,1)
               Q2D(3,2)=Q4D(DOFA,3,DOFB,2)
               Q2D(3,3)=Q4D(DOFA,3,DOFB,3)
               
              TMP= 
     &        MATMUL(MATMUL(TRANSPOSE(MAT1),C_ELASTIC),MAT2)*
     &        DVOLMAT_REPOSITORY(IELEM) + 
     &        MATMUL(MATMUL(TRANSPOSE(MAT1),Q2D),MAT2)*
     &        DVOLMAT_REPOSITORY(IELEM)*
     &        (DVOLMAT_REPOSITORY(IELEM)/SUM(DVOLMAT_REPOSITORY)-1.D0)
     
               SKE((NODEA-1)*3+DOFA,(NODEB-1)*3+DOFB)=TMP(1,1)
            ENDDO
            ENDDO
            
            
            
            DO I=1,TotalElmsInPatch
                
                IF (I.NE.IELEM) THEN

                    DO NODEB=1,4
                        
                        MAT2(1:3,1)=SHPtauMAT_REPOSITORY(1:3,NODEB,I)
                    
                        
                        DO DOFB=1,3
                            
                            Q2D(1,1)=Q4D(DOFA,1,DOFB,1)
                            Q2D(1,2)=Q4D(DOFA,1,DOFB,2)
                            Q2D(1,3)=Q4D(DOFA,1,DOFB,3)
                            Q2D(2,1)=Q4D(DOFA,2,DOFB,1)
                            Q2D(2,2)=Q4D(DOFA,2,DOFB,2)
                            Q2D(2,3)=Q4D(DOFA,2,DOFB,3)
                            Q2D(3,1)=Q4D(DOFA,3,DOFB,1)
                            Q2D(3,2)=Q4D(DOFA,3,DOFB,2)
                            Q2D(3,3)=Q4D(DOFA,3,DOFB,3)
                   
                   TMP=
     &               MATMUL(MATMUL(TRANSPOSE(MAT1),Q2D),MAT2)*
     &              DVOLMAT_REPOSITORY(IELEM)*
     &             (DVOLMAT_REPOSITORY(I)/SUM(DVOLMAT_REPOSITORY))
     
           SKE_NonLocal((NODEA-1)*3+DOFA,(NODEB-1)*3+DOFB,I)=TMP(1,1)
                            
                        ENDDO
                    ENDDO
                ENDIF
            ENDDO            

            
            
         ENDDO
         ENDDO

      ENDDO


      IF(IBELEM(JELEM).NE.0)THEN
	


         DO I=1,MCRD
            DO J=1,NNODE
               COORDS(I,J)=XEUN(I,J)
            ENDDO
         ENDDO

         UE(1:MCRD,1:NNODE)=RESHAPE(VTAU_E,(/MCRD,NNODE/))
         XE_TAU=COORDS+UE
   
         IFS=IBELEM(JELEM)
         IST=3*(IFS-1)
         IBNODE(1:3)=IFACE_TET(IST+1:IST+3)


         XCG(1:3)=1.D0/4.D0*SUM(XE_TAU(1:3,1:4),2)
         UV1(1:3)=XCG-XE_TAU(1:3,IBNODE(1))
         UV2(1:3)=XE_TAU(1:3,IBNODE(2))-XE_TAU(1:3,IBNODE(1))
         UV3(1:3)=XE_TAU(1:3,IBNODE(3))-XE_TAU(1:3,IBNODE(1))


         CALL CROSS(UV4,UV3,UV2)
         CALL DOT3(TEMP,UV1,UV4)

         IF(ILF(JELEM).EQ.1)THEN
         CALL DLOAD(JELEM,PR,TIME)
         ENDIF

          
C************ KOUROSH-PRESSURE ****************          
         CALL DOT3(TEMP,UV4,UV4)
         UV4_NORMAL=UV4/DSQRT(TEMP)
         Q=PR*PRESSURE_SCALE
         ELEMENT_AREA=DSQRT(TEMP)*DABS(UV4_NORMAL(IDIR_PRESSURE))/2.D0
         FLOAD=0.D0
         FLOAD(IDIR_PRESSURE)=Q*ELEMENT_AREA/3.D0
         FLOAD(3+IDIR_PRESSURE)=Q*ELEMENT_AREA/3.D0
         FLOAD(6+IDIR_PRESSURE)=Q*ELEMENT_AREA/3.D0

         DO I=1,3
            IN=IBNODE(I)
            F1(3*IN-2:3*IN)=F1(3*IN-2:3*IN)-FLOAD(3*I-2:3*I)  
            FE_APPL(3*IN-2:3*IN)=FLOAD(3*I-2:3*I)   ! KOUROSH_CONVERGENCE_CRITERION
         ENDDO
C************ KOUROSH-PRESSURE ****************  

      ENDIF



      FE(1:NDOFEL)=-F1

      
      DO I=1,NSVARS*NGAUSS
         SVARS2(NSV+I)=SVARS(I)
      ENDDO


      RETURN
      END


****************************************************
      FUNCTION DOT(A,B,N)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(1),B(1)
      DOT=0.D0
      DO 100 I=1,N
  100 DOT=DOT+A(I)*B(I)
      RETURN
      END
****************************************************
      FUNCTION FDOT(A,B)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A(1),B(1)
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
      FDOT=0.D0
      DO 100 I=1,NEQ
  100 FDOT=FDOT+A(I)*B(I)
      RETURN
      END
****************************************************
      SUBROUTINE GET_SUBSCRIPT(IPROC,FNAME)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      CHARACTER*30 FNAME
      DIMENSION IN(10),INTMP(10)                                          !  <---------------------
      
      IPROC1=IPROC
      I=0

      DO WHILE(IPROC1.GT.9)
         I1=IPROC1/10
         I2=IPROC1-I1*10
         I=I+1
         IPROC1=I1
         IN(I)=I2
      ENDDO

      I=I+1
      IN(I)=IPROC1

      DO J=1,I
         INTMP(I-J+1)=IN(J)
      ENDDO

      DO J=1,I
         IN(J)=INTMP(J)
      ENDDO

      IF(I.EQ.1)THEN
         FNAME='_'//ACHAR(48+IN(1))//'.OUT'
      ELSEIF(I.EQ.2)THEN
         FNAME='_'//ACHAR(48+IN(1))//ACHAR(48+IN(2))//'.OUT'
      ELSE
         FNAME='_'//ACHAR(48+IN(1))//ACHAR(48+IN(2))//ACHAR(48+IN(3))
     &   //'.OUT'
      ENDIF
      
      
      RETURN
      END
!---------------------------------------------------------------
! RETURNS TIME BASED ON DWELL LOAD SUCH THAT A TIME POINT IS PRESENT 
! AT THE CHANGE POINTS
      SUBROUTINE TDWELL(FINTME,DELTME,XIDT,XMINDT,NSEG,
     &           TSEG,SHARP_POINT) !****************** KOUROSH_LOADING_UNLOADING *
      IMPLICIT REAL*8(A-H,O-Z)

      DIMENSION TSEG(NSEG+1)
      LOGICAL::SHARP_POINT !****************** KOUROSH_LOADING_UNLOADING *

      COMMON/LOAD_DWELL/TPERIOD,TRAMP,T_NODWELL,T_DWELL,SAMP,SMIN,
     &       UAMP,UMIN
      COMMON/LOAD_MONO/SLOAD,ST_RATE
      COMMON/ICYC_FLAG/ICYC

       
      XTOL=1D-6

      SHARP_POINT=.FALSE.  !****************** KOUROSH_LOADING_UNLOADING *
      
      IF(TRAMP-FINTME.GT.XTOL)THEN

         FINTME1=FINTME+DELTME+XMINDT
         IF(FINTME1.GE.TRAMP)THEN
            DELTME=TRAMP-FINTME
         ENDIF



      ELSE

         FINTME2=FINTME-TRAMP

         REM=DMOD(FINTME2,TPERIOD)

         IF(DABS(REM).LT.XTOL.OR.DABS(REM-TPERIOD).LT.XTOL)THEN
            XNCYC=FINTME/TPERIOD
         ELSE
            XNCYC=(FINTME2-REM)/TPERIOD
         ENDIF
 
         T=FINTME2-XNCYC*TPERIOD


         DO I=1,NSEG

            TST=TSEG(I)
            TEND=TSEG(I+1)

            TEQ=T-TST


         IF((TEQ.GT.XTOL.OR.DABS(TEQ).LT.XTOL).AND.
     &      (TEND-T).GT.XTOL)THEN


               T1=T+DELTME+XMINDT
               T1EQ=T1-TEND

               IF(T1EQ.GT.XTOL.OR.DABS(T1EQ).LT.XTOL)DELTME=TEND-T
               IF(DABS(TEQ).LT.XTOL)DELTME=XIDT


            ENDIF


         ENDDO

      ENDIF
      
      
C****************** KOUROSH_LOADING_UNLOADING *************************
       FINTME2=FINTME-TRAMP
       REM=DMOD(FINTME2,TPERIOD)
       
       IF (REM.LT.XTOL.AND.FINTME.GT.TRAMP) SHARP_POINT=.TRUE.  ! CORRESPONDS TO THE POINT WHERE MINIMUM PRESSURE IS APPLIED
       
       FINTME2=FINTME-TRAMP+TSEG(NSEG+1)-TSEG(NSEG)
       REM=DMOD(FINTME2,TPERIOD)
       IF (REM.LT.XTOL.AND.FINTME.GT.TRAMP) SHARP_POINT=.TRUE.  ! CORRESPONDS TO THE POINT WHERE THE STRESS GOES FROM PLATEAU TO UNLOADING
C****************** KOUROSH_LOADING_UNLOADING *************************      
      
      RETURN
      END

!----------------------------------------------------------------------------------
      SUBROUTINE BROY_PARALLEL(DVTAU,VAR,WAR,NUMUPD,NMPD1,MAXREF,
     &             IN_ST,N_UPDATE,NEQ,STEP,IPROC)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      INCLUDE 'mpif.h'
      DIMENSION DVTAU(MDOFX),VAR(*),WAR(*)
      ALLOCATABLE::V(:),W(:),V_RECV(:),W_RECV(:)
 
      ALLOCATE(V(NEQ),W(NEQ),V_RECV(NEQ),W_RECV(NEQ))
      V(1:NEQ)=0.D0
      W(1:NEQ)=0.D0
      V_RECV(1:NEQ)=0.D0
      W_RECV(1:NEQ)=0.D0

      STEP1=1.D0/STEP


      IF(NUMUPD.EQ.0)GOTO 30

      DO I=1,NUMUPD
         NN=(I-1)*N_UPDATE

         V(1:NEQ)=0.D0
         W(1:NEQ)=0.D0

         V(IN_ST+1:IN_ST+N_UPDATE)=VAR(NN+1:NN+N_UPDATE)
         W(IN_ST+1:IN_ST+N_UPDATE)=WAR(NN+1:NN+N_UPDATE)
      
         CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
         CALL MPI_ALLREDUCE(V,V_RECV,NEQ,MPI_DOUBLE_PRECISION,
     &    MPI_SUM,MPI_COMM_WORLD,IERROR)
        
         CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
         CALL MPI_ALLREDUCE(W,W_RECV,NEQ,MPI_DOUBLE_PRECISION,
     &    MPI_SUM,MPI_COMM_WORLD,IERROR)
      

         V(1:NEQ)=V_RECV(1:NEQ)
         W(1:NEQ)=W_RECV(1:NEQ)

         DW=DOT_PRODUCT(DVTAU(1:NEQ),W)

         DVTAU(1:NEQ)=DVTAU(1:NEQ)+DW*V(1:NEQ)
      ENDDO
 30   CONTINUE

      NUMUPD=NUMUPD+1
      
      W(1:NEQ)=0.D0
      W(IN_ST+1:IN_ST+N_UPDATE)=WAR(NMPD1+1:NMPD1+N_UPDATE)

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
      CALL MPI_ALLREDUCE(W,W_RECV,NEQ,MPI_DOUBLE_PRECISION,
     & MPI_SUM,MPI_COMM_WORLD,IERROR)

      W(1:NEQ)=W_RECV(1:NEQ)

      V(1:NEQ)=STEP1*W(1:NEQ)-DVTAU(1:NEQ)

      FAC1=DOT_PRODUCT(V,W)

      FAC=1.D0/FAC1


      V(1:NEQ)=(W(1:NEQ)-V(1:NEQ))*FAC

      VAR(NMPD1+1:NMPD1+N_UPDATE)=V(IN_ST+1:IN_ST+N_UPDATE)

      DW=DOT_PRODUCT(DVTAU(1:NEQ),W)

      DVTAU(1:NEQ)=DVTAU(1:NEQ)+DW*V(1:NEQ)

      DEALLOCATE(V,W,V_RECV,W_RECV)

      RETURN
      END
!--------------------------------------------------------------------



      SUBROUTINE FROB_NORM(X,N,XNORM)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION X(N,N)
      
      XNORM=0.D0
      DO I=1,N
         DO J=1,N
            XNORM=XNORM+X(I,J)**2.D0
         ENDDO
      ENDDO

      XNORM=DSQRT(XNORM)
      RETURN
      END 
! RETURNS TIME BASED FOR CREEP LOAD OR CONSTANT STRAIN RATE PROBLEM
      SUBROUTINE T_CREEP_CSTRAINRATE(FINTME,DELTME,XIDT,XMINDT,XMAXDT)
      IMPLICIT REAL*8(A-H,O-Z)

      COMMON/LOAD_DWELL/TPERIOD,TRAMP,T_NODWELL,T_DWELL,SAMP,SMIN,
     &       UAMP,UMIN
      COMMON/LOAD_MONO/SLOAD,ST_RATE
      COMMON/ICYC_FLAG/ICYC

      IF(DELTME.GT.XMAXDT)THEN
         DELTME=XMAXDT
      ENDIF

       
      XTOL=1D-6

      
      IF(TRAMP-FINTME.GT.XTOL)THEN

         FINTME1=FINTME+DELTME+XMINDT
         IF(FINTME1.GE.TRAMP)THEN
            DELTME=TRAMP-FINTME
         ENDIF

      ENDIF

      RETURN      
      END


!----------------------------------------------------------------------------------

!*******************************************************************
!     CREATES A SEQUENCE OF PROCESSOR PAIRS COMMUNICATING(SEND/RECV)
!     OBJECTIVE: REDUCES WAITING TIME FOR COMMUNICATING PROCESSOR
!*******************************************************************
      SUBROUTINE PROC_COMB(NPROC,MPIINF,MPIINFSIZE)
      IMPLICIT REAL*8 (A-H,O-Z)
      INCLUDE 'PARDIS.H'

      DIMENSION MPIINF(MPIINFSIZEMAX)
      DIMENSION IPARR1(MAXCOMM,2),IPARR(2)

      ALLOCATABLE::IGRPARR(:,:),IGRPNDX(:)
      
!*******************************************************************
!     NPROC(I)-NUMBER OF PROCESSORS
!     MPIINF(O)-PROCESSOR NUMBERS IN PAIRS. EX - (0123..), MEANS 
!     01,23,.. ARE DIFFERENT COMMUNICATING PAIRS
!     MPIINFSIZE(O)-LENGTH OF ARRAY MPIINF
!*******************************************************************

      NSIZE=NPROC*(NPROC-1)

      IF(NSIZE.GT.MAXCOMM)THEN
        WRITE(*,*)'INCRESE MAXCOMM TO',NSIZE
        STOP
      ENDIF

      
      ILEVEL=0
      I2=NPROC
      DO WHILE(I2.GT.1)
         ILEVEL=ILEVEL+1
         I1=I2/2
         I2=I2-I1
      ENDDO


      ALLOCATE(IGRPARR(MAXCOMM,ILEVEL),IGRPNDX(ILEVEL))
         
      DO I=1,ILEVEL
            
         IF(I.EQ.1)THEN
           I1=NPROC/2
           I2=NPROC-I1
           IGRPNDX(I)=2
           IGRPARR(1,I)=I2
           IGRPARR(2,I)=I1
               
         ELSE
           ILOOP=IGRPNDX(I-1)
           ICNT=0
               
           DO J=1,ILOOP
              IP1=IGRPARR(J,I-1)
              IF(IP1.GT.1)THEN
                 I1=IP1/2
                 I2=IP1-I1
                 ICNT=ICNT+1
                 IGRPARR(ICNT,I)=I2
                 ICNT=ICNT+1
                 IGRPARR(ICNT,I)=I1
              ELSE
                 ICNT=ICNT+1
                 IGRPARR(ICNT,I)=0
              ENDIF
           ENDDO

           IGRPNDX(I)=ICNT

         ENDIF

      ENDDO

      IGNDX=0
      DO I=1,ILEVEL
         ITEMP=1
         IPROCBEGIN=0

         DO J=1,IGRPNDX(I)

            IF(ITEMP.LE.2)THEN
               IP1=IGRPARR(J,I)
               IF(IP1.EQ.0) THEN
                  IPROCBEGIN=IPROCBEGIN+1
                  GOTO 130
               ELSE
                  IPARR(ITEMP)=IGRPARR(J,I)
                  DO K=0,IPARR(ITEMP)-1
                     IPARR1(K+1,ITEMP)=IPROCBEGIN
                     IPROCBEGIN=IPROCBEGIN+1
                  ENDDO
                  ITEMP=ITEMP+1
               ENDIF
 130        ENDIF
   
            IF(ITEMP.GT.2)THEN
               ITEMP=1
               INDX1=1
               INDX2=2
               IF(IPARR(1).GT.IPARR(2)) THEN
                  INDX1=2
                  INDX2=1
               ENDIF

               DO K=0,IPARR(INDX2)-1
                  DO L=1,IPARR(INDX1)
                     IGNDX=IGNDX+1
                     MPIINF(IGNDX)=IPARR1(L,INDX1)
                     IGNDX=IGNDX+1
                     I1=IPARR(INDX2)
                     I2=L+K
                     I3=(I2-1)/I1
                     ILOC=I2-I3*I1
                     MPIINF(IGNDX)=IPARR1(ILOC,INDX2)
                  ENDDO
               ENDDO
                  
            ENDIF

         ENDDO
      ENDDO

      MPIINFSIZE=IGNDX

      IF(MPIINFSIZE.GT.MPIINFSIZEMAX)THEN
         WRITE(*,*)'MPIINFSIZE',MPIINFSIZE,MPIINFSIZEMAX
         STOP
      ENDIF

      DEALLOCATE(IGRPARR,IGRPNDX)

      END
!***************************END*****************************************
!***********************************************************************
!     EACH PROCESSOR IDENTIFIES ITS SEQUENCE OF COMMUNICATION WITH
!     OTHER PROCESSORS FORM MPIINF ARRAY
!***********************************************************************
      SUBROUTINE ARRANGE_PROC(IPROCINFO,ISENDORRECV,MPIINF,           
     &			MPIINFSIZE,IPROC)

      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION  MPIINF(MPIINFSIZEMAX),IPROCINFO(MAXPROC)
      DIMENSION ISENDORRECV(MAXPROC)
!***********************************************************************
!     IPROCINFO(O)-ARRAY CONTAINING PROCESSOR NUMBERS ARRANGED IN BEST
!     COMMUNICATION SEQUENCE
!     ISENDORRECV(O)-ARRAY CONTAING TAG WHICH DECIDES WHETHER PROC A WOULD
!     SEND AND THEN RECEIVE FROM B, OR VICE VERSA
!     TAG 1 - SEND FIRST
!     TAG 2 - RECEIVE FIRST
!     MPIINF(I)
!      MPIINFSIZE(I)
!     IPROC(I)
!***********************************************************************
      INDX=0

      DO I=1,MPIINFSIZE,2
	 IP1=MPIINF(I)
	 IP2=MPIINF(I+1)
	 IF(IP1.EQ.IPROC)THEN
	    INDX=INDX+1
	    IPROCINFO(INDX)=IP2
	    ISENDORRECV(INDX)=1
         ELSEIF(IP2.EQ.IPROC)THEN
            INDX=INDX+1
            IPROCINFO(INDX)=IP1
            ISENDORRECV(INDX)=2
	 ENDIF
      ENDDO

      END
!***********************END*********************************************



      SUBROUTINE NODE_ELEM_CONNEC_SCAN(IJK,NDF,IELEMNO_SIZE,
     &        ElementsInPatch,NElementsInPatch,MaxElemInPatch,NPATCH)    !F-BAR METHOD
      IMPLICIT NONE
      INCLUDE 'PARDIS.H'
      INTEGER::NX,NELX,NEQ,NPLANE,NODE,NDF,NPATCH,MaxElemInPatch,
     &         IPATCH,IELEM,JELEM,ELEMI,ELEMJ,INODE,NODEI,I,J,K,ITEMP,
     &         IELEMNO_SIZE
      
      INTEGER::ElementsInPatch(NPATCH,MaxElemInPatch),
     &         NElementsInPatch(NPATCH),
     &         NelementsAlreadyConsidered(NX),
     &         IJK(mnelx),
     &         IELEMNO1(MAXNODE,MAXADJGRAINELEM*MaxElemInPatch)
      LOGICAL::ALREADY_CONSIDERED
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
!***********************************************************************
!C     IELEMNO: STORES ELEMENT NUMBERS
!C     NINDX: STORES START AND END INDEX IN IELEMNO ARRAY FOR
!C            EACH NODE
!***********************************************************************
      NelementsAlreadyConsidered=0
      IELEMNO1=0
      
      DO IPATCH=1,NPATCH
         DO IELEM=1,NElementsInPatch(IPATCH)
            ELEMI=ElementsInPatch(IPATCH,IELEM)
            DO INODE=1,4
               NODEI=IJK((ELEMI-1)*4+INODE)
               DO JELEM=1,NElementsInPatch(IPATCH)
               
                  ELEMJ=ElementsInPatch(IPATCH,JELEM)
                  ALREADY_CONSIDERED=.FALSE.
                  DO I=1,NelementsAlreadyConsidered(NODEI)
                     IF(IELEMNO1(NODEI,I).EQ.ELEMJ) THEN
                        ALREADY_CONSIDERED=.TRUE.
                        EXIT
                     ENDIF
                  ENDDO
                  
                  IF (.NOT.ALREADY_CONSIDERED) THEN
                    NelementsAlreadyConsidered(NODEI)=
     &                             NelementsAlreadyConsidered(NODEI)+1
                    IELEMNO1(NODEI,NelementsAlreadyConsidered(NODEI))=
     &                             ELEMJ
                  ENDIF
                  
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      IELEMNO_SIZE=MAXNODE*MAXVAL(NelementsAlreadyConsidered)

      RETURN
      END













!***********************************************************************
!C     CREATES LINEAR ARRAY OF ELEMENT NUMBERS FOR EACH NODE
!***********************************************************************      
      SUBROUTINE NODE_ELEM_CONNEC(IJK,NDF,IELEMNO,NINDX,
     &           ElementsInPatch,IELEMNO_SIZE,NElementsInPatch,
     &           MaxElemInPatch,NPATCH)    !F-BAR METHOD
      IMPLICIT NONE
      INCLUDE 'PARDIS.H'
      INTEGER::NX,NELX,NEQ,NPLANE,NODE,NDF,NPATCH,MaxElemInPatch,
     &         IPATCH,IELEM,JELEM,ELEMI,ELEMJ,INODE,NODEI,I,J,K,ITEMP,
     &         IELEMNO_SIZE
      
      INTEGER::ElementsInPatch(NPATCH,MaxElemInPatch),
     &         NElementsInPatch(NPATCH),
     &         NelementsAlreadyConsidered(NX),
     &         IJK(mnelx),NINDX(MAXNODE),
     &         IELEMNO(IELEMNO_SIZE),
     &         IELEMNO1(MAXNODE,IELEMNO_SIZE/MAXNODE)
      LOGICAL::ALREADY_CONSIDERED
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
!***********************************************************************
!C     IELEMNO: STORES ELEMENT NUMBERS
!C     NINDX: STORES START AND END INDEX IN IELEMNO ARRAY FOR
!C            EACH NODE
!***********************************************************************
      NelementsAlreadyConsidered=0
      IELEMNO1=0
      
      DO IPATCH=1,NPATCH
         DO IELEM=1,NElementsInPatch(IPATCH)
            ELEMI=ElementsInPatch(IPATCH,IELEM)
            DO INODE=1,4
               NODEI=IJK((ELEMI-1)*4+INODE)
               DO JELEM=1,NElementsInPatch(IPATCH)
               
                  ELEMJ=ElementsInPatch(IPATCH,JELEM)
                  ALREADY_CONSIDERED=.FALSE.
                  DO I=1,NelementsAlreadyConsidered(NODEI)
                     IF(IELEMNO1(NODEI,I).EQ.ELEMJ) THEN
                        ALREADY_CONSIDERED=.TRUE.
                        EXIT
                     ENDIF
                  ENDDO
                  
                  IF (.NOT.ALREADY_CONSIDERED) THEN
                    NelementsAlreadyConsidered(NODEI)=
     &                             NelementsAlreadyConsidered(NODEI)+1
                    IELEMNO1(NODEI,NelementsAlreadyConsidered(NODEI))=
     &                             ELEMJ
                  ENDIF
                  
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      
      
! MAKING SURE THAT TWO SIMILAR ELEMENTS ARE NOT IN IELEMNO
      DO I=1,NX
         DO J=1,NelementsAlreadyConsidered(I)
            DO K=1,NelementsAlreadyConsidered(I)
               
               IF (J.NE.K) THEN
                  IF (IELEMNO1(I,J).EQ.IELEMNO1(I,K)) THEN
                     WRITE(*,*) 'ERROR IN IELEMNO CONSTRUCTION'
                     STOP
                  ENDIF
               ENDIF
               
            ENDDO
         ENDDO
      ENDDO


      
      IF ((MAXADJGRAINELEM*MaxElemInPatch).LT.
     &     MAXVAL(NelementsAlreadyConsidered)) THEN
            
            WRITE(*,*) 'MAXADJGRAINELEM*MaxElemInPatch IS NOT ENOUGH'
            STOP
      ENDIF

      ITEMP=1
      DO I=1,NX
         DO J=1,NelementsAlreadyConsidered(I)
            IELEMNO(ITEMP)=IELEMNO1(I,J)
            ITEMP=ITEMP+1
         END DO
         NINDX(I)=ITEMP
      END DO

      RETURN
      END

!*************************END*******************************************
!***********************************************************************
!C     INITIALIZES SIZE OF MODIFIED SPARSE ROW FORMAT
!***********************************************************************
      SUBROUTINE INIT_SIZE_SLU(IJK,NDF,IPROC,N_UPDATE,IN_ST,
     &               IELEMNO_SIZE,ISIZE,IELEMNO,NINDX)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
       DIMENSION IJK(MNELX),IELEMNO(IELEMNO_SIZE),NINDX(MAXNODE)
       DIMENSION INCOMP(MAXNODE)
!***********************************************************************
! C     IJK(I)
! C     N_UPDATE(I)
! C     NDF(I)
!***********************************************************************

       INDX=1

       DO I=1,N_UPDATE

          NNO=(IN_ST+I-1)/NDF+1
          ICOMP=0
          
          IF(NNO.EQ.1)THEN
            ISTART=1
          ELSE
            ISTART=NINDX(NNO-1)
          ENDIF

          IEND=NINDX(NNO)-1
          
          DO  J=ISTART,IEND

              IELNO=IELEMNO(J)

              DO K=1,NODE

                 IMATCH=0
                 NN=IJK((IELNO-1)*NODE+K)

                 DO LL=1,ICOMP
                    IF(NN.EQ.INCOMP(LL))THEN
                       IMATCH=1
                       GOTO 30
                    ENDIF
                 ENDDO

  30             IF(IMATCH.EQ.0)THEN

                    ICOMP=ICOMP+1
                    INCOMP(ICOMP)=NN

                 ENDIF

             ENDDO
          ENDDO



          DO J=1,NX
             DO K=1,ICOMP
                IF(J.EQ.INCOMP(K))THEN
                   DO L=1,NDF
                      INDX=INDX+1
                   ENDDO
                ENDIF
             ENDDO
          ENDDO


      ENDDO

      ISIZE=INDX-1

      END
      
      
!*************************END*******************************************
!***********************************************************************
!C     INITIALIZES MODIFIED SPARSE ROW FORMAT
!***********************************************************************
      SUBROUTINE INIT_MSR_SLU(IJK,NDF,IPROC,N_UPDATE,IN_ST,    
     & IELEMNO_SIZE,IROWPTR,IBINDX,ISIZE,IELEMNO,NINDX)
!      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
      INCLUDE 'PARDIS.H'
      
      INTEGER:: IELEMNO_SIZE
      INTEGER:: IJK(MNELX),NINDX(MAXNODE),IELEMNO(IELEMNO_SIZE)
      INTEGER:: INCOMP(MAXNODE)
!***********************************************************************
! C     IJK(I)
! C     N_UPDATE(I)
! C     IUPDATE(I)-CONTAINS ROWS TO BE UPDATED BY THIS PROCESSOR
! C     IBINDX(O)-ARRAY CONTAINING NON-ZERO COLUMN INDICES FOR EACH ROW
! C     CONSISTENT WITH AZTEC
! C     VAL(O)-ARRAY WITH ELEMENTS INITIALIZED TO ZERO
! C     NDF(I)
!***********************************************************************
      INTEGER:: IBINDX(ISIZE),IROWPTR(N_UPDATE+1)
      INTEGER:: N_UPDATE,IMATCH,IEND,IELNO
      INTEGER:: ICOMP,NNO,K
      INTEGER::ISTART, NN,INDX,LL
      INTEGER:: IN_ST,NDF,I,NODE
      INTEGER:: J,NX,L,IPROC
      INTEGER:: ISIZE,NELX,NEQ,NPLANE 
      
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE

      
      INDX=1
      
      DO I=1,N_UPDATE

         NNO=(IN_ST+I-1)/NDF+1
         ICOMP=0
         IROWPTR(I)=INDX
          
          IF(NNO.EQ.1)THEN
            ISTART=1
          ELSE
            ISTART=NINDX(NNO-1)
          ENDIF

          IEND=NINDX(NNO)-1
          DO  J=ISTART,IEND

              IELNO=IELEMNO(J)

              DO K=1,NODE

                 IMATCH=0
                 NN=IJK((IELNO-1)*NODE+K)

                 DO LL=1,ICOMP
                    IF(NN.EQ.INCOMP(LL))THEN
                       IMATCH=1
                       GOTO 30
                    ENDIF
                 ENDDO

  30             IF(IMATCH.EQ.0)THEN

                    ICOMP=ICOMP+1
                    INCOMP(ICOMP)=NN

                 ENDIF

             ENDDO
          ENDDO



          DO J=1,NX
             DO K=1,ICOMP
                IF(J.EQ.INCOMP(K))THEN
                   DO L=1,NDF
                      IBINDX(INDX)=(J-1)*NDF+L
                      INDX=INDX+1
                   ENDDO
                ENDIF
             ENDDO
          ENDDO


      ENDDO
      

      IROWPTR(N_UPDATE+1)=INDX-1

      END
      
      
      
!*************************END*******************************************
!***********************************************************************
!C     FINDS PROC NO ASSOCIATED WITH A NODE CONSIDERING LINEAR
!C     PARTITIONING OF GRID POINTS
!***********************************************************************
      SUBROUTINE FINDPROC(NNO,NPROCS,NODEINPROC,NDF)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
!***********************************************************************
!C     NNO(I)-NODE NUMBER
!C     NPROCS-NUMBER OF PROCESSORS
!C     NODEINPROC(O)-PROCESSOR NUMBER TO WHICH NODE IS ASSOCIATED
!***********************************************************************

      I1=NX/NPROCS+1
      I2=NX/NPROCS

      IA=NX-I2*NPROCS
      IB=NPROCS-IA


      IF(NNO.LE.I1*IA)THEN
         NODEINPROC=(NNO-1)/I1
      ELSE
         NNO1=NNO-I1*IA
         NODEINPROC=IA+(NNO1-1)/I2
      ENDIF


      END
!*************************END*******************************************
C************** F-bar Method **************************************
      SUBROUTINE UPDATE_LOCAL_ARRAY(IJK, NROWS,RECVVAL,RECVRHS,
     & IROWARR,IELARR,IRECVRINDX,NDF,IPROC,N_UPDATE,IN_ST,IROWPTR,
     & IBINDX,VAL,RESID,ISIZE,IFLAG,
     & IRECVROW_RHS,NRECVROWS_RHS)

      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION IJK(MNELX)
      DIMENSION IROWARR(MDOFPROC),IELARR(MDOFPROC),
     &          RECVVAL(MDOFPROC*MAXDIM*4),RECVRHS(MDOFPROC),
     &          IRECVRINDX(MDOFPROC)
      DIMENSION ICOLMARR(MDOFE)

      DIMENSION IRECVROW_RHS(MDOFPROC)

      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE
      DIMENSION IBINDX(ISIZE),VAL(ISIZE),RESID(N_UPDATE),
     & IROWPTR(N_UPDATE+1)



      DO I=1,NROWS
         IROWNO=IROWARR(I)
         IELNO=IELARR(I)


         DO J=1,NODE
            NNO=IJK((IELNO-1)*NODE+J)
            DO K=1,NDF
               ITEMP=(NNO-1)*NDF+K
               ICOLMARR((J-1)*NDF+K)=ITEMP
            ENDDO
         ENDDO

         IROW1=IROWNO-IN_ST
         IS=IROWPTR(IROW1)
         IE=IROWPTR(IROW1+1)-1
               
         IF(IROW1.EQ.N_UPDATE)IE=IROWPTR(N_UPDATE+1)

         IF(IFLAG.EQ.0)THEN
               
            DO K=IS,IE
               ICOLM=IBINDX(K)
               DO KK=1,NODE*NDF
                  IF(ICOLM.EQ.ICOLMARR(KK))THEN
                     VAL(K)=VAL(K)+RECVVAL((I-1)*NDF*NODE+KK)
                     GOTO 120
                  ENDIF
               ENDDO
 120        ENDDO
         ENDIF




      ENDDO

      DO I=1,NRECVROWS_RHS
         IROWNO=IRECVROW_RHS(I)
         IROW1=IROWNO-IN_ST
         RESID(IROW1)=RESID(IROW1)+RECVRHS(I)
      ENDDO
         


      END
C************** F-bar Method **************************************
      
      
      
!*************************END*******************************************
!***********************************************************************

      SUBROUTINE REMOVE_DISPDOF(IBC,N_UPDATE,IN_ST,IROWPTR,IBINDX,VAL,  
     &             RESID,ISIZE,IFLAG)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      DIMENSION IBC(MDOFX)
      COMMON/ELEMNT/NX,NELX,NEQ,NPLANE,NODE

      DIMENSION IBINDX(ISIZE),VAL(ISIZE),RESID(N_UPDATE),               
     &            IROWPTR(N_UPDATE+1)

      XMAX_REAC=0.D0

      DO I=1,N_UPDATE
         IROWNO=IN_ST+I

         IS=IROWPTR(I)
         IE=IROWPTR(I+1)-1
               
         IF(I.EQ.N_UPDATE)IE=IROWPTR(N_UPDATE+1)
         
         IF(IBC(IROWNO).EQ.1)THEN
            IF(IFLAG.EQ.0)THEN
               DO J=IS,IE
C*********** KOUROSH_INT ****************               
C                  VAL(J)=0.D0
C                  IF(IBINDX(J).EQ.IROWNO)THEN
C                     VAL(J)=1.D0
C                  ENDIF

                  IF(IBINDX(J).NE.IROWNO)THEN
                     VAL(J)=0.D0
                  ENDIF
C*********** KOUROSH_INT ****************
               ENDDO
          ENDIF
 
            RESID(I)=0.D0
            GOTO 140
         ENDIF

         IF(IFLAG.EQ.0)THEN
            DO J=IS,IE
               ICOLM=IBINDX(J)
               IF(IBC(ICOLM).EQ.1)THEN
                  VAL(J)=0.D0
               ENDIF
            ENDDO
         ENDIF

 140  CONTINUE
      ENDDO
            
      END

!*************************END*******************************************
!******PERFORMS DIVISION OF DOF TO EACH PROCESSOR
      SUBROUTINE NODE_PARTITION(N_UPDATE,IN_ST,IPROC,NPROCS,NX,NDF)
      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'PARDIS.H'
      

      I1=NX/NPROCS+1
      I2=NX/NPROCS
      
      IA=NX-I2*NPROCS
      IB=NPROCS-IA

      I1=I1*NDF
      I2=I2*NDF

      IF((IPROC+1).LE.IA)THEN
         IN_ST=IPROC*I1
         N_UPDATE=I1

      ELSE
         IN_ST=IA*I1+(IPROC-IA)*I2
         N_UPDATE=I2
      ENDIF

      RETURN
      END



!******************** F-BAR METHOD ************************	
      SUBROUTINE SHAPE_TET4(XEL,SHP,DVOL)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION SHP1(3,4),SHP(3,4),XJAC(3,3),XJACINV(3,3),XEL(3,4),
     & XX(3),YY(3),ZZ(3)

      SHP1=0.D0
      SHP=0.D0

      SHP1(1,1)=1.D0

      SHP1(2,2)=1.D0
      
      SHP1(3,3)=1.D0


      SHP1(1,4)=-1.D0

      SHP1(2,4)=-1.D0

      SHP1(3,4)=-1.D0

      DO I=1,3
         XX(I)=XEL(1,I)-XEL(1,4)
         YY(I)=XEL(2,I)-XEL(2,4)
         ZZ(I)=XEL(3,I)-XEL(3,4)
      ENDDO

      XJAC(1:3,1)=XX(1:3)
      XJAC(1:3,2)=YY(1:3)
      XJAC(1:3,3)=ZZ(1:3)

      

      CALL MATINV3_UEL(XJAC,XJACINV,DVOL)
      IF(DVOL.LT.1D-16)THEN
         RETURN
      ENDIF


      DVOL=1.D0/6.D0*DABS(DVOL)
      
      DO INODE=1,4
         SHP(:,INODE)=MATMUL(XJACINV,SHP1(:,INODE))
      ENDDO
      
	

      RETURN
      END
     

      
C*************************************************************
C*************************************************************
      SUBROUTINE CALC_DFG_TET4(UE,NNODE,MCRD,DFGRD_ST,
     &	DVOL_ST,SHP_ST,IBAR,NGAUSS)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION UE(MCRD,NNODE),
     &   XJAC_ST(NNODE),DFGRD_ST(3,3,1),DFGRD_INV(3,3),
     &   DVOL_ST(1),SHP_ST(3,4,1),DFGRD(3,3)

      XJBAR=0.D0
      TV=0.D0
      DO IPT=1,NGAUSS
         
         DO I=1,MCRD
         DO J=1,MCRD
            DFGRD(I,J)=0.D0
            IF(I.EQ.J)DFGRD(I,J)=1.D0
            DO IN=1,NNODE
            DFGRD(I,J)=DFGRD(I,J)+SHP_ST(J,IN,IPT)*UE(I,IN)
            ENDDO
         ENDDO
         ENDDO
         
         CALL MATINV3_UEL(DFGRD,DFGRD_INV,XJAC)
         XJAC_ST(IPT)=XJAC
         DVOL=DVOL_ST(IPT)
         XJBAR=XJBAR+XJAC*DVOL
         TV=TV+DVOL
         DFGRD_ST(1:3,1:3,IPT)=DFGRD(1:3,1:3)
      ENDDO
      XJBAR=XJBAR/TV
      IF(IBAR.EQ.1)THEN
         DO IPT=1,NGAUSS
            FAC=XJBAR**(1.D0/3.D0)*XJAC_ST(IPT)**(-1.D0/3.D0)
            DFGRD_ST(1:3,1:3,IPT)=DFGRD_ST(1:3,1:3,IPT)*FAC
         ENDDO
      ENDIF
      RETURN
      END   

C*************************************************************
C*************************************************************
      SUBROUTINE MAKEGRAD_TET4(XB_ST,XG_ST,SHPTAU_ST,XB_DEV,XB_VOL,
     &                         XG_DEV,XG_VOL)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION XB_ST(6,12,1),SHPTAU_ST(3,4,1),XG_ST(9,12,1),
     &           H(6,9),XB_DEV(6,12,1),XB_VOL(6,12,1),XBTR_ST(12,1),
     &           XG_DEV(9,12,1),XG_VOL(9,12,1)

      XG_ST=0.D0
      XB_ST=0.D0


      XG_ST(1:3,1,1)=SHPTAU_ST(1:3,1,1)
      XG_ST(4:6,2,1)=SHPTAU_ST(1:3,1,1)
      XG_ST(7:9,3,1)=SHPTAU_ST(1:3,1,1)


      XG_ST(1:3,4,1)=SHPTAU_ST(1:3,2,1)
      XG_ST(4:6,5,1)=SHPTAU_ST(1:3,2,1)
      XG_ST(7:9,6,1)=SHPTAU_ST(1:3,2,1)


      XG_ST(1:3,7,1)=SHPTAU_ST(1:3,3,1)
      XG_ST(4:6,8,1)=SHPTAU_ST(1:3,3,1)
      XG_ST(7:9,9,1)=SHPTAU_ST(1:3,3,1)
         

      XG_ST(1:3,10,1)=SHPTAU_ST(1:3,4,1)
      XG_ST(4:6,11,1)=SHPTAU_ST(1:3,4,1)
      XG_ST(7:9,12,1)=SHPTAU_ST(1:3,4,1)


      H=0.D0
      H(1,1)=1.D0
      H(2,5)=1.D0
      H(3,9)=1.D0
      H(4,2)=1.D0
      H(4,4)=1.D0
      H(5,3)=1.D0
      H(5,7)=1.D0
      H(6,6)=1.D0
      H(6,8)=1.D0


      DO I=1,6
         DO J=1,12
            DO K=1,9
               XB_ST(I,J,1)=XB_ST(I,J,1)+H(I,K)*XG_ST(K,J,1)
            ENDDO
         ENDDO
      ENDDO
      
      	   
      XB_DEV=XB_ST  
      XB_VOL=0.0D0    

      DO J=1,12
         XBTR_ST(J,1)=0.D0
         DO I=1,3
            XBTR_ST(J,1)=XBTR_ST(J,1)+XB_ST(I,J,1)
         ENDDO
         
         DO I=1,3
            XB_DEV(I,J,1)=XB_DEV(I,J,1)-1.D0/3.D0*XBTR_ST(J,1)           !JIAHAO
            XB_VOL(I,J,1)=1.D0/3.D0*XBTR_ST(J,1)
         ENDDO
      ENDDO
      


      DO J=1,12
         JTEMP=(J-1)/3
         JTEMP1=JTEMP+1
         JTEMP2=J-JTEMP*3
         DO I=1,9
            ITEMP=(I-1)/3
            ITEMP1=ITEMP+1
            ITEMP2=I-ITEMP*3

            XG_DEV(I,J,1)=0.D0
            XG_VOL(I,J,1)=0.D0
            IF(JTEMP2.EQ.ITEMP1) THEN
               XG_DEV(I,J,1)=SHPTAU_ST(ITEMP2,JTEMP1,1)
            ENDIF

            IF(ITEMP2.EQ.ITEMP1)THEN
               XG_DEV(I,J,1)=XG_DEV(I,J,1)-1.D0/3.D0*XBTR_ST(J,1)
               XG_VOL(I,J,1)=XG_VOL(I,J,1)+1.D0/3.D0*XBTR_ST(J,1)
            ENDIF

         ENDDO
      ENDDO	      
		   


      RETURN
      END
!******************** F-BAR METHOD ************************	 	



      SUBROUTINE MAKESPIN(W,V)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION W(3,3),V(3)
      W=0.D0
      W(1,2)=-V(3)
      W(1,3)=V(2)
      W(2,1)=V(3)
      W(2,3)=-V(1)
      W(3,1)=-V(2)
      W(3,2)=V(1)
      RETURN
      END
!------------------------------------------------
      SUBROUTINE MATINV3_UEL(A,AI,DET)
	  IMPLICIT NONE
      REAL(8):: A(3,3), AI(3,3)
	  REAL(8)::DET
      
       DET=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)   
     &      *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)    
     &     *A(1,3)*A(2,2))
     
      AI(1,1) =  ( A(2,2)*A(3,3)-A(2,3)*A(3,2))/DET
      AI(1,2) = -( A(1,2)*A(3,3)-A(1,3)*A(3,2))/DET
      AI(1,3) = -(-A(1,2)*A(2,3)+A(1,3)*A(2,2))/DET
      AI(2,1) = -( A(2,1)*A(3,3)-A(2,3)*A(3,1))/DET
      AI(2,2) =  ( A(1,1)*A(3,3)-A(1,3)*A(3,1))/DET
      AI(2,3) = -( A(1,1)*A(2,3)-A(1,3)*A(2,1))/DET
      AI(3,1) =  ( A(2,1)*A(3,2)-A(2,2)*A(3,1))/DET
      AI(3,2) = -( A(1,1)*A(3,2)-A(1,2)*A(3,1))/DET
      AI(3,3) =  ( A(1,1)*A(2,2)-A(1,2)*A(2,1))/DET
      RETURN
      END
!---------------------------------------------------------------
      SUBROUTINE DOT3(DOTT,U,V)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION U(3),V(3),W(3)
      W(1)=U(1)*V(1)
      W(2)=U(2)*V(2)
      W(3)=U(3)*V(3)
      DOTT=SUM(W)
      RETURN
      END
!----------------------------------------------
      SUBROUTINE CROSS(W,U,V)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION W(3),U(3),V(3)
      W(1)=U(2)*V(3)-U(3)*V(2)
      W(2)=U(3)*V(1)-U(1)*V(3)
      W(3)=U(1)*V(2)-U(2)*V(1)
      RETURN
      END
!-----------------------------------------------
      SUBROUTINE RUDCMP_UEL(F,RD,UD) 
      IMPLICIT REAL*8 (A-H,O-Z)                                                      
      DIMENSION RD(3,3),F(3,3),UD(3,3) 

      O3=1.0D0/3.0D0
      ROOT3=DSQRT(3.D0)
      C11=F(1,1)*F(1,1)+F(2,1)*F(2,1)+F(3,1)*F(3,1)
      C12=F(1,1)*F(1,2)+F(2,1)*F(2,2)+F(3,1)*F(3,2)
      C13=F(1,1)*F(1,3)+F(2,1)*F(2,3)+F(3,1)*F(3,3)
      C23=F(1,2)*F(1,3)+F(2,2)*F(2,3)+F(3,2)*F(3,3)
      C22=F(1,2)*F(1,2)+F(2,2)*F(2,2)+F(3,2)*F(3,2)
      C33=F(1,3)*F(1,3)+F(2,3)*F(2,3)+F(3,3)*F(3,3)
      C1212=C12*C12
      C1313=C13*C13
      C2323=C23*C23
      C2313=C23*C13
      C1223=C12*C23
      C1213=C12*C13
      S11=C22*C33-C2323
      UI1=O3*(C11+C22+C33)
      UI2=S11+C11*C22+C33*C11-C1212-C1313
      UI3=C11*S11+C12*(C2313-C12*C33)+C13*(C1223-C22*C13)
      UI1S=UI1*UI1
      Q    =DSQRT(-DMIN1(O3*UI2-UI1S,0.D0))                     
      R    =0.5D0*(UI3-UI1*UI2)+UI1*UI1S
      XMOD =Q*Q*Q
      SCL1 =.5D0+DSIGN(.5D0,XMOD-1.D-30)                          
      SCL2 =.5D0+DSIGN(.5D0,XMOD-DABS(R))                   
      SCL0 =DMIN1(SCL1,SCL2)                               
      SCL1 =1.D0-SCL0
      SDETM=DACOS(R/(XMOD+SCL1))*O3
      Q  =SCL0*Q
      CT3=Q*DCOS(SDETM)
      ST3=Q*ROOT3*DSIN(SDETM)
      SDETM=SCL1*DSQRT(DMAX1(0.0D0,R))
      AA=2.D0*(CT3+SDETM)+UI1 
      BB=-CT3+ST3-SDETM+UI1
      CC=-CT3-ST3-SDETM+UI1                         
      XLAMDA1=DSQRT(DMAX1(AA,0.D0))
      XLAMDA2=DSQRT(DMAX1(BB,0.D0))
      XLAMDA3=DSQRT(DMAX1(CC,0.D0))
      SDETM=XLAMDA1*XLAMDA2
      XLI1=XLAMDA1+XLAMDA2+XLAMDA3
      XLI2= SDETM+XLAMDA2*XLAMDA3+XLAMDA3*XLAMDA1
      XLI3= SDETM*XLAMDA3/XLI1
      S11= C11+XLI3
      S22= C22+XLI3
      S33= C33+XLI3
      S12= C2313-C12*S33
      S13= C1223-S22*C13
      S23=-C2323+S22*S33
      SDETM=1.D0/(XLI1*(S11*S23+C12*S12+C13*S13))
      C11=C11+XLI2
      C22=C22+XLI2
      C33=C33+XLI2
      SI11=SDETM*S23
      SI12=SDETM*S12
      SI13=SDETM*S13
      SI22=SDETM*( S11*S33-C1313)
      SI23=SDETM*(-S11*C23+C1213)
      SI33=SDETM*( S11*S22-C1212)
      S12=C12*SI12
      S13=C13*SI13
      S23=C23*SI23
      UI11=C11*SI11+S12+S13
      UI22=S12+C22*SI22+S23
      UI33=S13+S23+C33*SI33
      UI12=C11*SI12+C12*SI22+C13*SI23
      UI13=C11*SI13+C12*SI23+C13*SI33
      UI23=C12*SI13+C22*SI23+C23*SI33
      RD(1,1)=F(1,1)*UI11+F(1,2)*UI12+F(1,3)*UI13
      RD(1,2)=F(1,1)*UI12+F(1,2)*UI22+F(1,3)*UI23
      RD(1,3)=F(1,1)*UI13+F(1,2)*UI23+F(1,3)*UI33
      RD(2,1)=F(2,1)*UI11+F(2,2)*UI12+F(2,3)*UI13
      RD(2,2)=F(2,1)*UI12+F(2,2)*UI22+F(2,3)*UI23
      RD(2,3)=F(2,1)*UI13+F(2,2)*UI23+F(2,3)*UI33
      RD(3,1)=F(3,1)*UI11+F(3,2)*UI12+F(3,3)*UI13
      RD(3,2)=F(3,1)*UI12+F(3,2)*UI22+F(3,3)*UI23
      RD(3,3)=F(3,1)*UI13+F(3,2)*UI23+F(3,3)*UI33 

      DO I=1,3
         DO J=1,3
            UD(I,J)=0.D0
            DO K=1,3
               UD(I,J)=UD(I,J)+RD(K,I)*F(K,J)
            ENDDO
         ENDDO
      ENDDO
      RETURN
      END
!--------------------------------------------------
      SUBROUTINE DROTMAT(R,A_2D)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION R(3,3),A_2D(6,6),A_4D(3,3,3,3)
      DO I=1,3
         DO J=1,3
            DO K=1,3
          DO L=1,3
             A_4D(I,J,K,L)=R(K,I)*R(L,J)
          ENDDO
            ENDDO
         ENDDO
      ENDDO
      DO I=1,3
         A_2D(I,1)=A_4D(I,I,1,1)
         A_2D(I,2)=A_4D(I,I,2,2)
         A_2D(I,3)=A_4D(I,I,3,3)
         A_2D(I,4)=A_4D(I,I,1,2)+A_4D(I,I,2,1)
         A_2D(I,5)=A_4D(I,I,1,3)+A_4D(I,I,3,1)
         A_2D(I,6)=A_4D(I,I,2,3)+A_4D(I,I,3,2)
      ENDDO

      DO I=1,3
         A_2D(4,I)=A_4D(1,2,I,I)
         A_2D(5,I)=A_4D(1,3,I,I)
         A_2D(6,I)=A_4D(2,3,I,I)
      ENDDO

      A_2D(4,4)=A_4D(1,2,1,2)+A_4D(1,2,2,1)
      A_2D(4,5)=A_4D(1,2,1,3)+A_4D(1,2,3,1)
      A_2D(4,6)=A_4D(1,2,2,3)+A_4D(1,2,3,2)

      A_2D(5,4)=A_4D(1,3,1,2)+A_4D(1,3,2,1)
      A_2D(5,5)=A_4D(1,3,1,3)+A_4D(1,3,3,1)
      A_2D(5,6)=A_4D(1,3,2,3)+A_4D(1,3,3,2)

      A_2D(6,4)=A_4D(2,3,1,2)+A_4D(2,3,2,1)
      A_2D(6,5)=A_4D(2,3,1,3)+A_4D(2,3,3,1)
      A_2D(6,6)=A_4D(2,3,2,3)+A_4D(2,3,3,2)

      A_2D(1:6,4:6)=A_2D(1:6,4:6)/2.D0

      A_2D(4:6,1:6)=2.D0*A_2D(4:6,1:6)
         
      RETURN
      END

	
	
C************* KOUROSH_LINESEARCH *******************************  	
       FUNCTION DOTPROD(A,B,NBC1,NEQ,NDF,NBOUD1,
     &                                   MBOUND1)
       IMPLICIT NONE
       
       
       INTEGER::NEQ,NDF,NDF1,I,NBOUD1,N,MBOUND1,INDX,NODE,NE
       REAL*8:: DOTPROD
       INTEGER::NBC1(MBOUND1)
       REAL*8::A(NEQ),B(NEQ)
       
       DOTPROD=DOT_PRODUCT(A,B)
       
       NDF1=NDF+1
       
       IF (NBOUD1.EQ.0) RETURN
       
       DO N=1,NBOUD1
              NODE=NBC1(NDF1*(N-1)+1)
              DO I=1,NDF
                     INDX=NBC1(NDF1*(N-1)+1+I)
                     IF (INDX.EQ.1) THEN
                            NE=NDF*(NODE-1)+I
                            DOTPROD=DOTPROD-A(NE)*B(NE)
                     ENDIF
              ENDDO
       ENDDO
       
       

       RETURN
       END
       
  
***********************************************************************
       SUBROUTINE LINE_SEARCH(STEP,G,G0,VTAU,VT,DVTAU,PROMAT2D,XUND,
     &            VAL_STORE,IJK,NBC1,IBC1,IMID,NPROPSG,IROWPTR,IBINDX,
     &            MNELX,MAXEL,KINC,NDF,IN_ST,ISIZE,IFLAG,NGAUSS,
     &            MAXGRP,MAXDIM,MAXPROPS,MBOUND1,MDOFX,DELTME,NBOUD1,
     &            NEQ,N_UPDATE,MAXCRD,IPROC,IFLAG_LINE_SEARCH,F,RESID,
     &            SVARS1,SVARS2,MAXSV,MaxElemInPatch,NElementsInPatch,
     &            ElementsInPatch,NPATCH,FLOW_RULE,BROY_BFGS)  ! KOUROSH_INT, KOUROSH_CONVERGENCE_CRITERION, F-BAR METHOD
       IMPLICIT NONE
       INTEGER,PARAMETER:: LINMAX=10
       REAL(8),PARAMETER:: STOL=0.9D0
       INTEGER::J,NEQ,N_UPDATE,MAXCRD,MNELX,MAXDIM,MAXEL,MAXPROPS,
     &          MBOUND1,MDOFX,KINC,NDF,IN_ST,ISIZE,IFLAG,NGAUSS,
     &          IFLAG_LINE_SEARCH,MAXGRP,IPROC,NBOUD1,MAXSV,
     &          MaxElemInPatch,NPATCH,FLOW_RULE,BROY_BFGS     ! F-BAR METHOD
       REAL*8::G0,G,GA,GB,SB,SA,STEP,DELTME,PNEWDT,DOTPROD
       INTEGER::IJK(MNELX),NPROPSG(MAXGRP),IMID(MAXDIM*MAXEL),
     &          NBC1(MBOUND1),IBC1(MDOFX),IROWPTR(N_UPDATE+1),
     &          IBINDX(ISIZE),NElementsInPatch(NPATCH),
     &          ElementsInPatch(NPATCH,MaxElemInPatch)
       REAL*8::VTAU(NEQ),DVTAU(NEQ),DVTAU0(NEQ),F(NEQ),VT(NEQ),
     &         RESID(N_UPDATE),XUND(MAXCRD),PROMAT2D(MAXPROPS,MAXGRP),
     &         VAL(ISIZE),VAL_STORE(ISIZE),F_EXT(MDOFX),
     &         SVARS1(MAXSV),SVARS2(MAXSV)   ! F-BAR METHOD
       
       IFLAG_LINE_SEARCH=0
       IF(G*G0.GT.0.D0) RETURN
       
       IF (IPROC.EQ.0) WRITE(*,*) 'ENTERING LINE SEARCH'
       J=0
       GA=G
       GB=G0
       SB=0.D0
       SA=1.D0
10     J=J+1
       STEP=SA-GA*(SA-SB)/(GA-GB)
       VTAU(1:NEQ)=VTAU(1:NEQ)+STEP*DVTAU(1:NEQ)
       DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)

       F(1:NEQ)=0.D0
       RESID(1:N_UPDATE)=0.D0
       
       CALL LOADVC(F,VTAU,DVTAU0,XUND,IJK,IMID,NPROPSG,
     &      PROMAT2D,DELTME,NBC1,IBC1,KINC,NDF,N_UPDATE,IN_ST,IROWPTR,
     &      IBINDX,VAL,RESID,ISIZE,IFLAG,NGAUSS,PNEWDT,VAL_STORE,
     &      F_EXT,SVARS1,SVARS2,MAXSV,MaxElemInPatch,NElementsInPatch,
     &      ElementsInPatch,NPATCH,FLOW_RULE,BROY_BFGS)  ! KOUROSH_INT, KOUROSH_CONVERGENCE_CRITERION, F-BAR METHOD
     
       VTAU(1:NEQ)=VTAU(1:NEQ)-STEP*DVTAU(1:NEQ)
       DVTAU0(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)
       
       IF (PNEWDT.LT.1.D0) THEN
              IF (IPROC.EQ.0) THEN
                     WRITE(*,*) 'UMAT DID NOT CONVERGE IN LINE SEARCH'
              ENDIF
              IFLAG_LINE_SEARCH=1
              RETURN
       ENDIF
     
       G=DOTPROD(DVTAU,F,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
     
       GB=0.5D0*GB
       IF(G*GA.GT.0.D0)GO TO 20
       SB=SA
       GB=GA
20     SA=STEP
       GA=G
       IF(J.EQ.LINMAX) THEN
              IF (IPROC.EQ.0) WRITE(*,*) 'LINE SEARCH DID NOT FINALIZE'
              IFLAG_LINE_SEARCH=1
              RETURN
       ENDIF
       
       IF(DABS(G).GT.STOL*DABS(G0)) GO TO 10
       IF(DABS(SB-SA).GT.STOL*0.5*(SA+SB)) GO TO 10       
       
       IF (IPROC.EQ.0) WRITE(*,*) 'LINE SEARCH ITERATIONS:',J
       
       RETURN
       END



C************* KOUROSH_LINESEARCH *******************************  




C********* KOUROSH_BFGS *********
      SUBROUTINE BFGS(D,FOLD,F,STEP,G,G0,DNORM,VAR,WAR,MDOFX,N_UPDATE,
     &                MAXREF,NUMUPD,NBC1,NEQ,NDF,NBOUD1,MBOUND1,PART,
     &                IPROC,NUPD)

C------------------------------------------------------------------------
C
C     PURPOSE: THIS SUBROUTINE PERFORMS ITERATIONS TO FIND THE
C              SEARCH DIRECTION.
C
C-------------------------------------------------------------------------
       IMPLICIT NONE
       INTEGER::MDOFX,N_UPDATE,MAXREF,NUMUPD,MBOUND1,NEQ,NDF,NBOUD1,
     &          NOEQ,NUPD,I,NMPD3,J,NMPD1,PART,NMPD2,IPROC
       REAL(8)::STEP,G,G0,DNORM,FACT1,FACT2,AUX,VW4,VV,WW,ESTCND,DW,
     &          DV,DELTAG,DELTAK,DOTPROD
       INTEGER::NBC1(MBOUND1)
       REAL(8)::D(NEQ),FOLD(MDOFX),F(MDOFX),VAR(NEQ*MAXREF),
     &          WAR(NEQ*MAXREF),V(NEQ),W(NEQ)
       


      IF (PART.NE.1) GO TO 64 
      
      NOEQ=NEQ
      DELTAG=STEP*(G0-G)
      DELTAK=STEP*STEP*G0
      IF(DELTAG.GT.0.D0.AND.DELTAK.GT.0.D0) THEN
       GO TO 20
      ENDIF 
      D=F
      FOLD=D
10    NUPD=0
      GO TO 60      

20    NUPD=1
      FACT1=1.D0+STEP*DSQRT(DELTAG/DELTAK)
      FACT2=-STEP/DELTAG
      DO 30 I=1,NEQ
      AUX=F(I)
      V(I)=FACT1*FOLD(I)-AUX
      W(I)=FACT2*D(I)
      FOLD(I)=AUX
30    D(I)=AUX       
       

      VW4=4.D0*FACT2*(FACT1*G0-G)+4.D0
      VV=DOTPROD(V,V,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
      WW=(DNORM/DELTAG)**2.D0
      ESTCND=((DSQRT(VV*WW)+DSQRT(DABS(VV*WW+VW4)))
     1 **2.D0)/DABS(VW4)
      IF(ESTCND.GE.1.0D+05) THEN
       IF (IPROC.EQ.0) WRITE(*,*) 'ESTCND.GE.1.0D+05 HAPPENED'
       GO TO 10
      ENDIF        
      NMPD3=NUMUPD*NEQ
      DO 51 I=1,NEQ
      VAR(NMPD3+I)=V(I)
51    WAR(NMPD3+I)=W(I)
      DW=FACT2*G
      DO 50 I=1,NEQ
50    D(I)=D(I)+DW*V(I)
60    IF(NUMUPD.EQ.0) RETURN       

      DO 70 J=1,NUMUPD
      NMPD1=(NUMUPD-J)*NEQ
      DO 61 I=1,NEQ
      V(I)=VAR(NMPD1+I)
61    W(I)=WAR(NMPD1+I)
      DW=DOTPROD(D,W,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
      DO 70 I=1,NEQ
70    D(I)=D(I)+DW*V(I)   
      RETURN 

      
64    CONTINUE
      IF(NUMUPD.EQ.0) THEN 
       IF (IPROC.EQ.0) WRITE(*,*) 'NUMUPD.EQ.0 HAPPENED'
       GO TO 100
      ENDIF 
      DO 90 J=1,NUMUPD
      NMPD2=NEQ*(J-1)
      DO 62 I=1,NEQ
      V(I)=VAR(NMPD2+I)
62    W(I)=WAR(NMPD2+I)
      DV=DOTPROD(D,V,NBC1,NEQ,NDF,NBOUD1,MBOUND1)
      DO 90 I=1,NEQ
90    D(I)=D(I)+DV*W(I)
100   CONTINUE !D=-D
      RETURN
      END

C********* KOUROSH_BFGS *********


C***************** KOUROSH_TIME CONTROL *******************************      
        SUBROUTINE TIME_CONTROL(TIME_LIST,TSEG,NSEG,N_TIME_STEP,NCYC,
     &                         FINTME,DELTME,TRAMP,SHARP_POINT)
       IMPLICIT NONE
       INTEGER::N_TIME_STEP,NCYC,I,NSEG
       REAL(8)::FINTME,DELTME,NEW_FINTME,TIME_AT_CYCLE_START,
     &          FINTME_IN_CYCLE,NEW_FINTME_IN_CYCLE,TRAMP,TOL
       REAL(8)::TIME_LIST(N_TIME_STEP),TSEG(NSEG+1)
       LOGICAL::SHARP_POINT
       
       SHARP_POINT=.FALSE.
       TOL=1.D-10
       
       IF (FINTME.LE.TOL) SHARP_POINT=.TRUE.
       
       IF (FINTME.LT.TRAMP) THEN
              RETURN
       ENDIF
       
       TIME_AT_CYCLE_START=(NCYC*1.D0)*TSEG(NSEG+1)+TRAMP
       FINTME_IN_CYCLE=FINTME-TIME_AT_CYCLE_START
       NEW_FINTME_IN_CYCLE=FINTME_IN_CYCLE+DELTME
       
       
       DO I=1,NSEG+1
          IF (DABS(FINTME_IN_CYCLE-TSEG(I)).LT.TOL) SHARP_POINT=.TRUE.
       ENDDO
       
       
C*********** CHECKS WHETHER FINTME IS ON ONE OF THE SHARP POINTS OF LOADING CURVE       
       DO I=1,NSEG
       
              IF (DABS(FINTME_IN_CYCLE-TSEG(I)).LT.TOL) THEN
                     IF (NEW_FINTME_IN_CYCLE.GT.TSEG(I+1)) THEN
                            DELTME=TSEG(I+1)-FINTME_IN_CYCLE
                            RETURN
                     ENDIF
                     GOTO 10       
              ENDIF
       ENDDO
C*********** CHECKS WHETHER FINTME IS ON ONE OF THE SHARP POINTS OF LOADING CURVE       
       
       DO I=1,NSEG
              IF (FINTME_IN_CYCLE.GT.TSEG(I).AND.
     &            FINTME_IN_CYCLE.LT.TSEG(I+1)) THEN
                     
                     IF (NEW_FINTME_IN_CYCLE.GT.TSEG(I+1)) THEN
                            DELTME=TSEG(I+1)-FINTME_IN_CYCLE
                            RETURN
                     ENDIF
                     
              ENDIF
       ENDDO
       
       
10     CONTINUE       
       IF (NEW_FINTME_IN_CYCLE.GT.TSEG(NSEG+1)) THEN
              DELTME=TSEG(NSEG+1)-FINTME_IN_CYCLE
              RETURN
       ENDIF
      
      
       
       
       DO I=1,N_TIME_STEP-1
       
              IF (DABS(FINTME_IN_CYCLE-TIME_LIST(I)).LT.TOL) THEN
                     IF (NEW_FINTME_IN_CYCLE.LT.TIME_LIST(I+1)) THEN
                            DELTME=TIME_LIST(I+1)-FINTME_IN_CYCLE
                            RETURN
                     ENDIF
                     RETURN       
              ENDIF
       ENDDO
       
       DO I=1,N_TIME_STEP-1
   
              IF (FINTME_IN_CYCLE.GT.TIME_LIST(I).AND.
     &            FINTME_IN_CYCLE.LT.TIME_LIST(I+1)) THEN
                     
                     IF (NEW_FINTME_IN_CYCLE.LT.TIME_LIST(I+1)) THEN
                            DELTME=TIME_LIST(I+1)-FINTME_IN_CYCLE
                            RETURN
                     ENDIF
                     
              ENDIF
       ENDDO
       
       RETURN
       END
C***************** KOUROSH_TIME CONTROL *******************************      

      SUBROUTINE ELSTNEW(XBAR_ST,XG_ST,DVOLTAU_ST,MCRD,
     &    NDOFEL,NNODE,COORDS,VTAU_E,DVTAU0_E,DFGRD0,DFGRD1,TIME,KINC,
     &    JELEM,IPT,DVOL0_ST,DVOLT_ST,DET_FTAU,DET_FT,SHPTAU_ST,
     &    PNEWDT)
     
      IMPLICIT REAL *8(A-H,O-Z)
      include 'PARDIS.H'


      DIMENSION XBAR_ST(2*MAXDIM,MAXDIM*NNODE,IPT),
     & XG_ST(MAXDIM**2,MAXDIM*NNODE,IPT),
     & SHPTAU_ST(3,NNODE,IPT),SHPTAU(3,NNODE),SHPT(3,NNODE),
     & XE_TAU(MAXDIM,NNODE),COORDS(3,NNODE),XE_T(MAXDIM,NNODE),
     & UE(MAXDIM,NNODE),
     & VTAU_E(NNODE*NDOFELX),U(12),SHPFTAU(NNODE),
     & DVOLTAU_ST(IPT),DVOLT_ST(IPT),
     & DFGRD0(3,3),DFGRD1(3,3),
     & UE_T(3,NNODE),DU(12,1),DVTAU0_E(NNODE*NDOFELX),
     & DFGRD1_ST(3,3,IPT),DFGRD0_ST(3,3,IPT),
     & DVOL0_ST(IPT),SHP0(3,NNODE),SHPF0(NNODE),XC0(MAXDIM),
     & SHP0_ST(3,NNODE,IPT),XJAC_ST(IPT),TIME(2),
     & XB_DEV(6,12,1),XB_VOL(6,12,1),XG_DEV(9,12,1),
     & XG_VOL(9,12,1)   

      


    
      PNEWDT=1.D0
     
      DO I=1,NDOFEL
         U(I)=VTAU_E(I)
         DU(I,1)=DVTAU0_E(I)
      ENDDO 


              
      DO I=1,NNODE
         DO J=1,MCRD
            UE(J,I)=U((I-1)*MCRD+J)
            UE_T(J,I)=UE(J,I)-DU((I-1)*MCRD+J,1)
         ENDDO
      ENDDO

      XE_TAU(1:MCRD,1:NNODE)=COORDS(1:MCRD,1:NNODE)+UE(1:MCRD,1:NNODE)
      XE_T(1:MCRD,1:NNODE)=COORDS(1:MCRD,1:NNODE)+UE_T(1:MCRD,1:NNODE)

      CALL SHAPE_TET4(COORDS,SHP0,DVOL0)
      CALL SHAPE_TET4(XE_TAU,SHPTAU,DVOLTAU)
      CALL SHAPE_TET4(XE_T,SHPT,DVOLT)

      IF(DVOLTAU.LT.1D-16.OR.DVOL0.LT.1D-16)THEN
         WRITE(11,*)'NEGATIVE VOLUME',DVOLTAU,DVOL0
         WRITE(11,*)'****JELEM=',JELEM
         DO I=1,NNODE
            WRITE(11,*)XE_TAU(1,I),XE_TAU(2,I),XE_TAU(3,I)       
         ENDDO
         WRITE(11,*)'COORDS'
         DO I=1,NNODE
            WRITE(11,*)COORDS(1,I),COORDS(2,I),COORDS(3,I)       
         ENDDO
         WRITE(11,*)'DISPLACEMENT'
         DO I=1,NNODE
            WRITE(11,*)UE(1,I),UE(2,I),UE(3,I)
         ENDDO
         WRITE(*,*) '########## NEGATIVE VOLUME###########',JELEM
         PNEWDT=0.5D0
      ENDIF


      DVOL0_ST(1)=DVOL0
      DVOLTAU_ST(1)=DVOLTAU
      DVOLT_ST(1)=DVOLT
      SHP0_ST(1:MCRD,1:NNODE,1)=SHP0(1:MCRD,1:NNODE)
      SHPTAU_ST(1:MCRD,1:NNODE,1)=SHPTAU(1:MCRD,1:NNODE)
 
      CALL CALC_DFG_TET4(UE_T,NNODE,MCRD,DFGRD0_ST,DVOL0_ST,
     &                   SHP0_ST,1,1)

      CALL CALC_DFG_TET4(UE,NNODE,MCRD,DFGRD1_ST,DVOL0_ST,
     &                   SHP0_ST,1,1)
     
      CALL MAKEGRAD_TET4(XBAR_ST,XG_ST,SHPTAU_ST,XB_DEV,XB_VOL,
     &                   XG_DEV,XG_VOL)

      DFGRD1(1:3,1:3)=DFGRD1_ST(1:3,1:3,1)
      DFGRD0(1:3,1:3)=DFGRD0_ST(1:3,1:3,1)

      CALL DET33(DFGRD1,DET_FTAU)
      CALL DET33(DFGRD0,DET_FT)
 
      RETURN
      END
             
       
C*********************************************************************       
      SUBROUTINE DET33(A,DET)
      IMPLICIT NONE
      REAL(8):: DET
      REAL(8):: A(3,3)
      
      DET=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)
     &     *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)
     &     *A(1,3)*A(2,2))

      RETURN
      END
       
C********************************************************************* 
      INTEGER FUNCTION KRON(I,J)
      IMPLICIT NONE
      INTEGER::I,J
      
      KRON=0
      IF (I.EQ.J) KRON=1
      
      RETURN
      END
       
 

C*********************************************************************
       SUBROUTINE FIRST_DISPLACEMENT_GUESS(RESIDUAL,VTAU,VT,VAL_STORE,
     &           IROWPTR,IBINDX,IBC1,NEQ,ISIZE,MDOFX,N_UPDATE,IN_ST)
       IMPLICIT NONE
       INTRINSIC::SUM
       
       INTEGER::NEQ,ISIZE,JELEM,NELX,IFS,IST,I,J,
     &          INODE,DOF,NELST,NELEND,MDOFX,N_UPDATE,IN_ST,IS,IE,
     &          IROWNO,ICOLM
       INTEGER::IBNODE(3),IBC1(MDOFX),IROWPTR(N_UPDATE+1),IBINDX(ISIZE) 
       REAL*8::DISP_INC(NEQ),VAL_STORE(ISIZE),RESIDUAL(MDOFX),
     &         XE_TAU(3,4),XE_T(3,4),TIME(2),VT(NEQ),
     &         FLOAD_TAU(9),FLOAD_T(9),FLOAD_DIFFERENCE(9),VTAU(NEQ)              
	
	
       
       
       DISP_INC(1:NEQ)=VTAU(1:NEQ)-VT(1:NEQ)


       
C      MODIFYING THE EXTERNAL FORCE: THE EXTERNAL FORCE CORRESPONDING TO 
C              DISPLACEMENT BCs SHOULD BE SET TO ZERO IN ALL PROCESSORS.
       DO I=1,NEQ
              IF (IBC1(I).EQ.1) RESIDUAL(I)=0.D0
       ENDDO
       
       
C      MODIFICATION OF EXTERNAL FORCE DUE TO THE DISPLACEMENT BCs
       DO I=1,N_UPDATE       
              IROWNO=IN_ST+I
              
              IS=IROWPTR(I)
              IE=IROWPTR(I+1)-1
              
              IF(I.EQ.N_UPDATE)IE=IROWPTR(N_UPDATE+1)
              
              IF (IBC1(IROWNO).EQ.1) THEN
                     DO J=IS,IE
                            ICOLM=IBINDX(J)
                            IF (ICOLM.EQ.IROWNO) RESIDUAL(IROWNO)=
     &                                   VAL_STORE(J)*DISP_INC(IROWNO) 
                     ENDDO
              ELSE
                     DO J=IS,IE
                            ICOLM=IBINDX(J)
                            IF (IBC1(ICOLM).EQ.1) THEN
                                   RESIDUAL(IROWNO)=
     &                                   RESIDUAL(IROWNO)-
     &                                   VAL_STORE(J)*DISP_INC(ICOLM)
 
                            ENDIF
                     ENDDO
              ENDIF       
       
       ENDDO
       
    
       
       RETURN
       END
        
 

C*********************************************************************
      SUBROUTINE NODNB(NATEL,IATEL,IJK,G0XYZ,IPERM,IELGR,
     &     CARTD,NELST,NELEND,MNELX,MAXCRD,NODE_IN_PROCESSOR,
     &     NELEMENTSINPROCESSOR,NELEMENTSINPATCH,NPATCH,
     &     MAXELEMINPATCH,ELEMENTSINPATCH,MAXADJELEM,
     &     MAXADJGRAIN,MAXADJGRAINELEM,MAXNODE,MAXEL,NNODGR,INODGR,
     &     NODE_FIRST,NODE_LAST)   !F-BAR METHOD
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
     
      INTEGER::ELEMENTSINPATCH(NPATCH,MAXELEMINPATCH)  !F-BAR METHOD
      DIMENSION IJK(MNELX),G0XYZ(MAXCRD),NATEL(NODE_IN_PROCESSOR),
     & IATEL(NODE_IN_PROCESSOR,MAXADJGRAINELEM),
     & ELCOD(4,3),XJACM(3,3),XJACI(3,3),DERIV(3,4),
     & CARTD(NELEMENTSINPROCESSOR,3,4),NELEMENTSINPATCH(NPATCH),  !F-BAR METHOD
     & IPERM(3,3,3),IELGR(MAXEL),NNODGR(MAXNODE),COORDS(MAXNODE,3),
     & X(MAXEL),Y(MAXEL),INODGR(MAXNODE,MAXADJGRAIN),Z(MAXEL)



      COORDS=0.D0
      DO I=1,MAXNODE
      DO J=1,3
         COORDS(I,J)=G0XYZ((I-1)*3+J)
      ENDDO
      ENDDO
      
      
      
      DO K=1,MAXEL
         
         X(K)=0.D0
         Y(K)=0.D0
         Z(K)=0.D0

         DO J=1,4
            X(K)=X(K)+COORDS(IJK((K-1)*4+J),1)
            Y(K)=Y(K)+COORDS(IJK((K-1)*4+J),2)
            Z(K)=Z(K)+COORDS(IJK((K-1)*4+J),3)
         ENDDO

         X(K)=X(K)/4.D0
         Y(K)=Y(K)/4.D0
         Z(K)=Z(K)/4.D0

      ENDDO



      XL=5.0D0

C-----------------PERMUTATION SYMBOL-----------------------

         DO I=1,3
           DO J=1,3
             DO K=1,3
               IF(I.EQ.J.OR.J.EQ.K.OR.I.EQ.K)IPERM(I,J,K)=0
               IF(I.EQ.1.AND.J.EQ.2.AND.K.EQ.3)IPERM(I,J,K)=1
               IF(I.EQ.2.AND.J.EQ.3.AND.K.EQ.1)IPERM(I,J,K)=1
               IF(I.EQ.3.AND.J.EQ.1.AND.K.EQ.2)IPERM(I,J,K)=1
               IF(I.EQ.3.AND.J.EQ.2.AND.K.EQ.1)IPERM(I,J,K)=-1
               IF(I.EQ.1.AND.J.EQ.3.AND.K.EQ.2)IPERM(I,J,K)=-1
               IF(I.EQ.2.AND.J.EQ.1.AND.K.EQ.3)IPERM(I,J,K)=-1
             ENDDO
           ENDDO
         ENDDO   


C     OBTAINING THE ELEMENTS THAT HAVE A SPECIFIC NODE AS THEIR VERTEX   

      NATEL=0
      IATEL=0



      DO IEL=1,MAXEL 
         DO IN=1,4
            KM=IJK((IEL-1)*4+IN)
            
            IF (KM.LT.NODE_FIRST.OR.KM.GT.NODE_LAST) CYCLE
            
            DIST=SQRT((X(IEL)-COORDS(KM,1))**2.0D0+
     &                (Y(IEL)-COORDS(KM,2))**2.0D0+
     &                (Z(IEL)-COORDS(KM,3))**2.0D0)
            NATEL(KM-NODE_FIRST+1)=NATEL(KM-NODE_FIRST+1)+1
            IATEL(KM-NODE_FIRST+1,NATEL(KM-NODE_FIRST+1))=IEL
 

            IF (NATEL(KM-NODE_FIRST+1).GT.MAXADJGRAINELEM) THEN
               WRITE(*,*) 'INCREASE MAXADJGRAINELEM FOR NODE=',KM
               STOP
            ENDIF

         END DO
      END DO 

C   CALCULATING CARTESIAN GRADIENTS OF SHAPE FUNCTIONS IN THE GAUSS POINT OF EACH ELEMENT
      KOUNT=0
      DO IPATCH=NELST,NELEND   !F-BAR METHOD
         DO I=1,NELEMENTSINPATCH(IPATCH)  !F-BAR METHOD
         
            IELEM=ELEMENTSINPATCH(IPATCH,I)  !F-BAR METHOD
            KOUNT=KOUNT+1	
            DO INODE=1,4
             DO IDIME=1,3
               ELCOD(INODE,IDIME)=COORDS(IJK((IELEM-1)*4+INODE),IDIME)
             END DO
            END DO
   
            XJACM(1,1)=ELCOD(2,1)-ELCOD(1,1)       
            XJACM(2,1)=ELCOD(3,1)-ELCOD(1,1)           
            XJACM(3,1)=ELCOD(4,1)-ELCOD(1,1) 
            XJACM(1,2)=ELCOD(2,2)-ELCOD(1,2) 
            XJACM(2,2)=ELCOD(3,2)-ELCOD(1,2) 
            XJACM(3,2)=ELCOD(4,2)-ELCOD(1,2)   
            XJACM(1,3)=ELCOD(2,3)-ELCOD(1,3) 
            XJACM(2,3)=ELCOD(3,3)-ELCOD(1,3)
            XJACM(3,3)=ELCOD(4,3)-ELCOD(1,3)

            CALL  MATINV4(XJACM,XJACI,DET)

            DERIV(1,1)=-1.0D0
            DERIV(1,2)=1.0D0
            DERIV(1,3)=0.0D0
            DERIV(1,4)=0.0D0
            DERIV(2,1)=-1.0D0
            DERIV(2,2)=0.0D0
            DERIV(2,3)=1.0D0
            DERIV(2,4)=0.0D0
            DERIV(3,1)=-1.0D0
            DERIV(3,2)=0.0D0
            DERIV(3,3)=0.0D0
            DERIV(3,4)=1.0D0      

            DO IDIME=1,3
            DO JNODE=1,4
            CARTD(KOUNT,IDIME,JNODE)=0.0D0
            DO KDIME=1,3
              CARTD(KOUNT,IDIME,JNODE)=CARTD(KOUNT,IDIME,JNODE)+   !CARTESIAN GRADIENTS OF SHAPE FUNCTIONS IN THE GAUSS POINT OF EACH ELEMENT
     &           XJACI(IDIME,KDIME)*DERIV(KDIME,JNODE)
            END DO
            END DO                                                                                                        
            END DO
         END DO   
      ENDDO

C----------------------------------------------------------------------
C     NNODGR(I)=J: NO. OF GRAINS THAT NODE I BELONGS TO IS J
C     INODGR(I,J)=K:JTH GRAIN THAT NODE I BELONGS TO IS GRAIN NO. K
C---------------------------------------------------------------------- 
	
      DO I=1,MAXNODE     
        NNODGR(I)=0
        DO J=1,MAXADJGRAIN
          INODGR(I,J)=0
        END DO
      END DO
	
      DO I=1,MAXEL    
        DO J=1,4
          INODE=IJK((I-1)*4+J)
          DO K=1,MAXADJGRAIN
            IF (INODGR(INODE,K).EQ.IELGR(I)) GO TO 876 
          END DO
          NNODGR(INODE)=NNODGR(INODE)+1
          INODGR(INODE,NNODGR(INODE))=IELGR(I)
		  
         IF (NNODGR(INODE).GT.MAXADJGRAIN) THEN
         WRITE(*,*) 'INCREASE MAXADJGRAIN, NODE=',INODE
         STOP
         ENDIF
		  
 876      CONTINUE
        END DO
      END DO

      RETURN
      END


C************************************************
C  CALCULATES THE INVERSE OF A 3*3 MATRIX  
C************************************************
      SUBROUTINE MATINV4(A,AI,DET)
C	IMPLICIT DOUBLE PRECISION*8(A-H,O-Z)
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)

C      INCLUDE 'ABA_PARAM.INC'

      DIMENSION A(3,3), AI(3,3)
C
      DET=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)
     &     *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)
     &     *A(1,3)*A(2,2))
      IF (DET.EQ.0.0D0) THEN    !TEST
      WRITE (*,*) 'DET=',DET   
      STOP
      END IF
      AI(1,1) =  ( A(2,2)*A(3,3)-A(2,3)*A(3,2))/DET
      AI(1,2) = -( A(1,2)*A(3,3)-A(1,3)*A(3,2))/DET
      AI(1,3) = -(-A(1,2)*A(2,3)+A(1,3)*A(2,2))/DET
      AI(2,1) = -( A(2,1)*A(3,3)-A(2,3)*A(3,1))/DET
      AI(2,2) =  ( A(1,1)*A(3,3)-A(1,3)*A(3,1))/DET
      AI(2,3) = -( A(1,1)*A(2,3)-A(1,3)*A(2,1))/DET
      AI(3,1) =  ( A(2,1)*A(3,2)-A(2,2)*A(3,1))/DET
      AI(3,2) = -( A(1,1)*A(3,2)-A(1,2)*A(3,1))/DET
      AI(3,3) =  ( A(1,1)*A(2,2)-A(1,2)*A(2,1))/DET
      RETURN
      END
 
 
C*********************************************************************
      SUBROUTINE HALLPETCH(JELEM,HARD,GRAIN_DIAMETER,MAXEL)
      IMPLICIT REAL*8(A-H,O-Z)
   
      DIMENSION G0_BCC(5),G0_AB_A(3),G0_AP_A(3),G0_AB_B(3),
     &          G0_AP_B(3),HARD(47),GRAIN_DIAMETER(MAXEL)


       GRAINSIZE=GRAIN_DIAMETER(JELEM)
	
      OPEN(UNIT=303,FILE='mat_singlet.dat')
C     PRIMARY ALPHA
         READ(303,*)(G0_AB_A(JJ),JJ=1,3),(G0_AP_A(JJ),JJ=1,3),G0_APYR,
     & G0_CPYR
C     TRANSFORMED BETA HCP
         READ(303,*)(G0_AB_B(JJ),JJ=1,3),(G0_AP_B(JJ),JJ=1,3)
C     TRANSFORMED BETA BCC
         READ(303,*)(G0_BCC(JJ),JJ=1,5)
C     SHEAR MODULUS AND POISSON'S RATIO
         READ(303,*)G_HCP_A,G_HCP_B,G_BCC,XNU_HCP_A,XNU_HCP_B,XNU_BCC
C     BURGER VECTOR
         READ(303,*)BUR_HCP,BUR_BCC
C     GRAIN AND LATH SIZE IN MICRONS (D,L_SINGLE,L-POLY)
         READ(303,*)DX,DY,XLSIN_HCP,XLPOLY_HCP,XLSIN_BCC,XLPOLY_BCC
         READ(303,*) COMP_OVER_TEN
         CLOSE(303)



        
      XNU=XNU_HCP_A
      PI=4.D0*DATAN(1.D0)
      GMOD=G_HCP_A
      XBAR=0.01*GMOD
      BUR=BUR_HCP
      
      
      XKSOFT=(DSQRT((2-XNU)*PI*GMOD*XBAR*
     &         BUR/(2*(1-XNU)))/(10.D0**6))*(10.D0**3)      
      XLEN_HCP=XKSOFT/DSQRT(GRAINSIZE)
      
      
      DO I=1,3
         G0_AB_A(I)=G0_AB_A(I)+XLEN_HCP
         G0_AP_A(I)=G0_AP_A(I)+XLEN_HCP		
      ENDDO
      
      G0_APYR=G0_APYR+XLEN_HCP
      G0_CPYR=G0_CPYR+XLEN_HCP
      
      HARD=0.D0
      HARD(1:3)=G0_AB_A(1:3)
      HARD(4:6)=G0_AP_A(1:3)
      HARD(7)=G0_APYR
      HARD(8)=G0_CPYR
          
       
       	
      DO I=1,3
        G0_AB_A(I)=(G0_AB_A(I)-XLEN_HCP)*COMP_OVER_TEN+XLEN_HCP
        G0_AP_A(I)=(G0_AP_A(I)-XLEN_HCP)*COMP_OVER_TEN+XLEN_HCP		
      ENDDO
      
      G0_APYR=COMP_OVER_TEN*(G0_APYR-XLEN_HCP)+XLEN_HCP
      G0_CPYR=COMP_OVER_TEN*(G0_CPYR-XLEN_HCP)+XLEN_HCP
      
      
      HARD(9:11)=G0_AB_A(1:3)
      HARD(12:14)=G0_AP_A(1:3)
      HARD(15)=G0_APYR
      HARD(16)=G0_CPYR

         
      RETURN
      END
 

      
       SUBROUTINE PARTITION_CALCULATOR(NELST,NELEND,NELNOX,
     &          N_TO_BE_DISTRIBUTED,NPROCS,IPROC)
       IMPLICIT NONE
       INTEGER::N_TO_BE_DISTRIBUTED,NPROCS,IPROC,I1,I2,IA,NELST,
     &          NELNOX,NELEND



       I1=N_TO_BE_DISTRIBUTED/NPROCS+1
       I2=N_TO_BE_DISTRIBUTED/NPROCS

       IA=N_TO_BE_DISTRIBUTED-I2*NPROCS


       IF((IPROC+1).LE.IA) THEN
              NELST=IPROC*I1+1
              NELNOX=I1
       ELSE
              NELST=IA*I1+(IPROC-IA)*I2+1
              NELNOX=I2
       ENDIF

       NELEND=NELST+NELNOX-1

       RETURN
       END      
       


      SUBROUTINE printMatrix(A,N,M,matName,printTo)
      implicit none
      integer, intent(in):: N,M
      real(8), intent(in):: A(N,M)
      character*(*),intent(in):: matName
      integer, intent(in):: printTo
      integer:: i,j
      if(LEN(TRIM(matName)).GT.0)then
         if(printTo.GT.0) then
            write(printTo,*) matName,':'
         else
            write(*,*) matName,':'
         endif
      endif
      do i=1,N
         if(printTo.GT.0) then
            write(printTo,*) (A(i,j), j=1,M)
         else
            write(*,*) (A(i,j), j=1,M)
         endif
      enddo
      END SUBROUTINE
             
 
  