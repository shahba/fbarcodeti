      SUBROUTINE POST_PROC(NNODE,NELEM,NPROCS,IJK,G0XYZ,MAXCRD, 
     &         VTAU,MDOFX,SVARS1,MAXSV,NELST,NELEND,NSVARS1,MNELX,  
     &         AVE_STRESS,AVE_STRAIN,VOL_TOTAL,NPATCH,MaxElemInPatch,
     &         NElementsInPatch,ElementsInPatch,FLOW_RULE,AVE_PSTRAIN,
     &         AVE_EFFECTIVEPSTRAIN)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTRINSIC::TRANSPOSE,MATMUL,DLOG
     
      INTEGER:: FLOW_RULE
      INTEGER::ElementsInPatch(NPATCH,MaxElemInPatch),
     &         NElementsInPatch(NPATCH)
      DIMENSION IJK(MNELX),COORD(NNODE,3),
     & DISPLACEMENT(NNODE,3),CURL_NODE(3,3),
     & DEFORMED_COORD(NNODE,3),A(4,4),VOLUME(NELEM),AVE_STRESS(6),
     & XE0(3,4),SHP0(3,4,1),UE(3,4),DFGRD(3,3),DFGRDT(3,3),
     & F_T_F(3,3),EIGVALUE(3),EIGVECTOR(3,3),STRAIN_2D(3,3),
     & STRAIN_1D(6),AVE_STRAIN(6),POINT_LOC(3000,3),TIME(3000),
     & NODE_BOUNDARY(NNODE),XSET(4),YSET(4),ZSET(4),B(3,1),AA(3,3),
     & AI(3,3),SHAPFN(3,1),DISTANCE(4),STR_NODE(NNODE,6),
     & DISPL(2,NNODE,3),DISP(1,NNODE,3),STRESS(NELEM,6),
     & CURL_FP(NELEM,3,3),RESULTOUT(12),
     & G0XYZ(MAXCRD),VTAU(MDOFX),SVARS1(MAXSV),VOL_TOTAL(1),
     & FP_T_FP(3,3),FP(3,3),CKRONE_2D(3,3),PLASTIC_STRAIN_2D(3,3),
     & PLASTIC_STRAIN_1D(6),PSTRAIN(NELEM,6),DUMMY(3,3),
     & EFFECTIVEPSTRAIN(NELEM),AVE_PSTRAIN(6),AVE_EFFECTIVEPSTRAIN(1)

	
      COORD=0.D0
      DO I=1,NNODE
         DO J=1,3
            COORD(I,J)=G0XYZ((I-1)*3+J)
         ENDDO
      ENDDO
	
	

C   READING DISPLACEMENTS
      DISPLACEMENT=0.D0

      DO INODE=1,NNODE
         DO J=1,3
            DISPLACEMENT(INODE,J)=VTAU((INODE-1)*3+J)
         ENDDO
      ENDDO

	
C	READING STRESS VALUES	
      CALL CAL_CKRONE_2D(CKRONE_2D)
      STRESS=0.D0
      EFFECTIVEPSTRAIN=0.D0
      PSTRAIN=0.D0
      
      KOUNT=0
      DO IPATCH=NELST,NELEND
      DO IELEM=1,NElementsInPatch(IPATCH)
      
         NEL=ElementsInPatch(IPATCH,IELEM)
         KOUNT=KOUNT+1
         
         IF (FLOW_RULE.EQ.1) ISHIFT=(KOUNT-1)*NSVARS1+210
         IF (FLOW_RULE.EQ.2) ISHIFT=(KOUNT-1)*NSVARS1+95
         
         DO JJJ=1,6	
            STRESS(NEL,JJJ)=SVARS1(ISHIFT+JJJ)
         ENDDO
         
         IF (FLOW_RULE.EQ.1) ISHIFT=(KOUNT-1)*NSVARS1+66
         IF (FLOW_RULE.EQ.2) ISHIFT=(KOUNT-1)*NSVARS1+101

         
         
         JOUNT=0
         DO I=1,3
         DO J=1,3	
            JOUNT=JOUNT+1
            FP(I,J)=SVARS1(ISHIFT+JOUNT)
         ENDDO
         ENDDO
         FP_T_FP=MATMUL(TRANSPOSE(FP),FP)
         PLASTIC_STRAIN_2D=5.D-1*(FP_T_FP - CKRONE_2D)

         CALL TR2TO1(PLASTIC_STRAIN_2D,PLASTIC_STRAIN_1D)
         PSTRAIN(NEL,1:6)=PLASTIC_STRAIN_1D(1:6)
         
         DUMMY=MATMUL(TRANSPOSE(PLASTIC_STRAIN_2D),PLASTIC_STRAIN_2D)
         EFFECTIVEPSTRAIN(NEL)=DSQRT(2.D0/3.D0*(DUMMY(1,1)+DUMMY(2,2)+
     &                               DUMMY(3,3)))
         
      ENDDO	
      ENDDO
	
	

C	UPDATING THE NODAL COORDINATES	
      DEFORMED_COORD=0.D0
      DO INODE=1,NNODE
         DO J=1,3
          DEFORMED_COORD(INODE,J)=COORD(INODE,J)+DISPLACEMENT(INODE,J)
         ENDDO
      ENDDO

	
C	CALCULATING VOLUME OF EACH ELEMENT
      VOLUME=0.D0	
      DO IPATCH=NELST,NELEND
      DO IELEM=1,NElementsInPatch(IPATCH)
      
         NEL=ElementsInPatch(IPATCH,IELEM)
         
         A=0.D0
         A(1,1)=1.D0;A(1,2)=1.D0;A(1,3)=1.D0;A(1,4)=1.D0
         DO J=1,4
            NODE=IJK((NEL-1)*4+J)
            A(2,J)=DEFORMED_COORD(NODE,1)
            A(3,J)=DEFORMED_COORD(NODE,2)
            A(4,J)=DEFORMED_COORD(NODE,3)
         ENDDO
         
         VOLUME(NEL)=1.D0/6.D0*XM44DET_POST(A)
         
      ENDDO
      ENDDO
	
C	CALCULATING AVERAGE STRESS	
      VOL_TOTAL=0.D0
      AVE_STRESS=0.D0
      AVE_PSTRAIN=0.D0

      DO IPATCH=NELST,NELEND
      DO IELEM=1,NElementsInPatch(IPATCH)
      
         NEL=ElementsInPatch(IPATCH,IELEM)
         AVE_STRESS(:)=AVE_STRESS(:)+VOLUME(NEL)*STRESS(NEL,:)
         AVE_PSTRAIN(:)=AVE_PSTRAIN(:)+VOLUME(NEL)*PSTRAIN(NEL,:)
         VOL_TOTAL(1)=VOL_TOTAL(1)+VOLUME(NEL)
         
      ENDDO
      ENDDO

C	CALCULATING AVERAGE TRUE STRAIN
      AVE_STRAIN=0.D0
      AVE_EFFECTIVEPSTRAIN=0.D0


      DO IPATCH=NELST,NELEND
      DO IELEM=1,NElementsInPatch(IPATCH)
      
         NEL=ElementsInPatch(IPATCH,IELEM)
         
         DO J=1,4
            NODE=IJK((NEL-1)*4+J)
            DO IDIME=1,3
               XE0(IDIME,J)=COORD(NODE,IDIME)
               UE(IDIME,J)=DISPLACEMENT(NODE,IDIME)
            ENDDO
         ENDDO
         
         CALL SHAPE_TET4_POST(XE0,SHP0)
         CALL CALC_DFG_TET4_POST(UE,DFGRD,SHP0,VOLUME(NEL))
         DFGRDT=TRANSPOSE(DFGRD)
         F_T_F=MATMUL(DFGRDT,DFGRD)

         CALL JACOBI_POST(F_T_F,3,3,EIGVALUE,EIGVECTOR,NROT)

         DO I=1,3
            DO J=1,3
               STRAIN_2D(I,J)=0.D0
               DO K=1,3
               
               IF (EIGVALUE(K).LE.0.D0) THEN
               WRITE(*,*) 'NEGATIVE EIGENVALUE @ STRAIN CAL.'
               STOP
               ENDIF
            
               STRAIN_2D(I,J)=STRAIN_2D(I,J)+0.5D0*
     &                       DLOG(EIGVALUE(K))*EIGVECTOR(I,K)*
     &                       EIGVECTOR(J,K)
               ENDDO
            ENDDO
         ENDDO

         CALL TR2TO1_POST(STRAIN_2D,STRAIN_1D)
         
         AVE_STRAIN(:)=AVE_STRAIN(:)+VOLUME(NEL)*STRAIN_1D(:)
         AVE_EFFECTIVEPSTRAIN(1)=AVE_EFFECTIVEPSTRAIN(1)+VOLUME(NEL)*
     &                           EFFECTIVEPSTRAIN(NEL)         
      ENDDO
      ENDDO

      RETURN	
      END
	
!***********************************************************************************************************************************
!  M44DET  -  COMPUTE THE DETERMINANT OF A 4X4 MATRIX.
!***********************************************************************************************************************************

      FUNCTION XM44DET_POST (A) RESULT (DET)

      IMPLICIT NONE

      DOUBLE PRECISION, DIMENSION(4,4), INTENT(IN)  :: A

      DOUBLE PRECISION :: DET


      DET =  A(1,1)*(A(2,2)*(A(3,3)*A(4,4)-A(3,4)*A(4,3))+A(2,3)*
     $      (A(3,4)*A(4,2)-A(3,2)*A(4,4))+A(2,4)*(A(3,2)*A(4,3)-
     $       A(3,3)*A(4,2)))-A(1,2)*(A(2,1)*(A(3,3)*A(4,4)-
     &       A(3,4)*A(4,3))+A(2,3)*(A(3,4)*A(4,1)-A(3,1)*A(4,4))+ 
     &       A(2,4)*(A(3,1)*A(4,3)-A(3,3)*A(4,1)))+A(1,3)*(A(2,1)*
     &      (A(3,2)*A(4,4)-A(3,4)*A(4,2))+A(2,2)*(A(3,4)*A(4,1)- 
     &       A(3,1)*A(4,4))+A(2,4)*(A(3,1)*A(4,2)-A(3,2)*A(4,1)))-
     &       A(1,4)*(A(2,1)*(A(3,2)*A(4,3)-A(3,3)*A(4,2))+ 
     &       A(2,2)*(A(3,3)*A(4,1)-A(3,1)*A(4,3))+A(2,3)*(A(3,1)*
     &       A(4,2)-A(3,2)*A(4,1)))


      RETURN

      END FUNCTION XM44DET_POST
	  

C**********************************************************************	  
      SUBROUTINE SHAPE_TET4_POST(XEL,SHP)	  
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION SHP1(3,4),SHP(3,4,1),XJAC(3,3),XJACINV(3,3),XEL(3,4),
     &     X(4),Y(4),Z(4),XX(3),YY(3),ZZ(3)

      SHP1=0.D0
      SHP=0.D0

      SHP1(1,1)=1.D0

      SHP1(2,2)=1.D0

      SHP1(3,3)=1.D0


      SHP1(1,4)=-1.D0

      SHP1(2,4)=-1.D0

      SHP1(3,4)=-1.D0


      DO I=1,4
         X(I)=XEL(1,I)
         Y(I)=XEL(2,I)
         Z(I)=XEL(3,I)
      ENDDO

      DO I=1,3
         XX(I)=XEL(1,I)-XEL(1,4)
         YY(I)=XEL(2,I)-XEL(2,4)
         ZZ(I)=XEL(3,I)-XEL(3,4)
      ENDDO

      XJAC(1:3,1)=XX(1:3)
      XJAC(1:3,2)=YY(1:3)
      XJAC(1:3,3)=ZZ(1:3)



      CALL MATINV3_POST(XJAC,XJACINV,DVOL)

      DO INODE=1,4
         SHP(:,INODE,1)=MATMUL(XJACINV,SHP1(:,INODE))
      ENDDO
	
      RETURN
      END

!------------------------------------------------
      SUBROUTINE MATINV3_POST(A,AI,DET)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      DIMENSION A(3,3), AI(3,3)
      DET=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)
     &     *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)
     &     *A(1,3)*A(2,2))
      AI(1,1) =  ( A(2,2)*A(3,3)-A(2,3)*A(3,2))/DET
      AI(1,2) = -( A(1,2)*A(3,3)-A(1,3)*A(3,2))/DET
      AI(1,3) = -(-A(1,2)*A(2,3)+A(1,3)*A(2,2))/DET
      AI(2,1) = -( A(2,1)*A(3,3)-A(2,3)*A(3,1))/DET
      AI(2,2) =  ( A(1,1)*A(3,3)-A(1,3)*A(3,1))/DET
      AI(2,3) = -( A(1,1)*A(2,3)-A(1,3)*A(2,1))/DET
      AI(3,1) =  ( A(2,1)*A(3,2)-A(2,2)*A(3,1))/DET
      AI(3,2) = -( A(1,1)*A(3,2)-A(1,2)*A(3,1))/DET
      AI(3,3) =  ( A(1,1)*A(2,2)-A(1,2)*A(2,1))/DET
      RETURN
      END


C*****************************************************************
      SUBROUTINE CALC_DFG_TET4_POST(UE,DFGRD_ST,SHP_ST,DVOL_ST)
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER(NNOD=4,MCRD=3,NGAUSS=1,IBAR=1)
      DIMENSION DFGRD0(3,3),DFGRD1(3,3),UE(MCRD,NNOD),
     &   XJAC_ST(NGAUSS),DFGRD_ST(3,3),DFGRD_INV(3,3),
     &   SHP_ST(MCRD,NNOD,NGAUSS),DFGRD(3,3)

      XJBAR=0.D0
      TV=0.D0
      IPT=1

      CALL CALC_DFGRD_TET4_POST(IPT,UE,MCRD,NNOD,SHP_ST,DFGRD)
      CALL MATINV3_POST(DFGRD,DFGRD_INV,XJAC)
      XJAC_ST(IPT)=XJAC
      DVOL=DVOL_ST
      XJBAR=XJBAR+XJAC*DVOL
      TV=TV+DVOL
      DFGRD_ST(1:3,1:3)=DFGRD(1:3,1:3)

      XJBAR=XJBAR/TV
      IF(IBAR.EQ.1)THEN
         FAC=XJBAR**(1.D0/3.D0)*XJAC_ST(IPT)**(-1.D0/3.D0)
         DFGRD_ST(1:3,1:3)=DFGRD_ST(1:3,1:3)*FAC
      ENDIF
      RETURN
      END	  
	
	
C*****************************************************************
      SUBROUTINE CALC_DFGRD_TET4_POST(IPT,U,MCRD,NNOD,SHP_ST,DFGRD)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION U(MCRD,NNOD),DFGRD(3,3),SHP_ST(3,4,1)

      DO I=1,MCRD
         DO J=1,MCRD
            DFGRD(I,J)=0.D0
            IF(I.EQ.J)DFGRD(I,J)=1.D0
            DO IN=1,NNOD
          DFGRD(I,J)=DFGRD(I,J)+SHP_ST(J,IN,IPT)*U(I,IN)
            ENDDO
         ENDDO
      ENDDO
      IF(MCRD.EQ.2)THEN
         DFGRD(1:3,3)=0.D0
         DFGRD(3,1:3)=0.D0
         DFGRD(3,3)=1.D0
      ENDIF
      RETURN
      END
	
C***************************************************************
      SUBROUTINE JACOBI_POST(A,N,NP,D,V,NROT)
      IMPLICIT REAL*8(A-H,O-Z)
      PARAMETER (NMAX=500)
      DIMENSION A(NP,NP),D(NP),V(NP,NP),B(NMAX),Z(NMAX)
      DO IP=1,N
         DO IQ=1,N
            V(IP,IQ)=0.D0
         ENDDO
         V(IP,IP)=1.D0
      ENDDO
      DO IP=1,N
         B(IP)=A(IP,IP)
         D(IP)=B(IP)
         Z(IP)=0.D0
      ENDDO
      NROT=0
      DO I=1,50
         SM=0.D0
         DO IP=1,N-1
            DO IQ=I+1,N
               SM=SM+DABS(A(IP,IQ))
            ENDDO
         ENDDO
         IF(DABS(SM).LT.1D-16)RETURN
         TRESH=1D-16
         DO IP=1,N-1
            DO IQ=IP+1,N
               IF((I.GT.400).AND.(DABS(A(IP,IQ)/D(IP))).LT.1.D-10                
     &                 .AND.(DABS(A(IP,IQ)/D(IQ)).LT.1.D-10))THEN
                  A(IP,IQ)=0.D0
               ELSEIF(DABS(A(IP,IQ)).GT.TRESH)THEN
                  H=D(IQ)-D(IP)
                  IF(DABS(A(IP,IQ)/H).LT.1.D-12)THEN
                     T=A(IP,IQ)/H
                  ELSE
                     THETA=0.5D0*H/A(IP,IQ)
                     T=1.D0/(DABS(THETA)+DSQRT(1.D0+THETA**2.D0))
                     IF(THETA.LT.0)T=-T  
                  ENDIF
                  C=1.D0/SQRT(1.D0+T**2.D0)
                  S=T*C
                  TAU=S/(1.D0+C)
                  H=T*A(IP,IQ)
                  Z(IP)=Z(IP)-H
                  Z(IQ)=Z(IQ)+H
                  D(IP)=D(IP)-H
                  D(IQ)=D(IQ)+H
                  A(IP,IQ)=0.D0
                  DO J=1,IP-1
                     G=A(J,IP)
                     H=A(J,IQ)
                     A(J,IP)=G-S*(H+G*TAU)
                     A(J,IQ)=H+S*(G-H*TAU)
                  ENDDO
                  DO J=IP+1,IQ-1
                     G=A(IP,J)
                     H=A(J,IQ)
                     A(IP,J)=G-S*(H+G*TAU)
                     A(J,IQ)=H+S*(G-H*TAU)
                  ENDDO
                  DO J=IQ+1,N
                     G=A(IP,J)
                     H=A(IQ,J)
                     A(IP,J)=G-S*(H+G*TAU)
                     A(IQ,J)=H+S*(G-H*TAU)
                  ENDDO
                  DO J=1,N
                     G=V(J,IP)
                     H=V(J,IQ)
                     V(J,IP)=G-S*(H+G*TAU)
                     V(J,IQ)=H+S*(G-H*TAU)
                  ENDDO
                  NROT=NROT+1
               ENDIF
            ENDDO
         ENDDO
         DO IP=1,N
            B(IP)=B(IP)+Z(IP)
            D(IP)=B(IP)
            Z(IP)=0.D0
         ENDDO
      ENDDO
      WRITE(*,*)'TOO MANY ITERATIONS FOR EIGENVALUES'
      RETURN
      END
	
		  
C-------------------------------------------------------------------------	  
      SUBROUTINE TR2TO1_POST(A_2D,A_1D)
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION A_2D(3,3),A_1D(6)
      A_1D(1)=A_2D(1,1)
      A_1D(2)=A_2D(2,2)
      A_1D(3)=A_2D(3,3)
      A_1D(4)=0.5D0*(A_2D(1,2)+A_2D(2,1))
      A_1D(5)=0.5D0*(A_2D(1,3)+A_2D(3,1))
      A_1D(6)=0.5D0*(A_2D(2,3)+A_2D(3,2))
      RETURN
      END
