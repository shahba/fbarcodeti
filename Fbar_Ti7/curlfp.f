      SUBROUTINE CURLFP(FP_NODAL,NELST,NELEND,NELNOX,
     &                  IJK,IPERM,IELGR,CARTD,EULER,GND_FOREST,
     &                  GND_PARALLEL,NPOST1,MAXEL,MNELX,MAXNODE,
     &                  MAXADJGRAIN,NELEMENTSINPROCESSOR,
     &                  NELEMENTSINPATCH,ELEMENTSINPATCH,NPATCH,
     &                  MAXELEMINPATCH,NNODGR,INODGR)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      DIMENSION CARTD(NELEMENTSINPROCESSOR,3,4),FPGRD(3,3,3),
     & IJK(MNELX),CURL_FP(3,3),IPERM(3,3,3),IELGR(MAXEL),
     & FP_NODAL(MAXNODE*MAXADJGRAIN*9),TR(3,3),TR_T(3,3),
     & INODGR(MAXNODE,MAXADJGRAIN),EULER(NELEMENTSINPROCESSOR,3),
     & DUM(3,3),CURL_FP_VECTOR(9,1),B_MAT(63,9),GND_VECTOR(63,1),
     & GND_SCREW(30),GND_ET(30),GND_EN(30),CMT(3,100),CNT(3,100),
     & CTT(3,100),COS_NM(30,30),COS_NT(30,30),COS_NN(30,30),
     & GND_FOREST(30,NELEMENTSINPROCESSOR),NNODGR(MAXNODE),
     & GND_PARALLEL(30,NELEMENTSINPROCESSOR),
     & SIN_NM(30,30),SIN_NT(30,30),SIN_NN(30,30),EUL(3),
     & NELEMENTSINPATCH(NPATCH)
      INTEGER::ELEMENTSINPATCH(NPATCH,MAXELEMINPATCH)     
 


C****** CALCULATING THE COSINE OF ANGLES BETWEEN M,N AND T
	CALL SLIP_DIRECTION(CMT,CNT)
	CTT=0.D0
	DO ISYS=1,30
			CTT(1,ISYS)=CNT(2,ISYS)*CMT(3,ISYS)-CNT(3,ISYS)*
     &              	CMT(2,ISYS)
			CTT(2,ISYS)=CNT(3,ISYS)*CMT(1,ISYS)-CNT(1,ISYS)*
     &              	CMT(3,ISYS)
			CTT(3,ISYS)=CNT(1,ISYS)*CMT(2,ISYS)-CNT(2,ISYS)*
     &              	CMT(1,ISYS)	
	ENDDO
	
	DO I=1,30
		DO J=1,30
			COS_NM(I,J)=CNT(1,I)*CMT(1,J)+CNT(2,I)*CMT(2,J)+
     &                           CNT(3,I)*CMT(3,J) 
			COS_NT(I,J)=CNT(1,I)*CTT(1,J)+CNT(2,I)*CTT(2,J)+
     &                           CNT(3,I)*CTT(3,J)
			COS_NN(I,J)=CNT(1,I)*CNT(1,J)+CNT(2,I)*CNT(2,J)+
     &                           CNT(3,I)*CNT(3,J)

			SIN_NM(I,J)=DSQRT(DABS(1.-COS_NM(I,J)*COS_NM(I,J)))
			SIN_NT(I,J)=DSQRT(DABS(1.-COS_NT(I,J)*COS_NT(I,J)))
			SIN_NN(I,J)=DSQRT(DABS(1.-COS_NN(I,J)*COS_NN(I,J)))
			
		ENDDO
	ENDDO

C------CALCULATING GRADIENTS OF FP--------------------------
	KOUNT=0
      DO IPATCH=NELST,NELEND
       DO NEL=1,NELEMENTSINPATCH(IPATCH)
       IELEM=ELEMENTSINPATCH(IPATCH,NEL)
	 KOUNT=KOUNT+1
	 IGRAIN=IELGR(IELEM)
        K=0
        DO IDIME=1,3
          DO JDIME=1,3
            K=K+1
            FPGRD(IDIME,JDIME,1)=0.0D0   
            FPGRD(IDIME,JDIME,2)=0.0D0   
            FPGRD(IDIME,JDIME,3)=0.0D0

		DO INODE=1,4
			KNODE=IJK((IELEM-1)*4+INODE)
			
			DO I=1,NNODGR(KNODE)
				IF (INODGR(KNODE,I).EQ.IGRAIN) IG=I
			ENDDO
			
			LOCATION=(KNODE-1)*MAXADJGRAIN*9+(IG-1)*9+K
			ARSFP=FP_NODAL(LOCATION)
			
			DO KDIME=1,3    
				FPGRD(IDIME,JDIME,KDIME)=
     &                            FPGRD(IDIME,JDIME,KDIME)+
     &                            ARSFP*CARTD(KOUNT,KDIME,INODE) 
		       ENDDO
	       ENDDO
	   ENDDO
	 ENDDO


C-------------CURL FP--------------------------------------------------

      DO II=1,3    
        DO JJ=1,3
          CURL_FP(II,JJ)=0.D0
          DO IR=1,3
            DO IS=1,3
             CURL_FP(II,JJ)=CURL_FP(II,JJ)+
     &                         IPERM(IS,JJ,IR)*FPGRD(II,IR,IS)
            END DO
          END DO
	      IF (NPOST1.EQ.0) WRITE(509,*)CURL_FP(II,JJ)
        END DO
      END DO
	
C**************** TRANSFERRING CURLFP FROM GLOBAL COORD SYSTEM TO CRYSTAL COORD SYSTEM
	EUL(1:3)=EULER(KOUNT,1:3)
      	CALL EULER_SLIP(EUL,TR)
	CALL TRANS(TR,TR_T)
	CALL MAT33(DUM,TR_T,CURL_FP,3)
	CALL MAT33(CURL_FP,DUM,TR,3)
	
	CURL_FP_VECTOR=0.D0
	CURL_FP_VECTOR(1,1)=CURL_FP(1,1)
	CURL_FP_VECTOR(2,1)=CURL_FP(1,2)
	CURL_FP_VECTOR(3,1)=CURL_FP(1,3)
	CURL_FP_VECTOR(4,1)=CURL_FP(2,1)
	CURL_FP_VECTOR(5,1)=CURL_FP(2,2)
	CURL_FP_VECTOR(6,1)=CURL_FP(2,3)
	CURL_FP_VECTOR(7,1)=CURL_FP(3,1)
	CURL_FP_VECTOR(8,1)=CURL_FP(3,2)
	CURL_FP_VECTOR(9,1)=CURL_FP(3,3)
	
	CALL B_MATRIX(B_MAT)
	
	CALL MATMULX(B_MAT,CURL_FP_VECTOR,GND_VECTOR,63,9,1)
	
	
C*************************************************************
C     GND DENSITY FOR SOME SLIP SYSTEMS ARE EQUAL, INCLUDING
C     				SCREW GND: 
C				 1=6=9=12
C				 2=5=8=11
C				 3=4=7=10
C				 13=24=30
C				 14=19=25
C				 15=20=26
C				 16=21=27
C				 17=22=28
C				 18=23=29

C    DENSITIES FOR 6 EDGE_T GNDS ARE EQUAL TO 6 EDGE_N GNDS
C               EDGE_T GND         EDGE_N GND
C                   1        =         6
C                   2        =         5
C                   3        =         4
C                   4        =         3
C                   5        =         2
C                   6        =         1

C    GND DENSITIES ARE EQUALLY DISTRIBUTED BETWEEN THESE SYSTEMS

	GND_SCREW(1)=GND_VECTOR(1,1)/4.D0
	GND_SCREW(2)=GND_VECTOR(2,1)/4.D0
	GND_SCREW(3)=GND_VECTOR(3,1)/4.D0
	GND_SCREW(4)=GND_SCREW(3)	
	GND_SCREW(5)=GND_SCREW(2)	
	GND_SCREW(6)=GND_SCREW(1)
	GND_SCREW(7)=GND_SCREW(3)
	GND_SCREW(8)=GND_SCREW(2)
	GND_SCREW(9)=GND_SCREW(1)
	GND_SCREW(10)=GND_SCREW(3)
	GND_SCREW(11)=GND_SCREW(2)	
	GND_SCREW(12)=GND_SCREW(1)
	GND_SCREW(13)=GND_VECTOR(4,1)/3.D0
	GND_SCREW(14)=GND_VECTOR(5,1)/3.D0	
	GND_SCREW(15)=GND_VECTOR(6,1)/3.D0
	GND_SCREW(16)=GND_VECTOR(7,1)/3.D0
	GND_SCREW(17)=GND_VECTOR(8,1)/3.D0
	GND_SCREW(18)=GND_VECTOR(9,1)/3.D0
	GND_SCREW(19)=GND_SCREW(14)
	GND_SCREW(20)=GND_SCREW(15)
	GND_SCREW(21)=GND_SCREW(16)
	GND_SCREW(22)=GND_SCREW(17)
	GND_SCREW(23)=GND_SCREW(18)
	GND_SCREW(24)=GND_SCREW(13)
	GND_SCREW(25)=GND_SCREW(14)
	GND_SCREW(26)=GND_SCREW(15)
	GND_SCREW(27)=GND_SCREW(16)
	GND_SCREW(28)=GND_SCREW(17)
	GND_SCREW(29)=GND_SCREW(18)
	GND_SCREW(30)=GND_SCREW(13)
	
	GND_EN(7:30)=GND_VECTOR(10:33,1)
	GND_EN(6)=GND_VECTOR(34,1)/2.D0
	GND_EN(5)=GND_VECTOR(35,1)/2.D0
	GND_EN(4)=GND_VECTOR(36,1)/2.D0
	GND_EN(3)=GND_VECTOR(37,1)/2.D0
	GND_EN(2)=GND_VECTOR(38,1)/2.D0
	GND_EN(1)=GND_VECTOR(39,1)/2.D0
	
	
	GND_ET(1)=GND_EN(6)
	GND_ET(2)=GND_EN(5)
	GND_ET(3)=GND_EN(4)
	GND_ET(4)=GND_EN(3)
	GND_ET(5)=GND_EN(2)
	GND_ET(6)=GND_EN(1)
	GND_ET(7:30)=GND_VECTOR(40:63,1)
	
C********* CALCULATING FOREST GND DENSITY ********************
	DO ISYS=1,30
		GND_FOREST(ISYS,KOUNT)=0.D0
		GND_PARALLEL(ISYS,KOUNT)=0.D0
		
		DO JSYS=1,30
			GND_FOREST(ISYS,KOUNT)=GND_FOREST(ISYS,KOUNT)+
     &                 DABS(GND_SCREW(JSYS)*COS_NM(ISYS,JSYS))+
     &                 DABS(GND_ET(JSYS)*COS_NT(ISYS,JSYS))+
     &                 DABS(GND_EN(JSYS)*COS_NN(ISYS,JSYS))

			GND_PARALLEL(ISYS,KOUNT)=GND_PARALLEL(ISYS,KOUNT)+
     &                 DABS(GND_SCREW(JSYS)*SIN_NM(ISYS,JSYS))+
     &                 DABS(GND_ET(JSYS)*SIN_NT(ISYS,JSYS))+
     &                 DABS(GND_EN(JSYS)*SIN_NN(ISYS,JSYS))     
		ENDDO
	ENDDO	
	
      END DO  
      ENDDO

      IF (NPOST1.EQ.0) CALL FLUSH(509)
      RETURN

      END
      
      
C*************************************************************
C*************************************************************
C*************************************************************
C*************************************************************      
	SUBROUTINE B_MATRIX(B)
	IMPLICIT DOUBLE PRECISION (A-H,O-Z)
	DIMENSION B(63,9)
	
	
       B (  1 ,  1 ) =    1.128880E-01
	B (  1 ,  2 ) =   -8.166213E-02
	B (  1 ,  3 ) =    0.000000E+00
	B (  1 ,  4 ) =   -8.166213E-02
	B (  1 ,  5 ) =    2.071833E-01
	B (  1 ,  6 ) =    0.000000E+00
	B (  1 ,  7 ) =    0.000000E+00
	B (  1 ,  8 ) =    0.000000E+00
	B (  1 ,  9 ) =    5.776589E-02
	B (  2 ,  1 ) =    1.128880E-01
	B (  2 ,  2 ) =    8.166213E-02
	B (  2 ,  3 ) =    0.000000E+00
	B (  2 ,  4 ) =    8.166213E-02
	B (  2 ,  5 ) =    2.071833E-01
	B (  2 ,  6 ) =    0.000000E+00
	B (  2 ,  7 ) =    0.000000E+00
	B (  2 ,  8 ) =    0.000000E+00
	B (  2 ,  9 ) =    5.776589E-02
	B (  3 ,  1 ) =    2.543310E-01
	B (  3 ,  2 ) =    0.000000E+00
	B (  3 ,  3 ) =    0.000000E+00
	B (  3 ,  4 ) =    0.000000E+00
	B (  3 ,  5 ) =    6.574036E-02
	B (  3 ,  6 ) =    0.000000E+00
	B (  3 ,  7 ) =    0.000000E+00
	B (  3 ,  8 ) =    0.000000E+00
	B (  3 ,  9 ) =    5.776589E-02
	B (  4 ,  1 ) =    7.333343E-02
	B (  4 ,  2 ) =    2.306295E-02
	B (  4 ,  3 ) =    4.359713E-02
	B (  4 ,  4 ) =    2.306295E-02
	B (  4 ,  5 ) =    9.996424E-02
	B (  4 ,  6 ) =    7.551245E-02
	B (  4 ,  7 ) =    2.376822E-02
	B (  4 ,  8 ) =    4.116776E-02
	B (  4 ,  9 ) =    1.377837E-01
	B (  5 ,  1 ) =    7.333343E-02
	B (  5 ,  2 ) =   -2.306295E-02
	B (  5 ,  3 ) =   -4.359713E-02
	B (  5 ,  4 ) =   -2.306295E-02
	B (  5 ,  5 ) =    9.996424E-02
	B (  5 ,  6 ) =    7.551245E-02
	B (  5 ,  7 ) =   -2.376822E-02
	B (  5 ,  8 ) =    4.116776E-02
	B (  5 ,  9 ) =    1.377837E-01
	B (  6 ,  1 ) =    1.132796E-01
	B (  6 ,  2 ) =    0.000000E+00
	B (  6 ,  3 ) =   -8.719426E-02
	B (  6 ,  4 ) =    0.000000E+00
	B (  6 ,  5 ) =    6.001803E-02
	B (  6 ,  6 ) =    0.000000E+00
	B (  6 ,  7 ) =   -4.753643E-02
	B (  6 ,  8 ) =    0.000000E+00
	B (  6 ,  9 ) =    1.377837E-01
	B (  7 ,  1 ) =    7.333343E-02
	B (  7 ,  2 ) =    2.306295E-02
	B (  7 ,  3 ) =   -4.359713E-02
	B (  7 ,  4 ) =    2.306295E-02
	B (  7 ,  5 ) =    9.996424E-02
	B (  7 ,  6 ) =   -7.551245E-02
	B (  7 ,  7 ) =   -2.376822E-02
	B (  7 ,  8 ) =   -4.116776E-02
	B (  7 ,  9 ) =    1.377837E-01
	B (  8 ,  1 ) =    7.333343E-02
	B (  8 ,  2 ) =   -2.306295E-02
	B (  8 ,  3 ) =    4.359713E-02
	B (  8 ,  4 ) =   -2.306295E-02
	B (  8 ,  5 ) =    9.996424E-02
	B (  8 ,  6 ) =   -7.551245E-02
	B (  8 ,  7 ) =    2.376822E-02
	B (  8 ,  8 ) =   -4.116776E-02
	B (  8 ,  9 ) =    1.377837E-01
	B (  9 ,  1 ) =    1.132796E-01
	B (  9 ,  2 ) =    0.000000E+00
	B (  9 ,  3 ) =    8.719426E-02
	B (  9 ,  4 ) =    0.000000E+00
	B (  9 ,  5 ) =    6.001803E-02
	B (  9 ,  6 ) =    0.000000E+00
	B (  9 ,  7 ) =    4.753643E-02
	B (  9 ,  8 ) =    0.000000E+00
	B (  9 ,  9 ) =    1.377837E-01
	B ( 10 ,  1 ) =    0.000000E+00
	B ( 10 ,  2 ) =   -1.452474E-01
	B ( 10 ,  3 ) =    8.376144E-02
	B ( 10 ,  4 ) =   -2.046507E-02
	B ( 10 ,  5 ) =    0.000000E+00
	B ( 10 ,  6 ) =    0.000000E+00
	B ( 10 ,  7 ) =    8.704741E-03
	B ( 10 ,  8 ) =    0.000000E+00
	B ( 10 ,  9 ) =    0.000000E+00
	B ( 11 ,  1 ) =    7.175562E-02
	B ( 11 ,  2 ) =   -2.096306E-02
	B ( 11 ,  3 ) =    4.188072E-02
	B ( 11 ,  4 ) =    1.038193E-01
	B ( 11 ,  5 ) =   -7.175562E-02
	B ( 11 ,  6 ) =    7.253953E-02
	B ( 11 ,  7 ) =    4.352370E-03
	B ( 11 ,  8 ) =    7.538527E-03
	B ( 11 ,  9 ) =    0.000000E+00
	B ( 12 ,  1 ) =   -7.175562E-02
	B ( 12 ,  2 ) =   -2.096306E-02
	B ( 12 ,  3 ) =   -4.188072E-02
	B ( 12 ,  4 ) =    1.038193E-01
	B ( 12 ,  5 ) =    7.175562E-02
	B ( 12 ,  6 ) =    7.253953E-02
	B ( 12 ,  7 ) =   -4.352370E-03
	B ( 12 ,  8 ) =    7.538527E-03
	B ( 12 ,  9 ) =    0.000000E+00
	B ( 13 ,  1 ) =    0.000000E+00
	B ( 13 ,  2 ) =   -1.452474E-01
	B ( 13 ,  3 ) =   -8.376144E-02
	B ( 13 ,  4 ) =   -2.046507E-02
	B ( 13 ,  5 ) =    0.000000E+00
	B ( 13 ,  6 ) =    0.000000E+00
	B ( 13 ,  7 ) =   -8.704741E-03
	B ( 13 ,  8 ) =    0.000000E+00
	B ( 13 ,  9 ) =    0.000000E+00
	B ( 14 ,  1 ) =    7.175562E-02
	B ( 14 ,  2 ) =   -2.096306E-02
	B ( 14 ,  3 ) =   -4.188072E-02
	B ( 14 ,  4 ) =    1.038193E-01
	B ( 14 ,  5 ) =   -7.175562E-02
	B ( 14 ,  6 ) =   -7.253953E-02
	B ( 14 ,  7 ) =   -4.352370E-03
	B ( 14 ,  8 ) =   -7.538527E-03
	B ( 14 ,  9 ) =    0.000000E+00
	B ( 15 ,  1 ) =   -7.175562E-02
	B ( 15 ,  2 ) =   -2.096306E-02
	B ( 15 ,  3 ) =    4.188072E-02
	B ( 15 ,  4 ) =    1.038193E-01
	B ( 15 ,  5 ) =    7.175562E-02
	B ( 15 ,  6 ) =   -7.253953E-02
	B ( 15 ,  7 ) =    4.352370E-03
	B ( 15 ,  8 ) =   -7.538527E-03
	B ( 15 ,  9 ) =    0.000000E+00
	B ( 16 ,  1 ) =   -3.224887E-03
	B ( 16 ,  2 ) =   -3.859451E-02
	B ( 16 ,  3 ) =    2.225672E-02
	B ( 16 ,  4 ) =   -5.437889E-03
	B ( 16 ,  5 ) =   -7.949124E-02
	B ( 16 ,  6 ) =    2.497762E-02
	B ( 16 ,  7 ) =    2.312986E-03
	B ( 16 ,  8 ) =   -6.102021E-02
	B ( 16 ,  9 ) =    4.509505E-02
	B ( 17 ,  1 ) =   -4.135806E-02
	B ( 17 ,  2 ) =    2.745409E-02
	B ( 17 ,  3 ) =   -1.050289E-02
	B ( 17 ,  4 ) =    6.061071E-02
	B ( 17 ,  5 ) =   -4.135806E-02
	B ( 17 ,  6 ) =    3.176370E-02
	B ( 17 ,  7 ) =    5.400154E-02
	B ( 17 ,  8 ) =   -2.850700E-02
	B ( 17 ,  9 ) =    4.509505E-02
	B ( 18 ,  1 ) =   -7.949124E-02
	B ( 18 ,  2 ) =   -3.859451E-02
	B ( 18 ,  3 ) =   -3.275961E-02
	B ( 18 ,  4 ) =   -5.437889E-03
	B ( 18 ,  5 ) =   -3.224887E-03
	B ( 18 ,  6 ) =    6.786080E-03
	B ( 18 ,  7 ) =    5.168856E-02
	B ( 18 ,  8 ) =    3.251321E-02
	B ( 18 ,  9 ) =    4.509505E-02
	B ( 19 ,  1 ) =   -3.224887E-03
	B ( 19 ,  2 ) =   -3.859451E-02
	B ( 19 ,  3 ) =   -2.225672E-02
	B ( 19 ,  4 ) =   -5.437889E-03
	B ( 19 ,  5 ) =   -7.949124E-02
	B ( 19 ,  6 ) =   -2.497762E-02
	B ( 19 ,  7 ) =   -2.312986E-03
	B ( 19 ,  8 ) =    6.102021E-02
	B ( 19 ,  9 ) =    4.509505E-02
	B ( 20 ,  1 ) =   -4.135806E-02
	B ( 20 ,  2 ) =    2.745409E-02
	B ( 20 ,  3 ) =    1.050289E-02
	B ( 20 ,  4 ) =    6.061071E-02
	B ( 20 ,  5 ) =   -4.135806E-02
	B ( 20 ,  6 ) =   -3.176370E-02
	B ( 20 ,  7 ) =   -5.400154E-02
	B ( 20 ,  8 ) =    2.850700E-02
	B ( 20 ,  9 ) =    4.509505E-02
	B ( 21 ,  1 ) =   -7.949124E-02
	B ( 21 ,  2 ) =   -3.859451E-02
	B ( 21 ,  3 ) =    3.275961E-02
	B ( 21 ,  4 ) =   -5.437889E-03
	B ( 21 ,  5 ) =   -3.224887E-03
	B ( 21 ,  6 ) =   -6.786080E-03
	B ( 21 ,  7 ) =   -5.168856E-02
	B ( 21 ,  8 ) =   -3.251321E-02
	B ( 21 ,  9 ) =    4.509505E-02
	B ( 22 ,  1 ) =   -3.224887E-03
	B ( 22 ,  2 ) =    3.859451E-02
	B ( 22 ,  3 ) =   -2.225672E-02
	B ( 22 ,  4 ) =    5.437889E-03
	B ( 22 ,  5 ) =   -7.949124E-02
	B ( 22 ,  6 ) =    2.497762E-02
	B ( 22 ,  7 ) =   -2.312986E-03
	B ( 22 ,  8 ) =   -6.102021E-02
	B ( 22 ,  9 ) =    4.509505E-02
	B ( 23 ,  1 ) =   -7.949124E-02
	B ( 23 ,  2 ) =    3.859451E-02
	B ( 23 ,  3 ) =   -3.275961E-02
	B ( 23 ,  4 ) =    5.437889E-03
	B ( 23 ,  5 ) =   -3.224887E-03
	B ( 23 ,  6 ) =   -6.786080E-03
	B ( 23 ,  7 ) =    5.168856E-02
	B ( 23 ,  8 ) =   -3.251321E-02
	B ( 23 ,  9 ) =    4.509505E-02
	B ( 24 ,  1 ) =   -4.135806E-02
	B ( 24 ,  2 ) =   -2.745409E-02
	B ( 24 ,  3 ) =   -1.050289E-02
	B ( 24 ,  4 ) =   -6.061071E-02
	B ( 24 ,  5 ) =   -4.135806E-02
	B ( 24 ,  6 ) =   -3.176370E-02
	B ( 24 ,  7 ) =    5.400154E-02
	B ( 24 ,  8 ) =    2.850700E-02
	B ( 24 ,  9 ) =    4.509505E-02
	B ( 25 ,  1 ) =   -3.224887E-03
	B ( 25 ,  2 ) =    3.859451E-02
	B ( 25 ,  3 ) =    2.225672E-02
	B ( 25 ,  4 ) =    5.437889E-03
	B ( 25 ,  5 ) =   -7.949124E-02
	B ( 25 ,  6 ) =   -2.497762E-02
	B ( 25 ,  7 ) =    2.312986E-03
	B ( 25 ,  8 ) =    6.102021E-02
	B ( 25 ,  9 ) =    4.509505E-02
	B ( 26 ,  1 ) =   -7.949124E-02
	B ( 26 ,  2 ) =    3.859451E-02
	B ( 26 ,  3 ) =    3.275961E-02
	B ( 26 ,  4 ) =    5.437889E-03
	B ( 26 ,  5 ) =   -3.224887E-03
	B ( 26 ,  6 ) =    6.786080E-03
	B ( 26 ,  7 ) =   -5.168856E-02
	B ( 26 ,  8 ) =    3.251321E-02
	B ( 26 ,  9 ) =    4.509505E-02
	B ( 27 ,  1 ) =   -4.135806E-02
	B ( 27 ,  2 ) =   -2.745409E-02
	B ( 27 ,  3 ) =    1.050289E-02
	B ( 27 ,  4 ) =   -6.061071E-02
	B ( 27 ,  5 ) =   -4.135806E-02
	B ( 27 ,  6 ) =    3.176370E-02
	B ( 27 ,  7 ) =   -5.400154E-02
	B ( 27 ,  8 ) =   -2.850700E-02
	B ( 27 ,  9 ) =    4.509505E-02
	B ( 28 ,  1 ) =   -2.481466E-02
	B ( 28 ,  2 ) =    3.676235E-02
	B ( 28 ,  3 ) =   -1.823385E-02
	B ( 28 ,  4 ) =    3.676235E-02
	B ( 28 ,  5 ) =   -6.726416E-02
	B ( 28 ,  6 ) =    3.158195E-02
	B ( 28 ,  7 ) =    2.876961E-02
	B ( 28 ,  8 ) =   -4.983043E-02
	B ( 28 ,  9 ) =    5.019939E-02
	B ( 29 ,  1 ) =   -8.848891E-02
	B ( 29 ,  2 ) =    0.000000E+00
	B ( 29 ,  3 ) =   -3.646770E-02
	B ( 29 ,  4 ) =    0.000000E+00
	B ( 29 ,  5 ) =   -3.589914E-03
	B ( 29 ,  6 ) =    0.000000E+00
	B ( 29 ,  7 ) =    5.753922E-02
	B ( 29 ,  8 ) =    0.000000E+00
	B ( 29 ,  9 ) =    5.019939E-02
	B ( 30 ,  1 ) =   -2.481466E-02
	B ( 30 ,  2 ) =   -3.676235E-02
	B ( 30 ,  3 ) =   -1.823385E-02
	B ( 30 ,  4 ) =   -3.676235E-02
	B ( 30 ,  5 ) =   -6.726416E-02
	B ( 30 ,  6 ) =   -3.158195E-02
	B ( 30 ,  7 ) =    2.876961E-02
	B ( 30 ,  8 ) =    4.983043E-02
	B ( 30 ,  9 ) =    5.019939E-02
	B ( 31 ,  1 ) =   -2.481466E-02
	B ( 31 ,  2 ) =    3.676235E-02
	B ( 31 ,  3 ) =    1.823385E-02
	B ( 31 ,  4 ) =    3.676235E-02
	B ( 31 ,  5 ) =   -6.726416E-02
	B ( 31 ,  6 ) =   -3.158195E-02
	B ( 31 ,  7 ) =   -2.876961E-02
	B ( 31 ,  8 ) =    4.983043E-02
	B ( 31 ,  9 ) =    5.019939E-02
	B ( 32 ,  1 ) =   -8.848891E-02
	B ( 32 ,  2 ) =    0.000000E+00
	B ( 32 ,  3 ) =    3.646770E-02
	B ( 32 ,  4 ) =    0.000000E+00
	B ( 32 ,  5 ) =   -3.589914E-03
	B ( 32 ,  6 ) =    0.000000E+00
	B ( 32 ,  7 ) =   -5.753922E-02
	B ( 32 ,  8 ) =    0.000000E+00
	B ( 32 ,  9 ) =    5.019939E-02
	B ( 33 ,  1 ) =   -2.481466E-02
	B ( 33 ,  2 ) =   -3.676235E-02
	B ( 33 ,  3 ) =    1.823385E-02
	B ( 33 ,  4 ) =   -3.676235E-02
	B ( 33 ,  5 ) =   -6.726416E-02
	B ( 33 ,  6 ) =    3.158195E-02
	B ( 33 ,  7 ) =   -2.876961E-02
	B ( 33 ,  8 ) =   -4.983043E-02
	B ( 33 ,  9 ) =    5.019939E-02
	B ( 34 ,  1 ) =    8.166213E-02
	B ( 34 ,  2 ) =    2.385720E-02
	B ( 34 ,  3 ) =    0.000000E+00
	B ( 34 ,  4 ) =   -1.181525E-01
	B ( 34 ,  5 ) =   -8.166213E-02
	B ( 34 ,  6 ) =    0.000000E+00
	B ( 34 ,  7 ) =    0.000000E+00
	B ( 34 ,  8 ) =    0.000000E+00
	B ( 34 ,  9 ) =    0.000000E+00
	B ( 35 ,  1 ) =   -8.166213E-02
	B ( 35 ,  2 ) =    2.385720E-02
	B ( 35 ,  3 ) =    0.000000E+00
	B ( 35 ,  4 ) =   -1.181525E-01
	B ( 35 ,  5 ) =    8.166213E-02
	B ( 35 ,  6 ) =    0.000000E+00
	B ( 35 ,  7 ) =    0.000000E+00
	B ( 35 ,  8 ) =    0.000000E+00
	B ( 35 ,  9 ) =    0.000000E+00
	B ( 36 ,  1 ) =    0.000000E+00
	B ( 36 ,  2 ) =    1.653002E-01
	B ( 36 ,  3 ) =    0.000000E+00
	B ( 36 ,  4 ) =    2.329046E-02
	B ( 36 ,  5 ) =    0.000000E+00
	B ( 36 ,  6 ) =    0.000000E+00
	B ( 36 ,  7 ) =    0.000000E+00
	B ( 36 ,  8 ) =    0.000000E+00
	B ( 36 ,  9 ) =    0.000000E+00
	B ( 37 ,  1 ) =    0.000000E+00
	B ( 37 ,  2 ) =    0.000000E+00
	B ( 37 ,  3 ) =   -1.754553E-01
	B ( 37 ,  4 ) =    0.000000E+00
	B ( 37 ,  5 ) =    0.000000E+00
	B ( 37 ,  6 ) =    0.000000E+00
	B ( 37 ,  7 ) =   -1.823385E-02
	B ( 37 ,  8 ) =    0.000000E+00
	B ( 37 ,  9 ) =    0.000000E+00
	B ( 38 ,  1 ) =    0.000000E+00
	B ( 38 ,  2 ) =    0.000000E+00
	B ( 38 ,  3 ) =   -8.772767E-02
	B ( 38 ,  4 ) =    0.000000E+00
	B ( 38 ,  5 ) =    0.000000E+00
	B ( 38 ,  6 ) =   -1.519488E-01
	B ( 38 ,  7 ) =   -9.116924E-03
	B ( 38 ,  8 ) =   -1.579098E-02
	B ( 38 ,  9 ) =    0.000000E+00
	B ( 39 ,  1 ) =    0.000000E+00
	B ( 39 ,  2 ) =    0.000000E+00
	B ( 39 ,  3 ) =    8.772767E-02
	B ( 39 ,  4 ) =    0.000000E+00
	B ( 39 ,  5 ) =    0.000000E+00
	B ( 39 ,  6 ) =   -1.519488E-01
	B ( 39 ,  7 ) =    9.116924E-03
	B ( 39 ,  8 ) =   -1.579098E-02
	B ( 39 ,  9 ) =    0.000000E+00
	B ( 40 ,  1 ) =    0.000000E+00
	B ( 40 ,  2 ) =    7.891340E-02
	B ( 40 ,  3 ) =    1.541707E-01
	B ( 40 ,  4 ) =    1.111874E-02
	B ( 40 ,  5 ) =    0.000000E+00
	B ( 40 ,  6 ) =    0.000000E+00
	B ( 40 ,  7 ) =    1.602188E-02
	B ( 40 ,  8 ) =    0.000000E+00
	B ( 40 ,  9 ) =    0.000000E+00
	B ( 41 ,  1 ) =   -3.898506E-02
	B ( 41 ,  2 ) =    1.138930E-02
	B ( 41 ,  3 ) =    7.708534E-02
	B ( 41 ,  4 ) =   -5.640537E-02
	B ( 41 ,  5 ) =    3.898506E-02
	B ( 41 ,  6 ) =    1.335157E-01
	B ( 41 ,  7 ) =    8.010941E-03
	B ( 41 ,  8 ) =    1.387536E-02
	B ( 41 ,  9 ) =    0.000000E+00
	B ( 42 ,  1 ) =    3.898506E-02
	B ( 42 ,  2 ) =    1.138930E-02
	B ( 42 ,  3 ) =   -7.708534E-02
	B ( 42 ,  4 ) =   -5.640537E-02
	B ( 42 ,  5 ) =   -3.898506E-02
	B ( 42 ,  6 ) =    1.335157E-01
	B ( 42 ,  7 ) =   -8.010941E-03
	B ( 42 ,  8 ) =    1.387536E-02
	B ( 42 ,  9 ) =    0.000000E+00
	B ( 43 ,  1 ) =    0.000000E+00
	B ( 43 ,  2 ) =    7.891340E-02
	B ( 43 ,  3 ) =   -1.541707E-01
	B ( 43 ,  4 ) =    1.111874E-02
	B ( 43 ,  5 ) =    0.000000E+00
	B ( 43 ,  6 ) =    0.000000E+00
	B ( 43 ,  7 ) =   -1.602188E-02
	B ( 43 ,  8 ) =    0.000000E+00
	B ( 43 ,  9 ) =    0.000000E+00
	B ( 44 ,  1 ) =   -3.898506E-02
	B ( 44 ,  2 ) =    1.138930E-02
	B ( 44 ,  3 ) =   -7.708534E-02
	B ( 44 ,  4 ) =   -5.640537E-02
	B ( 44 ,  5 ) =    3.898506E-02
	B ( 44 ,  6 ) =   -1.335157E-01
	B ( 44 ,  7 ) =   -8.010941E-03
	B ( 44 ,  8 ) =   -1.387536E-02
	B ( 44 ,  9 ) =    0.000000E+00
	B ( 45 ,  1 ) =    3.898506E-02
	B ( 45 ,  2 ) =    1.138930E-02
	B ( 45 ,  3 ) =    7.708534E-02
	B ( 45 ,  4 ) =   -5.640537E-02
	B ( 45 ,  5 ) =   -3.898506E-02
	B ( 45 ,  6 ) =   -1.335157E-01
	B ( 45 ,  7 ) =    8.010941E-03
	B ( 45 ,  8 ) =   -1.387536E-02
	B ( 45 ,  9 ) =    0.000000E+00
	B ( 46 ,  1 ) =   -4.988725E-02
	B ( 46 ,  2 ) =   -4.762038E-03
	B ( 46 ,  3 ) =   -4.005471E-03
	B ( 46 ,  4 ) =   -7.255670E-02
	B ( 46 ,  5 ) =    9.432920E-03
	B ( 46 ,  6 ) =    2.081304E-02
	B ( 46 ,  7 ) =   -7.021235E-02
	B ( 46 ,  8 ) =    1.134682E-02
	B ( 46 ,  9 ) =    2.205483E-02
	B ( 47 ,  1 ) =    2.808287E-02
	B ( 47 ,  2 ) =    2.754063E-02
	B ( 47 ,  3 ) =   -2.002735E-02
	B ( 47 ,  4 ) =   -4.025403E-02
	B ( 47 ,  5 ) =   -6.853720E-02
	B ( 47 ,  6 ) =    6.937678E-03
	B ( 47 ,  7 ) =   -4.493281E-02
	B ( 47 ,  8 ) =   -5.513227E-02
	B ( 47 ,  9 ) =    2.205483E-02
	B ( 48 ,  1 ) =   -3.887712E-02
	B ( 48 ,  2 ) =    7.891340E-02
	B ( 48 ,  3 ) =   -1.602188E-02
	B ( 48 ,  4 ) =    1.111874E-02
	B ( 48 ,  5 ) =   -1.577209E-03
	B ( 48 ,  6 ) =   -1.387536E-02
	B ( 48 ,  7 ) =    2.527954E-02
	B ( 48 ,  8 ) =   -6.647909E-02
	B ( 48 ,  9 ) =    2.205483E-02
	B ( 49 ,  1 ) =   -4.988725E-02
	B ( 49 ,  2 ) =   -4.762038E-03
	B ( 49 ,  3 ) =    4.005471E-03
	B ( 49 ,  4 ) =   -7.255670E-02
	B ( 49 ,  5 ) =    9.432920E-03
	B ( 49 ,  6 ) =   -2.081304E-02
	B ( 49 ,  7 ) =    7.021235E-02
	B ( 49 ,  8 ) =   -1.134682E-02
	B ( 49 ,  9 ) =    2.205483E-02
	B ( 50 ,  1 ) =    2.808287E-02
	B ( 50 ,  2 ) =    2.754063E-02
	B ( 50 ,  3 ) =    2.002735E-02
	B ( 50 ,  4 ) =   -4.025403E-02
	B ( 50 ,  5 ) =   -6.853720E-02
	B ( 50 ,  6 ) =   -6.937678E-03
	B ( 50 ,  7 ) =    4.493281E-02
	B ( 50 ,  8 ) =    5.513227E-02
	B ( 50 ,  9 ) =    2.205483E-02
	B ( 51 ,  1 ) =   -3.887712E-02
	B ( 51 ,  2 ) =    7.891340E-02
	B ( 51 ,  3 ) =    1.602188E-02
	B ( 51 ,  4 ) =    1.111874E-02
	B ( 51 ,  5 ) =   -1.577209E-03
	B ( 51 ,  6 ) =    1.387536E-02
	B ( 51 ,  7 ) =   -2.527954E-02
	B ( 51 ,  8 ) =    6.647909E-02
	B ( 51 ,  9 ) =    2.205483E-02
	B ( 52 ,  1 ) =    4.988725E-02
	B ( 52 ,  2 ) =   -4.762038E-03
	B ( 52 ,  3 ) =   -4.005471E-03
	B ( 52 ,  4 ) =   -7.255670E-02
	B ( 52 ,  5 ) =   -9.432920E-03
	B ( 52 ,  6 ) =   -2.081304E-02
	B ( 52 ,  7 ) =   -7.021235E-02
	B ( 52 ,  8 ) =   -1.134682E-02
	B ( 52 ,  9 ) =   -2.205483E-02
	B ( 53 ,  1 ) =    3.887712E-02
	B ( 53 ,  2 ) =    7.891340E-02
	B ( 53 ,  3 ) =    1.602188E-02
	B ( 53 ,  4 ) =    1.111874E-02
	B ( 53 ,  5 ) =    1.577209E-03
	B ( 53 ,  6 ) =   -1.387536E-02
	B ( 53 ,  7 ) =   -2.527954E-02
	B ( 53 ,  8 ) =   -6.647909E-02
	B ( 53 ,  9 ) =   -2.205483E-02
	B ( 54 ,  1 ) =   -2.808287E-02
	B ( 54 ,  2 ) =    2.754063E-02
	B ( 54 ,  3 ) =    2.002735E-02
	B ( 54 ,  4 ) =   -4.025403E-02
	B ( 54 ,  5 ) =    6.853720E-02
	B ( 54 ,  6 ) =    6.937678E-03
	B ( 54 ,  7 ) =    4.493281E-02
	B ( 54 ,  8 ) =   -5.513227E-02
	B ( 54 ,  9 ) =   -2.205483E-02
	B ( 55 ,  1 ) =    4.988725E-02
	B ( 55 ,  2 ) =   -4.762038E-03
	B ( 55 ,  3 ) =    4.005471E-03
	B ( 55 ,  4 ) =   -7.255670E-02
	B ( 55 ,  5 ) =   -9.432920E-03
	B ( 55 ,  6 ) =    2.081304E-02
	B ( 55 ,  7 ) =    7.021235E-02
	B ( 55 ,  8 ) =    1.134682E-02
	B ( 55 ,  9 ) =   -2.205483E-02
	B ( 56 ,  1 ) =    3.887712E-02
	B ( 56 ,  2 ) =    7.891340E-02
	B ( 56 ,  3 ) =   -1.602188E-02
	B ( 56 ,  4 ) =    1.111874E-02
	B ( 56 ,  5 ) =    1.577209E-03
	B ( 56 ,  6 ) =    1.387536E-02
	B ( 56 ,  7 ) =    2.527954E-02
	B ( 56 ,  8 ) =    6.647909E-02
	B ( 56 ,  9 ) =   -2.205483E-02
	B ( 57 ,  1 ) =   -2.808287E-02
	B ( 57 ,  2 ) =    2.754063E-02
	B ( 57 ,  3 ) =   -2.002735E-02
	B ( 57 ,  4 ) =   -4.025403E-02
	B ( 57 ,  5 ) =    6.853720E-02
	B ( 57 ,  6 ) =   -6.937678E-03
	B ( 57 ,  7 ) =   -4.493281E-02
	B ( 57 ,  8 ) =    5.513227E-02
	B ( 57 ,  9 ) =   -2.205483E-02
	B ( 58 ,  1 ) =    4.339781E-02
	B ( 58 ,  2 ) =    1.267846E-02
	B ( 58 ,  3 ) =   -1.337656E-02
	B ( 58 ,  4 ) =   -6.278993E-02
	B ( 58 ,  5 ) =   -4.339781E-02
	B ( 58 ,  6 ) =   -7.722959E-03
	B ( 58 ,  7 ) =   -6.408926E-02
	B ( 58 ,  8 ) =   -3.700195E-02
	B ( 58 ,  9 ) =    0.000000E+00
	B ( 59 ,  1 ) =    0.000000E+00
	B ( 59 ,  2 ) =    8.784567E-02
	B ( 59 ,  3 ) =    0.000000E+00
	B ( 59 ,  4 ) =    1.237728E-02
	B ( 59 ,  5 ) =    0.000000E+00
	B ( 59 ,  6 ) =   -1.544592E-02
	B ( 59 ,  7 ) =    0.000000E+00
	B ( 59 ,  8 ) =   -7.400390E-02
	B ( 59 ,  9 ) =    0.000000E+00
	B ( 60 ,  1 ) =   -4.339781E-02
	B ( 60 ,  2 ) =    1.267846E-02
	B ( 60 ,  3 ) =    1.337656E-02
	B ( 60 ,  4 ) =   -6.278993E-02
	B ( 60 ,  5 ) =    4.339781E-02
	B ( 60 ,  6 ) =   -7.722959E-03
	B ( 60 ,  7 ) =    6.408926E-02
	B ( 60 ,  8 ) =   -3.700195E-02
	B ( 60 ,  9 ) =    0.000000E+00
	B ( 61 ,  1 ) =    4.339781E-02
	B ( 61 ,  2 ) =    1.267846E-02
	B ( 61 ,  3 ) =    1.337656E-02
	B ( 61 ,  4 ) =   -6.278993E-02
	B ( 61 ,  5 ) =   -4.339781E-02
	B ( 61 ,  6 ) =    7.722959E-03
	B ( 61 ,  7 ) =    6.408926E-02
	B ( 61 ,  8 ) =    3.700195E-02
	B ( 61 ,  9 ) =    0.000000E+00
	B ( 62 ,  1 ) =    0.000000E+00
	B ( 62 ,  2 ) =    8.784567E-02
	B ( 62 ,  3 ) =    0.000000E+00
     	B ( 62 ,  4 ) =    1.237728E-02
	B ( 62 ,  5 ) =    0.000000E+00
	B ( 62 ,  6 ) =    1.544592E-02
	B ( 62 ,  7 ) =    0.000000E+00
	B ( 62 ,  8 ) =    7.400390E-02
	B ( 62 ,  9 ) =    0.000000E+00
	B ( 63 ,  1 ) =   -4.339781E-02
	B ( 63 ,  2 ) =    1.267846E-02
	B ( 63 ,  3 ) =   -1.337656E-02
	B ( 63 ,  4 ) =   -6.278993E-02
	B ( 63 ,  5 ) =    4.339781E-02
	B ( 63 ,  6 ) =    7.722959E-03
	B ( 63 ,  7 ) =   -6.408926E-02
	B ( 63 ,  8 ) =    3.700195E-02
	B ( 63 ,  9 ) =    0.000000E+00
	
	RETURN
	END

	

C----------------------------------------------------
	SUBROUTINE SLIP_DIRECTION(CMT,CNT)
     
	IMPLICIT REAL*8 (A-H,O-Z)  

	PARAMETER(NSLIP=100,ITOTSLIPS=5)
	DIMENSION CNT(3,NSLIP),CMT(3,NSLIP),NSLIPS(ITOTSLIPS)


	R2=1.D0/2.D0
	R3=DSQRT(3.D0)
	IC=0

	CMT=0.D0
	CNT=0.D0	


	NSLIPS(1)=3
	NSLIPS(2)=3
	NSLIPS(3)=6
	NSLIPS(4)=12
	NSLIPS(5)=6




C---BASAL SLIP SYSTEM
	   DO IS=1,3
	      CNT(1,IS)=  0.0
	      CNT(2,IS)=  0.0 
	      CNT(3,IS)=  1.0D0
	   ENDDO

	   CMT(1,1)=  R2
	   CMT(2,1)= -R2*R3 
	   CMT(3,1)=  0.0D0
         
	   CMT(1,2)=  R2
	   CMT(2,2)=  R3*R2
	   CMT(3,2)=  0.0D0
      
	   CMT(1,3)= -1.0D0
	   CMT(2,3)=  0.0D0
	   CMT(3,3)=  0.0D0
	   IC=IC+NSLIPS(1)



C---    PRISMATIC SLIP SYSTEM
	   CNT(1,1+IC)=  0.0
	   CNT(2,1+IC)=  1.0D0 
	   CNT(3,1+IC)=  0.0
	   
	   CNT(1,2+IC)= -R3*R2
	   CNT(2,2+IC)=  R2
	   CNT(3,2+IC)=  0.0
	   
	   CNT(1,3+IC)= -R3*R2
	   CNT(2,3+IC)= -R2 
	   CNT(3,3+IC)=  0.0
	   
	   
	   CMT(1,1+IC)=  1.0D0
	   CMT(2,1+IC)=  0.0
	   CMT(3,1+IC)=  0.0
	   
	   CMT(1,2+IC)=  R2
	   CMT(2,2+IC)=  R3*R2
	   CMT(3,2+IC)=  0.0
	   
	   CMT(1,3+IC)= -R2
	   CMT(2,3+IC)=  R3*R2
	   CMT(3,3+IC)=  0.0
	   IC=IC+NSLIPS(2)

	   

C       PYRAMIDAL A SLIP
	   BUNBO=SQRT(4*(1.587)**2+3)
	   BUNSHI=1.587
	   CNT(1,1+IC)=0.0
	   CNT(2,1+IC)=-2*BUNSHI/BUNBO
	   CNT(3,1+IC)=R3/BUNBO
         
	   CNT(1,2+IC)=R3*BUNSHI/BUNBO
	   CNT(2,2+IC)=-BUNSHI/BUNBO
	   CNT(3,2+IC)=R3/BUNBO
	   
	   CNT(1,3+IC)= R3*BUNSHI/BUNBO
	   CNT(2,3+IC)= BUNSHI/BUNBO
	   CNT(3,3+IC)= R3/BUNBO 
	   
	   CNT(1,4+IC)= 0.0
	   CNT(2,4+IC)= 2*BUNSHI/BUNBO
	   CNT(3,4+IC)= R3/BUNBO
	   
	   CNT(1,5+IC)=-R3*BUNSHI/BUNBO
	   CNT(2,5+IC)= BUNSHI/BUNBO
	   CNT(3,5+IC)= R3/BUNBO
         
	   CNT(1,6+IC)=-R3*BUNSHI/BUNBO
	   CNT(2,6+IC)=-BUNSHI/BUNBO
	   CNT(3,6+IC)= R3/BUNBO
	   

	   CMT(1,1+IC)= 1.0D0
	   CMT(2,1+IC)= 0.0
	   CMT(3,1+IC)= 0.0 
         
	   CMT(1,2+IC)= R2
	   CMT(2,2+IC)= R2*R3
	   CMT(3,2+IC)= 0.0        
	   
	   CMT(1,3+IC)=-R2
	   CMT(2,3+IC)= R2*R3
	   CMT(3,3+IC)= 0.0 
	   
	   CMT(1,4+IC)=-1.0D0
	   CMT(2,4+IC)= 0.0
	   CMT(3,4+IC)= 0.0
	   
	   CMT(1,5+IC)=-R2
	   CMT(2,5+IC)=-R2*R3
	   CMT(3,5+IC)= 0.0  
         
	   CMT(1,6+IC)= R2
	   CMT(2,6+IC)=-R2*R3
	   CMT(3,6+IC)= 0.0
	   IC=IC+NSLIPS(3)


C       1ST ORDER <C+A> SLIP
	 BUNBO=SQRT(4.0*(1.587)**2+3.0)
	 BUNSHI=1.587
	    
	 CNT(1,1+IC)=  0.0
	 CNT(2,1+IC)=  -2*BUNSHI/BUNBO 
	 CNT(3,1+IC)=  R3/BUNBO
	 
	 CNT(1,2+IC)=  R3*BUNSHI/BUNBO
	 CNT(2,2+IC)=  -BUNSHI/BUNBO 
	 CNT(3,2+IC)=  R3/BUNBO
	 
	 CNT(1,3+IC)=  R3*BUNSHI/BUNBO
	 CNT(2,3+IC)=  BUNSHI/BUNBO 
	 CNT(3,3+IC)=  R3/BUNBO
	
	 CNT(1,4+IC)=  0.0
	 CNT(2,4+IC)=  2*BUNSHI/BUNBO 
	 CNT(3,4+IC)=  R3/BUNBO
	 
	 CNT(1,5+IC)=  -R3*BUNSHI/BUNBO
	 CNT(2,5+IC)=  BUNSHI/BUNBO 
	 CNT(3,5+IC)=  R3/BUNBO
	 
	 CNT(1,6+IC)=  -R3*BUNSHI/BUNBO
	 CNT(2,6+IC)=  -BUNSHI/BUNBO 
	 CNT(3,6+IC)=  R3/BUNBO
	
	 CNT(1,7+IC)=  0.0
	 CNT(2,7+IC)=  -2*BUNSHI/BUNBO 
	 CNT(3,7+IC)=  R3/BUNBO
	 
	 CNT(1,8+IC)=  R3*BUNSHI/BUNBO
	 CNT(2,8+IC)=  -BUNSHI/BUNBO 
	 CNT(3,8+IC)=  R3/BUNBO
	 
	 CNT(1,9+IC)=  R3*BUNSHI/BUNBO
	 CNT(2,9+IC)=  BUNSHI/BUNBO 
	 CNT(3,9+IC)=  R3/BUNBO
	 
	 CNT(1,10+IC)=  0.0
	 CNT(2,10+IC)=  2*BUNSHI/BUNBO 
	 CNT(3,10+IC)=  R3/BUNBO
	 
	 CNT(1,11+IC)= - R3*BUNSHI/BUNBO
	 CNT(2,11+IC)=  BUNSHI/BUNBO 
	 CNT(3,11+IC)=  R3/BUNBO
	
	 CNT(1,12+IC)=  -R3*BUNSHI/BUNBO
	 CNT(2,12+IC)=  -BUNSHI/BUNBO 
	 CNT(3,12+IC)=  R3/BUNBO


	 BUNB=2.0*(SQRT((1.587)**2+1.0))
	 BUSH=1.587
	 CMT(1,1+IC)=  1.0/BUNB
	 CMT(2,1+IC)=  R3/BUNB 
	 CMT(3,1+IC)=  2*BUSH/BUNB

	 CMT(1,2+IC)=  -1.0/BUNB
	 CMT(2,2+IC)=  R3/BUNB 
	 CMT(3,2+IC)=  2*BUSH/BUNB

	 CMT(1,3+IC)=  -2.0/BUNB
	 CMT(2,3+IC)=  0.0 
	 CMT(3,3+IC)=  2*BUSH/BUNB

	 CMT(1,4+IC)=  -1.0/BUNB
	 CMT(2,4+IC)=  -R3/BUNB 
	 CMT(3,4+IC)=  2*BUSH/BUNB

	 CMT(1,5+IC)=  1.0/BUNB
	 CMT(2,5+IC)=  -R3/BUNB 
	 CMT(3,5+IC)=  2*BUSH/BUNB

	 CMT(1,6+IC)=  2.0/BUNB
	 CMT(2,6+IC)=  0.0 
	 CMT(3,6+IC)=  2*BUSH/BUNB

	 CMT(1,7+IC)=  -1.0/BUNB
	 CMT(2,7+IC)=  R3/BUNB 
	 CMT(3,7+IC)=  2*BUSH/BUNB

	 CMT(1,8+IC)=  -2.0/BUNB
	 CMT(2,8+IC)=  0.0 
	 CMT(3,8+IC)=  2*BUSH/BUNB

	 CMT(1,9+IC)=  -1.0/BUNB
	 CMT(2,9+IC)=  -R3/BUNB 
	 CMT(3,9+IC)=  2*BUSH/BUNB

	 CMT(1,10+IC)=  1.0/BUNB
	 CMT(2,10+IC)=  -R3/BUNB 
	 CMT(3,10+IC)=  2*BUSH/BUNB
	
	 CMT(1,11+IC)=  2.0/BUNB
	 CMT(2,11+IC)=  0.0 
	 CMT(3,11+IC)=  2*BUSH/BUNB

	 CMT(1,12+IC)=  1.0/BUNB
	 CMT(2,12+IC)=  R3/BUNB 
	 CMT(3,12+IC)=  2*BUSH/BUNB

	 IC=IC+NSLIPS(4)



C       2ND ORDER <C+A> SLIP
	   BUNB=2.0*(SQRT((1.587)**2+1.0))
	   BUSH=1.587
	   CNT(1,1+IC)=  BUSH/BUNB
	   CNT(2,1+IC)=  -R3*BUSH/BUNB 
	   CNT(3,1+IC)=  2.0/BUNB
	   
	   CNT(1,2+IC)= 2*BUSH/BUNB
	   CNT(2,2+IC)=  0.0
	   CNT(3,2+IC)=  2.0/BUNB
	   
	   CNT(1,3+IC)=  BUSH/BUNB
	   CNT(2,3+IC)=  R3*BUSH/BUNB 
	   CNT(3,3+IC)=  2.0/BUNB

	   CNT(1,4+IC)=  -BUSH/BUNB
	   CNT(2,4+IC)=  R3*BUSH/BUNB 
	   CNT(3,4+IC)=  2.0/BUNB
	   
	   CNT(1,5+IC)= -2.0*BUSH/BUNB
	   CNT(2,5+IC)= 0.0 
	   CNT(3,5+IC)=  2.0/BUNB
	   
	   CNT(1,6+IC)=  -BUSH/BUNB
	   CNT(2,6+IC)=  -R3*BUSH/BUNB 
	   CNT(3,6+IC)=  2.0/BUNB
	   
	   CMT(1,1+IC)=  -1.0/BUNB
	   CMT(2,1+IC)=  R3/BUNB 
	   CMT(3,1+IC)=  2.0*BUSH/BUNB
	   
	   CMT(1,2+IC)=  -2.0/BUNB
	   CMT(2,2+IC)=  0.0 
	   CMT(3,2+IC)=  2.0*BUSH/BUNB
	   
	   CMT(1,3+IC)=  -1.0/BUNB
	   CMT(2,3+IC)=  -R3/BUNB 
	   CMT(3,3+IC)=  2.0*BUSH/BUNB
	   
	   CMT(1,4+IC)=  1.0/BUNB
	   CMT(2,4+IC)=  -R3/BUNB 
	   CMT(3,4+IC)=  2.0*BUSH/BUNB
	   
	   CMT(1,5+IC)=  2.0/BUNB
	   CMT(2,5+IC)=  0.0 
	   CMT(3,5+IC)=  2.0*BUSH/BUNB
	   
	   CMT(1,6+IC)=  1.0/BUNB
	   CMT(2,6+IC)=  R3/BUNB 
	   CMT(3,6+IC)=  2.0*BUSH/BUNB
	   IC=IC+NSLIPS(5)



	RETURN
	END	
	