
      integer::minproc,maxproc,maxcomm,maxnodelem,maxdim
      integer::maxnp,maxeln,maxstatev,msv,maxgrp,maxprops,maxel
      integer::maxnode,maxnset,maxns,maxdf,ndofelx
      integer::mdofe,mdofx,mcstr,mbound1,mbound2,mbound3
      integer::maxcrd,mnelx,lr,lw,nnodex,nslip,mfac,mdofproc
      integer::maxcut,maxelset,maxels,maxpress,mphase
      integer::ibrickortet,npost,maxnrhs,nflush
      integer::mpiinfsizemax, errorcalornot
      integer::maxadjelem,maxadjgrain,maxadjgrainelem
      INTEGER::SPR_NO_COEF,NSP_SIZE
      INTEGER::IRESTART_WRITE,N_RESTART_WRITE

      integer, parameter :: MAX_HISTORY_N = 4001
	  
      parameter(minproc=1,maxproc=4)
      parameter(maxcomm=maxproc*maxproc,maxnodelem=120)
      parameter(maxdim=3,ndofelx=maxdim)
      parameter(maxnp=1,maxeln=4)
      parameter(maxstatev=461,msv=maxstatev*maxnp)  ! maxstatev=461 for PL, maxstatev=600 for DD
      parameter(maxgrp=1,maxprops=224)    ! maxprops=224  for PL, maxprops=141  for DD
      parameter(maxel=6,maxnode=8)
      parameter(maxns=4,maxnset=10)
      parameter(maxelset=12,maxels=1400,maxpress=4,mphase=2)
      parameter(maxdf=ndofelx,mdofe=maxdf*maxeln)
      parameter(mdofx=ndofelx*maxnode,mcstr=mdofx)
      parameter(mbound1=80000,mbound2=10,mbound3=10)
      parameter(maxcrd=maxdim*maxnode,mnelx=maxeln*maxel)
      parameter(lr=5,lw=6)
      parameter(nnodex=maxnode,nslip=100)
      parameter(mfac=100,mdofproc=mdofx*mfac/minproc)
      parameter(maxcut=5)
      parameter(ibrickortet=2)
      parameter(npost=1,nflush=1)
      parameter(mpiinfsizemax=maxcomm)
      parameter(maxadjelem=120,maxadjgrain=10,maxadjgrainelem=120)
      PARAMETER(SPR_NO_COEF=10,NSP_SIZE=600)
      PARAMETER(IRESTART_WRITE=0,N_RESTART_WRITE=25)
