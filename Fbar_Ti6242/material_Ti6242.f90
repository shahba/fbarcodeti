      module material_Ti6242
      
      use crystal_Titanium    ! slip system definitions
      
      implicit none
      
      integer, parameter :: mat_nstate = 386
      
      ! MATERIAL PARAMETERS
      !-------------------------
      ! variables below store parameters for tension/compression and pri-A/trans-B:
      ! 1st index: slip system, or slip family (..._fam)
      ! 2nd index: 1: tension. 2: compression
      ! 3rd index: Phase: 1: HCP in primary Alpha. 2: HCP in transformed Beta
      
      ! HCP - hardening related params
      ! - per family
      real(8) :: g0_bar_famHCP(nslipfam,2,2) ! internal slip resistance
      real(8) :: xh0_famHCP(nslipfam,2,2)    ! max (initial) hardening rates
      real(8) :: xs_bar_famHCP(nslipfam,2,2) ! max (reference) limit slip resistances
      real(8) :: xn_famHCP(nslipfam,2,2)     ! sensitivity of limit strength to shear rate
      real(8) :: xr_famHCP(nslipfam,2,2)     ! sensitivity of hardening rate to slip resistance
      ! - per slip system
      real(8) :: g0_bar_HCP(nslip_max,2,2)    ! internal slip resistances
      real(8) :: xh0_HCP(nslip_max,2,2)
      real(8) :: xs_bar_HCP(nslip_max,2,2)
      real(8) :: xn_HCP(nslip_max,2,2)        
      real(8) :: xr_HCP(nslip_max,2,2)        

      ! BCC - hardening related parameters
      ! - per family
      real(8) :: g0_bar_famBCC(nslipfam,2)     ! internal slip resistance:
                                               ! 1st index: slip families: {110},{112}soft,{112}hard,{123}soft,{123}hard
                                               ! 2nd index: 1: b=[-111] systems, 2: all others
      real(8) :: xh_0_famBCC(nslipfam)         ! BCC hardening params per slip family
      real(8) :: xh_inf_famBCC(nslipfam)       ! BCC hardening params per slip family
      real(8) :: xg_0_famBCC(nslipfam)         ! BCC hardening params per slip family
      real(8) :: xg_inf_famBCC(nslipfam)       ! BCC hardening params per slip family
      ! - per slip system.
      ! 1st index: slip system
      ! 2nd index: 1: soft 2: hard direction
      real(8) :: g0_bar_BCC(nslip_max,2)    ! internal slip resistance (for hard/soft directions)
      real(8) :: xh_0_BCC(nslip_max,2)      ! BCC hardening params per slip family
      real(8) :: xh_inf_BCC(nslip_max,2)    ! BCC hardening params per slip family
      real(8) :: xg_0_BCC(nslip_max,2)      ! BCC hardening params per slip family
      real(8) :: xg_inf_BCC(nslip_max,2)    ! BCC hardening params per slip family
      
      ! flow related parameters
      real(8) :: burgers_famHCP(nslipfam)     ! HCP - burgers vector lengths
      real(8) :: burgers_HCP(nslip_max)       
      real(8) :: burgers_famBCC(nslipfam)     ! BCC - burgers vector lengths
      real(8) :: burgers_BCC(nslip_max,2)
      ! HCP - per family
      real(8) :: a_dot_famHCP(nslipfam)      ! HCP reference slip rate (power law formulation)
      real(8) :: a_dot_therm_famHCP(nslipfam)! HCP reference slip rate (thermally activated flow)
      real(8) :: rate_exp_famHCP(nslipfam)   ! HCP flow sensitivity    (power law formulation)
      real(8) :: E_act_famHCP(nslipfam)      ! HCP slip activation energy (thermally activated flow)
      ! HCP - per slip system
      real(8) :: a_dot_HCP(nslip_max)         ! reference slip rate (power law formulation)
      real(8) :: a_dot_therm_HCP(nslip_max)   ! reference slip rate (thermally activated flow)
      real(8) :: rate_exp_HCP(nslip_max)      ! flow sensitivity (power law formulation)
      real(8) :: E_act_HCP(nslip_max)         ! Slip activation energy at each slip system
      real(8) :: softening_T_char_HCP(nslip_max)  ! characteristic temperature for drag-dominated flow (full thermal activation)
      real(8) :: softening_g_therm_HCP(nslip_max) ! thermally activated (local) stress barrier to flow
      real(8) :: g_drop_c_a_300_400               ! drop in the internal slip resistance of the c+a systems rom 300K to 400K (MPa)
      
      ! BCC - per family
      real(8) :: a_dot_famBCC(nslipfam)             ! BCC reference slip rate  (power law formulation)
      real(8) :: a_dot_therm_famBCC(nslipfam)       ! BCC reference slip rate  (thermally activated flow)
      real(8) :: rate_exp_famBCC(nslipfam)          ! BCC flow sensitivity     (power law formulation)
      real(8) :: E_act_famBCC(nslipfam)             ! BCC slip activation energy (thermally activated flow)
      real(8) :: softening_T_char_BCC(nslip_max)    ! characteristic temperature for drag-dominated flow (full thermal activation)
      real(8) :: softening_g_therm_BCC(nslip_max)   ! thermally activated (local) stress barrier to flow
      ! BCC - per slip system
      ! 2nd index: 1: soft direction (-), 2: hard direction (+)
      real(8) :: a_dot_BCC(nslip_max,2)       ! reference slip rate (soft/hard)(power law formulation)
      real(8) :: a_dot_therm_BCC(nslip_max,2) ! reference slip rate (soft/hard)(thermally activated flow)
      real(8) :: rate_exp_BCC(nslip_max,2)    ! flow sensitivity (soft/hard)(power law formulation)
      real(8) :: E_act_BCC(nslip_max,2)       ! Slip activation energy at each slip system (soft/hard)
      
      ! Hall-Petch parameters
      ! if, D_hp < 0, grain size is used for that slip system
      real(8) :: K_hp_HCP(nslipfam,2,2)   ! Hall Petch coefficient HCP tens/Comp, pri-A/trans-B
      real(8) :: D_hp_HCP(nslipfam,2)     ! charactrs. slip length HCP pri-A/trans-B
      real(8) :: K_hp_BCC(2)              ! Hall Petch coefficient BCC
      real(8) :: D_hp_BCC(2)              ! charactrs. slip length BCC
      
      ! Material Internal Variables - to be passed down to flow rule
      ! (generic, for HCP/BCC)
      real(8) :: a_dot(nslip_max)
      real(8) :: a_dot_therm(nslip_max)
      real(8) :: rate_exp(nslip_max)
      real(8) :: E_act(nslip_max)
      real(8) :: burgers(nslip_max)
      ! hardening variables
      real(8) :: g0_BCC(nslip_max,2)      ! BCC, initial slip resistance (g0_bar + g_hp)
      real(8) :: g_BCC(nslip_max,2)       ! BCC, current slip resistance (g0_bar + g_hp + g_hardening)
      real(8) :: g_bar_HCP(nslip_max,2)   ! HCP, initial slip resistance (g0_bar + g_hp)
      real(8) :: g0_HCP(nslip_max,2)      ! HCP, initial slip resistance (g0_bar + g_hp)
      real(8) :: g_HCP(nslip_max,2)       ! HCP, current slip resistance (g0_bar + g_hp + g_hardening)
      real(8) :: dd_g(nslip_max)          ! change in work-hardening over this time step (tension/compression independent, per slip system)
      real(8) :: chi_t(nslip_max)         ! back stress at time t
      real(8) :: chi_tau(nslip_max)       ! back stress at time t
      real(8) :: lambda_beta_t(nslip_max) ! lattice incompatibilities on crystal planes
      
!      integer :: iCrystal
!      integer :: iPhase
      
      real(8) :: xc  ! back stress evolution: kinematic hardening coefficient
      real(8) :: xd  ! back stress evolution: dynamic recovery coefficient
      
      real(8) :: hardeningSelf         ! self-hardening coefficient q(i,i)
      real(8) :: hardeningLatent       ! latent-hardening coefficient q(i,j)
      
      real(8) :: p_therm, q_therm      ! flow exponents p and q for the thermally activated flow rule
      
      real(8) :: heatCapacityVolumetric! volumetric heat capacity (J/Kelvins/m^3)
      
      ! damage
      real(8) :: slipSystemLife           ! maximum plastic deformation
      real(8) :: slipSystemDecohesionLife ! decohesion strain
      real(8) :: slipSystemExhaustionExp  ! damage = (tot_gamma/slipSystemLife) ^ exp
      real(8) :: wpBackStress_t(4)        ! total dissipated energy on the back-stress, on the basal plane and 3 prism planes (time t)
      real(8) :: wpBackStress_t_NonLocal(4)! non-local/regularized plastic work dissipated on the back-stress...
      real(8) :: wpBackStress_tau(4)      ! total dissipated energy on the back-stress, on the basal plane and 3 prism planes (time tau)
      real(8) :: nucleationFactor(4)      ! thermally activated void nucleation probability
      
      real(8) :: c11_HCP,c12_HCP,c13_HCP,c33_HCP,c55_HCP ! HCP elasticity coeffs
      real(8) :: c1_BCC,c2_BCC,c3_BCC                    ! BCC elasticity coeffs

      real(8) :: c11_k_T_HCP,c12_k_T_HCP,c13_k_T_HCP, &  ! rate of change of HCP elastic coefficients with temperature
                 c33_k_T_HCP,c55_k_T_HCP 
      real(8) :: c1_k_T_BCC,c2_k_T_BCC,c3_k_T_BCC        ! rate of change of BCC elastic coefficients with temperature

      real(8) :: thrm_C(3,2)   ! thermal expansion coefficients along crystal axes 1,2,3 (HCP/BCC)
      
      contains
      
      SUBROUTINE initialize_Parameters(props,nprops,matName)
      
      implicit none
      
      real(8), intent(in) :: props(nprops)
      integer, intent(in) :: nprops
      character(*), intent(in) :: matName
      
      integer :: iTC
      
      ! initialize crystal slip systems
      call init_crys_HCP()
      call init_crys_BCC()
      
      xc=props(137)  ! back-stress accummulation coefficient
      xd=props(138)  ! back-stress dynamic recovery  coefficient

      hardeningSelf = props(20)   ! self and latent hardening coefficients for the phenomenological hardening rule
      hardeningLatent = props(21)
      
      p_therm = props(22)  ! p and q - rate exponents for the thermally activated flow rule
      q_therm = props(23)
      
      if (hardeningSelf == 0.d0 .OR. hardeningLatent == 0.d0) then
         write(*,*) 'WARNING !!! - set hardeningSelf, hardeningLatent in props.dat'
         stop
      endif
      
      slipSystemLife = props(17)             ! total allowed slip system plastic deformation
      slipSystemDecohesionLife = props(18)   ! decohesion strain after exhaustion of slip life
      slipSystemExhaustionExp = props(19)    ! damage = (tot_gamma/slipSystemLife) ^ exp
      wpBackStress_t(:) = 0.d0               ! total dissipated energy on the back-stress, on the basal plane and 3 prism planes (time t)
      wpBackStress_tau(:) = 0.d0             ! total dissipated energy on the back-stress, on the basal plane and 3 prism planes (time t)
      
      heatCapacityVolumetric = props(24)     ! volumetric heat capacity (megaJ/Kelvins/m^3) -- pure Titanium: 2.43 (megaJ/K/m^3=MPa/K) --> wp(MPa=MJ/m^3) / VHC (MJ/K/m^3) = dT (K)
      
      ! read in elasticity coefficients
      ! -------------------------------
      ! HCP
      c11_HCP=props(1)
      c12_HCP=props(2)
      c13_HCP=props(3)
      c33_HCP=props(4)
      c55_HCP=props(5)
      ! BCC
      c1_BCC=props(297)
      c2_BCC=props(298)
      c3_BCC=props(299)
      
      ! read in thermal softening rate of elastic constants
      ! HCP
      c11_k_T_HCP=props(139)
      c12_k_T_HCP=props(140)
      c13_k_T_HCP=props(141)
      c33_k_T_HCP=props(142)
      c55_k_T_HCP=props(143)
      ! BCC
      c1_k_T_BCC=props(365)
      c2_k_T_BCC=props(366)
      c3_k_T_BCC=props(367)
      
      
      ! read in thermal expansion coefficients         
      ! -------------------------------
      ! HCP - transverse isotropy
      thrm_C(1,HCP) = props(6)    
      thrm_C(2,HCP) = props(6)
      thrm_C(3,HCP) = props(7)         
      ! BCC - isotropic elasticity
      thrm_C(1:3,BCC) = props(300)
      
      ! read in flow rule parameters
      ! -------------------------------      
      ! HCP
      burgers_famHCP(1:8)    = props(9:16)
      a_dot_famHCP(1:8)      = props(25:32)  
      rate_exp_famHCP(1:8)   = props(33:40)
      a_dot_therm_famHCP(1:8)= props(41:48)
      E_act_famHCP(1:8)      = props(49:56)
      ! distribute to slip systems
      call slipFamily_to_SystemsHCP(burgers_famHCP,    burgers_HCP,    nslips_famHCP)      
      call slipFamily_to_SystemsHCP(a_dot_famHCP,      a_dot_HCP,      nslips_famHCP)
      call slipFamily_to_SystemsHCP(a_dot_therm_famHCP,a_dot_therm_HCP,nslips_famHCP)
      call slipFamily_to_SystemsHCP(rate_exp_famHCP,   rate_exp_HCP,   nslips_famHCP)
      call slipFamily_to_SystemsHCP(E_act_famHCP,      E_act_HCP,      nslips_famHCP)
      
      ! thermal softening law:
      ! delta_g(dT) = -[1-exp(dT/T_c)]*Dg_therm where dT = T - 300K
      ! HCP - T_c
      softening_T_char_HCP(1:3)   = props(41)   ! 3 x basal a
      softening_T_char_HCP(4:6)   = props(42)   ! 3 x prism a
      softening_T_char_HCP(7:12)  = props(43)   ! 6 x pyramidal a
      softening_T_char_HCP(13:30) = props(44)   !18 x 1st & 2nd pyramidal c+a
      ! HCP - Dg_therm
      softening_g_therm_HCP(1:3)   = props(45)  ! 3 x basal a
      softening_g_therm_HCP(4:6)   = props(46)  ! 3 x prism a
      softening_g_therm_HCP(7:12)  = props(47)  ! 6 x pyramidal a
      softening_g_therm_HCP(13:30) = props(48)  !18 x 1st & 2nd pyramidal c+a
      ! drop in the internal slip resistance of the c+a systems rom 300K to 400K (MPa)
      g_drop_c_a_300_400 = props(8)
      
      ! BCC
      burgers_famBCC(1:5)    = props(301:305)
      a_dot_famBCC(1:5)      = props(313:317)
      rate_exp_famBCC(1:5)   = props(318:322)
      a_dot_therm_famBCC(1:5)= props(323:327)
      E_act_famBCC(1:5)      = props(328:332)
      ! distribute to slip systems
      call slipFamily_to_SystemsBCC(burgers_famBCC,    burgers_BCC,    nslips_famBCC)
      call slipFamily_to_SystemsBCC(a_dot_famBCC,      a_dot_BCC,      nslips_famBCC)
      call slipFamily_to_SystemsBCC(a_dot_therm_famBCC,a_dot_therm_BCC,nslips_famBCC)
      call slipFamily_to_SystemsBCC(rate_exp_famBCC,   rate_exp_BCC,   nslips_famBCC)
      call slipFamily_to_SystemsBCC(E_act_famBCC,      E_act_BCC,      nslips_famBCC)
      
      ! thermal softening law:
      ! delta_g(dT) = -[1-exp(dT/T_c)]*Dg_therm where dT = T - 300K
      ! BCC - T_c
      softening_T_char_BCC(1:48)  = props(41)   ! use the same for 3 x basal a
      ! BCC - Dg_therm
      softening_g_therm_BCC(1:48)   = props(41) ! use the same for 3 x basal a
      
      ! read in hardening parameters for HCP in primary-alpha grains
      ! ------------------------------------------------------------
      g0_bar_famHCP(1:8,1,pri_A) = props(57:64)   ! internal slip resistances (excluding HallPetch) for HCP/primary-a in tension    
      g0_bar_famHCP(1:8,2,pri_A) = props(65:72)   ! internal slip resistances (excluding HallPetch) for HCP/primary-a in compression    
      xn_famHCP(1:8,1,pri_A) = props(73:80)  ! limit CRSS strain-rate response exponent // HCP in primary-a // tension 
      xn_famHCP(1:8,2,pri_A) = props(81:88)  ! limit CRSS strain-rate response exponent // HCP in primary-a // compression 
      xr_famHCP(1:8,1,pri_A) = props(89:96)  ! CRSS saturation exponent // HCP in primary-a // tension 
      xr_famHCP(1:8,2,pri_A) = props(97:104) ! CRSS saturation exponent // HCP in primary-a // compression 
      xh0_famHCP(1:8,1,pri_A) = props(105:112) ! h0: initial hardening rate // HCP in primary-a // tension
      xh0_famHCP(1:8,2,pri_A) = props(113:120) ! h0: initial hardening rate // HCP in primary-a // compression
      xs_bar_famHCP(1:8,1,pri_A)=props(121:128) ! HCP in primary-a // tension
      xs_bar_famHCP(1:8,2,pri_A)=props(129:136) ! HCP in primary-a // compression
      ! distribute to slip systems
      do iTC = 1,2   ! tension/compression
         call slipFamily_to_SystemsHCP(g0_bar_famHCP(:,iTC,pri_A),g0_bar_HCP(:,iTC,pri_A),nslips_famHCP)
         call slipFamily_to_SystemsHCP(xn_famHCP(:,iTC,pri_A),    xn_HCP(:,iTC,pri_A),    nslips_famHCP)
         call slipFamily_to_SystemsHCP(xr_famHCP(:,iTC,pri_A),    xr_HCP(:,iTC,pri_A),    nslips_famHCP)
         call slipFamily_to_SystemsHCP(xh0_famHCP(:,iTC,pri_A),   xh0_HCP(:,iTC,pri_A),   nslips_famHCP)
         call slipFamily_to_SystemsHCP(xs_bar_famHCP(:,iTC,pri_A),xs_bar_HCP(:,iTC,pri_A),nslips_famHCP)
      enddo
      
      ! read in hardening parameters for HCP in transormed-Beta grains
      ! --------------------------------------------------------------
      g0_bar_famHCP(1:8,1,trans_B) = props(193:200)   ! internal slip resistances (excluding HallPetch) // HCP in trans-B // tension    
      g0_bar_famHCP(1:8,2,trans_B) = props(201:208)   ! internal slip resistances (excluding HallPetch) // HCP in trans-B // compression    
      xn_famHCP(1:8,1,trans_B) = props(209:216)  ! n: limit CRSS strain-rate response exponent // HCP in trans-B // tension 
      xn_famHCP(1:8,2,trans_B) = props(217:224)  ! n: limit CRSS strain-rate response exponent // HCP in trans-B // compression 
      xr_famHCP(1:8,1,trans_B) = props(225:232)  ! r: CRSS saturation exponent // HCP in trans-Beta // tension 
      xr_famHCP(1:8,2,trans_B) = props(233:240)  ! r: CRSS saturation exponent // HCP in trans-Beta // compression 
      xh0_famHCP(1:8,1,trans_B) = props(241:248)    ! h0: initial hardening rate // HCP in trans-Beta // tension
      xh0_famHCP(1:8,2,trans_B) = props(249:256)    ! h0: initial hardening rate // HCP in trans-Beta // compression
      xs_bar_famHCP(1:8,1,trans_B) = props(257:264) ! HCP in trans-B // tension
      xs_bar_famHCP(1:8,2,trans_B) = props(265:272) ! HCP in trans-B // compression
      
      ! distribute to slip systems
      do iTC = 1,2   ! tension/compression
         call slipFamily_to_SystemsHCP(g0_bar_famHCP(:,iTC,trans_B),g0_bar_HCP(:,iTC,trans_B),nslips_famHCP)
         call slipFamily_to_SystemsHCP(xn_famHCP(:,iTC,trans_B),    xn_HCP(:,iTC,trans_B),    nslips_famHCP)
         call slipFamily_to_SystemsHCP(xr_famHCP(:,iTC,trans_B),    xr_HCP(:,iTC,trans_B),    nslips_famHCP)
         call slipFamily_to_SystemsHCP(xh0_famHCP(:,iTC,trans_B),   xh0_HCP(:,iTC,trans_B),   nslips_famHCP)
         call slipFamily_to_SystemsHCP(xs_bar_famHCP(:,iTC,trans_B),xs_bar_HCP(:,iTC,trans_B),nslips_famHCP)
      enddo
      
      ! BCC slip families (used in indexing of g0_fam,xh_0,xh_inf,xg_0,xg_inf)
      ! [1] = all n={101}, all b=<111> systems
      ! [2] = all n={112}, b=(soft) directions (-)
      ! [3] = all n={112}, b=(hard) directions (+)
      ! [4] = all n={123}, b=(soft) directions (-)
      ! [5] = all n={123}, b=(hard) directions (+)
      
      ! read in hardening parameters for BCC
      ! ------------------------------------------------------------
      g0_bar_famBCC(1:5,1)= props(333:337)   ! internal slip resistances (excluding HallPetch) // BCC in trans-B, b1[-111]
      g0_bar_famBCC(1:5,2)= props(338:342)   ! internal slip resistances (excluding HallPetch) // BCC in trans-B, all others
      xh_0_famBCC(1:5)  = props(345:349)    
      xh_inf_famBCC(1:5)= props(111:115)    
      xg_0_famBCC(1:5)  = props(355:359)    
      xg_inf_famBCC(1:5)= props(360:364)    
      ! distribute to slip systems
      call slipFamily_to_SystemsBCC(g0_bar_famBCC(:,2),g0_bar_BCC,nslips_famBCC)    ! distribute to all slip systems
      call slipFamily_to_SystemsBCC_b1(g0_bar_famBCC(:,1),g0_bar_BCC,nslips_famBCC) ! distribute only to b1=[-111] slip systems
      call slipFamily_to_SystemsBCC(xh_0_famBCC,  xh_0_BCC,  nslips_famBCC)
      call slipFamily_to_SystemsBCC(xh_inf_famBCC,xh_inf_BCC,nslips_famBCC)
      call slipFamily_to_SystemsBCC(xg_0_famBCC,  xg_0_BCC,  nslips_famBCC)
      call slipFamily_to_SystemsBCC(xg_inf_famBCC,xg_inf_BCC,nslips_famBCC)
      
      ! Hall-Petch parameters
      ! if, D_hp < 0, grain size is used for that slip system
      K_hp_HCP(1:8,1,pri_A)  = props(145:152)    ! Hall Petch coefficient HCP, tension, pri-A
      K_hp_HCP(1:8,2,pri_A)  = props(153:160)    ! Hall Petch coefficient HCP, compression, pri-A
      D_hp_HCP(1:8,pri_A)    = props(161:168)    ! charactrs. slip length HCP, pri-A
      K_hp_HCP(1:8,1,trans_B)= props(273:280)    ! Hall Petch coefficient HCP, tension, trans-B
      K_hp_HCP(1:8,2,trans_B)= props(281:288)    ! Hall Petch coefficient HCP, compression, trans-B
      D_hp_HCP(1:8,trans_B)  = props(289:296)    ! charactrs. slip length HCP, trans-B
      K_hp_BCC(1)            = props(369)        ! Hall Petch coefficient BCC, b1[-111]
      K_hp_BCC(2)            = props(370)        ! Hall Petch coefficient BCC, all others
      D_hp_BCC(1)            = props(371)        ! charactrs. slip length BCC, b1[-111]
      D_hp_BCC(2)            = props(372)        ! charactrs. slip length BCC, all others
      

      END SUBROUTINE
      
      SUBROUTINE initialize_StateVariables(stateVars,nsvars,elemID,grainSizes,grainTexture,grainPhases,nGrains,grainIDelem,NELX)
      
      implicit none
      
      real(8), intent(out):: stateVars(nsvars)
      real(8), intent(in) :: grainSizes(nGrains)
      real(8), intent(in) :: grainTexture(3,nGrains)
      integer, intent(in) :: grainPhases(nGrains)
      integer, intent(in) :: elemID,nGrains,NELX,nsvars
      integer, intent(in) :: grainIDelem(NELX)

      
      integer :: grainID, iFam, phase, ps
      real(8) :: euler(3), ckrone(9), grainSize
      real(8) :: g_hp_fam(nslipfam),g_hp(nslip_max)
      real(8) :: g_hp_famBCC(nslipfam,2)
      
      ckrone = 0.d0
      ckrone(1) = 1.d0 ! ROW-MAJOR
      ckrone(5) = 1.d0 ! ROW-MAJOR
      ckrone(9) = 1.d0 ! ROW-MAJOR
   
      grainID = grainIDelem(elemID)
      grainSize = grainSizes(grainID)
      euler(1:3) = grainTexture(1:3,grainID)
      phase = grainPhases(grainID)
      
      stateVars(1 :30)  = 0.d0  ! delta_g, work hardening   HCP
      stateVars(31:60)  = 0.d0  ! tot_gamma                 HCP
      stateVars(61:66)  = 0.d0  ! 2nd PK   (Voigt)          HCP
      stateVars(67:75)  = ckrone! Fp       (Row Major)      HCP
      stateVars(76:123) = 0.d0  ! delta_g, work hardening   BCC
      stateVars(124:171)= 0.d0  ! tot_gamma                 BCC      
      stateVars(172:177)= 0.d0  ! 2nd PK   (Voigt)          BCC      
      stateVars(178:186)= ckrone! Fp       (Row Major)      BCC         
      stateVars(187:192)= 0.d0  ! Logarithmic Strain (Voigt)-generic, output only-
      stateVars(193)    = 0.d0  ! Local Temperature (ambient+adiabatic) -generic, output only-
      stateVars(211:216)= 0.d0  ! Cauchy Stress      (Voigt)-generic, output only-
      stateVars(221:250)= 0.d0  ! Chi, back stress          HCP
      stateVars(251:298)= 0.d0  ! Chi, back stress          BCC
      stateVars(299)       = 0.d0  ! w_p, plastic work         -generic-
      stateVars(300)       = 0.d0  ! volume                    -generic, used by FEM-UEL only-
      stateVars(301:303)= euler ! Crys.Orient.(Euler angles)-generic, output only-
      stateVars(304)       = phase ! phase (1:priA 2:traB)     -generic, input  only-
         
      ! determine hall-petch contributions to strength
      if (phase==pri_A) then
         ! HCP in pri-A, tension
         do iFam=1,nslipfam
            CALL calc_HallPetchStrength(K_hp_HCP(iFam,1,pri_A),D_hp_HCP(iFam,pri_A), &
                                        grainSize,g_hp_fam(iFam))
         enddo
         stateVars(305:312) = g_hp_fam(1:8)  ! HCP in primary-a, Tension
         ! HCP in pri-A, compression
         do iFam=1,nslipfam
            CALL calc_HallPetchStrength(K_hp_HCP(iFam,2,pri_A),D_hp_HCP(iFam,pri_A), &
                                        grainSize,g_hp_fam(iFam))
         enddo
         stateVars(313:320) = g_hp_fam(1:8)  ! HCP in primary-a, Compression
         
      elseif(phase==trans_B) then
         ! HCP in trans_B, tension
         do iFam=1,nslipfam
            CALL calc_HallPetchStrength(K_hp_HCP(iFam,1,trans_B),D_hp_HCP(iFam,trans_B), &
                                        grainSize,g_hp_fam(iFam))
         enddo
         stateVars(305:312) = g_hp_fam(1:8)  ! HCP in trans-B, Tension
         ! HCP in trans_B, compression
         do iFam=1,nslipfam
            CALL calc_HallPetchStrength(K_hp_HCP(iFam,2,trans_B),D_hp_HCP(iFam,trans_B), &
                                        grainSize,g_hp_fam(iFam))
         enddo
         stateVars(313:320) = g_hp_fam(1:8)  ! HCP in trans-B, Compression

         ! BCC in trans_B, only b=[-111] slip systems
         CALL calc_HallPetchStrength(K_hp_BCC(1),D_hp_BCC(1), &
                                     grainSize,g_hp_famBCC(1,1))
         g_hp_famBCC(2:5,1)=g_hp_famBCC(1,1)
         stateVars(337:341) = g_hp_famBCC(1:5,1)   ! hall-petch contributions for systems with b=[-111]
         
         ! BCC in trans_B, all other slip systems
         CALL calc_HallPetchStrength(K_hp_BCC(2),D_hp_BCC(2), &
                                     grainSize,g_hp_famBCC(1,2))
         g_hp_famBCC(2:5,2)=g_hp_famBCC(1,2)
         stateVars(342:346) = g_hp_famBCC(1:5,2)   ! hall-petch contributions for all others
            
      endif
      
      stateVars(321:326)= 0.d0      ! Green-Lagrange Creep Strain
      
      ! deniz - removed in FBar version - this block will be used for Fp_mix
      !stateVars(327) = 0.d0         ! non-local/regularized damage energy on the basal plane  -HCP-
      !stateVars(328:330)= 0.d0      ! non-local/regularized damage energy on the prism planes -HCP-
      
      stateVars(327:335) = ckrone    ! Fp_mix (homogenized Fp)
      
      stateVars(347:351)= 0.d0  ! not yet used
      
      
      stateVars(352)       = 0.d0   ! normal stress on basal   HCP, output only
      stateVars(353)       = 0.d0   ! shear stress on basal    HCP, output only
      stateVars(354:356)= 0.d0   ! c-axis dir               HCP, output only
      stateVars(357:365)= 0.d0   ! Nye tensor               generic, input only
      stateVars(366)       = grainSize
      stateVars(367)       = 0.d0   ! NOT USED
      stateVars(368)       = 0.d0   ! norm of delta_gamma     generic, output only
      stateVars(369:377)= 0.d0   ! R_cryst, cur.lattice ori. (Row Major) -generic, output only

      stateVars(378)   = 0.d0    ! plastic work on the back stress on the basal plane  -HCP-
      stateVars(379:381)= 0.d0   ! plastic work on the back stress on the prism planes -HCP-
      stateVars(382:385)= 0.d0   ! thermally activated void nucleation factor
      stateVars(386)= 0.d0       ! not yet used
      
      END SUBROUTINE
      
      SUBROUTINE calc_HallPetchStrength(K_hp,D_hp,grainSize,g_hp)
      implicit none
      real(8), intent(in) :: K_hp, D_hp, grainSize
      real(8), intent(out):: g_hp
      if (D_hp < 0.d0) then
         g_hp = K_hp / dsqrt(grainSize)
      else
         g_hp = K_hp / dsqrt(D_hp)
      endif
      END SUBROUTINE
      
      SUBROUTINE calc_SlipSysStrengthsBCC(g0_BCC,g_BCC,g_hp_famBCC,delta_g_BCC, &
                                          temperature,thermalSoftening)
      implicit none
      real(8), intent(in) :: g_hp_famBCC(nslipfam,2)
      real(8), intent(in) :: delta_g_BCC(nslip_max)
      real(8), optional, intent(in) :: temperature
      logical, optional, intent(in) :: thermalSoftening
      real(8), intent(out):: g0_BCC(nslip_max,2)     ! for each slip sys, soft/hard directions
      real(8), intent(out):: g_BCC(nslip_max,2)
      ! locals
      real(8) :: g0_hp_BCC(nslip_max,2)      
      real(8) :: g0_softening_BCC(nslip_max)
      integer :: isys
      
      ! calculate thermal softening at each slip system
      g0_softening_BCC(:) = 0.d0
      if(present(temperature).and.present(thermalSoftening)) then
         if(thermalSoftening) then
            do isys = 1,nslip_BCC
               g0_softening_BCC(isys) = (1.d0 - dexp(-(temperature-300.d0)/softening_T_char_BCC(isys)))*softening_g_therm_BCC(isys)
            enddo
         endif
      endif
      
      ! distribute the Hall Petch contributions to slip systems
      call slipFamily_to_SystemsBCC(g_hp_famBCC(:,2),g0_hp_BCC,nslips_famBCC)    ! distribute to all slip systems
      call slipFamily_to_SystemsBCC_b1(g_hp_famBCC(:,1),g0_hp_BCC,nslips_famBCC) ! distribute only to b1=[-111] slip systems
      
      ! now add hall petch contributions to internal resistances of each slip system
      g0_BCC = g0_bar_BCC + g0_hp_BCC
      ! add work-hardening to get the current slip system resistance
      g_BCC(:,1) = g0_BCC(:,1) + delta_g_BCC(:) ! accumulated work-hardening is 
      g_BCC(:,2) = g0_BCC(:,2) + delta_g_BCC(:) ! independent of (soft/hard) slip direction
      
      END SUBROUTINE

      SUBROUTINE calc_SlipSysStrengthsHCP(g0_HCP,g_HCP,phase,g_hp_famHCP,delta_g_HCP, &
                                          temperature,thermalSoftening)

      implicit none
      real(8), intent(in) :: g_hp_famHCP(nslipfam,2) ! for each slip fam, tension/compression
      real(8), intent(in) :: delta_g_HCP(nslip_max)
      integer, intent(in) :: phase
      real(8), optional, intent(in) :: temperature
      logical, optional, intent(in) :: thermalSoftening
      real(8), intent(out):: g0_HCP(nslip_max,2)     ! for each slip sys, tension/compression
      real(8), intent(out):: g_HCP(nslip_max,2)
      ! locals
      real(8) :: g0_hp_HCP(nslip_max,2)      ! tension/compression   
      real(8) :: g0_softening_HCP(nslip_max)
      integer :: isys
      
      ! calculate thermal softening at each slip system
      g0_softening_HCP(:) = 0.d0
      if(present(temperature).and.present(thermalSoftening)) then
         if(thermalSoftening) then
            do isys = 1,12
               g0_softening_HCP(isys) = (1.d0 - dexp(-(temperature-300.d0)/softening_T_char_HCP(isys))) &
                                        *softening_g_therm_HCP(isys)
            enddo
            do isys = 13,30   ! <c+a> systems
               if(temperature < 400.d0) then
                  g0_softening_HCP(isys) = g_drop_c_a_300_400*(temperature-300.d0)/100.d0
               else
                  g0_softening_HCP(isys) = g_drop_c_a_300_400 + &
                              (1.d0 - dexp(-(temperature-400.d0)/softening_T_char_HCP(isys))) &
                              *softening_g_therm_HCP(isys)
               endif
            enddo
         endif
      endif
      
      ! distribute the Hall Petch contributions to slip systems
      call slipFamily_to_SystemsHCP(g_hp_famHCP(:,1),g0_hp_HCP(:,1),nslips_famHCP) ! tension values
      call slipFamily_to_SystemsHCP(g_hp_famHCP(:,2),g0_hp_HCP(:,2),nslips_famHCP) ! compression values
      
      ! now add hall petch contributions to internal resistances of each slip system
      g0_HCP(:,:) = g0_bar_HCP(:,:,phase) + g0_hp_HCP(:,:)
      ! add work-hardening to get the current slip system resistance
      g_HCP(:,1) = g0_HCP(:,1) + delta_g_HCP(:) - g0_softening_HCP(:) ! accumulated work-hardening and thermal softening are  
      g_HCP(:,2) = g0_HCP(:,2) + delta_g_HCP(:) - g0_softening_HCP(:) ! independent of tension/compression
      
      END SUBROUTINE
      
      
      SUBROUTINE calc_ElasticConstantsTemp_HCP(c11_T_HCP,c12_T_HCP,c13_T_HCP, &
                                               c33_T_HCP,c55_T_HCP, &
                                               temperature,thermalSoftening)

      implicit none
      real(8), intent(out) :: c11_T_HCP,c12_T_HCP,c13_T_HCP, &
                              c33_T_HCP,c55_T_HCP                 ! HCP elasticity coeffs at ambient temperature
      real(8), optional, intent(in) :: temperature
      logical, optional, intent(in) :: thermalSoftening
      
      if ( thermalSoftening ) then
         c11_T_HCP = c11_HCP - (temperature-300.d0)*c11_k_T_HCP
         c12_T_HCP = c12_HCP - (temperature-300.d0)*c12_k_T_HCP
         c13_T_HCP = c13_HCP - (temperature-300.d0)*c13_k_T_HCP
         c33_T_HCP = c33_HCP - (temperature-300.d0)*c33_k_T_HCP
         c55_T_HCP = c55_HCP - (temperature-300.d0)*c55_k_T_HCP
      else
         c11_T_HCP = c11_HCP
         c12_T_HCP = c12_HCP
         c13_T_HCP = c13_HCP
         c33_T_HCP = c33_HCP
         c55_T_HCP = c55_HCP
      endif
      END SUBROUTINE
      
      SUBROUTINE calc_ElasticConstantsTemp_BCC(c1_T_BCC,c2_T_BCC,c3_T_BCC, &
                                               temperature,thermalSoftening)

      implicit none
      real(8), intent(out) :: c1_T_BCC,c2_T_BCC,c3_T_BCC    ! BCC elasticity coeffs at ambient temperature
      real(8), optional, intent(in) :: temperature
      logical, optional, intent(in) :: thermalSoftening
      
      if ( thermalSoftening ) then
         c1_T_BCC = c1_BCC - (temperature-300.d0)*c1_k_T_BCC
         c2_T_BCC = c2_BCC - (temperature-300.d0)*c2_k_T_BCC
         c3_T_BCC = c3_BCC - (temperature-300.d0)*c3_k_T_BCC
      else
         c1_T_BCC = c1_BCC
         c2_T_BCC = c2_BCC
         c3_T_BCC = c3_BCC
      endif
      END SUBROUTINE
      
      
      
      SUBROUTINE calc_AdibaticHeating(w_p_t, dT_wp_t)
      implicit none
      real(8), intent(in) :: w_p_t
      real(8), intent(out):: dT_wp_t
      
      dT_wp_t = 0.9d0 * w_p_t / heatCapacityVolumetric
      
      END SUBROUTINE

      end module