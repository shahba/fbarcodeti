
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc	
      subroutine jacobian_new(dfgrd0,dfgrd1,fp_t,fp_tau,s_tan,
     & c_alpha_1d,tau,
     & delta_gamma,defe_1,
     & dtime,de_g,tpk_1d_tau,
     & ddsdde,ntens,islip) 
      
      implicit real*8(a-h,o-z)
      
      parameter (nslip=100)
      dimension de_g(3,3,3,3),defe_1(nslip),tpk_2d_g(3,3),
     &     ckrone(3,3,3,3),c_alpha(3,3,nslip),g_alpha_tau(nslip),
     &     dfgrd0(3,3),ddsdde(6,6),tau(nslip),dfgrd1(3,3)
     

      dimension c_alpha_1d(6,nslip),fe_t(3,3),fe_tau(3,3),
     &     fe_tau_t(3,3),tot_gamma(nslip),
     &     s_tan(3,3,nslip),
     &     fp_t(3,3),fp_t_inv(3,3),
     &     delta_gamma(nslip),fp_tau(3,3),fp_tau_inv(3,3),
     &     gamma_s_2d(3,3),ckrone_2d(3,3),
     &     tpk_1d_tau(6),
     &     dfgrdt(3,3),fp_tau_t(3,3)   


      
      real(8):: sqr_1(3,3,islip)
      real(8)::sqr_2(3,3,3,3),sqr_tmp(3,3,3,3)
      real(8)::sqr_3(3,3,3,3,islip),sqr_tmp1(3,3,3,3)
      real(8)::sqr_tmp2(3,3,3,3)
      real(8)::sqr_3_tmp(3,3),sqr_3_tmp_t(3,3)
      real(8):: fp_tau_inv_t(3,3)
      real(8)::cir_tmp1(3,3),cir_tmp2(3,3)
      real(8):: cir_1(3,3)
        real(8):: cir_2_tmp0(3,3,3,3),cir_2_tmp2_2d_inv(9,9)
        real(8):: cir_2_tmp1(3,3,3,3),cir_2_tmp2(3,3,3,3)
        real(8):: cir_2_tmp2_inv(3,3,3,3),cir_2_tmp3(3,3,3,3)
        real(8):: cir_2_tmp4(3,3,3,3),cir_2_tmp4_2d(9,9)
        real(8):: cir_2(3,3,3,3), cir_2_2d(9,9)
      real(8):: cir_3_tmp0(3,3),cir_3_tmp1(3,3)
      real(8):: cir_3(3,3,3,3)
      
      real(8):: final_0(3,3,3,3), final_0_2d_inv(9,9)
      real(8):: final_1(3,3,3,3),final_2(3,3,3,3)
      real(8):: final_3(3,3,3,3),final_4(3,3,3,3)
      real(8):: final_3_tmp(3,3),final_2_tmp(3,3)
      real(8):: final_5(3,3,3,3),final_6(3,3,3,3)
      real(8):: final_7(3,3,3,3),final_7_2d(9,9)
      real(8):: final_4d(3,3,3,3),final_2d_tmp(6,6)
      real(8):: final_2d(9,9),ddsdde_SE(6,6)
      real(8):: TS_ddsdde(3,3,3,3),dfgrd1_inv(3,3)
      real(8):: ddsdde_TK(6,6)
      
      integer:: a,b,m,n,i,j,k,l
      integer :: INFO
      


!__________________________________________________________
! initialization
!__________________________________________________________

      sqr_2=0.0
      sqr_3_tmp=0.0
      sqr_3=0.0
      cir_2_tmp1=0.0
      cir_2_tmp3=0.0
      cir_2_2d=0.0
      cir_3_tmp0=0.0
      cir_3_tmp1=0.0
      cir_3=0.0
      final_2_tmp=0.0
      final_3_tmp=0.0
      final_6=0.0
      final_2d=0.0



      do isys=1,islip

        do i=1,3
          do j=1,3
            sqr_1(i,j,isys)=0.5d0*defe_1(isys)
     &        *(s_tan(i,j,isys)+s_tan(j,i,isys))
          enddo
        enddo
      enddo
      
      
      
      
 !___________________________________________________________  
 !square_2
 !___________________________________________________________  

      
      
      call matinv3(fp_tau,fp_tau_inv,det_fp)
      do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3
      sqr_tmp(i,j,k,l)=fp_tau_inv(i,k)*fp_tau_inv(j,l)
                enddo
            enddo
         enddo
      enddo
      
      
      do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3  
                    do m=1,3
                        do n=1,3  
      sqr_2(i,j,k,l)=sqr_2(i,j,k,l)+de_g(i,j,m,n)*sqr_tmp(m,n,k,l)   
                        enddo
                    enddo
                enddo
            enddo
         enddo
      enddo
      
      
      
      
!___________________________________________________________       
!sqr_3
!___________________________________________________________ 

    
      
      do isys=1,islip   

      sqr_3_tmp=0.0
       do i=1,3
        do j=1,3
            do k=1,3
      
        sqr_3_tmp(i,j)=sqr_3_tmp(i,j)+fp_tau_inv(i,k)*s_tan(k,j,isys)
      
            enddo
        enddo
      enddo
      
      call trans(sqr_3_tmp,sqr_3_tmp_t)
      call trans(fp_tau_inv,fp_tau_inv_t)
      
       do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3
      sqr_tmp1(i,j,k,l)=fp_tau_inv_t(i,k)*sqr_3_tmp_t(j,l)
      sqr_tmp2(i,j,k,l)=sqr_3_tmp_t(i,k)*fp_tau_inv_t(j,l)
      
                enddo
            enddo
         enddo
      enddo
      
      
      
       do i=1,3
        do j=1,3
            do k=1,3
                do l=1,3  
                    do m=1,3
                        do n=1,3 
      sqr_3(i,j,k,l,isys)=sqr_3(i,j,k,l,isys)+
     & de_g(i,j,m,n)*(sqr_tmp1(m,n,k,l)+sqr_tmp2(m,n,k,l)) 
                        enddo
                    enddo
                enddo
            enddo
         enddo
        enddo
      
      
      
      
      enddo
      
      
      
!___________________________________________________________        
! cir_1      
!___________________________________________________________       

      
      call sigmat(tpk_1d_tau,tpk_2d_g)
      call trans(fp_tau,fp_tau_t)
      
        call mat33(cir_tmp1,tpk_2d_g,fp_tau_t,3)
        call mat33(cir_tmp2,fp_tau,cir_tmp1,3)
      
        do i=1,3
        do j=1,3
        cir_1(i,j)=1.0/det_fp*cir_tmp2(i,j)
        enddo
        enddo
        
        
        
      
!___________________________________________________________        
! cir_2
!___________________________________________________________  

      
      
      call cal_ckrone_2d(ckrone_2d)  
      
      do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3 
      cir_2_tmp0(i,j,k,l)=ckrone_2d(i,k)*ckrone_2d(j,l)
                enddo
            enddo
          enddo
        enddo

      call sigmat_crys(c_alpha_1d,c_alpha,islip)


      do isys=1,islip
        
       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3   
        cir_2_tmp1(i,j,k,l)=cir_2_tmp1(i,j,k,l)+
     & c_alpha(i,j,isys)*sqr_1(k,l,isys)
                enddo
             enddo
         enddo
       enddo
       
       enddo
       
       
     
       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3    
                
        cir_2_tmp2(i,j,k,l)= cir_2_tmp0(i,j,k,l)+cir_2_tmp1(i,j,k,l)   
        
                enddo
             enddo
         enddo
       enddo
      
       call tr4dto2d(cir_2_tmp2,cir_2_tmp2_2d_inv) 
     
       call lapack_invert(9,cir_2_tmp2_2d_inv,INFO)
       
      do isys=1,islip

       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3  
           cir_2_tmp3(i,j,k,l)=cir_2_tmp3(i,j,k,l)+
     & delta_gamma(isys)*sqr_3(i,j,k,l,isys)    
                enddo
             enddo
         enddo
       enddo
       
      enddo 
       
       do i=1,3
         do j=1,3
            do k=1,3
                do l=1,3
       cir_2_tmp4(i,j,k,l)=sqr_2(i,j,k,l)-cir_2_tmp3(i,j,k,l)
                enddo
             enddo
         enddo
       enddo


      call tr4dto2d(cir_2_tmp4,cir_2_tmp4_2d)
      
      do i=1,9
      do j=1,9
      do k=1,9
      
      cir_2_2d(i,j)=cir_2_2d(i,j)+
     &  cir_2_tmp2_2d_inv(i,k)*cir_2_tmp4_2d(k,j)
     
      enddo
      enddo
      enddo

      call tr2dto4d(cir_2_2d, cir_2)
      
      
      

!___________________________________________________________       
!cir_3
!___________________________________________________________  

      
      
      do isys=1,islip

      cir_3_tmp0=0.0
      do i=1,3
      do j=1,3
      do k=1,3
      cir_3_tmp0(i,j)=cir_3_tmp0(i,j)+s_tan(i,k,isys)*fp_t(k,j)
      enddo
      enddo
      enddo
      
      cir_3_tmp1=0.0
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      cir_3_tmp1(i,j)=cir_3_tmp1(i,j)+
     &sqr_1(k,l,isys)*cir_2(i,j,k,l)
      enddo
      enddo
      enddo
      enddo
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      cir_3(i,j,k,l)=cir_3(i,j,k,l)+
     &cir_3_tmp0(i,j)*cir_3_tmp1(k,l)
      enddo  
      enddo
      enddo
      enddo
      
      enddo
      
      
!___________________________________________________________      
! final jacobian
!___________________________________________________________



      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      
      final_0(i,j,k,l)=fp_tau(i,k)*fp_tau(j,l)
      
      enddo  
      enddo
      enddo
      enddo   
      
      
      call tr4dto2d(final_0, final_0_2d_inv)
      call lapack_invert(9,final_0_2d_inv,INFO)
      
      
      
      do i=1,9
      do j=1,9 
      
       final_0_2d_inv(i,j)=det_fp*final_0_2d_inv(i,j)     
      enddo  
      enddo
      
      
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
       final_1(i,j,k,l)=cir_1(i,j)*fp_tau_inv_t(k,l)       
      enddo  
      enddo
      enddo
      enddo 
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      final_2_tmp(j,i)=final_2_tmp(j,i)+
     &tpk_2d_g(i,k)*fp_tau_t(k,j) 
      enddo  
      enddo
      enddo
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_2(i,j,k,l)=ckrone_2d(i,k)*final_2_tmp(j,l)
      enddo  
      enddo
      enddo
      enddo
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      final_3_tmp(i,j)=final_3_tmp(i,j)+
     &fp_tau(i,k)*tpk_2d_g(k,j) 
      enddo  
      enddo
      enddo
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_3(i,j,k,l)=final_3_tmp(i,l)*ckrone_2d(j,k)
      enddo  
      enddo
      enddo
      enddo
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3

      final_4(i,j,k,l)=1.0/det_fp*(final_2(i,j,k,l)+final_3(i,j,k,l))
     
      enddo
      enddo
      enddo
      enddo
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_5(i,j,k,l)=final_1(i,j,k,l)-final_4(i,j,k,l)
      enddo  
      enddo
      enddo
      enddo
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      do m=1,3
      do n=1,3
      final_6(i,j,k,l)=final_6(i,j,k,l)+final_5(i,j,m,n)*cir_3(m,n,k,l)
      enddo
      enddo
      enddo  
      enddo
      enddo
      enddo      
      
      
      
      do i=1,3
      do j=1,3
      do k=1,3
      do l=1,3
      final_7(i,j,k,l)=cir_2(i,j,k,l)+final_6(i,j,k,l)
      enddo  
      enddo
      enddo
      enddo
      
      
      call tr4dto2d(final_7,final_7_2d)
      
      do i=1,9
      do j=1,9
      do k=1,9
      final_2d(i,j)=final_2d(i,j)+final_0_2d_inv(i,k)*
     & final_7_2d(k,j) 
      enddo
      enddo
      enddo




      call tr2dto4d(final_2d,final_4d)
      ! final_4d is for:  T_ij=final_4d_ijkl*E_kl
      
      call tr4to2(final_4d,ddsdde)
       

    
      return
      end
      
      
      
      
      
      
      
      
      
      
      subroutine cvrt(a,b,c)
      
      implicit none
      integer:: a,b,c
      
      if (a==1) then
      b=1
      c=1
      elseif (a==2) then
      b=2
      c=2
      elseif (a==3) then
      b=3
      c=3
      elseif (a==4) then
      b=1
      c=2
      elseif (a==5) then
      b=1
      c=3
      elseif (a==6) then
      b=2
      c=3
      elseif (a==7) then
      b=2
      c=1
      elseif (a==8) then
      b=3
      c=1
      elseif (a==9) then
      b=3
      c=2
      endif
      
      return 
      end
      
      
      subroutine cvrt_inv(b,c,a)
      
      implicit none
      integer:: a,b,c
      
      if ((b==1).and.(c==1)) then
      a=1
      elseif ((b==2).and.(c==2)) then
      a=2
      elseif ((b==3).and.(c==3)) then
      a=3
      elseif ((b==1).and.(c==2)) then
      a=4
      elseif ((b==1).and.(c==3)) then
      a=5
      elseif ((b==2).and.(c==3))then
      a=6
      elseif ((b==2).and.(c==1)) then
      a=7
      elseif ((b==3).and.(c==1)) then
      a=8
      elseif ((b==3).and.(c==2)) then
      a=9
      endif
      
      return 
      end     
      
      
      
      
      subroutine tr4dto2d(A_4d, A_2d)
      
      implicit none
      real(8):: A_4d(3,3,3,3), A_2d(9,9)
      integer:: i,j,k,l,m,n
      
      
      do i=1,3
      do j=1,3
      call cvrt_inv(i,j,m)
      
      do k=1,3
      do l=1,3
      call cvrt_inv(k,l,n)
      
      A_2d(m,n)=A_4d(i,j,k,l)
      enddo
      enddo
      enddo
      enddo
      
      end
      
      
      
      
      
      
      subroutine tr2dto4d(A_2d, A_4d)
      
      implicit none
      real(8):: A_4d(3,3,3,3), A_2d(9,9)
      integer:: i,j,k,l,m,n
      
      
      do m=1,9
      call cvrt(m,i,j)
      do n=1,9
      call cvrt(n,k,l)
      
      A_4d(i,j,k,l)=A_2d(m,n)
      enddo
      enddo
      
      end 
      
      
      