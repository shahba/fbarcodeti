      MODULE options

      integer, parameter :: MAXNOPTIONS = 200
      
      private :: options_SetDefaults
      private :: parseString

      ! program options (will be imported from options.inp)
      character(len=32) :: option_names(MAXNOPTIONS)
      integer :: options_Int(MAXNOPTIONS)
      real(8) :: options_Float(MAXNOPTIONS)
      logical :: options_Bool(MAXNOPTIONS)
      character(len=32) :: options_String(MAXNOPTIONS)
      integer :: nOptions

      ! -------------------------------------------
      ! PROGRAM options
      ! -------------------------------------------
      logical :: DEBUG_general     ! export debugging information during calculation of nodal values
      logical :: DEBUG_UMAT        ! export UMAT debugging information
      logical :: DEBUG_grains      ! export debug info for grain identification
      logical :: DEBUG_errInd      ! export debug info for error indicators
      logical :: DEBUG_nodalValues ! export debugging information during calculation of nodal values
      logical :: DEBUG_timer       ! export timing information
      logical :: DEBUG_RESID       ! export residual for debugging
      logical :: DEBUG_linesearch  ! export line search reports for debugging
      integer :: dataExportN
      logical :: do_errAnalysis    ! whether to do (on the run) error analysis/adaptivity
      logical :: exportSolution    ! export solution at the end of the calculation to continuing later on
      logical :: continueSolution  ! read-in and continue solution
      logical :: verboseUmat       ! umat exports detailed error messages for each element
      logical :: EXPORT_watmus     ! export inputs fir WATMUS-acceleration
      logical :: EXPORT_delta_gamma! export delta_gamma.out
      logical :: EXPORT_temperature! export local temperature
      logical :: EXPORT_wp         ! export dissipated energies: dissipated energy density, back-stress dissipation on basal plane and the 3 prismatic planes
      logical :: EXPORT_damage     ! export damage.out, damage related variables
      logical :: EXPORT_fp         ! export fp.out
      logical :: EXPORT_creep      ! export creep.out
      logical :: EXPORT_nye        ! export Nye tensor
      logical :: EXPORT_nodePos    ! export nodal positions
      logical :: EXPORT_cauchy     ! export nodal positions
      logical :: EXPORT_grainAVG   ! export grain-averaged quantities
      logical :: EXPORT_grainRot   ! export grain rotation matrices (for plotting textures)
      logical :: EXPORT_misorient  ! export grain-grain misorientation info
      logical :: EXPORT_hardening  ! export max basal/prismatic hardening parameters (CRSS)
      logical :: EXPORT_specialOnly! data export is done only at the special time points of a load form
                     !  (extrema/zeroes of sine, begin/end/zeros of dwell and times marked as 'export' in explicit load histories)
                     !  this option overrides dataExportT/dataExportN
      logical :: suppress_specialPoints   ! if set, the special points of a load waveform are limited to a single, maximum-load point
      real(8) :: EXPORT_allAtLast  ! export all variables at the end of the simulation
                     ! this can be used to limit disk usage. set EXPORT_ variables of only the essential variables to true
      logical :: UMAT_lsch         ! enables line search in umat solver
      logical :: UMAT_halfStep     ! dampens NR iteration step by half for the first iteration to avoid overshooting the flow rule (for CPFE umat)
      logical :: UMAT_stabilize    ! if second level iteration takes long to converge, dampens the delta_g update steps. also suppresses h0 values if g is close to g_saturation.
      integer :: improve_U0        ! improve initial trial disp. field. 0: no improv. 1: Jiahao's 2. Deniz's.
      real(8) :: STEP_v            ! velocity field factor for initial disp. field guess
      real(8) :: WLF_c2            ! Wolfe-2 condition coefficient
      real(8) :: STEP_max          ! max allowed step length in line search
      integer :: lschMethod        ! specifies the line search algorithm to use. 1: deniz 2: matthies, strang, 1979
      integer :: solverMethod      ! specified the nonlinear solver method: 1:newton-Raphson 2:modif Newt 3:modif Newt (Keep K) 4: BFGS 5: BFGS (Keep K) 6: Broyden?
      logical :: BFGS_incl_U0    ! whether to include 'settlement/constraint forces' in the initial residual for BFGS update algortihm
      logical :: adaptive_time_stepping
      integer :: convergenceNorm   ! 1: L_max, 2: L_2 norm used in calculating tolerance for convergence
      real(8) :: eps_F, eps_u      ! convergence tolerance/precision on force and displacement

      ! -------------------------------------------
      ! options for MATERIAL BEHAVIOR
      ! -------------------------------------------
      logical :: ELASTIC_ONLY ! disable plastic behavior (works by setting a_dot=0)
      integer :: tensOnly ! for stress-dependent material properties, only use params for tension(+1), compression(-1), automatic(0)
      integer :: softOnly ! for flow-direction dependent material properties (BCC), only use values for 'soft'(+1) direction, hard (-1), automatic (0)
      logical :: HARDENING_DISABLED ! disable hardening
      logical :: BACKSTRESS_DISABLED ! disable back stress (e.g. single crystals)
      logical :: HALLPETCH_DISABLED ! disable hall petch effect
      logical :: PRISMATIC_ONLY ! disable all slip systems but prismatic-a's
      logical :: BASAL_ONLY     ! disable all slip systems but basal-a's
      logical :: GND_DISABLED ! disable GND hardening
      logical :: USE_POWER_LAW! use power law, instead of the thermally activated flow
      logical :: thermalSoftening_enabled ! decrease the intrinsic slip resistance in the power law flow rule, for thermal softening
      logical :: adiabaticHeating_enabled ! plastic dissipation locally heats up the material (valid at very high strain rates only -- check the heat diffusion time scale)
      logical :: damage_enabled  ! introduce damage (softening) to slip sytstems g, after exhausting plastic deformation lfie
      logical :: damage_gsat_enabled  ! introduce damage (softening) to slip sytstems g_sat, as voids form
      logical :: damage_nonlocal  ! regularize damage variable
      real(8) :: wt_BCC
      
      ! applying pressure/creep loads
      logical :: pressureDirectionZ
      logical :: keepForceTheSame  ! in creep/dwell loading adjusts the load such that total force remains the same

      ! PROGRAM and POST-PROCESSING options.
      ! (values are imported from options.inp)
      ! -----------------------------------------
      ! error analysis / nodal value calculation
      ! -----------------------------------------
      integer :: posQuantity,nQuantDOF ! position of the quantity of interest, in the state var. vector
      character(len=32) :: errInd_method ! identifies the method used in calculating the error indicator: jump, ZZ, norm_of_gradient
      integer :: methodSPR             ! how to deal with insufficient data pts in SPR. see recoverySPR()
      integer :: p_deg, nNeighDeg
      integer :: recoveryMethod        ! recovery method to use to calc nodal Fp. 1: nodalAVG, 2: SPR. 3: LeastSQ
      ! calculate nodal values using nodal averaging, least sq. approx, and SPR
      logical :: do_err_AVG, do_err_LSQ, do_err_SPR

      contains
            
      SUBROUTINE readOptions(strOptionsFile)
      
      implicit none
      
      character(len=*), intent(in) :: strOptionsFile
      ! locals
      integer :: error
      character(len=32) :: strOptionName
      character(len=32) :: strOptionValue
      character(len=300) :: strLine
      real(8) :: floBuffer
      integer :: wPos(10,2), wCount, I
      
      CALL options_SetDefaults()

      open(104,file=trim(strOptionsFile))
      nOptions=0
      do while(.TRUE.)
         read(104,'(A)',iostat=error) strLine
         if (error.NE.0) exit             ! EOF / Error
         if (strLine(1:2).EQ.'//') cycle  ! comment. skip.
         
         ! extract the option name and value
         read(strLine,*,iostat=error) strOptionName, floBuffer
         if (error.NE.0) floBuffer=0      ! option value is not a number
         ! extract option value as string

         CALL parseString(strLine,' ',wPos(:,:),10,wCount)
         if(wCount.NE.2) cycle            ! incomplete input. skip.
         
         nOptions=nOptions+1
         option_names(nOptions) = strOptionName
         options_Float(nOptions) = floBuffer
         options_Int(nOptions) = floBuffer
         options_Bool(nOptions) = (options_Int(nOptions).NE.0)
         options_String(nOptions) = strLine(wPos(2,1):wPos(2,2))
      enddo
      close(104)

      if(nOptions.GT.MAXNOPTIONS) then
         write(*,*) 'INCREASE MAXNOPTIONS to ',nOptions
         STOP
      endif
      
      do I=1,nOptions
         if (trim(option_names(I)).EQ.'dataExportN') then
            dataExportN = options_Int(I)            
            if (dataExportN == 0) dataExportN = 1
         elseif (trim(option_names(I)).EQ.'DEBUG_general') then
            DEBUG_general = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'DEBUG_UMAT') then
            DEBUG_UMAT = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'DEBUG_timer') then
            DEBUG_timer = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'DEBUG_RESID') then
            DEBUG_RESID = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'DEBUG_linesearch') then
            DEBUG_linesearch = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'DEBUG_grains') then
            DEBUG_grains = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'DEBUG_errInd') then
            DEBUG_errInd = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'do_errAnalysis') then
            do_errAnalysis = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'exportSolution') then
            exportSolution = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'continueSolution') then
            continueSolution = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'verboseUmat') then
            verboseUmat = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_watmus') then
            EXPORT_watmus = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_delta_gamma') then
            EXPORT_delta_gamma = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_wp') then
            EXPORT_wp = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_temperature') then
            EXPORT_temperature = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_damage') then
            EXPORT_damage = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_fp') then
            EXPORT_fp = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_creep') then
            EXPORT_creep = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_nye') then
            EXPORT_nye = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_nodePos') then
            EXPORT_nodePos = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_cauchy') then
            EXPORT_cauchy = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_grainAVG') then
            EXPORT_grainAVG = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_grainRot') then
            EXPORT_grainRot = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_misorient') then
            EXPORT_misorient = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_hardening') then
            EXPORT_hardening = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'suppress_specialPoints') then
            suppress_specialPoints = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_specialOnly') then
            EXPORT_specialOnly = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'EXPORT_allAtLast') then
            EXPORT_allAtLast = options_Float(I)
         elseif (trim(option_names(I)).EQ.'DEBUG_nodalValues') then
            DEBUG_nodalValues = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'posQuantity') then
            posQuantity = options_Int(I)
         elseif (trim(option_names(I)).EQ.'nQuantDOF') then
            nQuantDOF = options_Int(I)
         elseif (trim(option_names(I)).EQ.'errInd_method') then
            errInd_method = options_String(I)
            ! 'norm_of_gradient'
            ! 'jump'
            ! 'ZZ'
         elseif (trim(option_names(I)).EQ.'recoveryMethod') then
            ! 1: nodal AVG
            ! 2: SPR
            ! 3: least SQ
            recoveryMethod = options_Int(I)
         elseif (trim(option_names(I)).EQ.'methodSPR') then
            methodSPR = options_Int(I)
         elseif (trim(option_names(I)).EQ.'nNeighDeg') then
            nNeighDeg = options_Int(I)
         elseif (trim(option_names(I)).EQ.'p_deg') then
            p_deg = options_Int(I)
         elseif (trim(option_names(I)).EQ.'elastic_only') then
            ELASTIC_ONLY = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'tensOnly') then
            tensOnly = options_Int(I)
         elseif (trim(option_names(I)).EQ.'softOnly') then
            softOnly = options_Int(I)
         elseif (trim(option_names(I)).EQ.'prismatic_only') then
            PRISMATIC_ONLY = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'basal_only') then
            BASAL_ONLY = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'hardening_disabled') then
            HARDENING_DISABLED = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'backstress_disabled') then
            BACKSTRESS_DISABLED = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'hallpetch_disabled') then
            HALLPETCH_DISABLED = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'GND_disabled') then
            GND_DISABLED = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'use_power_law') then
            USE_POWER_LAW = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'thermalSoftening_enabled') then
            thermalSoftening_enabled = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'adiabaticHeating_enabled') then
            adiabaticHeating_enabled = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'damage_enabled') then
            damage_enabled = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'damage_gsat_enabled') then
            damage_gsat_enabled = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'damage_nonlocal') then
            damage_nonlocal = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'wt_BCC') then
            wt_BCC = options_Float(I)
         elseif (trim(option_names(I)).EQ.'pressureDirectionZ') then
            pressureDirectionZ = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'keepForceTheSame') then
            keepForceTheSame = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'UMAT_lsch') then
            UMAT_lsch = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'UMAT_halfStep') then
            UMAT_halfStep = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'UMAT_stabilize') then
            UMAT_stabilize = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'improve_U0') then
            improve_U0 = options_Int(I)
         elseif (trim(option_names(I)).EQ.'STEP_v') then
            STEP_v = options_Float(I)
         elseif (trim(option_names(I)).EQ.'WLF_c2') then
            WLF_c2 = options_Float(I)
         elseif (trim(option_names(I)).EQ.'lschMethod') then
            lschMethod = options_Int(I)
         elseif (trim(option_names(I)).EQ.'STEP_max') then
            STEP_max = options_Float(I)
         elseif (trim(option_names(I)).EQ.'STEP_max') then
            STEP_max = options_Float(I)
         elseif (trim(option_names(I)).EQ.'solverMethod') then
            solverMethod = options_Int(I)
         elseif (trim(option_names(I)).EQ.'BFGS_incl_U0') then
            BFGS_incl_U0 = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'adaptive_time_stepping') then
            adaptive_time_stepping = options_Bool(I)
         elseif (trim(option_names(I)).EQ.'convergenceNorm') then
            convergenceNorm = options_Int(I)
         elseif (trim(option_names(I)).EQ.'eps_F') then
            eps_F = options_Float(I)
         elseif (trim(option_names(I)).EQ.'eps_u') then
            eps_u = options_Float(I)
         endif
      enddo
      
      END SUBROUTINE readOptions
      
      SUBROUTINE options_SetDefaults()
         implicit none
               ! initialize to default values
         dataExportN = 1
         DEBUG_general = .FALSE.
         DEBUG_UMAT = .FALSE.
         DEBUG_timer = .FALSE.
         DEBUG_grains = .FALSE.
         DEBUG_errInd = .FALSE.
         DEBUG_RESID = .FALSE.
         DEBUG_linesearch = .FALSE.
         DEBUG_nodalValues=.FALSE.
         do_errAnalysis = .FALSE.
         exportSolution = .FALSE.
         continueSolution = .FALSE.
         verboseUmat         = .FALSE.
         EXPORT_watmus = .FALSE.
         EXPORT_delta_gamma = .FALSE.
         EXPORT_wp = .FALSE.
         EXPORT_temperature = .FALSE.
         EXPORT_damage = .FALSE.
         EXPORT_fp = .FALSE.
         EXPORT_creep = .FALSE.
         EXPORT_nye = .FALSE.
         EXPORT_nodePos = .FALSE.
         EXPORT_cauchy = .TRUE.
         EXPORT_grainAVG = .TRUE.
         EXPORT_grainRot = .FALSE.
         EXPORT_misorient = .FALSE.
         EXPORT_hardening = .FALSE.
         EXPORT_specialOnly = .FALSE.
         suppress_specialPoints = .FALSE.
         EXPORT_allAtLast = 1.D0
         do_err_SPR=.FALSE.
         do_err_LSQ=.FALSE.
         do_err_AVG=.TRUE.
         posQuantity = 211
         nQuantDOF = 6
         p_deg=1
         nNeighDeg=1
         methodSPR = 1
         recoveryMethod = 1
         errInd_method = 'none'
         ELASTIC_ONLY = .FALSE.
         tensOnly = 0
         softOnly = 0
         HARDENING_DISABLED = .FALSE. 
         BACKSTRESS_DISABLED = .FALSE.
         HALLPETCH_DISABLED = .FALSE.
         PRISMATIC_ONLY = .FALSE.
         BASAL_ONLY = .FALSE.
         GND_DISABLED = .FALSE.
         USE_POWER_LAW = .FALSE.
         thermalSoftening_enabled = .FALSE.
         adiabaticHeating_enabled = .FALSE.
         damage_enabled = .FALSE.
         damage_gsat_enabled = .FALSE.
         damage_nonlocal = .FALSE.
         wt_BCC = 0.12d0   ! % volume fraction of BCC phase in trans-B grains
         pressureDirectionZ = .FALSE.
         keepForceTheSame = .FALSE.
         UMAT_lsch = .FALSE.
         UMAT_halfStep = .TRUE.
         UMAT_stabilize = .TRUE.
         improve_U0 = 2
         STEP_v = 1.0
         WLF_c2 = 0.9
         STEP_max = 10.0
         lschMethod = 0
         solverMethod = 5  !BFGS (keep)
         BFGS_incl_U0 = .TRUE.
         adaptive_time_stepping = .TRUE.
         convergenceNorm = 1
         eps_F = 0.01    ! convergence tolerance/precision on force and displacement
         eps_u = 0.025   
         
      END SUBROUTINE options_SetDefaults
      
      SUBROUTINE summarizeOptions()
      
         if (dataExportN.NE.0) write(*,*)  & 
               'data export increment period:',dataExportN
      
         if(ELASTIC_ONLY)  & 
            write(*,*) 'plasticity disabled'
         if(GND_DISABLED) & 
            write(*,*) 'GND hardening disabled'
         if(.not.USE_POWER_LAW) & 
            write(*,*) 'Thermally Activated Flow rule instead of power law'
         if(thermalSoftening_enabled) & 
            write(*,*) 'thermal softening is enabled !'
         if(adiabaticHeating_enabled) & 
            write(*,*) 'adiabatic heating is enabled !'
         if(recoveryMethod.EQ.1) then
            write(*,*) 'Recovery method: nodal averaging'
         elseif(recoveryMethod.EQ.2) then
            write(*,*) 'Recovery method: SPR'
         elseif(recoveryMethod.EQ.3) then
            write(*,*) 'Recovery method: least square fit'
         endif
         if(keepForceTheSame) & 
            write(*,*) 'Pressure will be scaled to compensate for the area reductions'

         
         if(do_errAnalysis) then
            write(*,*) 'error analysis ON'
         endif

         if(improve_U0.EQ.0) then
            write(*,*) & 
       'no improvement for initial displacement field'
         elseif(improve_U0.EQ.1) then
            write(*,*) & 
       'improve initial displacement fields using: Jiahao''s method'
         elseif(improve_U0.EQ.3) then
            write(*,*) & 
       'improve initial displacement fields using: (quadratic) Jiahao''s method'
         elseif(improve_U0.EQ.2) then
            write(*,*) & 
       'improve initial displacement fields using: Deniz''s method'
         elseif(improve_U0.EQ.4) then
            write(*,*) & 
       'improve initial displacement fields using most recent tangent matrix'
         else
            write(*,*) & 
       'improve initial guess - method not recognised:',improve_U0
         endif

         if(lschMethod.EQ.0) write(*,*) & 
       'line search disabled.'
         if(lschMethod.EQ.1) write(*,*) & 
       'line search enabled. will use method: Deniz'
         if(lschMethod.EQ.2) write(*,*) & 
       'line search enabled. will use method: Matthies, Strang, 1979'

         write(*,*) '# options imported:',nOptions
         
      END SUBROUTINE
      
      SUBROUTINE parseString(str, del, wordPos, arrSize, wordCount)
      implicit none
      character(len=*), intent(in) :: str
      character, intent(in) :: del
      integer, intent(in) :: arrSize
      integer, intent(out) :: wordPos(arrSize,2)
      integer, intent(out) :: wordCount
      
      integer :: i,wordCountMax
      character :: prevChar, thisChar, del2
      
      ! space and tab alternatively
      del2=del
      if(del.EQ.' ') del2=ACHAR(9)
      if(IACHAR(del).EQ.9) del2=' '
      
      wordCountMax = wordCount
      wordCount = 0
      wordPos(:,:) = 0
      prevChar = del
      do i=1,len(str)
         thisChar = str(i:i)
         if((prevChar.NE.del.AND.prevChar.NE.del2)   &
       .AND.(thisChar.EQ.del.OR.thisChar.EQ.del2)) then
            ! end of a word
            wordPos(wordCount,2) = i - 1
         elseif((prevChar.EQ.del.OR.prevChar.EQ.del2) &
       .AND.(thisChar.NE.del.AND.thisChar.NE.del2))  then
            ! start of a word
            wordCount = wordCount + 1
            wordPos(wordCount,1) = i
         endif
         prevChar = thisChar
      enddo
      if(prevChar.NE.del.AND.prevChar.NE.del2) then
         !end of string ends the word
         wordPos(wordCount,2) = len(str)
      endif
      END SUBROUTINE

      END MODULE options
      