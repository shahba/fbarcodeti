%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% added feature:
%  1) divides patches with more than maximum elements into two smaller patches
%  2) creates patches for bulk nodes first and then for boundary nodes
%  3) destructs patches with 1 element and assigns them to other patches
%  4) IT HAS THE BEST SEARCHING ALGORITHM FOR FINDING ELEMENT NEIGHBORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% part 1 reading input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
clc;
maxelm_in_patch=input('Max Element in Patch: ') ;
minelm_in_patch=input('Min Element in Patch: ') ;
maxadjgrainelem=input('maxadjgrainelem: ') ;
maxadjelem=input('maxadjelem: ') ;
maxadjgrain=input('maxadjgrain: ') ;
meshFileName=input('Name of mesh file with extension in '': (for example "tet.inp")');


load 'ielgr.inp'
ielgr(:,1)='';

id1=fopen(meshFileName,'r');
id2=fopen('elem_matlab1.inp','w');


string1 = fgets(id1);
string1 = fgets(id1);
string1 = fgets(id1);
nnode=fscanf(id1,'%g',1);
string1 = fgets(id1);
for i=1:nnode
    string1 = fgets(id1);
    modifiedStr = regexprep(string1, 'D', 'e');
    fprintf(id2,'%s',modifiedStr);
end

string1 = fgets(id1);
nelem=fscanf(id1,'%g',1);
string1 = fgets(id1);
for i=1:nelem
    string1 = fgets(id1);
    modifiedStr = regexprep(string1, 'D', 'e');
    fprintf(id2,'%s',modifiedStr);
end

fclose(id2);
fclose(id1);

id1=fopen('elem_matlab1.inp','r');
id2=fopen('elem_matlab2.inp','w');

for i=1:(nelem+nnode)
    string1 = fgets(id1);
    modifiedStr = regexprep(string1, 'd', 'e');
    fprintf(id2,'%s',modifiedStr);
end
fclose(id2);
fclose(id1);


id2=fopen('elem_matlab2.inp');
coords=zeros(nnode,3);
ijk=zeros(nelem,4);


for i=1:nnode
    id=fscanf(id2,'%g',1);
    coords(i,1)=fscanf(id2,'%g',1);
    coords(i,2)=fscanf(id2,'%g',1);
    coords(i,3)=fscanf(id2,'%g',1);
end


for i=1:nelem
    id=fscanf(id2,'%g',1);
    ijk(i,1)=fscanf(id2,'%g',1);
    ijk(i,2)=fscanf(id2,'%g',1);
    ijk(i,3)=fscanf(id2,'%g',1);
    ijk(i,4)=fscanf(id2,'%g',1);
end


fclose all;

nelx=nelem;
nx=nnode;
max_number_of_patches=floor(nelem/minelm_in_patch);

delete('elem_matlab1.inp','elem_matlab2.inp')
disp('finish reading input file')





%% part2- nodal elemental connectivity arrays

nnodgr=zeros(nx,1);
inodgr=zeros(nx,maxadjgrain);

text=['Calculating inodgr and nnodgr'];
disp (text);
for i=1:nelx
    for j=1:4
        inode=ijk(i,j);
        
        happen=false;
        for k=1:maxadjgrain
            if (inodgr(inode,k)==ielgr(i)), happen=true; end
        end
        
        if (~happen)
            
            nnodgr(inode)=nnodgr(inode)+1;
            inodgr(inode,nnodgr(inode))=ielgr(i);
            
            if (nnodgr(inode)>maxadjgrain)
                disp 'Increase maxadjgrain'
                stop
            end
        end
    end
end




natel=zeros(nx,1);
iatel=zeros(nx,maxadjgrainelem);

text=['Calculating natel and iatel'];
disp (text);
for iel=1:nelx
    for in=1:4
        km=ijk(iel,in);
        natel(km)=natel(km)+1;
        iatel(km,natel(km))=iel;
        
        if (natel(km)>maxadjgrainelem)
            disp('increase maxadjgrainelem')
        end
    end
    
end


natel_tet=zeros(nx,maxadjgrain);
iatel_tet=zeros(nx,maxadjgrain,maxadjelem);



text=['Calculating natel_tet and iatel_tet'];
disp (text);
for i=1:nx
    for j=1:nnodgr(i)
        for k=1:natel(i)
            iel=iatel(i,k);
            if (ielgr(iel)==inodgr(i,j))
                natel_tet(i,j)=natel_tet(i,j)+1;
                iatel_tet(i,j,natel_tet(i,j))=iel;
                
                if (natel_tet(i,j)>maxadjelem)
                    disp 'Increase maxadjelem'
                    stop
                end
            end
        end
    end
end




element_neighbor_repository=zeros(nelx,4);
number_of_neighbors_already_found=zeros(nelx,1);

for ielem=1:nelx
    text=['FINDING NEIGHBORS FOR ELEMENT' num2str(ielem)];
    disp (text);
    
    
    ALREADY_CONSIDERED=false(nelx,1);
    
    for INODE=1:4
        for JEL=1:natel(ijk(ielem,INODE))
            jelem=iatel(ijk(ielem,INODE),JEL);
            common_nodes=0;
            for inode=1:4
                nodeI=ijk(ielem,inode);
                
                for jnode=1:4
                    nodeJ=ijk(jelem,jnode);
                    
                    if (nodeI==nodeJ)
                        common_nodes=common_nodes+1;
                        break
                    end
                    
                end
                
                if (common_nodes==3), break, end
            end
            
            if (common_nodes==3 && ~ALREADY_CONSIDERED(jelem) && ielem~=jelem)
                number_of_neighbors_already_found(ielem) = number_of_neighbors_already_found(ielem) + 1;
                element_neighbor_repository(ielem,number_of_neighbors_already_found(ielem))=jelem;
                ALREADY_CONSIDERED(jelem)=true;
            end
        end
    end
    
    
end



%% part3- Patch Construction

assigned_to_patch=false(nelx,1);
element_to_patch_repository=zeros(nelx,1);
number_of_elements_in_patch=zeros(max_number_of_patches,1);
patch_to_element_repository=zeros(max_number_of_patches,4*maxelm_in_patch);

patch_number=0;




boundary_node=false(nx,1);
for i=1:nx
    if (abs(coords(i,1)-min(coords(:,1)))<1e-7 || abs(coords(i,2)-min(coords(:,2)))<1e-7 || abs(coords(i,3)-min(coords(:,3)))<1e-7 || ...
            abs(coords(i,1)-max(coords(:,1)))<1e-7 || abs(coords(i,2)-max(coords(:,2)))<1e-7 || abs(coords(i,3)-max(coords(:,3)))<1e-7 )
        boundary_node(i)=true;
    end
end







for inode=1:nx
    
    if (~boundary_node(inode))
        
        text=['BULK: Calculating patches for node ' num2str(inode)];
        disp (text);
        
        for igrain=1:nnodgr(inode)
            
            nCandidateElementsToBeAssigned=0;
            CandidateElementsToBeAssigned=zeros(maxadjgrainelem,1);
            
            for i=1:natel_tet(inode,igrain)
                if (~assigned_to_patch(iatel_tet(inode,igrain,i)))
                    nCandidateElementsToBeAssigned=nCandidateElementsToBeAssigned+1;
                    CandidateElementsToBeAssigned(nCandidateElementsToBeAssigned)=iatel_tet(inode,igrain,i);
                end
            end
            
            
            for iCandidate=1:nCandidateElementsToBeAssigned
                if(assigned_to_patch(CandidateElementsToBeAssigned(iCandidate)))
                    continue;
                end
                groupTMP=zeros(maxelm_in_patch,1);
                assignedTMP=false(nelx,1);
                groupTMP(1)=CandidateElementsToBeAssigned(iCandidate);
                assignedTMP(CandidateElementsToBeAssigned(iCandidate))=true;
                nElementInTMPgroup=1;
                
                
                elemToBeProcessed = 1;
                while (nElementInTMPgroup<maxelm_in_patch)
                    
                    for ineighbor=1:number_of_neighbors_already_found(groupTMP(elemToBeProcessed));
                        b = find(CandidateElementsToBeAssigned == element_neighbor_repository(groupTMP(elemToBeProcessed),ineighbor));
                        if (~isempty(b) && ~assignedTMP(CandidateElementsToBeAssigned(b(1))) && ~assigned_to_patch(CandidateElementsToBeAssigned(b(1))))
                            nElementInTMPgroup = nElementInTMPgroup + 1;
                            groupTMP(nElementInTMPgroup) = CandidateElementsToBeAssigned(b(1));
                            assignedTMP(CandidateElementsToBeAssigned(b(1)))=true;
                        end
                        
                        if (nElementInTMPgroup >= maxelm_in_patch), break; end
                    end
                    
                    if(elemToBeProcessed < nElementInTMPgroup)
                        elemToBeProcessed = elemToBeProcessed + 1;
                    else
                        break
                    end
                end
                
                
                if (nElementInTMPgroup >= minelm_in_patch)
                    
                    patch_number=patch_number+1;
                    for i=1:nElementInTMPgroup
                        element_to_patch_repository(groupTMP(i))=patch_number;
                        number_of_elements_in_patch(patch_number)=nElementInTMPgroup;
                        patch_to_element_repository(patch_number,i)=groupTMP(i);
                        assigned_to_patch(groupTMP(i))=true;
                    end
                    
                end
                
            end
            
        end
    end
end



for inode=1:nx
    
    if (boundary_node(inode))
        
        text=['BOUNDARY: Calculating patches for node ' num2str(inode)];
        disp (text);
        
        for igrain=1:nnodgr(inode)
            
            nCandidateElementsToBeAssigned=0;
            CandidateElementsToBeAssigned=zeros(maxadjgrainelem,1);
            
            for i=1:natel_tet(inode,igrain)
                if (~assigned_to_patch(iatel_tet(inode,igrain,i)))
                    nCandidateElementsToBeAssigned=nCandidateElementsToBeAssigned+1;
                    CandidateElementsToBeAssigned(nCandidateElementsToBeAssigned)=iatel_tet(inode,igrain,i);
                end
            end
            
            
            for iCandidate=1:nCandidateElementsToBeAssigned
                if(assigned_to_patch(CandidateElementsToBeAssigned(iCandidate)))
                    continue;
                end
                groupTMP=zeros(maxelm_in_patch,1);
                assignedTMP=false(nelx,1);
                groupTMP(1)=CandidateElementsToBeAssigned(iCandidate);
                assignedTMP(CandidateElementsToBeAssigned(iCandidate))=true;
                nElementInTMPgroup=1;
                
                
                elemToBeProcessed = 1;
                while (nElementInTMPgroup<maxelm_in_patch)
                    
                    for ineighbor=1:number_of_neighbors_already_found(groupTMP(elemToBeProcessed));
                        b = find(CandidateElementsToBeAssigned == element_neighbor_repository(groupTMP(elemToBeProcessed),ineighbor));
                        if (~isempty(b) && ~assignedTMP(CandidateElementsToBeAssigned(b(1))) && ~assigned_to_patch(CandidateElementsToBeAssigned(b(1))))
                            nElementInTMPgroup = nElementInTMPgroup + 1;
                            groupTMP(nElementInTMPgroup) = CandidateElementsToBeAssigned(b(1));
                            assignedTMP(CandidateElementsToBeAssigned(b(1)))=true;
                        end
                        
                        if (nElementInTMPgroup >= maxelm_in_patch), break; end
                    end
                    
                    if(elemToBeProcessed < nElementInTMPgroup)
                        elemToBeProcessed = elemToBeProcessed + 1;
                    else
                        break
                    end
                end
                
                
                if (nElementInTMPgroup >= minelm_in_patch)
                    
                    patch_number=patch_number+1;
                    for i=1:nElementInTMPgroup
                        element_to_patch_repository(groupTMP(i))=patch_number;
                        number_of_elements_in_patch(patch_number)=nElementInTMPgroup;
                        patch_to_element_repository(patch_number,i)=groupTMP(i);
                        assigned_to_patch(groupTMP(i))=true;
                    end
                    
                end
                
            end
            
        end
    end
end




% Now we assign the unassigned elements to proper patches
cycle=0;
while (~isempty(find(~element_to_patch_repository, 1)) && cycle<10)
    
    nUnassignedElements=length(find(~element_to_patch_repository));
    UnassignedElements=find(~element_to_patch_repository);
    
    disp ('Begining of cycle')
    cycle
    disp ('number of UNassigned elements is at:')
    nUnassignedElements
    
    cycle=cycle+1;
    
    
    
    for i=1:nUnassignedElements
        
        nNeighborAssigned=0;
        AssignedNeighboringElement=zeros(4,1);
        
        for ineighbor=1:number_of_neighbors_already_found(UnassignedElements(i))
            if (assigned_to_patch(element_neighbor_repository(UnassignedElements(i),ineighbor)) && ielgr(UnassignedElements(i))==ielgr(element_neighbor_repository(UnassignedElements(i),ineighbor)))
                nNeighborAssigned=nNeighborAssigned+1;
                AssignedNeighboringElement(nNeighborAssigned)=element_neighbor_repository(UnassignedElements(i),ineighbor);
            end
        end
        
        
        if (nNeighborAssigned~=0)
            minElemInPatch=number_of_elements_in_patch(element_to_patch_repository(AssignedNeighboringElement(1)));
            proper_patch=element_to_patch_repository(AssignedNeighboringElement(1));
            
            for j=2:nNeighborAssigned
                if (minElemInPatch > number_of_elements_in_patch(element_to_patch_repository(AssignedNeighboringElement(j))))
                    minElemInPatch=number_of_elements_in_patch(element_to_patch_repository(AssignedNeighboringElement(j)));
                    proper_patch=element_to_patch_repository(AssignedNeighboringElement(j));
                end
            end
            
            element_to_patch_repository(UnassignedElements(i))=proper_patch;
            number_of_elements_in_patch(proper_patch)=number_of_elements_in_patch(proper_patch)+1;
            patch_to_element_repository(proper_patch,number_of_elements_in_patch(proper_patch))=UnassignedElements(i);
            assigned_to_patch(UnassignedElements(i))=true;
            
            
        end
        
    end
    
end



% Dividing the patches with number of elements more than the maximum to
% two smaller patches

for ipatch=1:patch_number
    
    if (number_of_elements_in_patch(ipatch) > maxelm_in_patch)
        
        
        nCandidateElementsToBeAssigned=number_of_elements_in_patch(ipatch);
        CandidateElementsToBeAssigned=patch_to_element_repository(ipatch,1:nCandidateElementsToBeAssigned);
        
        number_of_elements_in_patch(ipatch)=0;
        patch_to_element_repository(ipatch,:)=0;
        for i=1:nCandidateElementsToBeAssigned
            element_to_patch_repository(CandidateElementsToBeAssigned(i))=0;
            assigned_to_patch(CandidateElementsToBeAssigned(i))=false;
        end
        
        
        % constructing the first sub-patch
        iCandidate=1;
        
        groupTMP1=zeros(maxelm_in_patch,1);
        assignedTMP1=false(nelx,1);
        groupTMP1(1)=CandidateElementsToBeAssigned(iCandidate);
        assignedTMP1(CandidateElementsToBeAssigned(iCandidate))=true;
        nElementInTMPgroup1=1;
        
        
        elemToBeProcessed = 1;
        while (nElementInTMPgroup1<floor(nCandidateElementsToBeAssigned/2))
            
            for ineighbor=1:number_of_neighbors_already_found(groupTMP1(elemToBeProcessed));
                b = find(CandidateElementsToBeAssigned == element_neighbor_repository(groupTMP1(elemToBeProcessed),ineighbor));
                if (~isempty(b) && ~assignedTMP1(CandidateElementsToBeAssigned(b(1))))
                    nElementInTMPgroup1 = nElementInTMPgroup1 + 1;
                    groupTMP1(nElementInTMPgroup1) = CandidateElementsToBeAssigned(b(1));
                    assignedTMP1(CandidateElementsToBeAssigned(b(1)))=true;
                end
                
                if (nElementInTMPgroup1 >= floor(nCandidateElementsToBeAssigned/2)), break; end
            end
            
            if(elemToBeProcessed < nElementInTMPgroup1)
                elemToBeProcessed = elemToBeProcessed + 1;
            else
                break
            end
        end
        
        
        % constructing the second sub-patch
        for jCandidate=1:nCandidateElementsToBeAssigned
            
            if (~assignedTMP1(CandidateElementsToBeAssigned(jCandidate)))
                groupTMP2=zeros(maxelm_in_patch,1);
                assignedTMP2=false(nelx,1);
                groupTMP2(1)=CandidateElementsToBeAssigned(jCandidate);
                assignedTMP2(CandidateElementsToBeAssigned(jCandidate))=true;
                nElementInTMPgroup2=1;
                
                
                elemToBeProcessed = 1;
                while (nElementInTMPgroup2<floor(nCandidateElementsToBeAssigned/2))
                    
                    for ineighbor=1:number_of_neighbors_already_found(groupTMP2(elemToBeProcessed));
                        b = find(CandidateElementsToBeAssigned == element_neighbor_repository(groupTMP2(elemToBeProcessed),ineighbor));
                        if (~isempty(b) && ~assignedTMP1(CandidateElementsToBeAssigned(b(1)))  && ~assignedTMP2(CandidateElementsToBeAssigned(b(1))))
                            nElementInTMPgroup2 = nElementInTMPgroup2 + 1;
                            groupTMP2(nElementInTMPgroup2) = CandidateElementsToBeAssigned(b(1));
                            assignedTMP2(CandidateElementsToBeAssigned(b(1)))=true;
                        end
                        
                        if (nElementInTMPgroup2 >= floor(nCandidateElementsToBeAssigned/2)), break; end
                    end
                    
                    if(elemToBeProcessed < nElementInTMPgroup2)
                        elemToBeProcessed = elemToBeProcessed + 1;
                    else
                        break
                    end
                end
                if (nElementInTMPgroup2 > 1), break; end
            end
            
        end
        
        
        for icyc=1:4
            for iCandidate=1:nCandidateElementsToBeAssigned
                
                if (~assignedTMP1(CandidateElementsToBeAssigned(iCandidate)) && ~assignedTMP2(CandidateElementsToBeAssigned(iCandidate)))
                    
                    for ineighbor=1:number_of_neighbors_already_found(CandidateElementsToBeAssigned(iCandidate))
                        
                        b1=find(groupTMP1 == element_neighbor_repository(CandidateElementsToBeAssigned(iCandidate),ineighbor));
                        b2=find(groupTMP2 == element_neighbor_repository(CandidateElementsToBeAssigned(iCandidate),ineighbor));
                        if (~isempty(b1))
                            nElementInTMPgroup1 = nElementInTMPgroup1 + 1;
                            groupTMP1(nElementInTMPgroup1) = CandidateElementsToBeAssigned(iCandidate);
                            assignedTMP1(CandidateElementsToBeAssigned(iCandidate))=true;
                            break;
                        elseif (~isempty(b2))
                            nElementInTMPgroup2 = nElementInTMPgroup2 + 1;
                            groupTMP2(nElementInTMPgroup2) = CandidateElementsToBeAssigned(iCandidate);
                            assignedTMP2(CandidateElementsToBeAssigned(iCandidate))=true;
                            break;
                        end
                    end
                    
                end
                
            end
        end
        
        
        for i=1:nElementInTMPgroup1
            element_to_patch_repository(groupTMP1(i))=ipatch;
            number_of_elements_in_patch(ipatch)=nElementInTMPgroup1;
            patch_to_element_repository(ipatch,i)=groupTMP1(i);
            assigned_to_patch(groupTMP1(i))=true;
        end
        
        
        patch_number=patch_number+1;
        for i=1:nElementInTMPgroup2
            element_to_patch_repository(groupTMP2(i))=patch_number;
            number_of_elements_in_patch(patch_number)=nElementInTMPgroup2;
            patch_to_element_repository(patch_number,i)=groupTMP2(i);
            assigned_to_patch(groupTMP2(i))=true;
        end
        
        
        
    end
    
    
    
end





if (~isempty(find(~element_to_patch_repository, 1)))
    disp ('some elements were not assigned to patches')
    nUnassignedElements=length(find(~element_to_patch_repository));
    UnassignedElements=find(~element_to_patch_repository);
    
    
    for i=1:nUnassignedElements
        patch_number=patch_number+1;
        element_to_patch_repository(UnassignedElements(i))=patch_number;
        number_of_elements_in_patch(patch_number)=1;
        patch_to_element_repository(patch_number,1)=UnassignedElements(i);
        assigned_to_patch(UnassignedElements(i))=true;
    end
    
end



RealNumberOfPatches=patch_number;

for ipatch=1:patch_number
    
    if (number_of_elements_in_patch(ipatch)==1)
        nElementsInNeighboringPatches=zeros(4,1);
        minimumNeighbor=0;
        minimumEleminNeighboringPatches=10*maxelm_in_patch;
        for ineighbor=1:number_of_neighbors_already_found(patch_to_element_repository(ipatch,1))
            nElementsInNeighboringPatches(ineighbor)=number_of_elements_in_patch(element_to_patch_repository(element_neighbor_repository(patch_to_element_repository(ipatch,1),ineighbor)));
            
            if (nElementsInNeighboringPatches(ineighbor)<minimumEleminNeighboringPatches && nElementsInNeighboringPatches(ineighbor)~=0 && ielgr(patch_to_element_repository(ipatch,1))==ielgr(element_neighbor_repository(patch_to_element_repository(ipatch,1),ineighbor)))
                minimumNeighbor=ineighbor;
                minimumEleminNeighboringPatches=nElementsInNeighboringPatches(ineighbor);
            end
        end
        
        destination_patch=element_to_patch_repository(element_neighbor_repository(patch_to_element_repository(ipatch,1),minimumNeighbor));
        element_to_patch_repository(patch_to_element_repository(ipatch,1))=destination_patch;
        number_of_elements_in_patch(destination_patch)=number_of_elements_in_patch(destination_patch)+1;
        patch_to_element_repository(destination_patch,number_of_elements_in_patch(destination_patch))=patch_to_element_repository(ipatch,1);
        
        
        number_of_elements_in_patch(ipatch)=0;
        patch_to_element_repository(ipatch,:)=0;
        
        
        RealNumberOfPatches=RealNumberOfPatches-1;
    end
    
    
end




% Check whether all the patches belong to the same grain
for i=1:patch_number
    
    if (number_of_elements_in_patch(i)==0), continue; end
    
    igrain=ielgr(patch_to_element_repository(i,1));
    
    for j=2:number_of_elements_in_patch(i)
        if (igrain ~=ielgr(patch_to_element_repository(i,j)))
            disp 'The elements in the following patch belong to different grains'
            i
            stop
        end
    end
end




for i=1:nelem
    
    if (element_to_patch_repository(i)==0)
        disp('Element does not belong to any patch!!!')
        stop
    end
end




sumElement=sum(number_of_elements_in_patch);

if (sumElement~=nelx)
    disp('some elements are missing in the patches')
end



disp('Writing output')

outID = fopen('PATCH.inp','w');
fprintf(outID,'%u %u %u \r\n',RealNumberOfPatches,max(number_of_elements_in_patch),min(number_of_elements_in_patch(number_of_elements_in_patch>0)));

for i=1:patch_number
    
    if (number_of_elements_in_patch(i)==0), continue; end
    
    fmtstring = repmat('%u ', 1, (number_of_elements_in_patch(i)+1));
    fprintf(outID, [fmtstring, '\r\n'], number_of_elements_in_patch(i),patch_to_element_repository(i,1:number_of_elements_in_patch(i)));
end
fclose(outID);








