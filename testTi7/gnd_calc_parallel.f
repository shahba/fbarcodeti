	PROGRAM CRACK_NUCLEATION
      USE nodalRecovery
	IMPLICIT NONE
      include 'mpif.h'    ! KOUROSH_MPI   
      include 'PARDIS.H'
	INTEGER,PARAMETER::NELX=maxel, NNODE=maxnode, NGRAINS=23, 
     &                    MAX_ELEM_IN_GRAIN=6, 
     &                    MAX_GRAIN_GRAIN_NEIGBOR=60,
     &                    MAX_GRAIN_PAIR=1,
     &                    MAX_COMMON_NODE_PAIR=1,
     &                    MAX_COMMON_SURFACE_IN_PAIR=1,
     &                    NPROCS=12 , 
     &                    CRACK_MODEL=2, LOADING_AXIS=2       
     
	REAL*8, PARAMETER::    BETA=0.7071,
     &                     CRACK_LENGTH_CONSTANT=572.96, BUR=2.94D-4


      ! deniz
      integer, allocatable :: IJK(:)
      real(8), allocatable :: G0XYZ(:)
      integer :: iElement, grainIdx, iPatchID, nodeID
      integer :: values_N
      real(8) :: values_t
      integer :: recoveryMethod
      integer :: p_deg
      integer :: localNonlocal
      logical :: regularizeBoundaries






     
	INTEGER:: I,J,K,IDUMMY,JDUMMY,TOTAL_PAIRS,KINC,IPROC,I1,I2,IA,
     &           NELST,NELEND,NELNOX,KOUNT,SPR_NUMBER_SIZE,ID_GRAIN,
     &           SPR_PATCH_SIZE,INODE,IGRAIN,NEW_NNODEX,NODE,IELEM,
     &           NPATCH,MaxElemInPatch,JJ,NUMBER_OF_PRINTS,kinc_max
	REAL*8:: TIME,START,FINISH, DUMMY
	INTEGER::IELGR(NELX),IGRAINELEM(NGRAINS,MAX_ELEM_IN_GRAIN),
     &          IJKL(NELX,4),NNEIGHBOR(NGRAINS),
     &          NEIGHBOR(NGRAINS,MAX_GRAIN_GRAIN_NEIGBOR),
     &          NGRAINELEM(NGRAINS),
     &          NUMBER_COMMON_NODES_IN_PAIR(MAX_GRAIN_PAIR),
     &          COMMON_NODE_IN_PAIR(MAX_GRAIN_PAIR,
     &                              MAX_COMMON_NODE_PAIR),
     &          GRAIN_PAIR(MAX_GRAIN_PAIR,2),INGR(NNODE),
     &          NATEL(NNODE),IATEL(NNODE,MAXADJGRAINELEM),
     &          NNODGR(NNODE),HARD_GRAIN(MAX_GRAIN_PAIR),
     &          INODGR(NNODE,MAXADJGRAIN),IJKL_UPDATED(NELX,4),
     &          NODE_WITH_MAX_R(MAX_GRAIN_PAIR),
     &          NODE_REPOSITORY(NNODE,MAXADJGRAIN)
	REAL*8::EUL(3),TR(3,3),C_VEC(NGRAINS,3),COORDS(NNODE,3),
     &		 ELVOL(NELX),STRESS(NELX,6),CFP(NELX,9),
     &         WATEL(NNODE,MAXADJGRAINELEM),TEFFMAX(MAX_GRAIN_PAIR),
     &         VNMAX(MAX_GRAIN_PAIR,3),VSMAX(MAX_GRAIN_PAIR,3),
     &         R(MAX_GRAIN_PAIR),CFP_NODAL(NNODE*MAXADJGRAIN*9),
     &         EULER(NGRAINS,3),DISP(NNODE,3),FP(NELX,9),
     &         STRESS_ELEMENTAL(NELX,9),GND_ELEMENTAL(NELX,2),
     &         PLASTIC_STRAIN_GP(NELX)
     
	INTEGER,ALLOCATABLE::SPR_NUMBER(:),SPR_PATCH(:),NODE_IN_GRAIN(:),
     &                  NElementsInPatch(:),ElementsInPatch(:,:)
	REAL*8,ALLOCATABLE::NODAL_GND(:,:),COORDS_UPDATED(:,:),
     &                     NODAL_STRESS(:,:),DISP_UPDATED(:,:),
     &                     SPR_weights(:),NODAL_PLASTIC_STRAIN(:)
	CHARACTER*30::FNAME
	CHARACTER*50::FILNAME
      CHARACTER*300::TEXT   


       INTEGER::IERROR,IPROC_MPI,NPROCS_MPI
       integer::proc_FIRST,proc_LAST,proc_IN_PROCESSOR
  
       CALL MPI_INIT(IERROR)

       CALL MPI_COMM_RANK(MPI_COMM_WORLD,IPROC_MPI,IERROR)
       CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NPROCS_MPI,IERROR)

      KINC_MAX = 0
      OPEN(1,FILE='time.out')         
      do
         read(1,*,iostat=ierror) dummy
         if (ierror.ne.0) exit 
         KINC_MAX=KINC_MAX+1
      enddo
      close(1)

      
	OPEN(1,FILE='grains.txt')
	DO I=1,NGRAINS
		READ(1,*) IDUMMY,EUL(1),EUL(2),EUL(3)
		CALL EULER_SLIP(EUL,TR)
		EULER(I,1)=EUL(1)
		EULER(I,2)=EUL(2)
		EULER(I,3)=EUL(3)
		C_VEC(I,:)=TR(:,3)
	ENDDO
	CLOSE(1)
	
   
      open(1,file='loadtime.inp')
      read(1,*) text
      read(1,*) text
      read(1,*) text
      read(1,*) fname
      close(1)
   
   
      FILNAME=trim(adjustl(fname))//'.inp'
      OPEN(1,FILE=FILNAME)
      read(1,*) text
      read(1,*) text
      read(1,*) text
      read(1,*) text
      DO I=1,NNODE
      READ(1,*) IDUMMY,(COORDS(I,J),J=1,3)
      ENDDO
      read(1,*) text
      read(1,*) text

      DO I=1,NELX
      READ(1,*) IDUMMY,(IJKL(I,J),J=1,4)
      ENDDO

      CLOSE(1)

   
      OPEN(201,FILE='PATCH.inp')
      READ(201,*) NPATCH,MaxElemInPatch
      
      ALLOCATE(NElementsInPatch(NPATCH))
      ALLOCATE(ElementsInPatch(NPATCH,MaxElemInPatch))
      
      NElementsInPatch=0
      ElementsInPatch=0
      
      DO I=1,NPATCH
         READ(201,*) NElementsInPatch(I),
     &               (ElementsInPatch(I,J),J=1,NElementsInPatch(I))      
      ENDDO
      
      CLOSE(201)   

      
      OPEN(1,FILE='ielgr.inp')
      DO I=1,NELX
         READ(1,*) IDUMMY,IELGR(I)
      ENDDO
      CLOSE(1)      

	CALL INITIALIZE(WATEL,ELVOL,NUMBER_COMMON_NODES_IN_PAIR,
     &            COMMON_NODE_IN_PAIR,GRAIN_PAIR,NGRAINELEM,
     &            IGRAINELEM,NNEIGHBOR,NEIGHBOR,INGR,NATEL,IATEL,
     &            IELGR,COORDS,IJKL,NNODGR,INODGR,NELX,MAXADJGRAIN,
     &            MAX_GRAIN_GRAIN_NEIGBOR,NNODE,MAX_ELEM_IN_GRAIN,
     &            NGRAINS,MAX_COMMON_SURFACE_IN_PAIR,MAX_GRAIN_PAIR,
     &            MAX_COMMON_NODE_PAIR,TOTAL_PAIRS,MAXADJGRAINELEM)
     

   ! deniz
      if (IPROC_MPI==0) open(997,file='debug_nodalrecovery.txt')
      if (IPROC_MPI==0) then
         write(*,*) 'p_deg ?'
         read(*,*) p_deg
         
         write(*,*) 'recoveryMethod?'
         read(*,*) recoveryMethod
         
         write(*,*) 'localNonlocal?'
         read(*,*) localNonlocal
         
         write(*,*) 'regularize boundaries?'
         read(*,*) regularizeBoundaries
      endif
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)


	
	
	OPEN(2,FILE='disp.out')
	
	
	DO IPROC=0,NPROCS-1
		CALL GET_SUBSCRIPT(IPROC,FNAME)
		FILNAME='stress'//FNAME
		OPEN(100+IPROC,FILE=FILNAME)
		FILNAME='curlfp'//FNAME
		OPEN(1000+IPROC,FILE=FILNAME)
      FILNAME='fp'//FNAME
      OPEN(7000+IPROC,FILE=FILNAME) 
	ENDDO


	OPEN(5000,FILE='TECPLOTU.PLT')
      OPEN(5001,FILE='TECPLOTD.PLT')
      OPEN(5002,FILE='TECPLOTUcell.PLT')
      OPEN(5003,FILE='TECPLOTDcell.PLT')
	text='TITLE = "Microstructure"'
	write(5000,*) text
	text='VARIABLES = "X","Y","Z","GND_DENSITY","NORM OF NYE","S11",
     & "S22","S33","S12","S13","S23","VON MISEES","NORM OF STRESS",
     & "HYDRO PRESSURE","U","V","W","PLASTIC STRAIN","GRAIN ID"'
	write(5000,*) text
      write(5001,*) text
      write(5002,*) text      
      write(5003,*) text
	
C******** INTRODUCING VIRTUAL NODES ON THE GRAIN BOUNDARIES ********
	KOUNT=0
	NODE_REPOSITORY=0
	DO INODE=1,NNODE
		NODE_REPOSITORY(INODE,1)=INODE   !NODE_REPOSITORY: THIS ARRAY STORES THE ORIGINAL+VIRUTAL NODE NUMBER. THE FIRST COLUMN IS ALWAYS THE ORIGINAL NODE NUMBER WHICH BELONGS TO THE FIRST GRAIN TO WHICH THIS NODE BELONGS TO. 
		IF (NNODGR(INODE).GT.1) THEN
			DO IGRAIN=2,NNODGR(INODE)
				KOUNT=KOUNT+1
				NODE_REPOSITORY(INODE,IGRAIN)=NNODE+KOUNT  ! THE SECOND, THIRD AND ... COLUMNS OF THIS ARRAY STORE THE VIRTUAL NODE NUMBERS
			ENDDO
		ENDIF
	ENDDO
	
	NEW_NNODEX=NNODE+KOUNT ! TOTAL NUMBER OF ORIGINAL+VIRTUAL NODES
	ALLOCATE(NODAL_GND(NEW_NNODEX,2))
	ALLOCATE(NODAL_STRESS(NEW_NNODEX,9))
	ALLOCATE(DISP_UPDATED(NEW_NNODEX,3))
      ALLOCATE(NODE_IN_GRAIN(NEW_NNODEX))
      ALLOCATE(NODAL_PLASTIC_STRAIN(NEW_NNODEX))

C******** ASSIGNING COORDINATES TO THE ORIGINAL+VIRTUAL NODES	
	ALLOCATE (COORDS_UPDATED(NEW_NNODEX,3))
	COORDS_UPDATED=0.D0
	DO INODE=1,NNODE
		DO IGRAIN=1,NNODGR(INODE)
			NODE=NODE_REPOSITORY(INODE,IGRAIN)
			COORDS_UPDATED(NODE,:)=COORDS(INODE,:)
         NODE_IN_GRAIN(NODE)=INODGR(INODE,IGRAIN)  ! STORES TO WHICH GRAIN THE NODE BELONGS
		ENDDO
	ENDDO	
	
	
C******** UPDATING THE CONNECTIVITY WITH VIRTUAL NODES ************
	DO IELEM=1,NELX
		ID_GRAIN=IELGR(IELEM)
		DO INODE=1,4
			NODE=IJKL(IELEM,INODE)
			IF (NNODGR(NODE).EQ.1) THEN
				IJKL_UPDATED(IELEM,INODE)=NODE
			ELSE
				DO IGRAIN=1,NNODGR(NODE)
					IF (INODGR(NODE,IGRAIN).EQ.ID_GRAIN) THEN
						IJKL_UPDATED(IELEM,INODE)=
     &						 NODE_REPOSITORY(NODE,IGRAIN)
						EXIT
					ENDIF
				ENDDO
			ENDIF
		ENDDO
	ENDDO	
	

      CALL PARTITION_CALCULATOR(proc_FIRST,proc_LAST,
     &   proc_IN_PROCESSOR,nprocs,NPROCS_MPI,IPROC_MPI) 
     
      proc_FIRST=proc_FIRST-1
      proc_LAST=proc_LAST-1

		
	
      NUMBER_OF_PRINTS=0
      
      
      
      
      ! deniz
      allocate(IJK(NELX*4))
      do iElement = 1,NELX
         IJK((iElement-1)*4+1:(iElement-1)*4+4) = IJKL(iElement,:)
      enddo
      allocate(G0XYZ(NNODE*3))
      do iNode = 1,NNODE
         G0XYZ((iNode-1)*3+1:(iNode-1)*3+3) = COORDS(iNode,:)
      enddo
   
      ! deniz
      CALL initialize_Patches('listPatches.dat',
     &               NNODE,NELX,IJK,G0XYZ)
      if (patchesInitialized) then
         write(*,*) 'patches imported:',nPatches
      else
         write(*,*) 'error importing listPatches.dat'
         stop
      endif
      ! deniz
	      
      
      
      
      
      
	
	DO KINC=1,KINC_MAX
		
      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)
      STRESS=0.d0
      CFP=0.d0
      Fp=0.d0
      
      
		IF (IPROC_MPI.EQ.0) WRITE(*,*) 'CALCULATING FOR CYCLE=',KINC
      CALL CPU_TIME(START)
		DO IPROC=proc_FIRST,proc_LAST
			    
         I1=NPATCH/NPROCS+1
         I2=NPATCH/NPROCS
         
         IA=NPATCH-I2*NPROCS
 
         
         IF((IPROC+1).LE.IA)THEN
            NELST=IPROC*I1+1
            NELNOX=I1
         ELSE
            NELST=IA*I1+(IPROC-IA)*I2+1
            NELNOX=I2
         ENDIF
         
         NELEND=NELST+NELNOX-1

            
			DO I=NELST,NELEND
         DO JJ=1,NElementsInPatch(I)
         
				DO J=1,6
					READ(100+IPROC,*) STRESS(ElementsInPatch(I,JJ),J)
				ENDDO
            
         ENDDO
         ENDDO
      
      ENDDO
      
      
      CALL CPU_TIME(FINISH)
      IF (IPROC_MPI.EQ.0) WRITE(*,*) 'STRESS READING:',(FINISH-START)
		CALL CPU_TIME(START)
		DO IPROC=proc_FIRST,proc_LAST
			    
         I1=NPATCH/NPROCS+1
         I2=NPATCH/NPROCS
         
         IA=NPATCH-I2*NPROCS
 
         
         IF((IPROC+1).LE.IA)THEN
            NELST=IPROC*I1+1
            NELNOX=I1
         ELSE
            NELST=IA*I1+(IPROC-IA)*I2+1
            NELNOX=I2
         ENDIF
         
         NELEND=NELST+NELNOX-1

         DO I=NELST,NELEND
         DO JJ=1,NElementsInPatch(I)
            KOUNT=0
            DO I1=1,3
               DO I2=1,3
                  KOUNT=KOUNT+1
                  READ(1000+IPROC,*) CFP(ElementsInPatch(I,JJ),KOUNT)
               ENDDO
            ENDDO
         ENDDO
         ENDDO
         
      enddo
      CALL CPU_TIME(FINISH)
      IF (IPROC_MPI.EQ.0) WRITE(*,*) 'CFP READING:',(FINISH-START)      
		CALL CPU_TIME(START)
		DO IPROC=proc_FIRST,proc_LAST
			    
         I1=NPATCH/NPROCS+1
         I2=NPATCH/NPROCS
         
         IA=NPATCH-I2*NPROCS
 
         
         IF((IPROC+1).LE.IA)THEN
            NELST=IPROC*I1+1
            NELNOX=I1
         ELSE
            NELST=IA*I1+(IPROC-IA)*I2+1
            NELNOX=I2
         ENDIF
         
         NELEND=NELST+NELNOX-1
      
         
			DO I=NELST,NELEND
         DO JJ=1,NElementsInPatch(I)
				KOUNT=0
				DO I1=1,3
					DO I2=1,3
                  KOUNT=KOUNT+1
						READ(7000+IPROC,*) FP(ElementsInPatch(I,JJ),KOUNT)
					ENDDO
				ENDDO
			ENDDO
         ENDDO
			
		ENDDO
      CALL CPU_TIME(FINISH)
      IF (IPROC_MPI.EQ.0) WRITE(*,*) 'FP READING:',(FINISH-START)
	

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)      
      call MPI_ALLREDUCE(MPI_IN_PLACE,stress,NELX*6,
     & MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)  
     

      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)      
      call MPI_ALLREDUCE(MPI_IN_PLACE,CFP,NELX*9,
     & MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)


      CALL MPI_BARRIER(MPI_COMM_WORLD,IERROR)      
      call MPI_ALLREDUCE(MPI_IN_PLACE,FP,NELX*9,
     & MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERROR)        
      


	
		DISP=0.D0
		DO I=1,NNODE
			READ(2,*) DISP(I,1),DISP(I,2),DISP(I,3)
		ENDDO

		
		DO INODE=1,NNODE
			DO IGRAIN=1,NNODGR(INODE)
				NODE=NODE_REPOSITORY(INODE,IGRAIN)
				DISP_UPDATED(NODE,:)=DISP(INODE,:)
			ENDDO
		ENDDO			
		
		IF (IPROC_MPI.EQ.0.and.kinc==kinc_max) THEN
		CALL CRITICAL_NODE_ORIENTATION(EULER,CFP_NODAL,TEFFMAX,
     &         VNMAX,VSMAX,HARD_GRAIN,NODE_WITH_MAX_R,INGR,C_VEC,WATEL,
     &            COORDS,CFP,STRESS,COMMON_NODE_IN_PAIR,NATEL,IATEL,
     &            GRAIN_PAIR,NUMBER_COMMON_NODES_IN_PAIR,IJKL,NNODGR,
     &            SPR_NUMBER,SPR_PATCH,IELGR,INODGR,TOTAL_PAIRS,
     &            MAX_GRAIN_PAIR,MAX_COMMON_NODE_PAIR,NNODE,
     &            MAXADJGRAIN,NELX,MAXADJGRAINELEM,NEW_NNODEX,
     &            SPR_NUMBER_SIZE,SPR_PATCH_SIZE,SPR_NO_COEF,
     &            SPR_WEIGHTS,NSP_SIZE,NGRAINS,BETA,NODE_REPOSITORY,
     &            NODAL_GND,BUR,NODAL_STRESS,FP,NODAL_PLASTIC_STRAIN,
     &            STRESS_ELEMENTAL,GND_ELEMENTAL,PLASTIC_STRAIN_GP,
     &         p_deg,recoveryMethod,localNonlocal,regularizeBoundaries)
     

	
		CALL TECPLOT(NEW_NNODEX,NELX,3,4,COORDS_UPDATED,
     +           IJKL_UPDATED,NODAL_GND,KINC,NODAL_STRESS,
     +           DISP_UPDATED,NODE_IN_GRAIN,NODAL_PLASTIC_STRAIN,
     +           NUMBER_OF_PRINTS,STRESS_ELEMENTAL,GND_ELEMENTAL,
     +           PLASTIC_STRAIN_GP,IELGR)
      ENDIF

		

      ENDDO

      CLOSE(2)
      CLOSE(5000)
      CLOSE(5001)

      DO IPROC=0,NPROCS-1
         CLOSE(100+IPROC)
         CLOSE(1000+IPROC)
      ENDDO	
      CALL MPI_FINALIZE(IERROR)!KOUROSH_MPI	
      END
	

C*************************************************************
C	CALCULATES NECESSARY MATRICES
C*************************************************************
	SUBROUTINE INITIALIZE(WATEL,ELVOL,NUMBER_COMMON_NODES_IN_PAIR,
     &            COMMON_NODE_IN_PAIR,GRAIN_PAIR,NGRAINELEM,
     &            IGRAINELEM,NNEIGHBOR,NEIGHBOR,INGR,NATEL,IATEL,
     &            IELGR,COORDS,IJKL,NNODGR,INODGR,NELX,MAXADJGRAIN,
     &            MAX_GRAIN_GRAIN_NEIGBOR,NNODE,MAX_ELEM_IN_GRAIN,
     &            NGRAINS,MAX_COMMON_SURFACE_IN_PAIR,MAX_GRAIN_PAIR,
     &            MAX_COMMON_NODE_PAIR,TOTAL_PAIRS,MAXADJGRAINELEM)
	IMPLICIT NONE
	INTEGER::I,J,NELX,IN1,IN2,IN3,IN4,NNODE,MAX_ELEM_IN_GRAIN,
     &          NGRAINS,MAX_GRAIN_GRAIN_NEIGBOR,IG,INEIGHBOR,JG,
     &          K,L,IEL,JEL,IELEM,JELEM,NODEI,NODEJ,IMATCH,
     &          MAX_COMMON_SURFACE_IN_PAIR,MAX_GRAIN_PAIR,
     &          TOTAL_PAIRS,MAX_COMMON_NODE_PAIR,IN_BULK,KOUNT,
     &          MNELX,MAXCRD,NODE,MAXADJGRAINELEM,MAXADJGRAIN
	REAL*8::AD1,AD2,AD3,BD1,BD2,BD3,CD1,CD2,CD3,BDCD1,BDCD2,BDCD3,
     &         DIST
	INTEGER::IJKL(NELX,4),IGRAINELEM(NGRAINS,MAX_ELEM_IN_GRAIN),
     &          NGRAINELEM(NGRAINS),IELGR(NELX),NNEIGHBOR(NGRAINS),
     &          NEIGHBOR(NGRAINS,MAX_GRAIN_GRAIN_NEIGBOR),ICNODE(3),
     &          COMMON_SURFACE(MAX_GRAIN_PAIR),IJK(NELX*4),
     &          GRAIN_PAIR(MAX_GRAIN_PAIR,2),INODGR(NNODE,MAXADJGRAIN),
     &          NODECOM(MAX_GRAIN_PAIR,MAX_COMMON_SURFACE_IN_PAIR,3),
     &          NUMBER_COMMON_NODES_IN_PAIR(MAX_GRAIN_PAIR),
     &          COMMON_NODE_IN_PAIR(MAX_GRAIN_PAIR,
     &                              MAX_COMMON_NODE_PAIR),
     &          INGR(NNODE),NATEL(NNODE),IATEL(NNODE,MAXADJGRAINELEM),
     &          NNODGR(NNODE)
	REAL*8::COORDS(NNODE,3),ELVOL(NELX),MIN_COORD(3),MAX_COORD(3),
     &         LIMIT(3),WATEL(NNODE,MAXADJGRAINELEM),GPXYZ(NNODE*3),
     &         CENTER(NELX,3)
	

	
	DO I=1,NELX
         IN1=IJKL(I,1)
         IN2=IJKL(I,2)
         IN3=IJKL(I,3)
         IN4=IJKL(I,4)
         AD1=COORDS(IN1,1)-COORDS(IN4,1)
         AD2=COORDS(IN1,2)-COORDS(IN4,2)
         AD3=COORDS(IN1,3)-COORDS(IN4,3)
         BD1=COORDS(IN2,1)-COORDS(IN4,1)         
         BD2=COORDS(IN2,2)-COORDS(IN4,2)        
         BD3=COORDS(IN2,3)-COORDS(IN4,3)   
         CD1=COORDS(IN3,1)-COORDS(IN4,1) 
         CD2=COORDS(IN3,2)-COORDS(IN4,2)  
         CD3=COORDS(IN3,3)-COORDS(IN4,3)        
         BDCD1=BD2*CD3-BD3*CD2
         BDCD2=BD3*CD1-BD1*CD3
         BDCD3=BD1*CD2-BD2*CD1
         ELVOL(I)=AD1*BDCD1+AD2*BDCD2+AD3*BDCD3
         ELVOL(I)=DABS(ELVOL(I))/6.0D0       
	ENDDO
	

C	CALCULATING NATEL, IATEL AND WEIGHTS
	NATEL=0
	IATEL=0
	WATEL=0.D0
	GPXYZ=0.D0
	IJK=0
	
	KOUNT=0
	DO I=1,NNODE
		DO J=1,3
			KOUNT=KOUNT+1
			GPXYZ(KOUNT)=COORDS(I,J)		
		ENDDO
	ENDDO
	
	KOUNT=0
	DO I=1,NELX
		DO J=1,4
			KOUNT=KOUNT+1
			IJK(KOUNT)=IJKL(I,J)
		ENDDO
	ENDDO
	
	
	MNELX=NELX*4
	MAXCRD=NNODE*3
		
	CALL CENTROID(CENTER,GPXYZ,IJK,MNELX,MAXCRD,NELX)
	
	DO I=1,NELX
		DO J=1,4
			NODE=IJKL(I,J)
			DIST=DSQRT( (COORDS(NODE,1)-CENTER(I,1))**2.D0+
     &                           (COORDS(NODE,2)-CENTER(I,2))**2.D0+
     &				     (COORDS(NODE,3)-CENTER(I,3))**2.D0 )
			NATEL(NODE)=NATEL(NODE)+1
			IATEL(NODE,NATEL(NODE))=I
			WATEL(NODE,NATEL(NODE))=DEXP(-1*DIST*DIST/50)*
     &                                       ELVOL(I)
     
			IF (NATEL(NODE).GT.MAXADJGRAINELEM) THEN
				STOP 'INCREASE MAXADJGRAINELEM'
			ENDIF	
     
		ENDDO
	ENDDO
	
C	CALCULATING NUMBER OF GRAINS ATTACHED TO A NODE	
	INODGR=0
	NNODGR=0
	
	DO I=1,NELX
		DO J=1,4
			NODE=IJKL(I,J)
			DO K=1,MAXADJGRAIN
				IF(INODGR(NODE,K).EQ.IELGR(I)) GOTO 1
			ENDDO
			NNODGR(NODE)=NNODGR(NODE)+1
			INODGR(NODE,NNODGR(NODE))=IELGR(I)
			
			IF (NNODGR(NODE).GT.MAXADJGRAIN) THEN
				STOP 'INCREASE MAXADJGRAIN'
			ENDIF
   1                 CONTINUE			
		ENDDO
	ENDDO
	
	
	RETURN
	END

	
C*************************************************************
C	FINDING CRITICAL NODE, CRITICAL ORIENTATION AND HARD GRAIN
C      FOR EACH GRAIN PAIR
C*************************************************************	
	SUBROUTINE CRITICAL_NODE_ORIENTATION(EULER,CFP_NODAL,TEFFMAX,
     &         VNMAX,VSMAX,HARD_GRAIN,NODE_WITH_MAX_R,INGR,C_VEC,WATEL,
     &            COORDS,CFP,STRESS,COMMON_NODE_IN_PAIR,NATEL,IATEL,
     &            GRAIN_PAIR,NUMBER_COMMON_NODES_IN_PAIR,IJKL,NNODGR,
     &            SPR_NUMBER,SPR_PATCH,IELGR,INODGR,TOTAL_PAIRS,
     &            MAX_GRAIN_PAIR,MAX_COMMON_NODE_PAIR,NNODE,
     &            MAXADJGRAIN,NELX,MAXADJGRAINELEM,NEW_NNODEX,
     &            SPR_NUMBER_SIZE,SPR_PATCH_SIZE,SPR_NO_COEF,
     &            SPR_WEIGHTS,NSP_SIZE,NGRAINS,BETA,NODE_REPOSITORY,
     &            NODAL_GND,BUR,NODAL_STRESS,FP,NODAL_PLASTIC_STRAIN,
     &            STRESS_ELEMENTAL,GND_ELEMENTAL,PLASTIC_STRAIN_GP,
     &         p_deg,recoveryMethod,localNonlocal,regularizeBoundaries)
     
      use nodalRecovery 
     
	IMPLICIT NONE
	INTEGER::I,J,K,TOTAL_PAIRS,MAX_GRAIN_PAIR,MAX_COMMON_NODE_PAIR,
     &          NODE,NNODE,MAXADJGRAIN,NELX,MAXADJGRAINELEM,MAXCRD,
     &          KOUNT,MNELX,SPR_NUMBER_SIZE,SPR_NO_COEF,NSP_SIZE,
     &          NCOMPONENT,GRAIN1,GRAIN2,LOCATION,NGRAINS,
     &          SPR_PATCH_SIZE,IGRAIN,VIRTUAL_NODE,NEW_NNODEX     
	REAL*8::SPR_WEIGHT,RMAX,BETA,TEFFMAX1,TEFFMAX2,AVERAGE_GND,BUR,
     &     S11,S22,S33,S12,S13,S23
	INTEGER::NUMBER_COMMON_NODES_IN_PAIR(MAX_GRAIN_PAIR),
     &          COMMON_NODE_IN_PAIR(MAX_GRAIN_PAIR,
     &                              MAX_COMMON_NODE_PAIR),
     &          IJK(NELX*4),IJKL(NELX,4),NNODGR(NNODE),
     &          SPR_NUMBER(SPR_NUMBER_SIZE),SPR_PATCH(SPR_PATCH_SIZE),
     &          IELGR(NELX),INODGR(NNODE,MAXADJGRAIN),
     &          NATEL(NNODE),IATEL(NNODE,MAXADJGRAINELEM),
     &          GRAIN_PAIR(MAX_GRAIN_PAIR,2),
     &          HARD_GRAIN(MAX_GRAIN_PAIR),
     &          NODE_WITH_MAX_R(MAX_GRAIN_PAIR),INGR(NNODE),
     &          NODE_REPOSITORY(NNODE,MAXADJGRAIN)	
	REAL*8::CFP_NODAL(NNODE*MAXADJGRAIN*9),CFP(NELX,9),
     &         WATEL(NNODE,MAXADJGRAINELEM),GPXYZ(NNODE*3),
     &         COORDS(NNODE,3),STRESS(NELX,6),
     &         STRESS_NODAL(NNODE*MAXADJGRAIN*6),STRESS1(3,3),
     &         STRESS2(3,3),CFP1(3,3),CFP2(3,3),VNMAX1(3),VNMAX2(3),
     &         VSMAX1(3),VSMAX2(3),C1(3),C2(3),C_VEC(NGRAINS,3),
     &         TEFFMAX(MAX_GRAIN_PAIR),VNMAX(MAX_GRAIN_PAIR,3),
     &         VSMAX(MAX_GRAIN_PAIR,3),EULER(NGRAINS,3),
     &         NODAL_GND(NEW_NNODEX,2),NODAL_STRESS(NEW_NNODEX,9),
     &         DUMMY(3,3),SPR_WEIGHTS(SPR_NUMBER_SIZE),FP(NELX,9),
     &         NODAL_PLASTIC_STRAIN(NEW_NNODEX),EYE(3,3),
     &         PLASTIC_STRAIN_NODAL(NNODE*MAXADJGRAIN),
     &         PLASTIC_STRAIN_GP(NELX),STRESS_ELEMENTAL(NELX,9),
     &         GND_ELEMENTAL(NELX,2)
     
     
      real(8), allocatable :: FP_new(:,:)
      real(8), allocatable :: FP_NODAL_new(:,:)
      real(8), allocatable :: CFP_new(:,:)
      real(8), allocatable :: CFP_NODAL_new(:,:)
      real(8), allocatable :: STRESS_new(:,:)
      real(8), allocatable :: STRESS_NODAL_new(:,:)
      real(8), allocatable :: PLASTIC_STRAIN_NODAL_new(:,:)
      real(8), allocatable :: PLASTIC_STRAIN_GP_new(:,:)
      integer, intent(in) :: p_deg
      integer, intent(in) :: recoveryMethod
      integer, intent(in) :: localNonlocal
      logical, intent(in) :: regularizeBoundaries
      integer :: iPatchID, nodeID, grainID, grainIdx
      integer :: nFailedPatches
      integer :: info
      logical, parameter :: DEBUG_nodalValues = .false.

      

      allocate(FP_new(9,NELX))
      allocate(FP_NODAL_new(9,nPatches))
      allocate(CFP_new(9,NELX))
      allocate(CFP_NODAL_new(9,nPatches))
      allocate(STRESS_new(6,NELX))
      allocate(STRESS_NODAL_new(6,nPatches))
      allocate(PLASTIC_STRAIN_GP_new(1,NELX))
      allocate(PLASTIC_STRAIN_NODAL_new(1,nPatches))


      
	GPXYZ=0.D0
	TEFFMAX=0.D0
	NODE_WITH_MAX_R=0
	VNMAX=0.D0
	VSMAX=0.D0
	HARD_GRAIN=0
	
       
       EYE=0.D0
       EYE(1,1)=1.D0
       EYE(2,2)=1.D0
       EYE(3,3)=1.D0
       
	KOUNT=0
	DO I=1,NNODE
		DO J=1,3
			KOUNT=KOUNT+1
			GPXYZ(KOUNT)=COORDS(I,J)		
		ENDDO
	ENDDO
	
	KOUNT=0
	DO I=1,NELX
		DO J=1,4
			KOUNT=KOUNT+1
			IJK(KOUNT)=IJKL(I,J)
		ENDDO
      
      DUMMY(1,1)=FP(I,1)
		DUMMY(1,2)=FP(I,2)
		DUMMY(1,3)=FP(I,3)
		DUMMY(2,1)=FP(I,4)
		DUMMY(2,2)=FP(I,5)
		DUMMY(2,3)=FP(I,6)
		DUMMY(3,1)=FP(I,7)
		DUMMY(3,2)=FP(I,8)
		DUMMY(3,3)=FP(I,9)
      
      DUMMY=1./2.*(MATMUL(TRANSPOSE(DUMMY),DUMMY)-EYE)
                     
      DUMMY=MATMUL(TRANSPOSE(DUMMY),DUMMY)
              
      PLASTIC_STRAIN_GP(I)=DSQRT((DUMMY(1,1)+DUMMY(2,2)+
     &                                    DUMMY(3,3))*2./3.)
     
     
      STRESS_ELEMENTAL(I,1:6)=STRESS(I,1:6)
     
      S11=STRESS(I,1)
      S22=STRESS(I,2)
      S33=STRESS(I,3)
      S12=STRESS(I,4)
      S13=STRESS(I,5)
      S23=STRESS(I,6)
      
      STRESS_ELEMENTAL(I,7)=DSQRT(0.5D0*((S11-S22)**2.+
     &   (S11-S33)**2.+(S22-S33)**2.+6.D0*(S12**2.+S23**2.+S13**2.)))  ! VON-MISES STRESS
     
      STRESS_ELEMENTAL(I,8)=DSQRT(S11**2.+S22**2.+S33**2.+
     &   2.*S12**2.+2.*S13**2.+2.*S23**2.)  ! L-2 NORM OF STRESS TENSOR
     
      STRESS_ELEMENTAL(I,9)=(S11+S22+S33)/3.  ! HYDRO-STATIC PRESSURE        


		CFP1(1,1)=CFP(I, 1)
		CFP1(1,2)=CFP(I, 2)
		CFP1(1,3)=CFP(I, 3)
		CFP1(2,1)=CFP(I, 4)
		CFP1(2,2)=CFP(I, 5)
		CFP1(2,3)=CFP(I, 6)
		CFP1(3,1)=CFP(I, 7)
		CFP1(3,2)=CFP(I, 8)
		CFP1(3,3)=CFP(I, 9)
		IGRAIN=IELGR(I)
		CALL GND_CALCULATE(AVERAGE_GND,CFP1,EULER,NGRAINS,IGRAIN)
		
		GND_ELEMENTAL(I,1)=AVERAGE_GND/BUR
		
     
      DUMMY=MATMUL(TRANSPOSE(CFP1),CFP1)
      GND_ELEMENTAL(I,2)=DUMMY(1,1)+DUMMY(2,2)+DUMMY(3,3)  ! L-2 NORM OF NYE TENSOR	

      
	ENDDO
	
	
	MNELX=NELX*4
	MAXCRD=NNODE*3
	
	
	
		NCOMPONENT=9
!	CALL SPR(CFP_NODAL,CFP,WATEL,GPXYZ,IJK,NNODGR,SPR_NUMBER,
!     &          SPR_PATCH,IELGR,INODGR,NATEL,IATEL,NELX,0,
!     &          NNODE,0,NNODE,SPR_NUMBER_SIZE,SPR_PATCH_SIZE,
!     &          MNELX,MAXCRD,SPR_NO_COEF,SPR_WEIGHTS,NSP_SIZE,
!     &          MAXADJGRAIN,MAXADJGRAINELEM,NCOMPONENT)
         ! deniz
         do I=1,NELX
            CFP_new(1:9,I) = CFP(I,1:9)
         enddo
         
         
         if (recoveryMethod == 1) then
         
             CALL nodalAveraging(CFP_NODAL_new,
     &                           CFP_new,
     &                          NCOMPONENT,DEBUG_nodalValues)
         elseif(recoveryMethod == 2) then
         
         CALL recoverySPR(CFP_NODAL_new,CFP_new,
     &                 NCOMPONENT,p_deg,regularizeBoundaries,
     &                            localNonlocal,nFailedPatches,
     &                        info,DEBUG_nodalValues)
     
         if (nFailedPatches /= 0) then
            write(*,*) 'failed patches:',nFailedPatches
         endif


         elseif (recoveryMethod == 3) then
         
            CALL recoveryLSQ(CFP_NODAL_new,
     &                       CFP_new,
     &                       NCOMPONENT,info,.false.)

         endif
     
                              
         do iPatchID = 1,nPatches
            nodeID = nodeIDFromPatchID(iPatchID)
            grainID = patchGrainID(iPatchID)
            do grainIdx=1,NNODGR(nodeID)
               if (INODGR(nodeID,grainIdx)==grainID) then
               CFP_NODAL((nodeID-1)*MAXADJGRAIN*NCOMPONENT+
     &(grainIdx-1)*NCOMPONENT+1:(nodeID-1)*MAXADJGRAIN*NCOMPONENT+
     &(grainIdx)*NCOMPONENT) = CFP_NODAL_new(1:NCOMPONENT,iPatchID)
                  exit
               endif
            enddo
         enddo
         ! deniz          

	
	NCOMPONENT=6
!	CALL SPR(STRESS_NODAL,STRESS,WATEL,GPXYZ,IJK,NNODGR,SPR_NUMBER,
!     &          SPR_PATCH,IELGR,INODGR,NATEL,IATEL,NELX,0,
!     &          NNODE,0,NNODE,SPR_NUMBER_SIZE,SPR_PATCH_SIZE,
!     &          MNELX,MAXCRD,SPR_NO_COEF,SPR_WEIGHTS,NSP_SIZE,
!     &          MAXADJGRAIN,MAXADJGRAINELEM,NCOMPONENT)
         ! deniz
         do I=1,NELX
            STRESS_new(1:6,I) = STRESS(I,1:6)
         enddo

         if (recoveryMethod == 1) then

            CALL nodalAveraging(STRESS_NODAL_new,STRESS_new,
     &                          NCOMPONENT,DEBUG_nodalValues)

         elseif (recoveryMethod == 2) then
         
            CALL recoverySPR(STRESS_NODAL_new,STRESS_new,
     &                 NCOMPONENT,p_deg,regularizeBoundaries,
     &                            localNonlocal,nFailedPatches,
     &                        info,DEBUG_nodalValues)    
            if (nFailedPatches /= 0) then
               write(*,*) 'failed patches:',nFailedPatches
            endif
            
         elseif (recoveryMethod == 3) then
         
            CALL recoveryLSQ(STRESS_NODAL_new,STRESS_new,
     &                              NCOMPONENT,info,.true.)

         endif
                              
         do iPatchID = 1,nPatches
            nodeID = nodeIDFromPatchID(iPatchID)
            grainID = patchGrainID(iPatchID)
            do grainIdx=1,NNODGR(nodeID)
               if (INODGR(nodeID,grainIdx)==grainID) then
               STRESS_NODAL((nodeID-1)*MAXADJGRAIN*NCOMPONENT+
     &(grainIdx-1)*NCOMPONENT+1:(nodeID-1)*MAXADJGRAIN*NCOMPONENT+
     &(grainIdx)*NCOMPONENT) = STRESS_NODAL_new(1:NCOMPONENT,iPatchID)
                  exit
               endif
            enddo
         enddo
         ! deniz  
     
	
      NCOMPONENT=1
!      CALL SPR(PLASTIC_STRAIN_NODAL,PLASTIC_STRAIN_GP,WATEL,GPXYZ,
!     &          IJK,NNODGR,SPR_NUMBER,
!     &          SPR_PATCH,IELGR,INODGR,NATEL,IATEL,NELX,0,
!     &          NNODE,0,NNODE,SPR_NUMBER_SIZE,SPR_PATCH_SIZE,
!     &          MNELX,MAXCRD,SPR_NO_COEF,SPR_WEIGHTS,NSP_SIZE,
!     &          MAXADJGRAIN,MAXADJGRAINELEM,NCOMPONENT)     
         ! deniz
         do I=1,NELX
            PLASTIC_STRAIN_GP_new(1,I) = PLASTIC_STRAIN_GP(I)
         enddo
         
         if (recoveryMethod == 1) then
         
             CALL nodalAveraging(PLASTIC_STRAIN_NODAL_new,
     &                           PLASTIC_STRAIN_GP_new,
     &                          NCOMPONENT,DEBUG_nodalValues)
         elseif(recoveryMethod == 2) then
         
         CALL recoverySPR(PLASTIC_STRAIN_NODAL_new,
     &PLASTIC_STRAIN_GP_new,NCOMPONENT,p_deg,regularizeBoundaries,
     &                            localNonlocal,nFailedPatches,
     &info,DEBUG_nodalValues)
        
            if (nFailedPatches /= 0) then
               write(*,*) 'failed patches:',nFailedPatches
            endif
            
         elseif (recoveryMethod == 3) then
         
            CALL recoveryLSQ(PLASTIC_STRAIN_NODAL_new,
     &                       PLASTIC_STRAIN_GP_new,
     &                       NCOMPONENT,info,.false.)

         endif
     

                              
         do iPatchID = 1,nPatches
            nodeID = nodeIDFromPatchID(iPatchID)
            grainID = patchGrainID(iPatchID)
            do grainIdx=1,NNODGR(nodeID)
               if (INODGR(nodeID,grainIdx)==grainID) then
               PLASTIC_STRAIN_NODAL((nodeID-1)*MAXADJGRAIN*NCOMPONENT+
     &(grainIdx-1)*NCOMPONENT+1:(nodeID-1)*MAXADJGRAIN*NCOMPONENT+
     &(grainIdx)*NCOMPONENT) = 
     &PLASTIC_STRAIN_NODAL_new(1:NCOMPONENT,iPatchID)
                  exit
               endif
            enddo
         enddo
         ! deniz     
          
     
	DO I=1,NNODE
	DO J=1,NNODGR(I)
		VIRTUAL_NODE=NODE_REPOSITORY(I,J)
		
		
		LOCATION=(I-1)*MAXADJGRAIN*9+(J-1)*9
		CFP1(1,1)=CFP_NODAL(LOCATION+1)
		CFP1(1,2)=CFP_NODAL(LOCATION+2)
		CFP1(1,3)=CFP_NODAL(LOCATION+3)
		CFP1(2,1)=CFP_NODAL(LOCATION+4)
		CFP1(2,2)=CFP_NODAL(LOCATION+5)
		CFP1(2,3)=CFP_NODAL(LOCATION+6)
		CFP1(3,1)=CFP_NODAL(LOCATION+7)
		CFP1(3,2)=CFP_NODAL(LOCATION+8)
		CFP1(3,3)=CFP_NODAL(LOCATION+9)
		IGRAIN=INODGR(I,J)
		CALL GND_CALCULATE(AVERAGE_GND,CFP1,EULER,NGRAINS,
     &                         IGRAIN)
		
		NODAL_GND(VIRTUAL_NODE,1)=AVERAGE_GND/BUR
		
     
      DUMMY=MATMUL(TRANSPOSE(CFP1),CFP1)
      NODAL_GND(VIRTUAL_NODE,2)=DUMMY(1,1)+DUMMY(2,2)+DUMMY(3,3)  ! L-2 NORM OF NYE TENSOR		
		
      LOCATION=(I-1)*MAXADJGRAIN+(J-1)
      NODAL_PLASTIC_STRAIN(VIRTUAL_NODE)=
     &                         PLASTIC_STRAIN_NODAL(LOCATION+1)   
      
      
		LOCATION=(I-1)*MAXADJGRAIN*6+(J-1)*6
		NODAL_STRESS(VIRTUAL_NODE,1:6)=STRESS_NODAL(LOCATION+1:
     &                                                    LOCATION+6)
     
     
     
      S11=STRESS_NODAL(LOCATION+1)
      S22=STRESS_NODAL(LOCATION+2)
      S33=STRESS_NODAL(LOCATION+3)
      S12=STRESS_NODAL(LOCATION+4)
      S13=STRESS_NODAL(LOCATION+5)
      S23=STRESS_NODAL(LOCATION+6)
      
      NODAL_STRESS(VIRTUAL_NODE,7)=DSQRT(0.5D0*((S11-S22)**2.+
     &   (S11-S33)**2.+(S22-S33)**2.+6.D0*(S12**2.+S23**2.+S13**2.)))  ! VON-MISES STRESS
     
      NODAL_STRESS(VIRTUAL_NODE,8)=DSQRT(S11**2.+S22**2.+S33**2.+
     &   2.*S12**2.+2.*S13**2.+2.*S23**2.)  ! L-2 NORM OF STRESS TENSOR
     
      NODAL_STRESS(VIRTUAL_NODE,9)=(S11+S22+S33)/3.  ! HYDRO-STATIC PRESSURE     
		
	ENDDO
	ENDDO


      deallocate(FP_new)
      deallocate(FP_NODAL_new)
      deallocate(CFP_new)
      deallocate(CFP_NODAL_new)
      deallocate(STRESS_new)
      deallocate(STRESS_NODAL_new)
      deallocate(PLASTIC_STRAIN_NODAL_new)
      deallocate(PLASTIC_STRAIN_GP_new)
   	
	
	RETURN
	END


C*************************************************************	
      SUBROUTINE EULER_SLIP(EULER,TLGT)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTRINSIC TRANSPOSE

      DIMENSION EULER(3),TLG(3,3),TLGT(3,3)

      PI=4.D0*DATAN(1.D0)

      PHI=EULER(1)
      THETA =EULER(2)
      OMEGA  =EULER(3)
  
      SP=DSIN(PHI)                      
      CP=DCOS(PHI)                     
      ST=DSIN(THETA)                     
      CT=DCOS(THETA)                    
      SO=DSIN(OMEGA)                    
      CO=DCOS(OMEGA)   
      TLG(1,1)=CO*CP-SO*SP*CT
      TLG(1,2)=CO*SP+SO*CT*CP   
      TLG(1,3)=SO*ST   
      TLG(2,1)=-SO*CP-SP*CO*CT 
      TLG(2,2)=-SO*SP+CT*CO*CP
      TLG(2,3)=CO*ST
      TLG(3,1)=SP*ST       
      TLG(3,2)=-ST*CP       
      TLG(3,3)=CT
	
      TLGT=TRANSPOSE(TLG)	

      RETURN
      END 

C*************************************************************
      SUBROUTINE GET_SUBSCRIPT(IPROC,FNAME)
      IMPLICIT REAL*8(A-H,O-Z)
      CHARACTER*30 FNAME
      DIMENSION IN(10),INTMP(10)           
      
      IPROC1=IPROC
      I=0

      DO WHILE(IPROC1.GT.9)
         I1=IPROC1/10
         I2=IPROC1-I1*10
         I=I+1
         IPROC1=I1
         IN(I)=I2
      ENDDO

      I=I+1
      IN(I)=IPROC1

      DO J=1,I
         INTMP(I-J+1)=IN(J)
      ENDDO

      DO J=1,I
         IN(J)=INTMP(J)
      ENDDO

      IF(I.EQ.1)THEN
         FNAME='_'//ACHAR(48+IN(1))//'.OUT'
      ELSEIF(I.EQ.2)THEN
         FNAME='_'//ACHAR(48+IN(1))//ACHAR(48+IN(2))//'.OUT'
      ELSE
         FNAME='_'//ACHAR(48+IN(1))//ACHAR(48+IN(2))//ACHAR(48+IN(3))
     &   //'.OUT'
      ENDIF
      
      
      RETURN
      END 


C*************************************************************
	SUBROUTINE SPR(FP_NODAL,FPEL,WATEL,GPXYZ,IJK,NNODGR,SPR_NUMBER,
     &                SPR_PATCH,IELGR,INODGR,NATEL,IATEL,NELX,NPROCS,
     &                NX,IPROC,NNODEX,SPR_NUMBER_SIZE,SPR_PATCH_SIZE,
     &                MNELX,MAXCRD,SPR_NO_COEF,SPR_WEIGHTS,NSP_SIZE,
     &                MAXADJGRAIN,MAXADJGRAINELEM,NCOMPONENT)
	IMPLICIT NONE
	
	INTEGER::NELX,ICOMPONENT,NPROCS,NX,I1,I2,IA,IPROC,
     &          NODE_FIRST,NODE_IN_PROCESSOR,NODE_LAST,NODE,
     &          NNODEX,IGRAIN,KOUNT,NELEM_IN_PATCH,
     &          SPR_NUMBER_SIZE,SPR_PATCH_SIZE,MNELX,MAXCRD,
     &          SPR_NO_COEF,NSP_SIZE,LOCATION,MAXADJGRAIN,K_SPR,
     &          MAXADJGRAINELEM,NCOMPONENT
	REAL*8::X_MIN,X_MAX,Y_MIN,Y_MAX,Z_MIN,Z_MAX,X_NODE,
     &         Y_NODE,Z_NODE,X_NODE_NORMALIZED,Y_NODE_NORMALIZED,
     &         Z_NODE_NORMALIZED,SPR_WEIGHT,UNITY
	INTEGER::NNODGR(NNODEX),SPR_NUMBER(SPR_NUMBER_SIZE),
     &          SPR_PATCH(SPR_PATCH_SIZE),IJK(MNELX),IELGR(NELX),
     &          NATEL(NNODEX),IATEL(NNODEX,MAXADJGRAINELEM),
     &          INODGR(NNODEX,MAXADJGRAIN)
	REAL*8::FPEL(NELX,NCOMPONENT),GPXYZ(MAXCRD),P_NODE(SPR_NO_COEF),
     &         CENTER(NELX,3),P_SPR(NSP_SIZE,SPR_NO_COEF),
     &         GAUSS_VALUES(NSP_SIZE,NCOMPONENT),
     &         FP_NODAL(NX*MAXADJGRAIN*NCOMPONENT),
     &         WATEL(NNODEX,MAXADJGRAINELEM),NODAL_VALUE(NCOMPONENT),
     &         SPR_WEIGHTS(SPR_NUMBER_SIZE)
	
		
		
	FP_NODAL=0.D0	
C************* PARTITIONING NODES TO PROCESSORS
C      I1=NX/NPROCS+1
C     I2=NX/NPROCS
C      
C     IA=NX-I2*NPROCS
C
C      IF((IPROC+1).LE.IA)THEN
C         NODE_FIRST=IPROC*I1+1
C         NODE_IN_PROCESSOR=I1
C      ELSE
C         NODE_FIRST=IA*I1+(IPROC-IA)*I2+1
C         NODE_IN_PROCESSOR=I2
C      ENDIF
C
C      NODE_LAST=NODE_FIRST+NODE_IN_PROCESSOR-1	 	
C************* END PARTITIONING NODES

	
	CALL CENTROID(CENTER,GPXYZ,IJK,MNELX,MAXCRD,NELX)
       NODE_FIRST=1
	NODE_LAST=NX
	
	KOUNT=0
	K_SPR=0
	DO NODE=NODE_FIRST,NODE_LAST
	
		X_NODE=GPXYZ(NODE*3-2)
		Y_NODE=GPXYZ(NODE*3-1)
		Z_NODE=GPXYZ(NODE*3)
			
		DO IGRAIN=1,NNODGR(NODE)
			KOUNT=KOUNT+1
			NELEM_IN_PATCH=SPR_NUMBER(KOUNT)
         SPR_WEIGHT=SPR_WEIGHTS(KOUNT)
			
				
		   IF (NELEM_IN_PATCH.LE.(SPR_NO_COEF+10)) THEN
			CALL WEIGHTED_AVERAGING(NODAL_VALUE,FPEL,
     &                          WATEL,IELGR,INODGR,IATEL,NATEL,NODE,
     &                          NELX,NNODEX,MAXADJGRAINELEM,IGRAIN,
     &                          MAXADJGRAIN,NCOMPONENT)
		   ELSE
			CALL MIN_MAX_PATCH_COORD(CENTER,
     &                          NELEM_IN_PATCH,
     &                          SPR_PATCH,SPR_PATCH_SIZE,K_SPR,NELX,
     &                          X_MIN,X_MAX,Y_MIN,Y_MAX,Z_MIN,Z_MAX,
     &                          NODE)
     
     
			IF((X_MAX-X_MIN).LT.1.D-9.OR.
     &                  (Y_MAX-Y_MIN).LT.1.D-9.OR.
     &                  (Z_MAX-Z_MIN).LT.1.D-9) THEN
				WRITE(*,*) 'CAUTION! MAX-MIN IN PATCH VERY CLOSE'
				WRITE(*,*) 'PATCH FOR NODE=',NODE,'IN GRAIN=',IGRAIN
				WRITE(*,*) 'X-MAX X-MIN',X_MAX,X_MIN
				WRITE(*,*) 'Y-MAX Y-MIN',Y_MAX,Y_MIN
				WRITE(*,*) 'Z-MAX Z-MIN',Z_MAX,Z_MIN
				STOP
			ENDIF
     
		
			X_NODE_NORMALIZED=-1.0+2.0*(X_NODE-
     &                                 X_MIN)/(X_MAX-X_MIN)
			Y_NODE_NORMALIZED=-1.0+2.0*(Y_NODE-
     &                                 Y_MIN)/(Y_MAX-Y_MIN)
			Z_NODE_NORMALIZED=-1.0+2.0*(Z_NODE-
     &                                 Z_MIN)/(Z_MAX-Z_MIN)
    				
     
			UNITY=1.D0
			CALL P_NODE_CALC(P_NODE,SPR_NO_COEF,
     &                          X_NODE_NORMALIZED,Y_NODE_NORMALIZED,
     &                          Z_NODE_NORMALIZED,UNITY)
     
			CALL SPR_MATRIX_CALC(P_SPR,GAUSS_VALUES,
     &                          FPEL,CENTER,SPR_PATCH,SPR_PATCH_SIZE,
     &       		           K_SPR,NELX,NELEM_IN_PATCH,
     &                          NSP_SIZE,
     &            		    X_MIN,X_MAX,Y_MIN,Y_MAX,Z_MIN,Z_MAX,
     &                          X_NODE_NORMALIZED,Y_NODE_NORMALIZED,
     &                          Z_NODE_NORMALIZED,SPR_WEIGHT,
     &                          SPR_NO_COEF,NODE,NCOMPONENT)
     
				
			CALL SPR_SOLUTION(P_NODE,P_SPR,
     &                          GAUSS_VALUES,NSP_SIZE,SPR_NO_COEF,
     &                          NELEM_IN_PATCH,NODAL_VALUE,
     &                          NODE,IGRAIN,NCOMPONENT)
     
		   ENDIF

  
				
		   LOCATION=(NODE-1)*MAXADJGRAIN*NCOMPONENT+
     &                    (IGRAIN-1)*NCOMPONENT
     
		   DO ICOMPONENT=1,NCOMPONENT
			IF(NODAL_VALUE(ICOMPONENT).NE.NODAL_VALUE(ICOMPONENT)) THEN
				WRITE(*,*) 'ERROR!NAN DETECTED IN SPR'
				WRITE(*,*) 'NODE,IGRAIN,NELEM_IN_PATCH'
				WRITE(*,*) NODE,IGRAIN,NELEM_IN_PATCH
				STOP
			ENDIF
	
			FP_NODAL(LOCATION+ICOMPONENT)=NODAL_VALUE(ICOMPONENT)
		   ENDDO
				
		   K_SPR=K_SPR+NELEM_IN_PATCH
				
		ENDDO
	ENDDO


	
	
	RETURN
	END
	
	
C*************************************************************
C	CALCULATES CENTROID OF ALL ELEMENTS
C*************************************************************
	SUBROUTINE CENTROID(CENTER,GPXYZ,IJK,MNELX,MAXCRD,NELX)
	IMPLICIT NONE
	INTEGER::ELEM,I,MAXCRD,NELX,MNELX
	INTEGER::IJK(MNELX),NODE_IN_ELEM(4)
	REAL*8::XSET(4),YSET(4),ZSET(4),GPXYZ(MAXCRD),CENTER(NELX,3)
	

	DO ELEM=1,NELX
		
		NODE_IN_ELEM(1)=IJK(ELEM*4-3)
		NODE_IN_ELEM(2)=IJK(ELEM*4-2)
		NODE_IN_ELEM(3)=IJK(ELEM*4-1)
		NODE_IN_ELEM(4)=IJK(ELEM*4)
		
		DO I=1,4
			XSET(I)=GPXYZ(NODE_IN_ELEM(I)*3-2)
			YSET(I)=GPXYZ(NODE_IN_ELEM(I)*3-1)
			ZSET(I)=GPXYZ(NODE_IN_ELEM(I)*3)		
		ENDDO
		
		   
		CENTER(ELEM,1)=1.0/4.0*(XSET(1)+XSET(2)+XSET(3)+XSET(4))
		CENTER(ELEM,2)=1.0/4.0*(YSET(1)+YSET(2)+YSET(3)+YSET(4))
		CENTER(ELEM,3)=1.0/4.0*(ZSET(1)+ZSET(2)+ZSET(3)+ZSET(4))	
	
	ENDDO
	
	RETURN
	END	
	
C*************************************************************
C	CALCULATES MAX AND MIN COORDINATE OF THE GAUSS POINTS IN THE PATCH
C*************************************************************
	SUBROUTINE MIN_MAX_PATCH_COORD(CENTER,NELEM_IN_PATCH,
     &            SPR_PATCH,SPR_PATCH_SIZE,K_SPR,NELX,
     &            X_MIN,X_MAX,Y_MIN,Y_MAX,Z_MIN,Z_MAX,NODE)
	IMPLICIT NONE
	INTEGER::NELEM_IN_PATCH,SPR_PATCH_SIZE,IELEM,ELEM,
     &           K_SPR,NELX,NODE
	REAL*8::X_MIN,X_MAX,Y_MIN,Y_MAX,Z_MIN,Z_MAX,X_CENTER,
     &         Y_CENTER,Z_CENTER
	INTEGER::SPR_PATCH(SPR_PATCH_SIZE)
	REAL*8::CENTER(NELX,3)
	
	
	X_MIN=1.D10
	Y_MIN=1.D10
	Z_MIN=1.D10
	X_MAX=-1.D10
	Y_MAX=-1.D10
	Z_MAX=-1.D10
	
	
	DO IELEM=1,NELEM_IN_PATCH
		ELEM=SPR_PATCH(K_SPR+IELEM)
		
		   
		X_CENTER=CENTER(ELEM,1)
		Y_CENTER=CENTER(ELEM,2)
		Z_CENTER=CENTER(ELEM,3)


       
		IF (X_CENTER.GT.X_MAX)THEN
			X_MAX=X_CENTER
		ENDIF
		IF (X_CENTER.LT.X_MIN)THEN
			X_MIN=X_CENTER
		ENDIF
		IF (Y_CENTER.GT.Y_MAX)THEN
			Y_MAX=Y_CENTER
		ENDIF
		IF (Y_CENTER.LT.Y_MIN)THEN
			Y_MIN=Y_CENTER
		ENDIF
		IF (Z_CENTER.GT.Z_MAX)THEN
			Z_MAX=Z_CENTER
		ENDIF
		IF (Z_CENTER.LT.Z_MIN)THEN
			Z_MIN=Z_CENTER
		ENDIF   		
	
	ENDDO
	
	RETURN
	END

		
C*************************************************************
C	P VECTOR FOR THE NODE
C*************************************************************	
	SUBROUTINE P_NODE_CALC(P_NODE,SPR_NO_COEF,X_NODE_NORMALIZED,
     &                   Y_NODE_NORMALIZED,Z_NODE_NORMALIZED,WEIGHT)
	IMPLICIT NONE
	INTEGER::SPR_NO_COEF
	REAL*8::X_NODE_NORMALIZED,Y_NODE_NORMALIZED,Z_NODE_NORMALIZED,
     &         WEIGHT
	REAL*8::P_NODE(SPR_NO_COEF)
	
	P_NODE=0.D0
	
	P_NODE(1)=1.D0 *WEIGHT
	P_NODE(2)=X_NODE_NORMALIZED*WEIGHT
	P_NODE(3)=Y_NODE_NORMALIZED*WEIGHT
	P_NODE(4)=Z_NODE_NORMALIZED*WEIGHT
				
	IF (SPR_NO_COEF.EQ.10) THEN
		P_NODE(5)=X_NODE_NORMALIZED**2.*WEIGHT
		P_NODE(6)=Y_NODE_NORMALIZED**2.*WEIGHT
		P_NODE(7)=Z_NODE_NORMALIZED**2.*WEIGHT
		P_NODE(8)=X_NODE_NORMALIZED*
     &                  Y_NODE_NORMALIZED*WEIGHT
		P_NODE(9)=X_NODE_NORMALIZED*
     &                  Z_NODE_NORMALIZED*WEIGHT
		P_NODE(10)=Y_NODE_NORMALIZED*
     &                   Z_NODE_NORMALIZED*WEIGHT
	ENDIF
							    
	IF (SPR_NO_COEF.EQ.19) THEN
		P_NODE(11)=X_NODE_NORMALIZED**3.0*WEIGHT
		P_NODE(12)=Y_NODE_NORMALIZED**3.0*WEIGHT
		P_NODE(13)=Z_NODE_NORMALIZED**3.0*WEIGHT
		P_NODE(14)=X_NODE_NORMALIZED**2.0*
     &                   Y_NODE_NORMALIZED*WEIGHT
		P_NODE(15)=X_NODE_NORMALIZED*
     &                   Y_NODE_NORMALIZED**2.0*WEIGHT
		P_NODE(16)=X_NODE_NORMALIZED**2.0*
     &                   Z_NODE_NORMALIZED*WEIGHT
		P_NODE(17)=X_NODE_NORMALIZED*
     &                   Z_NODE_NORMALIZED**2.0*WEIGHT
		P_NODE(18)=Y_NODE_NORMALIZED**2.0*
     &                   Z_NODE_NORMALIZED*WEIGHT
		P_NODE(19)=Y_NODE_NORMALIZED*
     &                   Z_NODE_NORMALIZED**2.0*WEIGHT
	ENDIF 
	
	RETURN
	END
	
	
C*************************************************************
C	CALCULATING MATRICES IN SPR METHOD
C*************************************************************	
	SUBROUTINE SPR_MATRIX_CALC(P_SPR,GAUSS_VALUES,FPEL,CENTER,
     &            SPR_PATCH,SPR_PATCH_SIZE,
     &            K_SPR,NELX,NELEM_IN_PATCH,NSP_SIZE,
     &            X_MIN,X_MAX,Y_MIN,Y_MAX,Z_MIN,Z_MAX,
     &            X_NODE_NORMALIZED,Y_NODE_NORMALIZED,
     &            Z_NODE_NORMALIZED,SPR_WEIGHT,SPR_NO_COEF,NODE,
     &            NCOMPONENT)
	IMPLICIT NONE
	INTEGER::SPR_PATCH_SIZE,K_SPR,NELX,NELEM_IN_PATCH,ELEM,
     &          NSP_SIZE,SPR_NO_COEF,IELEM,NODE,NCOMPONENT
	REAL*8::X,Y,Z,X_MIN,X_MAX,Y_MIN,Y_MAX,Z_MIN,Z_MAX,
     &         X_NODE_NORMALIZED,Y_NODE_NORMALIZED,Z_NODE_NORMALIZED,
     &         DISTANCE,SPR_WT,SPR_WEIGHT 
	INTEGER::SPR_PATCH(SPR_PATCH_SIZE)
	REAL*8::FPEL(NELX,NCOMPONENT),CENTER(NELX,3),
     &         GAUSS_VALUES(NSP_SIZE,NCOMPONENT),
     &         P(SPR_NO_COEF),P_SPR(NSP_SIZE,SPR_NO_COEF)
	
	
	P_SPR=0.D0
	GAUSS_VALUES=0.D0
	
	DO IELEM=1,NELEM_IN_PATCH
		ELEM=SPR_PATCH(K_SPR+IELEM)
		
		X=CENTER(ELEM,1)
		Y=CENTER(ELEM,2)
		Z=CENTER(ELEM,3)
       
		X=-1.0+2.0*(X-X_MIN)/(X_MAX-X_MIN)
		Y=-1.0+2.0*(Y-Y_MIN)/(Y_MAX-Y_MIN)
		Z=-1.0+2.0*(Z-Z_MIN)/(Z_MAX-Z_MIN)
		
		DISTANCE=DSQRT((X-X_NODE_NORMALIZED)**2.+
     &                       (Y-Y_NODE_NORMALIZED)**2.+
     &                       (Z-Z_NODE_NORMALIZED)**2.)
		
		SPR_WT=DEXP(-1.*SPR_WEIGHT*DISTANCE)
		
		
		GAUSS_VALUES(IELEM,1:NCOMPONENT)=
     &                          FPEL(ELEM,1:NCOMPONENT)*SPR_WT
		
		CALL P_NODE_CALC(P,SPR_NO_COEF,X,Y,Z,SPR_WT)
		
		P_SPR(IELEM,1:SPR_NO_COEF)=P(1:SPR_NO_COEF)
	

	ENDDO
	
	
	RETURN
	END
	
	
	
C*************************************************************
C	SOLVING SPR RELATIONS
C*************************************************************
	SUBROUTINE SPR_SOLUTION(P_NODE,P_SPR,GAUSS_VALUES,NSP_SIZE,
     &            SPR_NO_COEF,NELEM_IN_PATCH,NODAL_VALUE,NODE,IGRAIN,
     &            NCOMPONENT)
	IMPLICIT NONE
	INTRINSIC::TRANSPOSE
	INTEGER::NSP_SIZE,SPR_NO_COEF,NELEM_IN_PATCH,I,J,NODE,IGRAIN,
     &          NCOMPONENT
	REAL*8::XNORM,RATIO
	REAL*8::P_SPR(NSP_SIZE,SPR_NO_COEF),
     &         P_SPR_T(SPR_NO_COEF,NSP_SIZE),
     &         A_SPR(SPR_NO_COEF,SPR_NO_COEF),
     &         A_SPR_INV(SPR_NO_COEF,SPR_NO_COEF),
     &         GAUSS_VALUES(NSP_SIZE,NCOMPONENT),
     &         B_SPR(SPR_NO_COEF,NCOMPONENT),
     &         COEF_SPR(SPR_NO_COEF,NCOMPONENT),P_NODE(SPR_NO_COEF),
     &         CHECK(SPR_NO_COEF,SPR_NO_COEF),NODAL_VALUE(NCOMPONENT)
	
	
	
	P_SPR_T=TRANSPOSE(P_SPR)
	
	            
       CALL MATMUL_SPR(P_SPR_T(1:SPR_NO_COEF,1:NELEM_IN_PATCH),
     & P_SPR(1:NELEM_IN_PATCH,1:SPR_NO_COEF),
     & A_SPR,SPR_NO_COEF,NELEM_IN_PATCH,SPR_NO_COEF) 
      
        
	A_SPR_INV=A_SPR
	
       CALL MATINV_SPR(A_SPR_INV,SPR_NO_COEF,SPR_NO_COEF)
	

	CALL MATMUL_SPR(A_SPR_INV,A_SPR,CHECK,SPR_NO_COEF,
     &                 SPR_NO_COEF,SPR_NO_COEF)

       XNORM=0.D0
	DO I=1,SPR_NO_COEF
		DO J=1,SPR_NO_COEF
			XNORM=XNORM+CHECK(I,J)*CHECK(I,J)
		ENDDO
	ENDDO
	RATIO=XNORM/SPR_NO_COEF
	IF (RATIO.NE.RATIO.OR.RATIO.LT.0.9.OR.RATIO.GT.1.1) THEN
		WRITE(*,*) 'ERROR IN INVERSION IN SPR'
		WRITE(*,*) 'IN PATCH FOR NODE=',NODE,'IN GRAIN=',IGRAIN
		STOP
	ENDIF
	
	
       CALL MATMUL_SPR(P_SPR_T(1:SPR_NO_COEF,1:NELEM_IN_PATCH),
     &      GAUSS_VALUES(1:NELEM_IN_PATCH,1:NCOMPONENT),B_SPR,
     &      SPR_NO_COEF,NELEM_IN_PATCH,NCOMPONENT)

       CALL MATMUL_SPR(A_SPR_INV,B_SPR,COEF_SPR,SPR_NO_COEF,
     &      SPR_NO_COEF,NCOMPONENT)
     
       NODAL_VALUE=0.D0
	DO I=1,SPR_NO_COEF
		NODAL_VALUE(1:NCOMPONENT)=NODAL_VALUE(1:NCOMPONENT)+
     &                         P_NODE(I)*COEF_SPR(I,1:NCOMPONENT)
	ENDDO

	RETURN
	END

	
	
C*************************************************************
C	REGULAR WEIGHTED AVERAGING
C**************************************************************	
	SUBROUTINE WEIGHTED_AVERAGING(NODAL_VALUE,FPEL,WATEL,IELGR,
     &            INODGR,IATEL,NATEL,NODE,NELX,NNODEX,
     &            MAXADJGRAINELEM,IGRAIN,MAXADJGRAIN,NCOMPONENT)
	IMPLICIT NONE
	INTEGER::IGRAIN,I,NODE,NNODEX,IELEM,MAXADJGRAINELEM,NELX,
     &          IG,MAXADJGRAIN,NCOMPONENT
	REAL*8::WEIGHT_TOTAL
	INTEGER::NATEL(NNODEX),IATEL(NNODEX,MAXADJGRAINELEM),
     &          IELGR(NELX),INODGR(NNODEX,MAXADJGRAIN)
	REAL*8::FPEL(NELX,NCOMPONENT),WATEL(NNODEX,MAXADJGRAINELEM),
     &         NODAL_VALUE(NCOMPONENT)
	
	
	
	NODAL_VALUE=0.D0
	WEIGHT_TOTAL=0.D0
	IG=INODGR(NODE,IGRAIN)
	
	DO I=1,NATEL(NODE)
		IELEM=IATEL(NODE,I)
		IF(IELGR(IELEM).EQ.IG) THEN			
			NODAL_VALUE(1:NCOMPONENT)=NODAL_VALUE(1:NCOMPONENT)+
     &                     FPEL(IELEM,1:NCOMPONENT)*WATEL(NODE,I)
			WEIGHT_TOTAL=WEIGHT_TOTAL+WATEL(NODE,I)
		ENDIF
	ENDDO
	
	NODAL_VALUE=NODAL_VALUE/WEIGHT_TOTAL
	
	RETURN
	END
	
C*************************************************************
      SUBROUTINE MATMUL_SPR(AA,BB,CC,N,M,L)
      
C      INCLUDE 'ABA_PARAM.INC'
       IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C   THIS SUBROUTINE RETURNS MARTIX [CC] = MATRIX [AA] * MATRIX [BB]
C   N=NO. OF ROWS OF [AA]      =NO. OF ROWS OF [CC]
C   M=NO. OF COLUMNS OF [AA]   =NO. OF ROWS OF [BB]
C   L=NO. OF COLUMNS OF [BB]   =NO. OF COLUMNS OF [CC]

       DIMENSION AA(N,M),BB(M,L),CC(N,L)

       DO 10 I=1,N
       DO 10 J=1,L
         CC(I,J)=0.0
       DO 10 K=1,M
   10    CC(I,J)=CC(I,J)+AA(I,K)*BB(K,J)
       RETURN 
       END       
	

C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--       
      SUBROUTINE MATINV_SPR(A,N,ID)                                         
C     ******************************************************************
C     THIS SUBROUTINE COMPUTES THE INVERSE AND DETERMINANT OF MATRIX A *
C     OF ORDER N,BY THE GAUSS-JORDAN METHOD, A-INVERSE REPLACES A ,AND *
C     THE DETERMINANT OF A IS PLACED IN DETERM. IF M=1 THE VECTOR B    *
C     CONTANS THE CONSTANT VECTOR WHEN MATINV IS CALLED,AND THIS IS    *
C     REPLACED WITH THE SOLUTION VECTOR IF M=0,NO SIMULTANEOUS         *
C     EQUATION SOLUTION IS CALLED FOR, AND B IS NOT PERTINENT. N IS NOT*
C     TO EXCEED 100.                                                   *
C      A--IS THE MATRIX OF COEFFICIENTS OR THE MATRIX TO BE INVERTED.  *
C      A CONTANS A-INVERSE AFTER EXECUTION.                            *
C      N-- IS THE ORDER OF THE SQUARE MATRIX, A.                       *
C      B--IS THE MATRIX CONTANING COLUMN VECTORS OF CONSTANTS (EACH    *
C         COLUMN VECTOR IS ASSOCIATED WITH A IN THE FOLLOWING          *
C         MANNER--AX=B.).                                              *
C      M--IS THE NUMBER OF CRITERION VECTORS (COLUMN VECTORS OF        *
C         SIMULTANEOUS SOLUTIONS) TO BE CALCULATED AND STORED IN B.    *
C      M=0 RESULTS IN THE COMPUTATION OF ONLY THE INVERSE AND          *
C          DETERMINANT OF A.                                           *
C      DETERM--CONTANS THE VALUE OF THE DETERMINANT AFTER EXECUTION.   *
C     ******************************************************************
C     
       IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C      INCLUDE 'ABA_PARAM.INC'
C
      DIMENSION IPIVOT(1000),INDEX(1000,2),PIVOT(1000)                  
      DIMENSION A(ID,ID),B(500,1)                                     

C     INITIALIZATION                                                    
C      DETERM=1.0E0                                                      
C     SEARCH FOR PIVOT ELEMENT                                          
      DO 30 J=1,N                                                       
   30 IPIVOT (J)=0                                                      
      DO 470 I=1,N                                                      
      AMAX=0.0E0                                                        
      DO 150 J=1,N                                                      
      IF(IPIVOT(J).EQ.1) GO TO 150                                      
      DO 140 K=1,N                                                      
      IF(IPIVOT(K).GT.1) GO TO 590                                      
      IF(IPIVOT(K).EQ.1) GO TO 140                                      
      IF(DABS(AMAX).GE.DABS(A(J,K))) GO TO 140                          
      IROW=J                                                            
      ICOLUM=K                                                          
      AMAX=A(J,K)                                                       
  140 CONTINUE                                                          
  150 CONTINUE
	
	IF (ICOLUM.EQ.0) STOP'ICOLUM=ZERO IN MATINV'
	
      IPIVOT(ICOLUM)=IPIVOT(ICOLUM)+1                                   
C     INTERCHANGE ROWS TO PUT PIVOT ELEMENT ON DIAGONAL                 
      IF(IROW.EQ.ICOLUM) GO TO 280                                      
C      DETERM=-DETERM                                                    
      DO 220 L=1,N                                                      
      SWAP=A(IROW,L)                                                    
      A(IROW,L)=A(ICOLUM,L)                                             
  220 A(ICOLUM,L)=SWAP                                                  
C      DO 270 L=1,M                                                      
C      SWAP=B(IROW,L)                                                    
C      B(IROW,L)=B(ICOLUM,L)                                             
C  270 B(ICOLUM,L)=SWAP                                                  
  280 INDEX(I,1)=IROW                                                   
      INDEX(I,2)=ICOLUM                                                 
      PIVOT(I)=A(ICOLUM,ICOLUM)                                         
C      DETERM=DETERM*PIVOT(I)                                            
C     DIVIDE PIVOT ROW BY PIVOT ELEMENT                                 
      A(ICOLUM,ICOLUM)=1.0E0                                            
      DO 340 L=1,N                                                      
  340 A(ICOLUM,L)=A(ICOLUM,L)/PIVOT(I)                                  
C      IF(M.LE.0) GO TO 380                                              
C      DO 370 L=1,M                                                      
C  370 B(ICOLUM,L)=B(ICOLUM,L)/PIVOT(I)                                  
C     REDUCE NON-PIVOT ROWS                                             
  380 DO 470 L1=1,N                                                     
      IF(L1.EQ.ICOLUM) GO TO 470                                        
      T=A(L1,ICOLUM)                                                    
      A(L1,ICOLUM)=0.0E0                                                
      DO 430 L=1,N                                                      
  430 A(L1,L)=A(L1,L)-A(ICOLUM,L)*T                                     
C      IF(M.LE.0) GO TO 470                                              
C      DO 460 L=1,M                                                      
C  460 B(L1,L)=B(L1,L)-B(ICOLUM,L)*T                                     
  470 CONTINUE                                                          
C     INTERCHANGE COLUMNS                                               
      DO 580 I=1,N                                                      
      L=N+1-I                                                           
      IF(INDEX(L,1).EQ.INDEX(L,2)) GO TO 580                            
      JROW=INDEX(L,1)                                                   
      JCOLUM=INDEX(L,2)                                                 
      DO 570 K=1,N                                                      
      SWAP=A(K,JROW)                                                    
      A(K,JROW)=A(K,JCOLUM)                                             
      A(K,JCOLUM)=SWAP   
C       WRITE(115,*) K,JCOLUM,A(K,JCOLUM)                                               
  570 CONTINUE                                                          
  580 CONTINUE                                                          
  590 RETURN                                                            
      END 
	
C*************************************************************
      subroutine calc_det(a,det)
      implicit double precision (a-h,o-z)
      dimension a(3,3)
c
      det=(a(1,1)*a(2,2)*a(3,3)-a(1,1)*a(2,3)*a(3,2)-a(2,1)*a(1,2)
     &     *a(3,3)+a(2,1)*a(1,3)*a(3,2)+a(3,1)*a(1,2)*a(2,3)-a(3,1)
     &     *a(1,3)*a(2,2))

      return
      end

C----------------------------------------------------
C------CALCULATES THE INVERSE OF A 3*3 MATRIX--------
C----------------------------------------------------
      
      SUBROUTINE matinv3(a,ai,det)

      implicit integer*4 (i-n)
      implicit double precision (a-h,o-z)
      
      dimension a(3,3), ai(3,3)
c     
      det=(a(1,1)*a(2,2)*a(3,3)-a(1,1)*a(2,3)*a(3,2)-a(2,1)*a(1,2)
     &     *a(3,3)+a(2,1)*a(1,3)*a(3,2)+a(3,1)*a(1,2)*a(2,3)-a(3,1)
     &     *a(1,3)*a(2,2))
      if (det.eq.0.0d0) then    !test
         write (*,*) 'det=',det   
         stop
      end if
      ai(1,1) =  ( a(2,2)*a(3,3)-a(2,3)*a(3,2))/det
      ai(1,2) = -( a(1,2)*a(3,3)-a(1,3)*a(3,2))/det
      ai(1,3) = -(-a(1,2)*a(2,3)+a(1,3)*a(2,2))/det
      ai(2,1) = -( a(2,1)*a(3,3)-a(2,3)*a(3,1))/det
      ai(2,2) =  ( a(1,1)*a(3,3)-a(1,3)*a(3,1))/det
      ai(2,3) = -( a(1,1)*a(2,3)-a(1,3)*a(2,1))/det
      ai(3,1) =  ( a(2,1)*a(3,2)-a(2,2)*a(3,1))/det
      ai(3,2) = -( a(1,1)*a(3,2)-a(1,2)*a(3,1))/det
      ai(3,3) =  ( a(1,1)*a(2,2)-a(1,2)*a(2,1))/det
      return
      end      
      
      
C*********************************************************************     
      SUBROUTINE GND_CALCULATE(AVERAGE_GND,CURL_FP,EULER,NGRAINS,
     &                         IGRAIN)
      IMPLICIT NONE
	INTRINSIC::TRANSPOSE,MATMUL
	INTEGER::NGRAINS,IGRAIN,I
	REAL*8::AVERAGE_GND
	REAL*8::CURL_FP(3,3),GND_PER_SYSTEM(30),
     &         EULER(NGRAINS,3),EUL(3),TR(3,3),TR_T(3,3),DUM(3,3),
     &         CURL_FP_VECTOR(9,1),B_MAT(63,9),GND_VECTOR(63,1),
     &         GND_SCREW(30),GND_ET(30),GND_EN(30)


C**************** TRANSFERRING CURLFP FROM GLOBAL COORD SYSTEM TO CRYSTAL COORD SYSTEM
	EUL(1:3)=EULER(IGRAIN,1:3)
      	CALL EULER_SLIP(EUL,TR)
	TR_T=TRANSPOSE(TR)
	DUM=MATMUL(TR_T,CURL_FP)
	CURL_FP=MATMUL(DUM,TR)
	
	CURL_FP_VECTOR=0.D0
	CURL_FP_VECTOR(1,1)=CURL_FP(1,1)
	CURL_FP_VECTOR(2,1)=CURL_FP(1,2)
	CURL_FP_VECTOR(3,1)=CURL_FP(1,3)
	CURL_FP_VECTOR(4,1)=CURL_FP(2,1)
	CURL_FP_VECTOR(5,1)=CURL_FP(2,2)
	CURL_FP_VECTOR(6,1)=CURL_FP(2,3)
	CURL_FP_VECTOR(7,1)=CURL_FP(3,1)
	CURL_FP_VECTOR(8,1)=CURL_FP(3,2)
	CURL_FP_VECTOR(9,1)=CURL_FP(3,3)
	
	CALL B_MATRIX(B_MAT)
	
	CALL MATMULX(B_MAT,CURL_FP_VECTOR,GND_VECTOR,63,9,1)
	
	
C*************************************************************
C     GND DENSITY FOR SOME SLIP SYSTEMS ARE EQUAL, INCLUDING
C     				SCREW GND: 
C				 1=6=9=12
C				 2=5=8=11
C				 3=4=7=10
C				 13=24=30
C				 14=19=25
C				 15=20=26
C				 16=21=27
C				 17=22=28
C				 18=23=29

C    DENSITIES FOR 6 EDGE_T GNDS ARE EQUAL TO 6 EDGE_N GNDS
C               EDGE_T GND         EDGE_N GND
C                   1        =         6
C                   2        =         5
C                   3        =         4
C                   4        =         3
C                   5        =         2
C                   6        =         1

C    GND DENSITIES ARE EQUALLY DISTRIBUTED BETWEEN THESE SYSTEMS

	GND_SCREW(1)=GND_VECTOR(1,1)/4.D0
	GND_SCREW(2)=GND_VECTOR(2,1)/4.D0
	GND_SCREW(3)=GND_VECTOR(3,1)/4.D0
	GND_SCREW(4)=GND_SCREW(3)	
	GND_SCREW(5)=GND_SCREW(2)	
	GND_SCREW(6)=GND_SCREW(1)
	GND_SCREW(7)=GND_SCREW(3)
	GND_SCREW(8)=GND_SCREW(2)
	GND_SCREW(9)=GND_SCREW(1)
	GND_SCREW(10)=GND_SCREW(3)
	GND_SCREW(11)=GND_SCREW(2)	
	GND_SCREW(12)=GND_SCREW(1)
	GND_SCREW(13)=GND_VECTOR(4,1)/3.D0
	GND_SCREW(14)=GND_VECTOR(5,1)/3.D0	
	GND_SCREW(15)=GND_VECTOR(6,1)/3.D0
	GND_SCREW(16)=GND_VECTOR(7,1)/3.D0
	GND_SCREW(17)=GND_VECTOR(8,1)/3.D0
	GND_SCREW(18)=GND_VECTOR(9,1)/3.D0
	GND_SCREW(19)=GND_SCREW(14)
	GND_SCREW(20)=GND_SCREW(15)
	GND_SCREW(21)=GND_SCREW(16)
	GND_SCREW(22)=GND_SCREW(17)
	GND_SCREW(23)=GND_SCREW(18)
	GND_SCREW(24)=GND_SCREW(13)
	GND_SCREW(25)=GND_SCREW(14)
	GND_SCREW(26)=GND_SCREW(15)
	GND_SCREW(27)=GND_SCREW(16)
	GND_SCREW(28)=GND_SCREW(17)
	GND_SCREW(29)=GND_SCREW(18)
	GND_SCREW(30)=GND_SCREW(13)
	
	GND_EN(7:30)=GND_VECTOR(10:33,1)
	GND_EN(6)=GND_VECTOR(34,1)/2.D0
	GND_EN(5)=GND_VECTOR(35,1)/2.D0
	GND_EN(4)=GND_VECTOR(36,1)/2.D0
	GND_EN(3)=GND_VECTOR(37,1)/2.D0
	GND_EN(2)=GND_VECTOR(38,1)/2.D0
	GND_EN(1)=GND_VECTOR(39,1)/2.D0
	
	
	GND_ET(1)=GND_EN(6)
	GND_ET(2)=GND_EN(5)
	GND_ET(3)=GND_EN(4)
	GND_ET(4)=GND_EN(3)
	GND_ET(5)=GND_EN(2)
	GND_ET(6)=GND_EN(1)
	GND_ET(7:30)=GND_VECTOR(40:63,1)
	
	
	
	DO I=1,30
		GND_PER_SYSTEM(I)=DSQRT(GND_SCREW(I)*GND_SCREW(I)+
     &                                GND_EN(I)*GND_EN(I)+
     &                                GND_ET(I)*GND_ET(I))
	ENDDO
	
	AVERAGE_GND=0
	DO I=1,30
		AVERAGE_GND=AVERAGE_GND+GND_PER_SYSTEM(I)
	ENDDO
	
	AVERAGE_GND=AVERAGE_GND/30.D0

      RETURN

      END
      
      
C*************************************************************
C*************************************************************
C*************************************************************
C*************************************************************      
	SUBROUTINE B_MATRIX(B)
	IMPLICIT DOUBLE PRECISION (A-H,O-Z)
	DIMENSION B(63,9)
	
	
       B (  1 ,  1 ) =    1.128880E-01
	B (  1 ,  2 ) =   -8.166213E-02
	B (  1 ,  3 ) =    0.000000E+00
	B (  1 ,  4 ) =   -8.166213E-02
	B (  1 ,  5 ) =    2.071833E-01
	B (  1 ,  6 ) =    0.000000E+00
	B (  1 ,  7 ) =    0.000000E+00
	B (  1 ,  8 ) =    0.000000E+00
	B (  1 ,  9 ) =    5.776589E-02
	B (  2 ,  1 ) =    1.128880E-01
	B (  2 ,  2 ) =    8.166213E-02
	B (  2 ,  3 ) =    0.000000E+00
	B (  2 ,  4 ) =    8.166213E-02
	B (  2 ,  5 ) =    2.071833E-01
	B (  2 ,  6 ) =    0.000000E+00
	B (  2 ,  7 ) =    0.000000E+00
	B (  2 ,  8 ) =    0.000000E+00
	B (  2 ,  9 ) =    5.776589E-02
	B (  3 ,  1 ) =    2.543310E-01
	B (  3 ,  2 ) =    0.000000E+00
	B (  3 ,  3 ) =    0.000000E+00
	B (  3 ,  4 ) =    0.000000E+00
	B (  3 ,  5 ) =    6.574036E-02
	B (  3 ,  6 ) =    0.000000E+00
	B (  3 ,  7 ) =    0.000000E+00
	B (  3 ,  8 ) =    0.000000E+00
	B (  3 ,  9 ) =    5.776589E-02
	B (  4 ,  1 ) =    7.333343E-02
	B (  4 ,  2 ) =    2.306295E-02
	B (  4 ,  3 ) =    4.359713E-02
	B (  4 ,  4 ) =    2.306295E-02
	B (  4 ,  5 ) =    9.996424E-02
	B (  4 ,  6 ) =    7.551245E-02
	B (  4 ,  7 ) =    2.376822E-02
	B (  4 ,  8 ) =    4.116776E-02
	B (  4 ,  9 ) =    1.377837E-01
	B (  5 ,  1 ) =    7.333343E-02
	B (  5 ,  2 ) =   -2.306295E-02
	B (  5 ,  3 ) =   -4.359713E-02
	B (  5 ,  4 ) =   -2.306295E-02
	B (  5 ,  5 ) =    9.996424E-02
	B (  5 ,  6 ) =    7.551245E-02
	B (  5 ,  7 ) =   -2.376822E-02
	B (  5 ,  8 ) =    4.116776E-02
	B (  5 ,  9 ) =    1.377837E-01
	B (  6 ,  1 ) =    1.132796E-01
	B (  6 ,  2 ) =    0.000000E+00
	B (  6 ,  3 ) =   -8.719426E-02
	B (  6 ,  4 ) =    0.000000E+00
	B (  6 ,  5 ) =    6.001803E-02
	B (  6 ,  6 ) =    0.000000E+00
	B (  6 ,  7 ) =   -4.753643E-02
	B (  6 ,  8 ) =    0.000000E+00
	B (  6 ,  9 ) =    1.377837E-01
	B (  7 ,  1 ) =    7.333343E-02
	B (  7 ,  2 ) =    2.306295E-02
	B (  7 ,  3 ) =   -4.359713E-02
	B (  7 ,  4 ) =    2.306295E-02
	B (  7 ,  5 ) =    9.996424E-02
	B (  7 ,  6 ) =   -7.551245E-02
	B (  7 ,  7 ) =   -2.376822E-02
	B (  7 ,  8 ) =   -4.116776E-02
	B (  7 ,  9 ) =    1.377837E-01
	B (  8 ,  1 ) =    7.333343E-02
	B (  8 ,  2 ) =   -2.306295E-02
	B (  8 ,  3 ) =    4.359713E-02
	B (  8 ,  4 ) =   -2.306295E-02
	B (  8 ,  5 ) =    9.996424E-02
	B (  8 ,  6 ) =   -7.551245E-02
	B (  8 ,  7 ) =    2.376822E-02
	B (  8 ,  8 ) =   -4.116776E-02
	B (  8 ,  9 ) =    1.377837E-01
	B (  9 ,  1 ) =    1.132796E-01
	B (  9 ,  2 ) =    0.000000E+00
	B (  9 ,  3 ) =    8.719426E-02
	B (  9 ,  4 ) =    0.000000E+00
	B (  9 ,  5 ) =    6.001803E-02
	B (  9 ,  6 ) =    0.000000E+00
	B (  9 ,  7 ) =    4.753643E-02
	B (  9 ,  8 ) =    0.000000E+00
	B (  9 ,  9 ) =    1.377837E-01
	B ( 10 ,  1 ) =    0.000000E+00
	B ( 10 ,  2 ) =   -1.452474E-01
	B ( 10 ,  3 ) =    8.376144E-02
	B ( 10 ,  4 ) =   -2.046507E-02
	B ( 10 ,  5 ) =    0.000000E+00
	B ( 10 ,  6 ) =    0.000000E+00
	B ( 10 ,  7 ) =    8.704741E-03
	B ( 10 ,  8 ) =    0.000000E+00
	B ( 10 ,  9 ) =    0.000000E+00
	B ( 11 ,  1 ) =    7.175562E-02
	B ( 11 ,  2 ) =   -2.096306E-02
	B ( 11 ,  3 ) =    4.188072E-02
	B ( 11 ,  4 ) =    1.038193E-01
	B ( 11 ,  5 ) =   -7.175562E-02
	B ( 11 ,  6 ) =    7.253953E-02
	B ( 11 ,  7 ) =    4.352370E-03
	B ( 11 ,  8 ) =    7.538527E-03
	B ( 11 ,  9 ) =    0.000000E+00
	B ( 12 ,  1 ) =   -7.175562E-02
	B ( 12 ,  2 ) =   -2.096306E-02
	B ( 12 ,  3 ) =   -4.188072E-02
	B ( 12 ,  4 ) =    1.038193E-01
	B ( 12 ,  5 ) =    7.175562E-02
	B ( 12 ,  6 ) =    7.253953E-02
	B ( 12 ,  7 ) =   -4.352370E-03
	B ( 12 ,  8 ) =    7.538527E-03
	B ( 12 ,  9 ) =    0.000000E+00
	B ( 13 ,  1 ) =    0.000000E+00
	B ( 13 ,  2 ) =   -1.452474E-01
	B ( 13 ,  3 ) =   -8.376144E-02
	B ( 13 ,  4 ) =   -2.046507E-02
	B ( 13 ,  5 ) =    0.000000E+00
	B ( 13 ,  6 ) =    0.000000E+00
	B ( 13 ,  7 ) =   -8.704741E-03
	B ( 13 ,  8 ) =    0.000000E+00
	B ( 13 ,  9 ) =    0.000000E+00
	B ( 14 ,  1 ) =    7.175562E-02
	B ( 14 ,  2 ) =   -2.096306E-02
	B ( 14 ,  3 ) =   -4.188072E-02
	B ( 14 ,  4 ) =    1.038193E-01
	B ( 14 ,  5 ) =   -7.175562E-02
	B ( 14 ,  6 ) =   -7.253953E-02
	B ( 14 ,  7 ) =   -4.352370E-03
	B ( 14 ,  8 ) =   -7.538527E-03
	B ( 14 ,  9 ) =    0.000000E+00
	B ( 15 ,  1 ) =   -7.175562E-02
	B ( 15 ,  2 ) =   -2.096306E-02
	B ( 15 ,  3 ) =    4.188072E-02
	B ( 15 ,  4 ) =    1.038193E-01
	B ( 15 ,  5 ) =    7.175562E-02
	B ( 15 ,  6 ) =   -7.253953E-02
	B ( 15 ,  7 ) =    4.352370E-03
	B ( 15 ,  8 ) =   -7.538527E-03
	B ( 15 ,  9 ) =    0.000000E+00
	B ( 16 ,  1 ) =   -3.224887E-03
	B ( 16 ,  2 ) =   -3.859451E-02
	B ( 16 ,  3 ) =    2.225672E-02
	B ( 16 ,  4 ) =   -5.437889E-03
	B ( 16 ,  5 ) =   -7.949124E-02
	B ( 16 ,  6 ) =    2.497762E-02
	B ( 16 ,  7 ) =    2.312986E-03
	B ( 16 ,  8 ) =   -6.102021E-02
	B ( 16 ,  9 ) =    4.509505E-02
	B ( 17 ,  1 ) =   -4.135806E-02
	B ( 17 ,  2 ) =    2.745409E-02
	B ( 17 ,  3 ) =   -1.050289E-02
	B ( 17 ,  4 ) =    6.061071E-02
	B ( 17 ,  5 ) =   -4.135806E-02
	B ( 17 ,  6 ) =    3.176370E-02
	B ( 17 ,  7 ) =    5.400154E-02
	B ( 17 ,  8 ) =   -2.850700E-02
	B ( 17 ,  9 ) =    4.509505E-02
	B ( 18 ,  1 ) =   -7.949124E-02
	B ( 18 ,  2 ) =   -3.859451E-02
	B ( 18 ,  3 ) =   -3.275961E-02
	B ( 18 ,  4 ) =   -5.437889E-03
	B ( 18 ,  5 ) =   -3.224887E-03
	B ( 18 ,  6 ) =    6.786080E-03
	B ( 18 ,  7 ) =    5.168856E-02
	B ( 18 ,  8 ) =    3.251321E-02
	B ( 18 ,  9 ) =    4.509505E-02
	B ( 19 ,  1 ) =   -3.224887E-03
	B ( 19 ,  2 ) =   -3.859451E-02
	B ( 19 ,  3 ) =   -2.225672E-02
	B ( 19 ,  4 ) =   -5.437889E-03
	B ( 19 ,  5 ) =   -7.949124E-02
	B ( 19 ,  6 ) =   -2.497762E-02
	B ( 19 ,  7 ) =   -2.312986E-03
	B ( 19 ,  8 ) =    6.102021E-02
	B ( 19 ,  9 ) =    4.509505E-02
	B ( 20 ,  1 ) =   -4.135806E-02
	B ( 20 ,  2 ) =    2.745409E-02
	B ( 20 ,  3 ) =    1.050289E-02
	B ( 20 ,  4 ) =    6.061071E-02
	B ( 20 ,  5 ) =   -4.135806E-02
	B ( 20 ,  6 ) =   -3.176370E-02
	B ( 20 ,  7 ) =   -5.400154E-02
	B ( 20 ,  8 ) =    2.850700E-02
	B ( 20 ,  9 ) =    4.509505E-02
	B ( 21 ,  1 ) =   -7.949124E-02
	B ( 21 ,  2 ) =   -3.859451E-02
	B ( 21 ,  3 ) =    3.275961E-02
	B ( 21 ,  4 ) =   -5.437889E-03
	B ( 21 ,  5 ) =   -3.224887E-03
	B ( 21 ,  6 ) =   -6.786080E-03
	B ( 21 ,  7 ) =   -5.168856E-02
	B ( 21 ,  8 ) =   -3.251321E-02
	B ( 21 ,  9 ) =    4.509505E-02
	B ( 22 ,  1 ) =   -3.224887E-03
	B ( 22 ,  2 ) =    3.859451E-02
	B ( 22 ,  3 ) =   -2.225672E-02
	B ( 22 ,  4 ) =    5.437889E-03
	B ( 22 ,  5 ) =   -7.949124E-02
	B ( 22 ,  6 ) =    2.497762E-02
	B ( 22 ,  7 ) =   -2.312986E-03
	B ( 22 ,  8 ) =   -6.102021E-02
	B ( 22 ,  9 ) =    4.509505E-02
	B ( 23 ,  1 ) =   -7.949124E-02
	B ( 23 ,  2 ) =    3.859451E-02
	B ( 23 ,  3 ) =   -3.275961E-02
	B ( 23 ,  4 ) =    5.437889E-03
	B ( 23 ,  5 ) =   -3.224887E-03
	B ( 23 ,  6 ) =   -6.786080E-03
	B ( 23 ,  7 ) =    5.168856E-02
	B ( 23 ,  8 ) =   -3.251321E-02
	B ( 23 ,  9 ) =    4.509505E-02
	B ( 24 ,  1 ) =   -4.135806E-02
	B ( 24 ,  2 ) =   -2.745409E-02
	B ( 24 ,  3 ) =   -1.050289E-02
	B ( 24 ,  4 ) =   -6.061071E-02
	B ( 24 ,  5 ) =   -4.135806E-02
	B ( 24 ,  6 ) =   -3.176370E-02
	B ( 24 ,  7 ) =    5.400154E-02
	B ( 24 ,  8 ) =    2.850700E-02
	B ( 24 ,  9 ) =    4.509505E-02
	B ( 25 ,  1 ) =   -3.224887E-03
	B ( 25 ,  2 ) =    3.859451E-02
	B ( 25 ,  3 ) =    2.225672E-02
	B ( 25 ,  4 ) =    5.437889E-03
	B ( 25 ,  5 ) =   -7.949124E-02
	B ( 25 ,  6 ) =   -2.497762E-02
	B ( 25 ,  7 ) =    2.312986E-03
	B ( 25 ,  8 ) =    6.102021E-02
	B ( 25 ,  9 ) =    4.509505E-02
	B ( 26 ,  1 ) =   -7.949124E-02
	B ( 26 ,  2 ) =    3.859451E-02
	B ( 26 ,  3 ) =    3.275961E-02
	B ( 26 ,  4 ) =    5.437889E-03
	B ( 26 ,  5 ) =   -3.224887E-03
	B ( 26 ,  6 ) =    6.786080E-03
	B ( 26 ,  7 ) =   -5.168856E-02
	B ( 26 ,  8 ) =    3.251321E-02
	B ( 26 ,  9 ) =    4.509505E-02
	B ( 27 ,  1 ) =   -4.135806E-02
	B ( 27 ,  2 ) =   -2.745409E-02
	B ( 27 ,  3 ) =    1.050289E-02
	B ( 27 ,  4 ) =   -6.061071E-02
	B ( 27 ,  5 ) =   -4.135806E-02
	B ( 27 ,  6 ) =    3.176370E-02
	B ( 27 ,  7 ) =   -5.400154E-02
	B ( 27 ,  8 ) =   -2.850700E-02
	B ( 27 ,  9 ) =    4.509505E-02
	B ( 28 ,  1 ) =   -2.481466E-02
	B ( 28 ,  2 ) =    3.676235E-02
	B ( 28 ,  3 ) =   -1.823385E-02
	B ( 28 ,  4 ) =    3.676235E-02
	B ( 28 ,  5 ) =   -6.726416E-02
	B ( 28 ,  6 ) =    3.158195E-02
	B ( 28 ,  7 ) =    2.876961E-02
	B ( 28 ,  8 ) =   -4.983043E-02
	B ( 28 ,  9 ) =    5.019939E-02
	B ( 29 ,  1 ) =   -8.848891E-02
	B ( 29 ,  2 ) =    0.000000E+00
	B ( 29 ,  3 ) =   -3.646770E-02
	B ( 29 ,  4 ) =    0.000000E+00
	B ( 29 ,  5 ) =   -3.589914E-03
	B ( 29 ,  6 ) =    0.000000E+00
	B ( 29 ,  7 ) =    5.753922E-02
	B ( 29 ,  8 ) =    0.000000E+00
	B ( 29 ,  9 ) =    5.019939E-02
	B ( 30 ,  1 ) =   -2.481466E-02
	B ( 30 ,  2 ) =   -3.676235E-02
	B ( 30 ,  3 ) =   -1.823385E-02
	B ( 30 ,  4 ) =   -3.676235E-02
	B ( 30 ,  5 ) =   -6.726416E-02
	B ( 30 ,  6 ) =   -3.158195E-02
	B ( 30 ,  7 ) =    2.876961E-02
	B ( 30 ,  8 ) =    4.983043E-02
	B ( 30 ,  9 ) =    5.019939E-02
	B ( 31 ,  1 ) =   -2.481466E-02
	B ( 31 ,  2 ) =    3.676235E-02
	B ( 31 ,  3 ) =    1.823385E-02
	B ( 31 ,  4 ) =    3.676235E-02
	B ( 31 ,  5 ) =   -6.726416E-02
	B ( 31 ,  6 ) =   -3.158195E-02
	B ( 31 ,  7 ) =   -2.876961E-02
	B ( 31 ,  8 ) =    4.983043E-02
	B ( 31 ,  9 ) =    5.019939E-02
	B ( 32 ,  1 ) =   -8.848891E-02
	B ( 32 ,  2 ) =    0.000000E+00
	B ( 32 ,  3 ) =    3.646770E-02
	B ( 32 ,  4 ) =    0.000000E+00
	B ( 32 ,  5 ) =   -3.589914E-03
	B ( 32 ,  6 ) =    0.000000E+00
	B ( 32 ,  7 ) =   -5.753922E-02
	B ( 32 ,  8 ) =    0.000000E+00
	B ( 32 ,  9 ) =    5.019939E-02
	B ( 33 ,  1 ) =   -2.481466E-02
	B ( 33 ,  2 ) =   -3.676235E-02
	B ( 33 ,  3 ) =    1.823385E-02
	B ( 33 ,  4 ) =   -3.676235E-02
	B ( 33 ,  5 ) =   -6.726416E-02
	B ( 33 ,  6 ) =    3.158195E-02
	B ( 33 ,  7 ) =   -2.876961E-02
	B ( 33 ,  8 ) =   -4.983043E-02
	B ( 33 ,  9 ) =    5.019939E-02
	B ( 34 ,  1 ) =    8.166213E-02
	B ( 34 ,  2 ) =    2.385720E-02
	B ( 34 ,  3 ) =    0.000000E+00
	B ( 34 ,  4 ) =   -1.181525E-01
	B ( 34 ,  5 ) =   -8.166213E-02
	B ( 34 ,  6 ) =    0.000000E+00
	B ( 34 ,  7 ) =    0.000000E+00
	B ( 34 ,  8 ) =    0.000000E+00
	B ( 34 ,  9 ) =    0.000000E+00
	B ( 35 ,  1 ) =   -8.166213E-02
	B ( 35 ,  2 ) =    2.385720E-02
	B ( 35 ,  3 ) =    0.000000E+00
	B ( 35 ,  4 ) =   -1.181525E-01
	B ( 35 ,  5 ) =    8.166213E-02
	B ( 35 ,  6 ) =    0.000000E+00
	B ( 35 ,  7 ) =    0.000000E+00
	B ( 35 ,  8 ) =    0.000000E+00
	B ( 35 ,  9 ) =    0.000000E+00
	B ( 36 ,  1 ) =    0.000000E+00
	B ( 36 ,  2 ) =    1.653002E-01
	B ( 36 ,  3 ) =    0.000000E+00
	B ( 36 ,  4 ) =    2.329046E-02
	B ( 36 ,  5 ) =    0.000000E+00
	B ( 36 ,  6 ) =    0.000000E+00
	B ( 36 ,  7 ) =    0.000000E+00
	B ( 36 ,  8 ) =    0.000000E+00
	B ( 36 ,  9 ) =    0.000000E+00
	B ( 37 ,  1 ) =    0.000000E+00
	B ( 37 ,  2 ) =    0.000000E+00
	B ( 37 ,  3 ) =   -1.754553E-01
	B ( 37 ,  4 ) =    0.000000E+00
	B ( 37 ,  5 ) =    0.000000E+00
	B ( 37 ,  6 ) =    0.000000E+00
	B ( 37 ,  7 ) =   -1.823385E-02
	B ( 37 ,  8 ) =    0.000000E+00
	B ( 37 ,  9 ) =    0.000000E+00
	B ( 38 ,  1 ) =    0.000000E+00
	B ( 38 ,  2 ) =    0.000000E+00
	B ( 38 ,  3 ) =   -8.772767E-02
	B ( 38 ,  4 ) =    0.000000E+00
	B ( 38 ,  5 ) =    0.000000E+00
	B ( 38 ,  6 ) =   -1.519488E-01
	B ( 38 ,  7 ) =   -9.116924E-03
	B ( 38 ,  8 ) =   -1.579098E-02
	B ( 38 ,  9 ) =    0.000000E+00
	B ( 39 ,  1 ) =    0.000000E+00
	B ( 39 ,  2 ) =    0.000000E+00
	B ( 39 ,  3 ) =    8.772767E-02
	B ( 39 ,  4 ) =    0.000000E+00
	B ( 39 ,  5 ) =    0.000000E+00
	B ( 39 ,  6 ) =   -1.519488E-01
	B ( 39 ,  7 ) =    9.116924E-03
	B ( 39 ,  8 ) =   -1.579098E-02
	B ( 39 ,  9 ) =    0.000000E+00
	B ( 40 ,  1 ) =    0.000000E+00
	B ( 40 ,  2 ) =    7.891340E-02
	B ( 40 ,  3 ) =    1.541707E-01
	B ( 40 ,  4 ) =    1.111874E-02
	B ( 40 ,  5 ) =    0.000000E+00
	B ( 40 ,  6 ) =    0.000000E+00
	B ( 40 ,  7 ) =    1.602188E-02
	B ( 40 ,  8 ) =    0.000000E+00
	B ( 40 ,  9 ) =    0.000000E+00
	B ( 41 ,  1 ) =   -3.898506E-02
	B ( 41 ,  2 ) =    1.138930E-02
	B ( 41 ,  3 ) =    7.708534E-02
	B ( 41 ,  4 ) =   -5.640537E-02
	B ( 41 ,  5 ) =    3.898506E-02
	B ( 41 ,  6 ) =    1.335157E-01
	B ( 41 ,  7 ) =    8.010941E-03
	B ( 41 ,  8 ) =    1.387536E-02
	B ( 41 ,  9 ) =    0.000000E+00
	B ( 42 ,  1 ) =    3.898506E-02
	B ( 42 ,  2 ) =    1.138930E-02
	B ( 42 ,  3 ) =   -7.708534E-02
	B ( 42 ,  4 ) =   -5.640537E-02
	B ( 42 ,  5 ) =   -3.898506E-02
	B ( 42 ,  6 ) =    1.335157E-01
	B ( 42 ,  7 ) =   -8.010941E-03
	B ( 42 ,  8 ) =    1.387536E-02
	B ( 42 ,  9 ) =    0.000000E+00
	B ( 43 ,  1 ) =    0.000000E+00
	B ( 43 ,  2 ) =    7.891340E-02
	B ( 43 ,  3 ) =   -1.541707E-01
	B ( 43 ,  4 ) =    1.111874E-02
	B ( 43 ,  5 ) =    0.000000E+00
	B ( 43 ,  6 ) =    0.000000E+00
	B ( 43 ,  7 ) =   -1.602188E-02
	B ( 43 ,  8 ) =    0.000000E+00
	B ( 43 ,  9 ) =    0.000000E+00
	B ( 44 ,  1 ) =   -3.898506E-02
	B ( 44 ,  2 ) =    1.138930E-02
	B ( 44 ,  3 ) =   -7.708534E-02
	B ( 44 ,  4 ) =   -5.640537E-02
	B ( 44 ,  5 ) =    3.898506E-02
	B ( 44 ,  6 ) =   -1.335157E-01
	B ( 44 ,  7 ) =   -8.010941E-03
	B ( 44 ,  8 ) =   -1.387536E-02
	B ( 44 ,  9 ) =    0.000000E+00
	B ( 45 ,  1 ) =    3.898506E-02
	B ( 45 ,  2 ) =    1.138930E-02
	B ( 45 ,  3 ) =    7.708534E-02
	B ( 45 ,  4 ) =   -5.640537E-02
	B ( 45 ,  5 ) =   -3.898506E-02
	B ( 45 ,  6 ) =   -1.335157E-01
	B ( 45 ,  7 ) =    8.010941E-03
	B ( 45 ,  8 ) =   -1.387536E-02
	B ( 45 ,  9 ) =    0.000000E+00
	B ( 46 ,  1 ) =   -4.988725E-02
	B ( 46 ,  2 ) =   -4.762038E-03
	B ( 46 ,  3 ) =   -4.005471E-03
	B ( 46 ,  4 ) =   -7.255670E-02
	B ( 46 ,  5 ) =    9.432920E-03
	B ( 46 ,  6 ) =    2.081304E-02
	B ( 46 ,  7 ) =   -7.021235E-02
	B ( 46 ,  8 ) =    1.134682E-02
	B ( 46 ,  9 ) =    2.205483E-02
	B ( 47 ,  1 ) =    2.808287E-02
	B ( 47 ,  2 ) =    2.754063E-02
	B ( 47 ,  3 ) =   -2.002735E-02
	B ( 47 ,  4 ) =   -4.025403E-02
	B ( 47 ,  5 ) =   -6.853720E-02
	B ( 47 ,  6 ) =    6.937678E-03
	B ( 47 ,  7 ) =   -4.493281E-02
	B ( 47 ,  8 ) =   -5.513227E-02
	B ( 47 ,  9 ) =    2.205483E-02
	B ( 48 ,  1 ) =   -3.887712E-02
	B ( 48 ,  2 ) =    7.891340E-02
	B ( 48 ,  3 ) =   -1.602188E-02
	B ( 48 ,  4 ) =    1.111874E-02
	B ( 48 ,  5 ) =   -1.577209E-03
	B ( 48 ,  6 ) =   -1.387536E-02
	B ( 48 ,  7 ) =    2.527954E-02
	B ( 48 ,  8 ) =   -6.647909E-02
	B ( 48 ,  9 ) =    2.205483E-02
	B ( 49 ,  1 ) =   -4.988725E-02
	B ( 49 ,  2 ) =   -4.762038E-03
	B ( 49 ,  3 ) =    4.005471E-03
	B ( 49 ,  4 ) =   -7.255670E-02
	B ( 49 ,  5 ) =    9.432920E-03
	B ( 49 ,  6 ) =   -2.081304E-02
	B ( 49 ,  7 ) =    7.021235E-02
	B ( 49 ,  8 ) =   -1.134682E-02
	B ( 49 ,  9 ) =    2.205483E-02
	B ( 50 ,  1 ) =    2.808287E-02
	B ( 50 ,  2 ) =    2.754063E-02
	B ( 50 ,  3 ) =    2.002735E-02
	B ( 50 ,  4 ) =   -4.025403E-02
	B ( 50 ,  5 ) =   -6.853720E-02
	B ( 50 ,  6 ) =   -6.937678E-03
	B ( 50 ,  7 ) =    4.493281E-02
	B ( 50 ,  8 ) =    5.513227E-02
	B ( 50 ,  9 ) =    2.205483E-02
	B ( 51 ,  1 ) =   -3.887712E-02
	B ( 51 ,  2 ) =    7.891340E-02
	B ( 51 ,  3 ) =    1.602188E-02
	B ( 51 ,  4 ) =    1.111874E-02
	B ( 51 ,  5 ) =   -1.577209E-03
	B ( 51 ,  6 ) =    1.387536E-02
	B ( 51 ,  7 ) =   -2.527954E-02
	B ( 51 ,  8 ) =    6.647909E-02
	B ( 51 ,  9 ) =    2.205483E-02
	B ( 52 ,  1 ) =    4.988725E-02
	B ( 52 ,  2 ) =   -4.762038E-03
	B ( 52 ,  3 ) =   -4.005471E-03
	B ( 52 ,  4 ) =   -7.255670E-02
	B ( 52 ,  5 ) =   -9.432920E-03
	B ( 52 ,  6 ) =   -2.081304E-02
	B ( 52 ,  7 ) =   -7.021235E-02
	B ( 52 ,  8 ) =   -1.134682E-02
	B ( 52 ,  9 ) =   -2.205483E-02
	B ( 53 ,  1 ) =    3.887712E-02
	B ( 53 ,  2 ) =    7.891340E-02
	B ( 53 ,  3 ) =    1.602188E-02
	B ( 53 ,  4 ) =    1.111874E-02
	B ( 53 ,  5 ) =    1.577209E-03
	B ( 53 ,  6 ) =   -1.387536E-02
	B ( 53 ,  7 ) =   -2.527954E-02
	B ( 53 ,  8 ) =   -6.647909E-02
	B ( 53 ,  9 ) =   -2.205483E-02
	B ( 54 ,  1 ) =   -2.808287E-02
	B ( 54 ,  2 ) =    2.754063E-02
	B ( 54 ,  3 ) =    2.002735E-02
	B ( 54 ,  4 ) =   -4.025403E-02
	B ( 54 ,  5 ) =    6.853720E-02
	B ( 54 ,  6 ) =    6.937678E-03
	B ( 54 ,  7 ) =    4.493281E-02
	B ( 54 ,  8 ) =   -5.513227E-02
	B ( 54 ,  9 ) =   -2.205483E-02
	B ( 55 ,  1 ) =    4.988725E-02
	B ( 55 ,  2 ) =   -4.762038E-03
	B ( 55 ,  3 ) =    4.005471E-03
	B ( 55 ,  4 ) =   -7.255670E-02
	B ( 55 ,  5 ) =   -9.432920E-03
	B ( 55 ,  6 ) =    2.081304E-02
	B ( 55 ,  7 ) =    7.021235E-02
	B ( 55 ,  8 ) =    1.134682E-02
	B ( 55 ,  9 ) =   -2.205483E-02
	B ( 56 ,  1 ) =    3.887712E-02
	B ( 56 ,  2 ) =    7.891340E-02
	B ( 56 ,  3 ) =   -1.602188E-02
	B ( 56 ,  4 ) =    1.111874E-02
	B ( 56 ,  5 ) =    1.577209E-03
	B ( 56 ,  6 ) =    1.387536E-02
	B ( 56 ,  7 ) =    2.527954E-02
	B ( 56 ,  8 ) =    6.647909E-02
	B ( 56 ,  9 ) =   -2.205483E-02
	B ( 57 ,  1 ) =   -2.808287E-02
	B ( 57 ,  2 ) =    2.754063E-02
	B ( 57 ,  3 ) =   -2.002735E-02
	B ( 57 ,  4 ) =   -4.025403E-02
	B ( 57 ,  5 ) =    6.853720E-02
	B ( 57 ,  6 ) =   -6.937678E-03
	B ( 57 ,  7 ) =   -4.493281E-02
	B ( 57 ,  8 ) =    5.513227E-02
	B ( 57 ,  9 ) =   -2.205483E-02
	B ( 58 ,  1 ) =    4.339781E-02
	B ( 58 ,  2 ) =    1.267846E-02
	B ( 58 ,  3 ) =   -1.337656E-02
	B ( 58 ,  4 ) =   -6.278993E-02
	B ( 58 ,  5 ) =   -4.339781E-02
	B ( 58 ,  6 ) =   -7.722959E-03
	B ( 58 ,  7 ) =   -6.408926E-02
	B ( 58 ,  8 ) =   -3.700195E-02
	B ( 58 ,  9 ) =    0.000000E+00
	B ( 59 ,  1 ) =    0.000000E+00
	B ( 59 ,  2 ) =    8.784567E-02
	B ( 59 ,  3 ) =    0.000000E+00
	B ( 59 ,  4 ) =    1.237728E-02
	B ( 59 ,  5 ) =    0.000000E+00
	B ( 59 ,  6 ) =   -1.544592E-02
	B ( 59 ,  7 ) =    0.000000E+00
	B ( 59 ,  8 ) =   -7.400390E-02
	B ( 59 ,  9 ) =    0.000000E+00
	B ( 60 ,  1 ) =   -4.339781E-02
	B ( 60 ,  2 ) =    1.267846E-02
	B ( 60 ,  3 ) =    1.337656E-02
	B ( 60 ,  4 ) =   -6.278993E-02
	B ( 60 ,  5 ) =    4.339781E-02
	B ( 60 ,  6 ) =   -7.722959E-03
	B ( 60 ,  7 ) =    6.408926E-02
	B ( 60 ,  8 ) =   -3.700195E-02
	B ( 60 ,  9 ) =    0.000000E+00
	B ( 61 ,  1 ) =    4.339781E-02
	B ( 61 ,  2 ) =    1.267846E-02
	B ( 61 ,  3 ) =    1.337656E-02
	B ( 61 ,  4 ) =   -6.278993E-02
	B ( 61 ,  5 ) =   -4.339781E-02
	B ( 61 ,  6 ) =    7.722959E-03
	B ( 61 ,  7 ) =    6.408926E-02
	B ( 61 ,  8 ) =    3.700195E-02
	B ( 61 ,  9 ) =    0.000000E+00
	B ( 62 ,  1 ) =    0.000000E+00
	B ( 62 ,  2 ) =    8.784567E-02
	B ( 62 ,  3 ) =    0.000000E+00
     	B ( 62 ,  4 ) =    1.237728E-02
	B ( 62 ,  5 ) =    0.000000E+00
	B ( 62 ,  6 ) =    1.544592E-02
	B ( 62 ,  7 ) =    0.000000E+00
	B ( 62 ,  8 ) =    7.400390E-02
	B ( 62 ,  9 ) =    0.000000E+00
	B ( 63 ,  1 ) =   -4.339781E-02
	B ( 63 ,  2 ) =    1.267846E-02
	B ( 63 ,  3 ) =   -1.337656E-02
	B ( 63 ,  4 ) =   -6.278993E-02
	B ( 63 ,  5 ) =    4.339781E-02
	B ( 63 ,  6 ) =    7.722959E-03
	B ( 63 ,  7 ) =   -6.408926E-02
	B ( 63 ,  8 ) =    3.700195E-02
	B ( 63 ,  9 ) =    0.000000E+00
	
	RETURN
	END

	
C**********************************************************************	
      SUBROUTINE TECPLOT(NEW_NNODEX,NELX,NDIME,NPE,COORDS_UPDATED,
     +                IJKL_UPDATED,GND_NODAL,KINC,STRESS_NODAL,
     +                DISP_UPDATED,NODE_IN_GRAIN,PLASTIC_STRAIN,
     +                NUMBER_OF_PRINTS,STRESS_ELEMENTAL,GND_ELEMENTAL,
     +                PLASTIC_STRAIN_GP,IELGR)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION IJKL_UPDATED(NELX,NPE),STRESS_NODAL(NEW_NNODEX,9),
     +           COORDS_UPDATED(NEW_NNODEX,NDIME),
     +           GND_NODAL(NEW_NNODEX,2),DISP_UPDATED(NEW_NNODEX,3),
     +           NODE_IN_GRAIN(NEW_NNODEX),PLASTIC_STRAIN(NEW_NNODEX),
     +           STRESS_ELEMENTAL(NELX,9),GND_ELEMENTAL(NELX,2),
     +           PLASTIC_STRAIN_GP(NELX),IELGR(NELX)
      character*300::text,text1,text2
	

      NUMBER_OF_PRINTS=NUMBER_OF_PRINTS+1

      write(text1,'(i10)') NEW_NNODEX
      text1=adjustl(text1)
      write(text2,'(i10)') nelx
      text2=adjustl(text2)
      write(text,'(i10)') KINC
      TEXT=ADJUSTL(TEXT)
      
      
      IF (NUMBER_OF_PRINTS.EQ.1) THEN
      text='ZONE SOLUTIONTIME='//TRIM(TEXT)//' NODES='//trim(text1)//',
     & ELEMENTS='//trim(text2)//', DATAPACKING=POINT,ZONETYPE=
     & FETETRAHEDRON'
      ELSE
      text='ZONE SOLUTIONTIME='//TRIM(TEXT)//' NODES='//trim(text1)//',
     & ELEMENTS='//trim(text2)//', DATAPACKING=POINT,ZONETYPE=
     & FETETRAHEDRON, CONNECTIVITYSHAREZONE=1'
      ENDIF
      write(5000,*) text
      write(5001,*) text
      
      


      DO I=1,NEW_NNODEX
         WRITE(5000,'(18ES18.8,I5)') (COORDS_UPDATED(I,J),J=1,NDIME),
     +       GND_NODAL(I,1:2),STRESS_NODAL(I,1:9),DISP_UPDATED(I,1:3),
     +       PLASTIC_STRAIN(I),NODE_IN_GRAIN(I)
         WRITE(5001,'(18ES18.8,I5)') 
     +       ((COORDS_UPDATED(I,J)+DISP_UPDATED(I,J)),J=1,NDIME),
     +       GND_NODAL(I,1:2),STRESS_NODAL(I,1:9),DISP_UPDATED(I,1:3),
     +       PLASTIC_STRAIN(I),NODE_IN_GRAIN(I)     
      ENDDO
	
      
      IF (NUMBER_OF_PRINTS.EQ.1) THEN
         DO I=1,NELX
            WRITE(5000,'(4I8)') (IJKL_UPDATED(I,INODE),INODE=1,NPE)
            WRITE(5001,'(4I8)') (IJKL_UPDATED(I,INODE),INODE=1,NPE)
         ENDDO
      ENDIF
	




      write(text1,'(i10)') NEW_NNODEX
      text1=adjustl(text1)
      write(text2,'(i10)') nelx
      text2=adjustl(text2)
      write(text,'(i10)') KINC
      TEXT=ADJUSTL(TEXT)
      
      
      IF (NUMBER_OF_PRINTS.EQ.1) THEN
      text='ZONE SOLUTIONTIME='//TRIM(TEXT)//' NODES='//trim(text1)//',
     & ELEMENTS='//trim(text2)//', DATAPACKING=BLOCK, VARLOCATION = 
     & ([4-14,18-19] = CELLCENTERED),ZONETYPE=FETETRAHEDRON'
      ELSE
      text='ZONE SOLUTIONTIME='//TRIM(TEXT)//' NODES='//trim(text1)//',
     & ELEMENTS='//trim(text2)//', DATAPACKING=BLOCK, VARLOCATION = 
     & ([4-14,18-19] = CELLCENTERED),ZONETYPE=FETETRAHEDRON,
     &  CONNECTIVITYSHAREZONE=1'
      ENDIF
      
      write(5002,*) text
      write(5003,*) text
            
      NLINE=NEW_NNODEX/10
      NREMAIN=NEW_NNODEX-NLINE*10

      DO J=1,3
         DO ILINE=1,NLINE
            WRITE(5002,'(10ES18.8)') (COORDS_UPDATED(I,J),
     &      I=(ILINE-1)*10+1,ILINE*10)
            WRITE(5003,'(10ES18.8)') (COORDS_UPDATED(I,J)+
     &      DISP_UPDATED(I,J),I=(ILINE-1)*10+1,ILINE*10)     
         ENDDO
         IF (NREMAIN.NE.0) THEN
            WRITE(5002,'(10ES18.8)') (COORDS_UPDATED(I,J),
     &      I=NLINE*10+1,NEW_NNODEX)      
            WRITE(5003,'(10ES18.8)') (COORDS_UPDATED(I,J)+
     &      DISP_UPDATED(I,J),I=NLINE*10+1,NEW_NNODEX)      
         ENDIF
      ENDDO

      NLINEE=NELX/10
      NREMAINE=NELX-NLINEE*10
      
      DO J=1,2
         DO ILINE=1,NLINEE
            WRITE(5002,'(10ES18.8)') (GND_ELEMENTAL(I,J),
     &      I=(ILINE-1)*10+1,ILINE*10)
            WRITE(5003,'(10ES18.8)') (GND_ELEMENTAL(I,J),
     &      I=(ILINE-1)*10+1,ILINE*10)
         ENDDO
         IF (NREMAINE.NE.0) THEN
            WRITE(5002,'(10ES18.8)') (GND_ELEMENTAL(I,J),
     &      I=NLINEE*10+1,NELX)      
            WRITE(5003,'(10ES18.8)') (GND_ELEMENTAL(I,J),
     &      I=NLINEE*10+1,NELX)      
         ENDIF
      ENDDO


      
      DO J=1,9
      
         DO ILINE=1,NLINEE
            WRITE(5002,'(10ES18.8)') (STRESS_ELEMENTAL(I,J),
     &      I=(ILINE-1)*10+1,ILINE*10)
            WRITE(5003,'(10ES18.8)') (STRESS_ELEMENTAL(I,J),
     &      I=(ILINE-1)*10+1,ILINE*10)
         ENDDO
         IF (NREMAINE.NE.0) THEN
            WRITE(5002,'(10ES18.8)') (STRESS_ELEMENTAL(I,J),
     &      I=NLINEE*10+1,NELX)      
            WRITE(5003,'(10ES18.8)') (STRESS_ELEMENTAL(I,J),
     &      I=NLINEE*10+1,NELX)      
         ENDIF
      
      
      ENDDO
     
     
      
      DO J=1,3
      
       DO ILINE=1,NLINE
         WRITE(5002,'(10ES18.8)') (DISP_UPDATED(I,J),
     &   I=(ILINE-1)*10+1,ILINE*10)
         WRITE(5003,'(10ES18.8)') (DISP_UPDATED(I,J),
     &   I=(ILINE-1)*10+1,ILINE*10)     
      ENDDO
      IF (NREMAIN.NE.0) THEN
         WRITE(5002,'(10ES18.8,I5)') (DISP_UPDATED(I,J),
     &   I=NLINE*10+1,NEW_NNODEX)      
         WRITE(5003,'(10ES18.8,I5)') (DISP_UPDATED(I,J),
     &   I=NLINE*10+1,NEW_NNODEX)      
      ENDIF        
      
      
      ENDDO     

 
      DO ILINE=1,NLINEE
         WRITE(5002,'(10ES18.8)') (PLASTIC_STRAIN_GP(I),
     &   I=(ILINE-1)*10+1,ILINE*10)
         WRITE(5003,'(10ES18.8)') (PLASTIC_STRAIN_GP(I),
     &   I=(ILINE-1)*10+1,ILINE*10)
      ENDDO
      IF (NREMAINE.NE.0) THEN
         WRITE(5002,'(10ES18.8)') (PLASTIC_STRAIN_GP(I),
     &   I=NLINEE*10+1,NELX)      
         WRITE(5003,'(10ES18.8)') (PLASTIC_STRAIN_GP(I),
     &   I=NLINEE*10+1,NELX)      
      ENDIF
 

 
      DO ILINE=1,NLINEE
         WRITE(5002,'(10I5)') (IELGR(I),
     &   I=(ILINE-1)*10+1,ILINE*10)
         WRITE(5003,'(10I5)') (IELGR(I),
     &   I=(ILINE-1)*10+1,ILINE*10)
      ENDDO
      IF (NREMAINE.NE.0) THEN
         WRITE(5002,'(10I5)') (IELGR(I),
     &   I=NLINEE*10+1,NELX)      
         WRITE(5003,'(10I5)') (IELGR(I),
     &   I=NLINEE*10+1,NELX)      
      ENDIF 
 
      IF (NUMBER_OF_PRINTS.EQ.1) THEN
         DO I=1,NELX
            WRITE(5002,'(4I8)') (IJKL_UPDATED(I,INODE),INODE=1,NPE)
            WRITE(5003,'(4I8)') (IJKL_UPDATED(I,INODE),INODE=1,NPE)
         ENDDO
      ENDIF
	
   
   
   
      CALL FLUSH(5000)
      CALL FLUSH(5001)
      CALL FLUSH(5002)
      CALL FLUSH(5003)


	
      RETURN
      END
	


c---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
C    MATRIX MULTIPLICATION SUBROUTINE 

      subroutine matmulx(aa,bb,cc,n,m,l)
      
c      include 'aba_param.inc'
       implicit double precision(a-h,o-z)

c   this subroutine returns martix [cc] = matrix [aa] * matrix [bb]
c   n=no. of rows of [aa]      =no. of rows of [cc]
c   m=no. of columns of [aa]   =no. of rows of [bb]
c   l=no. of columns of [bb]   =no. of columns of [cc]

       dimension aa(n,m),bb(m,l),cc(n,l)

       do 10 i=1,n
       do 10 j=1,l
         cc(i,j)=0.0
       do 10 k=1,m
   10    cc(i,j)=cc(i,j)+aa(i,k)*bb(k,j)
       return 
       end  



C*********************************************************************	
        SUBROUTINE PARTITION_CALCULATOR(NELST,NELEND,NELNOX,
     &   N_TO_BE_DISTRIBUTED,NPROCS,IPROC)
       IMPLICIT NONE
       INTEGER::N_TO_BE_DISTRIBUTED,NPROCS,IPROC,I1,I2,IA,NELST,
     &   NELNOX,NELEND



       I1=N_TO_BE_DISTRIBUTED/NPROCS+1
       I2=N_TO_BE_DISTRIBUTED/NPROCS

       IA=N_TO_BE_DISTRIBUTED-I2*NPROCS


       IF((IPROC+1).LE.IA) THEN
              NELST=IPROC*I1+1
              NELNOX=I1
       ELSE
              NELST=IA*I1+(IPROC-IA)*I2+1
              NELNOX=I2
       ENDIF

       NELEND=NELST+NELNOX-1

       RETURN
       END           
     
     
     

      SUBROUTINE printMatrix(A,N,M,matName,printTo)
      implicit none
      integer, intent(in):: N,M
      real(8), intent(in):: A(N,M)
      character*(*),intent(in):: matName
      integer, intent(in):: printTo
      integer:: i,j
      if(LEN_TRIM(matName).GT.0)then
         if(printTo.GT.0) then
            write(printTo,*) matName,':'
         else
            write(*,*) matName,':'
         endif
      endif
      do i=1,N
         if(printTo.GT.0) then
            write(printTo,*) (A(i,j), j=1,M)
         else
            write(*,*) (A(i,j), j=1,M)
         endif
      enddo
      END SUBROUTINE
      
      
      
      
           