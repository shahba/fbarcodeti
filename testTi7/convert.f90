      program convert
      implicit none
      integer :: i, j, dummy, nElements,nGrains
      integer, allocatable :: ielgr(:)
      real(8), allocatable :: texture(:,:) , grainSize(:)
      
      open(1,file='elementGrains.inp')
      read(1,*) nElements
      
      allocate (ielgr(nElements))
      
      do i = 1 , nElements
         read(1,*) dummy, ielgr(i)
      enddo
      
      close (1)
      
      open(1,file = 'grainTexture.inp' )
      read(1,*) nGrains
      
      allocate (texture (nGrains,3))
      
      do i = 1, nGrains
         read(1,*) dummy, texture(i,:)
      enddo
      
      close (1)
      
      
      open(1,file = 'grainSizes.inp' )
      read(1,*) nGrains
      
      allocate (grainSize (nGrains))
      
      do i = 1, nGrains
         read(1,*) dummy, grainSize(i)
      enddo
      
      close (1)      
      
      
      
      open(1,file= 'texture.dat')
      open(2,file='size.dat')
      
      do i = 1 , nElements
         write(1,*) texture(ielgr(i),:),1
         write(2,*) i,grainSize(ielgr(i))
      enddo
      
      
      close(1)
      close(2)
      
      end