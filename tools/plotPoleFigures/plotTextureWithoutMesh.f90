      PROGRAM plotTexture

      use crystal_Titanium
      
      implicit none
      
      integer, parameter :: MAXN_GRAINS=10000
      
      real(8) :: phi1,theta,phi2
      real(8) :: xy_c(2,MAXN_GRAINS)
      real(8) :: xy_a1(2,MAXN_GRAINS)
      real(8) :: xy_a2(2,MAXN_GRAINS)
      real(8) :: xy_a3(2,MAXN_GRAINS)
      real(8) :: nc(3),na1(3),na2(3),na3(3)
      real(8) :: p_tex(3), tex_magn, tex_this
      real(8), parameter :: PI=3.14159265359D0
      character(50) :: strTextureFile
      logical :: fileExists
      integer :: nGrains,grainID
      real(8), allocatable :: grainTexture(:,:)
      
      ! slip systems/families
      real(8) :: gmt(3,nslip_max),gst(3,nslip_max)    ! gmt, gst - slip planes and slip directions in the global frame

      integer :: islip
      
      character :: eulerDirection
      real(8) :: grainRot(9,MAXN_GRAINS)
      real(8) :: R_cry(3,3), det
      integer :: i, iGrain
      integer :: error, istep, nstep, step_inp
      integer :: lastProgress
      real(8) :: time_inp, temp_inp

            
      ! initialize the HCP lattice 
      CALL init_crys_HCP()
      
      write(*,*) 'enter grain texture file name (e.g.: grainTexture.inp)'
      write(*,*) 'assumed format;'
      write(*,*) '# grains'
      write(*,*) 'grainID euler1 euler2 euler3'
      write(*,*) '...'
      read(*,*) strTextureFile
      
      !read texture: grainTexture(:)
      inquire(file=trim(strTextureFile),exist=fileExists)
      if(.NOT.fileExists) then
         write(*,*) 'cannot find ',strTextureFile
         stop
      endif

      open(105,file=trim(strTextureFile))
      READ(105,*) nGrains
      
      allocate(grainTexture(3,nGrains))
      grainTexture(:,:)=0.d0
      
      error = 0
      do I=1,nGrains
         READ(105,*,iostat=error) grainID, grainTexture(:,grainID)
         if(error.NE.0) exit
      enddo
      if (error/=0) then
         write(*,*) 'read error - unexpected file structure'
         write(*,*) 'format:'
         write(*,*) '# grains'
         write(*,*) 'grainID euler1 euler2 euler3'
         write(*,*) '...'
         stop
      endif
      
      
      eulerDirection=' '
      do while (eulerDirection.NE.'A'.AND.eulerDirection.NE.'P')
         write(*,*) 'Grain orientations are provided in form of Euler angles'
         write(*,*) 'What is the direction of the active transformation associated with the Euler angles?'
         write(*,*) '(A) Euler angles rotate the specimen coordinate axes onto the crystal coordinate axes '&
                   &'(OIM standard, Kourosh, Deniz)'
         write(*,*) '(P) Euler angles rotate the crystal coordinate axes onto the specimen coordinate axes '&
                   &'(was used by Deka)'
         write(*,*) 'What is the direction of the active transformation associated with the Euler angles?'
         read(*,'(A1)') eulerDirection
         if (eulerDirection.EQ.'a') eulerDirection='A'
         if (eulerDirection.EQ.'p') eulerDirection='P'
      enddo
      write(*,*)
      
      ! plot initial texture on a separate file

      ! open output file
      OPEN(UNIT=900,FILE='pole_figures_init_TP.dat')
      
      write(*,*) 'Plotting initial texture...'      
      
      ! calculate stereographs for all grains and crystallographic planes
      do iGrain = 1,nGrains
         ! read rotation matrix
         call euler_slip(grainTexture(1,iGrain),grainTexture(2,iGrain),grainTexture(3,iGrain),R_cry)
         if (eulerDirection=='P') then
            R_cry = TRANSPOSE(R_cry)
         endif
         ! rotate the lattice
         do islip = 1,nslip_HCP
            gmt(:,islip) = MATMUL(R_cry(:,:),cmt_HCP(:,islip))  ! rotate slip planes
            gst(:,islip) = MATMUL(R_cry(:,:),cst_HCP(:,islip))  ! rotate slip directions
         enddo
         ! calculate the stereographs of the <a> and <c> poles
         nc(:) = gmt(:,1)
         na1(:) = gst(:,1)
         na2(:) = gst(:,2)
         na3(:) = gst(:,3)
         if (nc(3).lt.0.d0) nc(:) = - nc(:)
         nc(:) = nc(:) / DSQRT(DOT_PRODUCT(nc,nc))
         nc(3) = nc(3) + 1.D0
         xy_c(1,iGrain) = nc(1) / nc(3)
         xy_c(2,iGrain) = nc(2) / nc(3)
         if (na1(3).lt.0.d0) na1(:) = - na1(:)
         na1(:) = na1(:) / DSQRT(DOT_PRODUCT(na1,na1))
         na1(3) = na1(3) + 1.D0
         xy_a1(1,iGrain) = na1(1) / na1(3)
         xy_a1(2,iGrain) = na1(2) / na1(3)
         if (na2(3).lt.0.d0) na2(:) = - na2(:)
         na2(:) = na2(:) / DSQRT(DOT_PRODUCT(na2,na2))
         na2(3) = na2(3) + 1.D0
         xy_a2(1,iGrain) = na2(1) / na2(3)
         xy_a2(2,iGrain) = na2(2) / na2(3)
         if (na3(3).lt.0.d0) na3(:) = - na3(:)
         na3(:) = na3(:) / DSQRT(DOT_PRODUCT(na3,na3))
         na3(3) = na3(3) + 1.D0
         xy_a3(1,iGrain) = na3(1) / na3(3)
         xy_a3(2,iGrain) = na3(2) / na3(3)
      enddo
      
      ! print zone header for c-axis pole figure
      write(900,*) 'VARIABLES = "X", "Y"'
      write(900,'(A,I0,A)') 'ZONE T="{0001}" I=',nGrains,', DATAPACKING=POINT'
      do iGrain=1,nGrains
         write(900,*) xy_c(1:2,iGrain)
      enddo
      ! print zone header for a-axis pole figure
      write(900,*) 'VARIABLES = "X", "Y"'
      write(900,'(A,I0,A)') 'ZONE T="(11-20)" I=',3*nGrains,', DATAPACKING=POINT'
      do iGrain=1,nGrains
         write(900,*) xy_a1(1:2,iGrain)
         write(900,*) xy_a2(1:2,iGrain)
         write(900,*) xy_a3(1:2,iGrain)
      enddo
      
      write(*,*) 'done. initial texture stereographs created for:'
      write(*,*) nGrains, ' grains'

      ! close input & output files
      close(900)
      
      write(*,*) 'stereographs created for:'
      write(*,*) nGrains, ' grains'
      
      ! close input & output files
      close(900)
      
      END PROGRAM
      
      ! verified to produce a uniform distribution of directions
      SUBROUTINE getRandomDirection(nx,ny,nz,phi1,theta)
      implicit none
      real(8), intent(out) :: nx,ny,nz
      real(8), intent(out) :: phi1,theta
      real(8), parameter :: PI=3.14159265359D0
      
      real(8) :: u,v
      
      CALL RANDOM_NUMBER(u)
      CALL RANDOM_NUMBER(v)
      
      phi1 = 2.D0*PI*u
      theta = acos(2*v-1)
      
      nx = sin(theta)*sin(phi1)
      ny = -sin(theta)*cos(phi1)
      nz = cos(theta)

      END SUBROUTINE
      

      SUBROUTINE getPhi1andTheta(nx,ny,nz,phi1,theta)
      implicit none
      real(8), intent(in) :: nx,ny,nz
      real(8), intent(out) :: phi1,theta
      real(8), parameter :: PI=3.14159265359D0

      theta = acos(nz)
      phi1 = acos(-ny/DSQRT(1-nz*nz))
      if(nx.LT.0.D0) phi1 = 2.D0*PI-phi1

      END SUBROUTINE

      SUBROUTINE getNc_fromEuler(nx,ny,nz,phi1,theta,phi2)
      implicit none
      real(8), intent(out) :: nx,ny,nz
      real(8), intent(in) :: phi1,theta,phi2
      real(8), parameter :: PI=3.14159265359D0

      nx = sin(theta)*sin(phi1)
      ny = -sin(theta)*cos(phi1)
      nz = cos(theta)

      END SUBROUTINE
      
      subroutine euler_slip(phi,theta,omega,tlgt)
      implicit double precision (a-h,o-z)  

      dimension tlg(3,3),tlgt(3,3)
      real(8) :: phi,theta,omega

      pi=4.d0*datan(1.d0)

  
      sp=dsin(phi)                      
      cp=dcos(phi)                     
      st=dsin(theta)                     
      ct=dcos(theta)                    
      so=dsin(omega)                    
      co=dcos(omega)   
      tlg(1,1)=co*cp-so*sp*ct
      tlg(1,2)=co*sp+so*ct*cp   
      tlg(1,3)=so*st   
      tlg(2,1)=-so*cp-sp*co*ct 
      tlg(2,2)=-so*sp+ct*co*cp
      tlg(2,3)=co*st
      tlg(3,1)=sp*st       
      tlg(3,2)=-st*cp       
      tlg(3,3)=ct

      tlgt = TRANSPOSE(tlg)

      return
      end   

      subroutine calcDet(A,det)
      implicit none
      real(8), intent(in) :: A(3,3)
      real(8), intent(out):: det
      
      det=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)  &
           *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)  &
           *A(1,3)*A(2,2))
      end subroutine
      

!*********************************************************
      subroutine READGM(G0XYZ,IJK,NX,NELX,meshName)
      implicit none
      include 'PARDIS.H'
      real(8), intent(out) :: G0XYZ(MAXCRD)
      integer, intent(out) :: IJK(MNELX)
      integer, intent(out) :: NX, NELX
      character*(*), intent(out) :: meshName
      !locals
      integer :: IJKNEL(MAXELN)
      real(8) :: xc(3)
      integer :: MDIM,NDFADD,NDF,NNODE,NGAUSS
      integer :: error
      character(len=50) :: fileName
      character(len=150) :: lineStr
      !dummies
      integer :: icyc,len,i,ii,j,jj,nnum,iElem,iNode,NEL,NEQ
      
      open(201,file='loadtime.inp')
      call READSTR(201,lineStr,error)
      read(201,*) icyc
      call READSTR(201,lineStr,error)
      read(201,*) meshName
      close(201)
      
      len=len_trim(meshName)
      fileName=meshName(1:len)//'.inp'

      open(unit=LR,file=fileName)

      call READSTR(LR,lineStr,error)
      read(LR,*)MDIM,NDFADD
      NDF=MDIM+NDFADD     
      
      call READSTR(LR,lineStr,error)
      read(LR,*) NX
      NEQ=NDF*NX
      
      IF(NDF.GT.NDOFELX)THEN
         WRITE(*,*)'INSUFFICIENT MEM-NDOFELX'
         STOP
      ENDIF

      !error check - deniz
      IF(NX.GT.MAXNODE)THEN
         write(*,*) 'Increase MAXNODE to ', NX
         WRITE(*,*)'Increase MAXNODE to ', NX
         STOP
      ENDIF
      NEQ=NDF*NX
      IF(NDF.GT.MDOFX)THEN
         WRITE(*,*)'INSUFFICIENT MEMORY-MDOFX'
         STOP
      ENDIF
      IF(NX*MDIM.GT.MAXCRD)THEN
         WRITE(*,*)"INSUFFICIENT MEM -MAXCRD"
         STOP
      ENDIF

      IF(MDIM.GT.MAXDIM)THEN
         WRITE(*,*)'INSUFFICIENT MEM-MAXDIM'
         STOP
      ENDIF
      
      ! READ INITIAL POSITIONS
      do iNode=1,NX
         read(LR,*) nnum,(xc(ii),ii=1,MDIM)
         do ii=1,MDIM
            G0XYZ(MDIM*(nnum-1)+ii)=xc(ii)
         enddo
      enddo
      
      
      call READSTR(LR,lineStr,error)
      if(IBRICKORTET.EQ.1)then
         NNODE=8
         NGAUSS=8
      else
         NGAUSS=1
         NNODE=4
      endif

      read(LR,*) NELX

      ! READ IN THE IJK/CONNECTIVITY MATRIX
      jj=0
      do iElem=1,NELX
         read(LR,*) NEL,(IJKNEL(j),j=1,NNODE)
         do j=1,NNODE
            jj=jj+1
            IJK(jj)=IJKNEL(j)
         enddo
      enddo
      
      close(LR)

      return
      end
      
      SUBROUTINE READSTR(fileNum,lineStr,error)
      IMPLICIT REAL*8(A-H,O-Z)
      ! this code skips 1 line of text in the input file.
      integer, intent(in) :: fileNum
      character(150), intent(out) :: lineStr
      integer, intent(out) :: error
      read(fileNum,'(A)',iostat=error) lineStr
      RETURN
      END
      
      SUBROUTINE cross(vc, v1, v2)
      implicit none
      real(8), intent(in) :: v1(3),v2(3)
      real(8), intent(out):: vc(3)
      
      vc(1)=+v1(2)*v2(3)-v1(3)*v2(2)
      vc(2)=-v1(1)*v2(3)+v1(3)*v2(1)
      vc(3)=+v1(1)*v2(2)-v1(2)*v2(1)
      
      END SUBROUTINE
